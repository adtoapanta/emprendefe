﻿$(document).ready(function () { initModeloNegocio(); });

var editor;
var btnTerminarCanvas = new BtnLoad("btnTerminarCanvas");

function initModeloNegocio() {
    console.log('init Modelo negocio ')

    $('.seleccionarSeccion').click(function (e) {
        seleccionarSeccion(this);
    });

    $('#btnGuardarCanvas').click(function (e) {
        guardarPregunta();
    });


    $('#btnTerminarCanvas').click(function (e) {
        completo = canvasCompleto();
        if (completo == false) {

            toastr.error('', 'Debe completar todos los campos antes de continuar', { positionClass: "toast-bottom-right" });

        }
        else {

            var idRequisito = $('#txtIdRequisito').val();
            location.href = SITEROOT + '/RequisitosPostulante/completarRequisito/' + idRequisito;

        }

    });

    $('#btnImprimirCanvas').click(function (e) {
        imprimirCanvas();
    });

    consultarPreguntasCanvas()


    $('#btnGuardarCamposEmprendimiento').click(function () {
        guardarCamposEmprendimiento();
    });

}

function imprimirCanvas() {
    window.print();
}

function seleccionarSeccion(div) {


    $('#modalRespondeCanvas').modal("show");

    var id = $(div).attr("id");
    $('#txtIdPregunta').val(id);
    $('#txtTextoGuia').html(preguntas[id - 1].textoGuia);
    $('#txtSeccion').html(preguntas[id - 1].seccion);
    $('#txtRespuesta').val(preguntas[id - 1].respuesta);

}

function consultarPreguntasCanvas() {

    htclibjs.Ajax({
        url: "/ModeloNegocio/consultarPreguntasCanvas",
        data: JSON.stringify({

        }),
        success: function (r) {

            var pregunta;
            $.each(r.data, function (i, item) {

                pregunta = new preguntaCanvas(1, 'Propuesta de Valor', item.propuesta, '');
                preguntas.push(pregunta);
                pregunta = new preguntaCanvas(2, 'Relaciones con Clientes', item.relaciones, '');
                preguntas.push(pregunta);
                pregunta = new preguntaCanvas(3, 'Canales', item.canales, '');
                preguntas.push(pregunta);
                pregunta = new preguntaCanvas(4, 'Segmento de Clientes', item.clientes, '');
                preguntas.push(pregunta);
                pregunta = new preguntaCanvas(5, 'Actividades Clave', item.actividades, '');
                preguntas.push(pregunta);
                pregunta = new preguntaCanvas(6, 'Recursos Clave', item.recursos, '');
                preguntas.push(pregunta);
                pregunta = new preguntaCanvas(7, 'Socios Clave', item.socios, '');
                preguntas.push(pregunta);
                pregunta = new preguntaCanvas(8, 'Fuente de Ingresos', item.ingresos, '');
                preguntas.push(pregunta);
                pregunta = new preguntaCanvas(9, 'Costos', item.egresos, '');
                preguntas.push(pregunta);

            });

            consultarCanvasEmprendimiento();

            console.log(preguntas);

        }
    });

}


function guardarPregunta() {

    var respuesta = $('#txtRespuesta').val();
    var idEmprendimiento = $('#txtIdEmprendimiento').val();
    var idPregunta = $('#txtIdPregunta').val();

    var boton = new BtnLoad("btnGuardarCanvas");
    boton.Start();

    if (respuesta == '') {

        toastr.error('', 'Escriba una respuesta antes de continuar', { positionClass: "toast-bottom-right" });

    }
    else {

        htclibjs.Ajax({
            url: "/ModeloNegocio/actualizaCanvasEmprendimiento",
            data: JSON.stringify({
                idEmprendimiento: idEmprendimiento, idPregunta: idPregunta, respuesta: respuesta
            }),

            success: function (r) {

                preguntas[idPregunta - 1].respuesta = respuesta;
                preguntas[idPregunta - 1].contestada = true;
                idPregunta = '#' + idPregunta
                $(idPregunta).html(respuesta);
                $('#modalRespondeCanvas').modal("hide");
                boton.Stop();
            }
        });

    }
    

}

function esContestada(respuesta) {
    if (respuesta == '')
        return false;
    else
        return true;
}

function consultarCanvasEmprendimiento() {

    var idEmprendimiento = $('#txtIdEmprendimiento').val();

    htclibjs.Ajax({
        url: "/ModeloNegocio/consultarCanvasEmprendimiento",
        data: JSON.stringify({
            idEmprendimiento: idEmprendimiento
        }),
        success: function (r) {

            var id = 1;
            var idDiv = '';
            console.log(r.data);

            $('#1').html(r.data.propuesta);
            preguntas[0].respuesta = r.data.propuesta;
            preguntas[0].contestada = esContestada(r.data.propuesta);
            $('#2').html(r.data.relaciones);
            preguntas[1].respuesta = r.data.relaciones;
            preguntas[1].contestada = esContestada(r.data.relaciones);
            $('#3').html(r.data.canales);
            preguntas[2].respuesta = r.data.canales;
            preguntas[2].contestada = esContestada(r.data.canales);
            $('#4').html(r.data.clientes);
            preguntas[3].respuesta = r.data.clientes;
            preguntas[3].contestada = esContestada(r.data.clientes);
            $('#5').html(r.data.actividades);
            preguntas[4].respuesta = r.data.actividades;
            preguntas[4].contestada = esContestada(r.data.actividades);
            $('#6').html(r.data.recursos);
            preguntas[5].respuesta = r.data.recursos;
            preguntas[5].contestada = esContestada(r.data.recursos);
            $('#7').html(r.data.socios);
            preguntas[6].respuesta = r.data.socios;
            preguntas[6].contestada = esContestada(r.data.socios);
            $('#8').html(r.data.ingresos);
            preguntas[7].respuesta = r.data.ingresos;
            preguntas[7].contestada = true;
            $('#9').html(r.data.egresos);
            preguntas[8].respuesta = r.data.egresos;
            preguntas[8].contestada = true;

        }
    });

}
var preguntas = [];

function preguntaCanvas(idPregunta, titulo, texto, respuestaPregunta) {

    var detalle =
    {
        id: idPregunta,
        seccion: titulo,
        textoGuia: texto,
        respuesta: respuestaPregunta,
        contestada: false
    }

    return detalle;

}


function guardarCamposEmprendimiento() {
    let idP = $('#txtIdPostulacion').val();
    let numClientes = $('#txtNumClientes').val();
    let numConsumidores = $('#txtNumConsumidores').val();
    let numEmpleados = $('#txtNumEmpleados').val();
    let numProveedores = $('#txtNumProveedores').val();

    var brief = $('#txtBriefEmprendimiento').val();

    htclibjs.Ajax({
        url: "/RequisitosPostulante/guardarModNegCamposEmprendimiento",
        data: JSON.stringify({
            idPostulacion: idP,
            pNumClientes: numClientes,
            pNumConsumidores: numConsumidores,
            pNumEmpleados: numEmpleados,
            pNumProveedores: numProveedores,
            brief: brief
        }),
        success: function (r) {
            alert('Datos guardados correctamente');
        }
    });
}

var completo;

function canvasCompleto() {

    var cont = 0;
    for (var i = 0; i < preguntas.length; i++) {
        console.log(preguntas[i].contestada);
        if (preguntas[i].contestada == false) {
            cont++;
        }
    }    
    if (cont > 0)
        return false;
    else
        return true;
}