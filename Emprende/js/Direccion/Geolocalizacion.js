﻿var map, infoWindow;
var latitud;
var longitud;
$(document).ready(function () { initGeolocalizacion(); });

function initGeolocalizacion() {
    console.log('init localizacion ')
    $('#btnGeolocalizar').click(function (e) {
        
        initMap();
    });


    $('#btnGuardarLocalizacion').click(function (e) {

        guardarLocalizacion();

    });
    

}

function guardarLocalizacion() {

    var idPostulacion = $('#txtIdPostulante').val();

    var boton = new BtnLoad("btnGuardarLocalizacion");
    boton.Start();
    htclibjs.Ajax({
        url: "/Direcciones/agregarLocalizacionIdPostulacion",
        data: JSON.stringify({
            idPostulacion: idPostulacion,
            latitud: latitud,
            longitud: longitud
        }),
        success: function (r) {

            toastr.success('', 'Localización guardada', { positionClass: "toast-bottom-right" });
            boton.Stop();
            $('#modalGeoreferenciacion').modal('hide');
        }
    });

}

function initMap() {
    console.log('got here');
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: -34.397, lng: 150.644 },
        zoom: 18
    });
    infoWindow = new google.maps.InfoWindow;

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            latitud = pos.lat;
            longitud = pos.lng;
            infoWindow.setPosition(pos);
            infoWindow.setContent('Usted se encuentra aqui.');
            infoWindow.open(map);
            map.setCenter(pos);            
        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}