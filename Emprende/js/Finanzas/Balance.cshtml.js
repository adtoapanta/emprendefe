﻿var _balanceExt = [];

var _totalIngresosEmprendedor = 0;
var _totalIngresosConyuge = 0;
var _otrosIngresos = 0; //esta es la suma de emprendedor y conyugue

var _totalCostosFijosMensuales = 0;
var _totalCostosFijosFamiliares = 0;
var _totalExcedenteFamiliarMensual = 0;
var _totalActivos = 0;
var _totalPasivos = 0;
var _totalCostosFijosMensuales_P = 0;
var _totalCostosFijosFamiliares_P = 0;
var _totalExcedenteFamiliarMensual_P = 0;
var _totalActivos_P = 0;
var _totalPasivos_P = 0;

$(document).ready(function () { init(); });

function init() {
    console.log('init');
    //iniciar loading
    showLoading();

    getBalance();

    $("input[type='text']").on("click", function () {
        $(this).select();
    });
}
function getBalance() {

    console.log('getBalance');

    var idP = $('#txtIdPostulacion').val();

    console.log('idP');
    console.log(idP);

    htclibjs.Ajax({
        url: "/finanzas/getDatosBalances",
        data: JSON.stringify({
            idPostulacion: idP
        }),
        success: function (r) {
            _balanceExt = r.data;

            llenaBalance();

            //todo quitar loading
            hideLoading();
        }
    });
}
function llenaBalance() {

    console.log('llenaBalance');

    $('#txt_idBalanceAct').val(_balanceExt.balanceAct.id);
    $('#txt_idBalanceProy').val(_balanceExt.balanceProy.id);
    //console.log(_balanceExt);

    var camposAct = _balanceExt.balanceAct.misCampos;
    for (var i = 0; i < camposAct.length; i++) {
        var campoAct = camposAct[i];

        $('#txt_' + campoAct.idCampo).val(campoAct.valor);
    }

    var camposProy = _balanceExt.balanceProy.misCampos;
    for (var i = 0; i < camposProy.length; i++) {
        var campo = camposProy[i];
        $('#txt_' + campo.idCampo + '_P').val(campo.valor);
    }

    //pongo los datos de ventas y costos 
    $('#txt_24').val(_balanceExt.ventasAct);
    $('#txt_24_P').val(_balanceExt.ventasProy);
    $('#txt_25').val(_balanceExt.costoAct);
    $('#txt_25_P').val(_balanceExt.costoProy);

    //value = "@Math.Round(ViewBag.balanceAct.balance.getCampo(1),2)"

    initIngresos();
    initCostos();
    initResultados();
    initBalanceGeneral();
}

//INGRESOS
function initIngresos() {
    //Emprendedor
    $("#txt_1").change(function () { CalcTotalIngEmprendedor(); });
    $("#txt_2").change(function () { CalcTotalIngEmprendedor(); });
    $("#txt_3").change(function () { CalcTotalIngEmprendedor(); });
    //Conyuge
    $("#txt_4").change(function () { calcTotalIngConyuge(); });
    $("#txt_5").change(function () { calcTotalIngConyuge(); });
    $("#txt_6").change(function () { calcTotalIngConyuge(); });

    CalcTotalIngEmprendedor();
    calcTotalIngConyuge();
}
function CalcTotalIngEmprendedor() {
    var ING_EMP_NegocioPropio = getValueTxt('txt_1');
    var ING_EMP_Empleado = getValueTxt('txt_2');
    var ING_EMP_Otro = getValueTxt('txt_3');

    _totalIngresosEmprendedor = ING_EMP_NegocioPropio + ING_EMP_Empleado + ING_EMP_Otro;

    $('#txt_TotalIngEmp').val(formatMoney(_totalIngresosEmprendedor));

    calcResultadosActuales();
    calcResultadosProyectado();//calculo también el proyectado porque el ingreso no tiene proyección a cambiar
}
function calcTotalIngConyuge() {
    var ING_CON_NegocioPropio = getValueTxt('txt_4');
    var ING_CON_Empleado = getValueTxt('txt_5');
    var ING_CON_Otro = getValueTxt('txt_6');

    _totalIngresosConyuge = ING_CON_NegocioPropio + ING_CON_Empleado + ING_CON_Otro;

    $('#txt_TotalIngCon').val(formatMoney(_totalIngresosConyuge));

    calcResultadosActuales();
    calcResultadosProyectado();//calculo también el proyectado porque el ingreso no tiene proyección a cambiar
}

//COSTOS
function initCostos() {
    //costos fijos mensuales
    $("#txt_7").change(function () { calcCostosFijosMensuales(); });
    $("#txt_8").change(function () { calcCostosFijosMensuales(); });
    $("#txt_9").change(function () { calcCostosFijosMensuales(); });
    $("#txt_10").change(function () { calcCostosFijosMensuales(); });
    $("#txt_11").change(function () { calcCostosFijosMensuales(); });
    $("#txt_12").change(function () { calcCostosFijosMensuales(); });
    $("#txt_13").change(function () { calcCostosFijosMensuales(); });
    $("#txt_14").change(function () { calcCostosFijosMensuales(); });

    //costos fijos familiares
    $("#txt_15").change(function () { calcCostosFijosFamiliares(); });
    $("#txt_16").change(function () { calcCostosFijosFamiliares(); });
    $("#txt_17").change(function () { calcCostosFijosFamiliares(); });
    $("#txt_18").change(function () { calcCostosFijosFamiliares(); });
    $("#txt_19").change(function () { calcCostosFijosFamiliares(); });
    $("#txt_20").change(function () { calcCostosFijosFamiliares(); });
    $("#txt_21").change(function () { calcCostosFijosFamiliares(); });
    $("#txt_22").change(function () { calcCostosFijosFamiliares(); });
    $("#txt_23").change(function () { calcCostosFijosFamiliares(); });

    //costos fijos mensuales Proyectado
    $("#txt_7_P").change(function () { calcCostosFijosMensuales_P(); });
    $("#txt_8_P").change(function () { calcCostosFijosMensuales_P(); });
    $("#txt_9_P").change(function () { calcCostosFijosMensuales_P(); });
    $("#txt_10_P").change(function () { calcCostosFijosMensuales_P(); });
    $("#txt_11_P").change(function () { calcCostosFijosMensuales_P(); });
    $("#txt_12_P").change(function () { calcCostosFijosMensuales_P(); });
    $("#txt_13_P").change(function () { calcCostosFijosMensuales_P(); });
    $("#txt_14_P").change(function () { calcCostosFijosMensuales_P(); });

    //costos fijos familiares Proyectado
    $("#txt_15_P").change(function () { calcCostosFijosFamiliares_P(); });
    $("#txt_16_P").change(function () { calcCostosFijosFamiliares_P(); });
    $("#txt_17_P").change(function () { calcCostosFijosFamiliares_P(); });
    $("#txt_18_P").change(function () { calcCostosFijosFamiliares_P(); });
    $("#txt_19_P").change(function () { calcCostosFijosFamiliares_P(); });
    $("#txt_20_P").change(function () { calcCostosFijosFamiliares_P(); });
    $("#txt_21_P").change(function () { calcCostosFijosFamiliares_P(); });
    $("#txt_22_P").change(function () { calcCostosFijosFamiliares_P(); });
    $("#txt_23_P").change(function () { calcCostosFijosFamiliares_P(); });

    calcCostosFijosMensuales();
    calcCostosFijosFamiliares();

    calcCostosFijosMensuales_P();
    calcCostosFijosFamiliares_P();
}
function calcCostosFijosMensuales() {
    var CFM_arriendo = getValueTxt('txt_7');
    var CFM_sueldos = getValueTxt('txt_8');
    var CFM_ServiciosBasicos = getValueTxt('txt_9');
    var CFM_internet = getValueTxt('txt_10');
    var CFM_movilizacion = getValueTxt('txt_11');
    var CFM_impuestos = getValueTxt('txt_12');
    var CFM_cuotaPrestamos = getValueTxt('txt_13');
    var CFM_otros = getValueTxt('txt_14');

    _totalCostosFijosMensuales = CFM_arriendo + CFM_sueldos + CFM_ServiciosBasicos +
        CFM_internet + CFM_movilizacion + CFM_impuestos + CFM_cuotaPrestamos + CFM_otros;

    $('#txt_TotalCostosFijosNegocio').val(formatMoney(_totalCostosFijosMensuales));

    calcResultadosActuales();

}
function calcCostosFijosMensuales_P() {
    var CFM_arriendo = getValueTxt('txt_7_P');
    var CFM_sueldos = getValueTxt('txt_8_P');
    var CFM_ServiciosBasicos = getValueTxt('txt_9_P');
    var CFM_internet = getValueTxt('txt_10_P');
    var CFM_movilizacion = getValueTxt('txt_11_P');
    var CFM_impuestos = getValueTxt('txt_12_P');
    var CFM_cuotaPrestamos = getValueTxt('txt_13_P');
    var CFM_otros = getValueTxt('txt_14_P');

    _totalCostosFijosMensuales_P = CFM_arriendo + CFM_sueldos + CFM_ServiciosBasicos +
        CFM_internet + CFM_movilizacion + CFM_impuestos + CFM_cuotaPrestamos + CFM_otros;

    $('#txt_TotalCostosFijosNegocio_P').val(formatMoney(_totalCostosFijosMensuales_P));

    calcResultadosProyectado();
}
function calcCostosFijosFamiliares() {
    var CFF_Comida = getValueTxt('txt_15');
    var CFF_Vivienda = getValueTxt('txt_16');
    var CFF_Vestido = getValueTxt('txt_17');
    var CFF_Internet = getValueTxt('txt_18');
    var CFF_Transporte = getValueTxt('txt_19');
    var CFF_ServPublicos = getValueTxt('txt_20');
    var CFF_Medicinas = getValueTxt('txt_21');
    var CFF_Educacion = getValueTxt('txt_22');
    var CFF_Otros = getValueTxt('txt_23');

    _totalCostosFijosFamiliares = CFF_Comida + CFF_Vivienda + CFF_Vestido + CFF_Internet +
        CFF_Transporte + CFF_ServPublicos + CFF_Medicinas + CFF_Educacion + CFF_Otros;

    $('#txt_TotalCostosFijosFamiliares').val(formatMoney(_totalCostosFijosFamiliares));

    calcResultadosActuales();
}
function calcCostosFijosFamiliares_P() {
    var CFF_Comida = getValueTxt('txt_15_P');
    var CFF_Vivienda = getValueTxt('txt_16_P');
    var CFF_Vestido = getValueTxt('txt_17_P');
    var CFF_Internet = getValueTxt('txt_18_P');
    var CFF_Transporte = getValueTxt('txt_19_P');
    var CFF_ServPublicos = getValueTxt('txt_20_P');
    var CFF_Medicinas = getValueTxt('txt_21_P');
    var CFF_Educacion = getValueTxt('txt_22_P');
    var CFF_Otros = getValueTxt('txt_23_P');

    _totalCostosFijosFamiliares_P = CFF_Comida + CFF_Vivienda + CFF_Vestido + CFF_Internet +
        CFF_Transporte + CFF_ServPublicos + CFF_Medicinas + CFF_Educacion + CFF_Otros;

    $('#txt_TotalCostosFijosFamiliares_P').val(formatMoney(_totalCostosFijosFamiliares_P));

    calcResultadosProyectado();
}

//RESULTADOS
function initResultados() {
    calcResultadosActuales();
    calcResultadosProyectado();
}
function calcResultadosActuales() {
    //ventas y costo de ventas ya deben haberse cargado por razor
    var ventas = getValueTxt('txt_24');
    var costoVentas = getValueTxt('txt_25');

    //utilidad bruta
    var utilidadBruta = ventas - costoVentas;
    $('#txt_UtilidadBruta').val(formatMoney(utilidadBruta));

    //utilidad neta negocio
    $('#txt_CostosFijosNegocio').val(formatMoney(_totalCostosFijosMensuales));
    var utilidadNetaNegocio = utilidadBruta - _totalCostosFijosMensuales;
    $('#txt_UtilidadNetaNegocio').val(formatMoney(utilidadNetaNegocio));

    //otros Ingresos
    _otrosIngresos = _totalIngresosEmprendedor + _totalIngresosConyuge;
    $('#txt_OtrosIngresos').val(formatMoney(_otrosIngresos));

    //excedenteFamiliar
    $('#txt_ER_CFF').val(formatMoney(_totalCostosFijosFamiliares));
    _totalExcedenteFamiliarMensual = utilidadNetaNegocio + _otrosIngresos - _totalCostosFijosFamiliares;
    $('#txt_ExcedenteFamiliarMensual').val(formatMoney(_totalExcedenteFamiliarMensual));

    //***** Balance General *****
    //$('#txt_BG_disponible').val(_totalExcedenteFamiliarMensual);


    calcTotalActivos();
}

function calcResultadosProyectado() {
    //ventas y costo de ventas ya deben haberse cargado por razor
    var ventas_P = getValueTxt('txt_24_P');
    var costoVentas_P = getValueTxt('txt_25_P');

    //utilidad bruta
    var utilidadBruta_P = ventas_P - costoVentas_P;
    $('#txt_UtilidadBruta_P').val(formatMoney(utilidadBruta_P));

    //utilidad neta negocio
    $('#txt_CostosFijosNegocio_P').val(formatMoney(_totalCostosFijosMensuales_P));
    var utilidadNetaNegocio_P = utilidadBruta_P - _totalCostosFijosMensuales_P;
    $('#txt_UtilidadNetaNegocio_P').val(formatMoney(utilidadNetaNegocio_P));

    //otros Ingresos
    _otrosIngresos_P = _totalIngresosEmprendedor + _totalIngresosConyuge;  //preguntar esto
    $('#txt_OtrosIngresos_P').val(formatMoney(_otrosIngresos_P));

    //exedenteFamiliar
    $('#txt_ER_CFF_P').val(formatMoney(_totalCostosFijosFamiliares_P));
    _totalExcedenteFamiliarMensual_P = utilidadNetaNegocio_P + _otrosIngresos_P - _totalCostosFijosFamiliares_P;
    $('#txt_ExcedenteFamiliarMensual_P').val(formatMoney(_totalExcedenteFamiliarMensual_P));

    calcTotalActivos_P();
}


//BALANCE GENERAL
function initBalanceGeneral() {
    initBGActivos();
    initBGPasivos();
}

//BG Activos
function initBGActivos() {

    $("#txt_26").change(function () { calcTotalActivos(); }); //disponible
    $("#txt_27").change(function () { calcTotalActivos(); }); //cxc clientes
    $("#txt_28").change(function () { calcTotalActivos(); }); //anticipos
    $("#txt_29").change(function () { calcTotalActivos(); }); //inventarios
    $("#txt_30").change(function () { calcTotalActivos(); }); //otros
    $("#txt_31").change(function () { calcTotalActivos(); }); //activo fijo

    calcTotalActivos();

    $("#txt_26_P").change(function () { calcTotalActivos_P(); });
    $("#txt_27_P").change(function () { calcTotalActivos_P(); });
    $("#txt_28_P").change(function () { calcTotalActivos_P(); });
    $("#txt_29_P").change(function () { calcTotalActivos_P(); });
    $("#txt_30_P").change(function () { calcTotalActivos_P(); });
    $("#txt_31_P").change(function () { calcTotalActivos_P(); });

    calcTotalActivos_P();
}
function calcTotalActivos() {
    var disponible = getValueTxt('txt_26');
    var cxcClientes = getValueTxt('txt_27');
    var anticiposATerceros = getValueTxt('txt_28');
    var inventarios = getValueTxt('txt_29');
    var otros = getValueTxt('txt_30');

    var totalActivoCorriente = disponible + cxcClientes + anticiposATerceros + inventarios + otros;
    $('#txt_BG_TotalActivoCorriente').val(totalActivoCorriente);

    var activoFijo = getValueTxt('txt_31');
    _totalActivos = totalActivoCorriente + activoFijo;
    $('#txt_BG_TotalActivos').val(formatMoney(_totalActivos));

    calcPatrimonio();
}
function calcTotalActivos_P() {
    var disponible = getValueTxt('txt_26_P');
    var cxcClientes = getValueTxt('txt_27_P');
    var anticiposATerceros = getValueTxt('txt_28_P');
    var inventarios = getValueTxt('txt_29_P');
    var otros = getValueTxt('txt_30_P');

    var totalActivoCorriente_P = disponible + cxcClientes + anticiposATerceros + inventarios + otros;
    $('#txt_BG_TotalActivoCorriente_P').val(formatMoney(totalActivoCorriente_P));

    var activoFijo_P = getValueTxt('txt_31_P');
    _totalActivos_P = totalActivoCorriente_P + activoFijo_P;
    $('#txt_BG_TotalActivos_P').val(formatMoney(_totalActivos_P));

    calcPatrimonio_P();
}

//BG Pasivos
function initBGPasivos() {
    $("#txt_32").change(function () { calcTotalPasivos(); });
    $("#txt_33").change(function () { calcTotalPasivos(); });
    $("#txt_34").change(function () { calcTotalPasivos(); });
    $("#txt_35").change(function () { calcTotalPasivos(); });

    calcTotalPasivos();

    $("#txt_32_P").change(function () { calcTotalPasivos_P(); });
    $("#txt_33_P").change(function () { calcTotalPasivos_P(); });
    $("#txt_34_P").change(function () { calcTotalPasivos_P(); });
    $("#txt_35_P").change(function () { calcTotalPasivos_P(); });

    calcTotalPasivos_P();
}
function calcTotalPasivos() {
    var CxPagarProveedores = getValueTxt('txt_32');
    var AnticiposDeTerceros = getValueTxt('txt_33');
    var otrasCXP = getValueTxt('txt_34');

    var TotalPasivoCorriente = CxPagarProveedores + AnticiposDeTerceros + otrasCXP;
    $('#txt_BG_TotalPasivoCorriente').val(formatMoney(TotalPasivoCorriente));

    var pasivosLargoPlazo = getValueTxt('txt_35');
    _totalPasivos = TotalPasivoCorriente + pasivosLargoPlazo
    $('#txt_BG_TotalPasivos').val(formatMoney(_totalPasivos));

    calcPatrimonio();
}
function calcTotalPasivos_P() {
    var CxPagarProveedores = getValueTxt('txt_32_P');
    var AnticiposDeTerceros = getValueTxt('txt_33_P');
    var otrasCXP = getValueTxt('txt_34_P');

    var TotalPasivoCorriente_P = CxPagarProveedores + AnticiposDeTerceros + otrasCXP;
    $('#txt_BG_TotalPasivoCorriente_P').val(formatMoney(TotalPasivoCorriente_P));

    var pasivosLargoPlazo_P = getValueTxt('txt_35_P');
    _totalPasivos_P = TotalPasivoCorriente_P + pasivosLargoPlazo_P
    $('#txt_BG_TotalPasivos_P').val(formatMoney(_totalPasivos_P));

    calcPatrimonio_P();
}

//PATRIMONIO
function calcPatrimonio() {
    var patrimonio = _totalActivos - _totalPasivos;
    $('#txt_BG_Patrimonio').val(formatMoney(patrimonio));

    var pasivosPatrimonio = patrimonio + _totalPasivos;
    $('#txt_BG_TotalPasivosPatrimonio').val(formatMoney(pasivosPatrimonio));
}
function calcPatrimonio_P() {
    var patrimonio_P = _totalActivos_P - _totalPasivos_P;
    $('#txt_BG_Patrimonio_P').val(formatMoney(patrimonio_P));

    var pasivosPatrimonio_P = patrimonio_P + _totalPasivos_P;
    $('#txt_BG_TotalPasivosPatrimonio_P').val(formatMoney(pasivosPatrimonio_P));
}

function getValueTxt(txtId) {
    var retorno = 0;
    retorno = parseFloat($('#' + txtId).val());

    if (isNaN(retorno)) {
        retorno = 0;
        $('#' + txtId).val(retorno);
    }

    return retorno;
}


function formatMoney(amount) {
    try {

        let decimalCount = 2;
        let decimal = '.';
        let thousands = ',';

        let retorno = '';

        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        retorno = negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
        return '$ ' + retorno;

    } catch (e) {
        console.log(e)
    }
};