﻿var _productos = [];
var _productoActual = {};
var _idEmprendimiento = 0;
var _idNuevoInsumo = 0;

$(document).ready(function () { init(); });

function init() {
    console.log('init');

    _idEmprendimiento = parseInt($('#lblIdEmprendimiento').text());

    initProductos();
    initInsumos();
}

//Productos
function initProductos() {
    $('#btnAddProducto').click(function (e) {
        e.preventDefault();

        _productoActual = { id: 0, Insumos: [] };
        //_productoActual.id = 0;
        //_productoActual.Insumos = [];

        llenaTablaInsumos();
    });

    $('#btnGuardarProducto').click(function (e) {
        e.preventDefault();

        guardarProducto();
    });

    $("#txtProductoPrecio").change(function () {
        calcularVentas();
    });
    $("#txtProductoUnidadesAct").change(function () {
        calcularVentas();
    });
    $("#txtProductoUnidadesProy").change(function () {
        calcularVentas();
    });

    getProductos();
}
function calcularVentas() {
    var unidadesAct = parseInt($("#txtProductoUnidadesAct").val());
    var unidadesProy = parseInt($("#txtProductoUnidadesProy").val());
    var precio = parseFloat($('#txtProductoPrecio').val());
    var costo = parseFloat($('#txtProductoCosto').val());

    var ventasAct = unidadesAct * precio;
    var ventasProy = unidadesProy * precio;
    var costoAct = unidadesAct * costo;
    var costoProy = unidadesProy * costo;

    //pongo el total de ventas
    $('#TotalVentaActual').val('$ ' + ventasAct);
    $('#TotalVentaProy').val('$ ' + ventasProy);

    //pongo el total de costos
    $('#TotalCostoActual').val('$ ' + costoAct);
    $('#TotalCostoProy').val('$ ' + costoProy);

    //actualizo el valor de las unidades de costo 
    $('#txtProductoUnidadesActC').val(unidadesAct);
    $('#txtProductoUnidadesProyC').val(unidadesProy);
}
function getProductos() {

    var idEmprendimiento = _idEmprendimiento;
    htclibjs.Ajax({
        url: "/finanzas/getProductos",
        data: JSON.stringify({
            pIdEmp: idEmprendimiento
        }),
        success: function (r) {
            _productos = r.data;

            console.log('_productos');
            console.log(_productos);

            llenaTablaProductos();
        }
    });
}
function llenaTablaProductos() {
    $("#tblProductos").DataTable({
        data: _productos,
        aaSorting: [],
        bDestroy: true,
        bFilter: false,
        bPaginate: false,
        bLengthChange: false,
        bInfo: false,
        "language": {
            "emptyTable": "Este emprendimiento no tiene productos"
        },
        aoColumns: [
            {
                mData: 'nombre', title: 'Nombre', mRender: function (data, type, full) {
                    var dataName = 'idProducto';
                    var dataValue = full.id;
                    var clase = 'editarProducto';
                    var modal = 'ModalProducto';
                    var texto = data;
                    var lnkNombre = getHtmlLink(dataName, dataValue, clase, modal, texto);

                    return lnkNombre;
                }
            },
            {
                mData: 'porcentaje', title: '% Contrib.', mRender: function (data) {
                    console.log(data);
                    var p = parseFloat(data);
                    p = p * 100;
                    
                    return p.toFixed(2) + ' %';
                }
            },
            {
                mData: 'costo', title: 'Costo', mRender: function (data) {
                    return '$ ' + data.toFixed(2);
                }
            },
            {
                mData: 'Precio', title: 'PVP', mRender: function (data) {
                    return '$ ' + data.toFixed(2);
                }
            },
            {
                mData: 'unidadesActual', title: 'Unidades', mRender: function (data) {
                    return data;
                }
            },
            {
                mData: 'ventaActual', title: 'Ventas', mRender: function (data) {
                    return '$ ' + data.toFixed(2);
                }
            },
            {
                mData: 'id', title: '', mRender: function (data, type, full) {
                    var dataName = 'idProducto';
                    var dataValue = data;
                    var clase = 'eliminarProducto';
                    var modal = '';
                    var texto = 'eliminar';
                    var lnkNombre = getHtmlLink(dataName, dataValue, clase, modal, texto);

                    return lnkNombre;
                }
            }
        ],
        "columnDefs": [
          { className: "text-right", "targets": [1, 2, 3, 4, 5] }
        ]
    });

    $(".editarProducto").click(function (e) {
        e.preventDefault();
        console.log('click .editarProducto');

        var idProducto = $(this).data("idproducto");
        $('#ModalProducto').attr("data-idProducto", idProducto);

        for (var i = 0; i < _productos.length; i++) {
            var actual = _productos[i];

            if (actual.id == idProducto) {

                //lo convierto en objeto js para poder usar la función calcularCosto
                //debe haber algo mejor que esto, tal vez herencia
                //o hacer _productoActual = actual; y enviar sólo el array a la función y no usar el objeto ProductoJS
                //_productoActual = new ProductoJS(actual.id, actual.nombre, actual.Precio, actual.unidadesActual, actual.unidadesProyectado, actual.Insumos, actual.idEmprendimiento);
                _productoActual = {
                    id: actual.id,
                    nombre: actual.nombre,
                    Precio: actual.Precio,
                    unidadesActual: actual.unidadesActual,
                    unidadesProyectado: actual.unidadesProyectado,
                    Insumos: actual.Insumos,
                    idEmprendimiento: actual.idEmprendimiento
                };

                _productoActual.costo = getCostoProducto(_productoActual);

                cargaProductoActual();
            }
        }
    });

    $(".eliminarProducto").click(function (e) {
        e.preventDefault();
        console.log('click .eliminarProducto');

        var idProducto = $(this).data("idproducto");

        for (var i = 0; i < _productos.length; i++) {
            var actual = _productos[i];

            if (actual.id == idProducto) {

                var resp = confirm("Eliminar " + actual.nombre + "?");

                if (resp) {
                    htclibjs.Ajax({
                        url: "/finanzas/eliminarProducto",
                        data: JSON.stringify({
                            id: actual.id
                        }),
                        success: function (r) {
                            alert('Producto eliminado');
                            //actualizar tabla
                            getProductos();
                        }
                    });
                }
            }
        }
    });
}
function cargaNuevoProducto() {
    console.log('_productoActual cargaNuevoProducto');

    var producto = new ProductoJS(0, '', 0, 0, 0, [], _idEmprendimiento);
    _productoActual = producto;

    cargaProductoActual();
}
function cargaProductoActual() {
    //cargo productoActual
    console.log('cargo productoActual');
    _productoActual.costo = getCostoProducto(_productoActual);

    $('#txtProductoNombre').val(_productoActual.nombre);
    $('#txtProductoPrecio').val(_productoActual.Precio);
    $('#txtProductoUnidadesAct').val(_productoActual.unidadesActual);
    $('#txtProductoUnidadesProy').val(_productoActual.unidadesProyectado);

    //costo
    //$('#spanCosto').text(_productoActual.costo.toFixed(2));
    $('#txtProductoCosto').val(_productoActual.costo.toFixed(2));
    $('#txtProductoUnidadesActC').val(_productoActual.unidadesActual);
    $('#txtProductoUnidadesProyC').val(_productoActual.unidadesProyectado);

    calcularVentas();
    llenaTablaInsumos();
}

function guardarProducto() {
    var nombre = $('#txtProductoNombre').val();
    var precio = parseFloat($('#txtProductoPrecio').val());
    var cantidadAct = parseInt($('#txtProductoUnidadesAct').val());
    var cantidadProy = parseInt($('#txtProductoUnidadesProy').val());

    if (nombre != '' && precio != '' && cantidadAct != '' && cantidadProy != '') {
        _productoActual.nombre = nombre;
        _productoActual.Precio = parseInt(precio);
        _productoActual.unidadesActual = parseInt(cantidadAct);
        _productoActual.unidadesProyectado = parseInt(cantidadProy);

        htclibjs.Ajax({
            url: "/finanzas/guardarProducto2",
            data: JSON.stringify({
                id: _productoActual.id,
                idEmprendimiento: _productoActual.idEmprendimiento,
                nombre: _productoActual.nombre,
                Precio: _productoActual.Precio,
                unidadesActual: _productoActual.unidadesActual,
                unidadesProyectado: _productoActual.unidadesProyectado,
                costo: _productoActual.costo,
                Insumos: _productoActual.Insumos
            }),
            success: function (r) {
                alert('Producto agregado con éxito');
                //vacío Modal
                vaciarModalProducto();
                //cerrar modal
                $('#ModalProducto').modal('hide'); // cerrar
                //actualizar tabla
                //initProductos();
                getProductos();
            }
        });



        //htclibjs.Ajax({
        //    url: "/finanzas/guardarProducto",
        //    data: JSON.stringify({
        //        pProducto: _productoActual
        //    }),
        //    success: function (r) {
        //        alert('Producto agregado con éxito');
        //        vaciarModalProducto()
        //    }
        //});
    }
    else {
        alert("los campos del producto deben estar llenos");
    }
}
function vaciarModalProducto() {
    $('#txtInsumoNombre').val('');
    $('#txtInsumoPrecio').val('');
    $('#txtInsumoCantidad').val('');
}

//Insumos
function initInsumos() {
    $('#btnAddInsumo').click(function (e) {
        e.preventDefault();
        agregarInsumo();
    });
}
function agregarInsumo() {
    var nombre = $('#txtInsumoNombre').val();
    var precio = parseFloat($('#txtInsumoPrecio').val());
    var cantidad = parseInt($('#txtInsumoCantidad').val());

    _idNuevoInsumo = _idNuevoInsumo - 1;

    if (nombre != '' && precio != '' && cantidad != '') {
        var insumo = new InsumoJS(_idNuevoInsumo, nombre, precio, cantidad, _productoActual.id);
        _productoActual.Insumos.push(insumo);

        _productoActual.costo = getCostoProducto(_productoActual);
        //$('#spanCosto').text(_productoActual.costo);
        $('#txtProductoCosto').val(_productoActual.costo.toFixed(2));

        calcularVentas();

        llenaTablaInsumos();

        $('#txtInsumoNombre').val('');
        $('#txtInsumoPrecio').val('');
        $('#txtInsumoCantidad').val('');

        $('#txtInsumoNombre').focus();
    }
    else {
        alert("los campos del insumo deben estar llenos");
    }
}
function llenaTablaInsumos() {
    console.log('llenaTablaInsumos');

    $("#tblInsumos").DataTable({
        data: _productoActual.Insumos,
        aaSorting: [],
        bDestroy: true,
        bFilter: false,
        bPaginate: false,
        bLengthChange: false,
        bInfo: false,
        "language": {
            "emptyTable": "Este producto no tiene insumos"
        },
        //Nombre, %,  ventas u., ventas $, Costo, Editar
        aoColumns: [
            {
                mData: 'Nombre', title: 'Nombre', mRender: function (data) {
                    return data;
                }
            },
            {
                mData: 'cantidad', title: 'Cantidad', mRender: function (data) {
                    return data;
                }
            },
            {
                mData: 'precio', title: 'Costo', mRender: function (data) {
                    return '$ ' + data.toFixed(2);
                }
            },
            {
                mData: 'id', title: 'Costo Total', mRender: function (data, type, full) {
                    var CT = full.precio * full.cantidad;
                    return '$ ' + CT.toFixed(2);
                }
            },
            {
                mData: 'id', title: '', mRender: function (data, type, full) {

                    var lnkEliminar = '';

                    if (data != 0) {
                        //link postergar
                        var dataName = 'idInsumo';
                        var dataValue = data;
                        var clase = 'eliminarInsumo';
                        var modal = '';
                        var texto = 'eliminar';
                        lnkEliminar = getHtmlLink(dataName, dataValue, clase, modal, texto);
                    }

                    return lnkEliminar;
                }
            },
        ],
        "columnDefs": [
          { className: "text-right", "targets": [1, 2, 3] }
        ]
    });

    $(".eliminarInsumo").click(function (e) {
        e.preventDefault();
        console.log('click .eliminarInsumo');

        var idInsumo = $(this).data("idinsumo");
        console.log('idInsumo');
        console.log(idInsumo);

        if (idInsumo < 0) {
            //cuando es menor que cero sólo le quito del array
            filtrarInsumos(idInsumo);
        }
        else {
            htclibjs.Ajax({
                url: "/finanzas/eliminarInsumo",
                data: JSON.stringify({
                    id: idInsumo
                }),
                success: function (r) {
                    filtrarInsumos(idInsumo);
                }
            });
        }

        calcularVentas();

    });
}
function filtrarInsumos(idEliminado) {
    var indiceAFiltrar = 0;

    for (var i = 0; i < _productoActual.Insumos.length; i++) {
        var actual = _productoActual.Insumos[i];

        if (actual.id == idEliminado) {
            indiceAFiltrar = i;
            break;
        }
    }

    console.log('filtrando');

    console.log('indiceAFiltrar');
    console.log(indiceAFiltrar);

    var filtrado = _productoActual.Insumos.filter(function (item, index) {
        return index !== indiceAFiltrar;
    });

    console.log('filtrado');
    console.log(filtrado);

    _productoActual.Insumos = filtrado;
    llenaTablaInsumos();

}
function getHtmlLink(dataName, dataValue, clase, modal, texto) {
    var lnk = '<a href="#"';
    lnk += "data-" + dataName + "='" + dataValue + "' ";
    lnk += 'class="' + clase + '" ';
    lnk += 'data-toggle="modal" ';
    lnk += 'data-target="#' + modal + '"> ';
    lnk += texto + '</a>';
    return lnk;
}

function getCostoInsumo(insumo) {
    return insumo.precio * insumo.cantidad;
}

function getCostoProducto(producto) {
    console.log('getCostoProducto');
    var costo = 0;
    $.each(producto.Insumos, function (index, item) {
        item.costo = getCostoInsumo(item);
        costo += item.costo;
    });

    return costo;
}


//terminar 
function terminarRequisito() {
    var btnTerminarRequisito = new BtnLoad("btnTerminarRequisito");
    btnTerminarRequisito.Start();

    var idRequisito = $('#lblIdRequisito').text();
    location.href = SITEROOT + '/RequisitosPostulante/completarRequisito/' + idRequisito;
}


//clases 
class ProductoJS {
    constructor(id, nombre, Precio, unidadesActual, unidadesProyectado, Insumos, idEmprendimiento) {
        this.id = id;
        this.nombre = nombre;
        this.Precio = Precio;
        this.unidadesActual = unidadesActual;
        this.unidadesProyectado = unidadesProyectado;
        this.idEmprendimiento = idEmprendimiento;

        this.Insumos = Insumos;
        this.costo = 0;

        //this.porcentaje = 0;
        //this.ventaActual = this.unidadesActual * precio;
        //this.ventaProyectada = this.unidadesProyectado * precio;
    }

    calcularCosto() {
        console.log('ProductoJS calcularPrecio');
        var costo = 0;
        $.each(this.Insumos, function (index, item) {
            console.log(item);
            item.calcularCosto();
            costo += item.costo;
        });

        this.costo = costo;
    }
}

class InsumoJS {
    constructor(id, nombre, precio, cantidad, idProducto) {
        this.id = id;
        this.Nombre = nombre;
        this.precio = precio;
        this.cantidad = cantidad;
        this.idProducto = idProducto;
    }

    calcularCosto() {
        this.costo = this.precio * this.cantidad;
    }
}