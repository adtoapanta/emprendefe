﻿var bandera = 0;
var direccion;
var provincias = [];

$(document).ready(function () {

    initEditarEmpredimiento();

    var idEmprendimiento = $('#txtIdEmprendimiento').val();
    Scoring.MostrarCuestionario(1, idEmprendimiento, 1, "#divCuestionario", false, guardarReq, function () { });
});

function initEditarEmpredimiento() {
    console.log('init Editar empredimiento ')

    $('#btnGuardarEmprendimiento').click(function (e) {
        console.log('boton clic');
        completarEmprendimiento();
    });

    consultarProvincias();

    $("#txtProvincia").change(function () {
        cargarCiudades();
    });

    $("#txtCiudad").change(function () {
        cargarParroquias();
    });

    $("#txtProductosComercializar").change(function () {

        var tieneProducto = $("#txtProductosComercializar").val();
        if (tieneProducto == 0) {
            $("#txtVentasProducto").prop('disabled', true);
            $("#txtVentasRecurrentes").prop('disabled', true);

            $("#txtVentasProducto").val(1);
            $("#txtVentasRecurrentes").val(1);
        }
        else {
            $("#txtVentasProducto").prop('disabled', false);
            $("#txtVentasRecurrentes").prop('disabled', false);
        }

    });

    $("#txtVentasProducto").change(function () {
        var tieneVentas = $("#txtVentasProducto").val();

        if (tieneVentas == 1) {
            $("#txtVentasRecurrentes").prop('disabled', true);
            
            $("#txtVentasRecurrentes").val(1);
        }
        else {
            $("#txtVentasRecurrentes").prop('disabled', false);
        }
    });

    $('input:radio[name=radioDireccion]').change(function () {
        if (this.value == '0') {
            $('#divDireccion').hide();;
            bandera = 0;
        }
        else if (this.value == '1') {
            $('#divDireccion').show();
            bandera = 1;
        }
    });

    $("#btnInformeDiagnosticoEmprendedor").click(function (e) {
        e.preventDefault();

        location.href = SITEROOT + "/Postulacion/DescargarDiagnosticoEmprendedor/?idPostulacion=" + self.idPostulante;
    });

    consultarEmprendimientoId();

}

function guardarReq() {
    alert("Se guardaron respuestas, ahora llamar a guardar req");
}

function yoGuardo() {
    //alert("llamar a guardar req antes de guardar preguntas");

    Scoring.GuardarExt(function () {
        alert("guardado. Tal vez salir..");
    })
}

function completarEmprendimiento() {

    // datos emprendimiento
    var idEmprendimiento = $('#txtIdEmprendimiento').val();
    var ruc = $('#txtRucEmprendimiento').val();
    var descripcion = $('#txtDescripcionEmprendimiento').val();
    var tieneVentas = $('#txtVentasProducto').val();
    var tiempoVentas = $('#txtVentasRecurrentes').val();
    var estaConstituida = setValue($('#txtConstituida').val());
    var tieneProducto = setValue($('#txtProductosComercializar').val());
    var idMedioInformacion = $('#txtMedioComunicacion').val();


    if (bandera == 1) {
        // datos direcion

        var idTipoLugar = $('#txtTipoLugar').val();
        var idRef = 0; //parametro se guarda el real en controlador
        var idTipo = 0; //parametro se guarda el real en controlador
        var linea1 = $('#txtCallePrincipal').val();
        var linea2 = $('#txtCalleSecundaria').val();
        var numero = $('#txtNumero').val();
        //var nombre = $('#txtNombre').val();
        var referencia = $('#txtReferencia').val();
        var barrio = $('#txtBarrio').val();
        var idParroquia = $('#txtParroquia').val();
        direccion = new direccionPersona(idTipoLugar, idRef, idTipo, linea1, linea2, numero, "Emprendimiento", referencia, barrio, idParroquia);
    }

    if (Scoring.Validar() === true) {

        

        htclibjs.Ajax({
            url: "/Emprendimiento/completarEmprendimiento",
            data: JSON.stringify({
                idEmprendimiento: idEmprendimiento, descripcion: descripcion, ruc: ruc, tieneProducto: tieneProducto, tieneVentas: tieneVentas, tiempoVentas: tiempoVentas, direccion: direccion, bandera: bandera, estaConstituida: estaConstituida, idMedioInformacion: idMedioInformacion
            }),
            success: function (r) {

                Scoring.GuardarExt();

                var url = SITEROOT;
                url += '/Persona/Dashboard';
                window.location.href = url;
            }
        });
    }
}

function consultarDireccionPersona() {

    htclibjs.Ajax({
        url: "/Persona/consultarDireccionPersona",
        data: JSON.stringify({

        }),
        success: function (r) {

            direccion = r.data;
        }
    });
}

function consultarProvincias() {

    htclibjs.Ajax({
        url: "/Provincia/consultarPronviciasInclude",
        data: JSON.stringify({

        }),
        success: function (r) {

            $.each(r.data, function (i, item) {

                provincias.push(item);

            });
            cargarProvincias();
        }
    });
}

function cargarProvincias() {

    provincias.sort(compare);
    $('#txtProvincia').append(new Option('seleccione', 0, true));
    for (var i = 0; i < provincias.length; i++) {

        $('#txtProvincia').append(new Option(provincias[i].nombre, provincias[i].id));

    }

}

function cargarCiudades() {

    var provinciaId = $('#txtProvincia').val();

    $("#txtCiudad").empty();
    $('#txtCiudad').append(new Option('seleccione', 0, true));

    for (var i = 0; i < provincias.length; i++) {

        if (provincias[i].id == provinciaId) {

            provincias[i].Ciudades.sort(compare);
            for (var j = 0; j < provincias[i].Ciudades.length; j++) {

                $('#txtCiudad').append(new Option(provincias[i].Ciudades[j].nombre, provincias[i].Ciudades[j].id));
            }
        }

    }

}

function cargarParroquias() {

    var idProvincia = $("#txtProvincia").val();
    var idCiudad = $("#txtCiudad").val();

    var provincia = provincias.find(x => x.id == idProvincia);
    var ciudad = provincia.Ciudades.find(x => x.id == idCiudad);

    $('#txtParroquia').empty();
    $('#txtParroquia').append(new Option('seleccione', 0, true));

    ciudad.Parroquias.sort(compare);

    for (var i = 0; i < ciudad.Parroquias.length; i++) {

        $('#txtParroquia').append(new Option(ciudad.Parroquias[i].nombre, ciudad.Parroquias[i].id));

    }
}


function direccionPersona(idTipoLugar, idRef, idTipo, linea1, linea2,
    numero, nombre, referencia, barrio, idParroquia) {

    var direccion =
    {
        id: 0,
        idTipoLugar: idTipoLugar,
        idRef: idRef,
        idTipo: idTipo,
        linea1: linea1,
        linea2: linea2,
        numero: numero,
        nombre: nombre,
        referencia: referencia,
        barrio: barrio,
        idParroquia: idParroquia
    }

    return direccion;

}

function consultarEmprendimientoId() {

    var idEmprendimiento = $('#txtIdEmprendimiento').val();

    htclibjs.Ajax({
        url: "/Emprendimiento/consultarEmprendimientoId",
        data: JSON.stringify({
            idEmprendimiento: idEmprendimiento
        }),
        success: function (r) {

            var emprendimiento = r.data;
            var direccion = emprendimiento.MIS_Direcciones;

            if (emprendimiento.completo == true) {

                $('#txtRucEmprendimiento').val(emprendimiento.ruc);
                $('#txtDescripcionEmprendimiento').val(emprendimiento.descripcion);
                $('#txtProductosComercializar').val(getValue(emprendimiento.tieneProducto));
                $('#txtVentasProducto').val(emprendimiento.tieneVentas);
                $('#txtVentasRecurrentes').val(emprendimiento.tiempoVentas);
                $('#txtConstituida').val(getValue(emprendimiento.estaConstituida));
                $('#txtMedioComunicacion').val(emprendimiento.idMediosInformacion);

                if (direccion.nombre == 'Hogar') {

                    $("#mismaDireccion").prop("checked", true);

                }

                else {

                    $("#otraDireccion").prop("checked", true);
                    $('#txtTipoLugar').val(direccion.idTipoLugar);
                    $('#txtCallePrincipal').val(direccion.linea1);
                    $('#txtCalleSecundaria').val(direccion.linea2);
                    $('#txtNumero').val(direccion.numero);
                    //var nombre = $('#txtNombre').val();
                    $('#txtReferencia').val(direccion.referencia);
                    $('#txtBarrio').val(direccion.barrio);
                    $('#txtProvincia').val(direccion.Parroquias.Ciudades.Provincias.id);
                    cargarCiudades();
                    $('#txtCiudad').val(direccion.Parroquias.Ciudades.id);
                    cargarParroquias();
                    $('#txtParroquia').val(direccion.idParroquia);

                    $('#divDireccion').show();
                    bandera = 1;

                }
            }
        }
    });

}

function getValue(valor) {

    if (valor == true)
        return 1;
    else
        return 0;

}

function setValue(valor) {
    if (valor == 1)
        return true;
    else
        return false;
}

function compare(a, b) {

    var textA = a.nombre.toUpperCase();
    var textB = b.nombre.toUpperCase();
    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
}