﻿$(document).ready(function () { initCrearEmpredimiento(); });

function initCrearEmpredimiento() {
    console.log('init crear empredimiento ')

    $('#btnCrearEmprendimiento').click(function (e) {
        crearEmprendimiento();
    });

    //$('#divCriteriosConvocatoria').html(htmlDecode(html));

    //$('#btnAbrirRegistro').click(function (e) {
    //    console.log('clic');
    //    $('#modalIniciarSesion').modal('hide')
    //});
    //$('#btnRegresar').click(function (e) {
    //    console.log('clic');
    //    $('#modalRegistrarUsuario').modal('hide')
    //});

}

function crearEmprendimiento() {
        
    var nombreEmprendimiento = $("#txtNombreEmprendimiento").val();
    var boton = new BtnLoad("btnCrearEmprendimiento");
    boton.Start();

    htclibjs.Ajax({
        url: "/Emprendimiento/guardarEmprendimiento",
        data: JSON.stringify({
            nombre: nombreEmprendimiento
        }),
        success: function (r) {

            var idEmprendimiento = r.data;
            var url = SITEROOT + '/Emprendimiento/EditarEmprendimiento/' + idEmprendimiento;
            window.location.href = url;
            boton.Stop();
        }
    });
}

function guardarSocio() {

    console.log("Ingreso guardar socio");
    var idEmprendimiento = $("#txtIdEmprendimiento").val();
    var idSocio = $("#txtIdSocio").val();
    var nombreSocio = $("#txtnombreSocio").val();
    var telefonoSocio = $("#txtTelefonoSocio").val();
    var cargoSocio = $("#txtCargoSocio").val();
    var aporteSocio = $("#txtAporteSocio").val();
    var porcentajeSocio = $("#txtPorcentajeSocio").val();




    htclibjs.Ajax({
        url: "/Socio/agregarSocioEmprendimiento",
        data: JSON.stringify({
            idEmprendimiento: idEmprendimiento, nombre: nombreSocio, telefono: telefonoSocio, cargo: cargoSocio, aporte: aporteSocio, porcentaje: porcentajeSocio
        }),
        success: function (r) {

            alert("Item Guardado");
            consultarMatrizInversion();

        }
    });

    limpiarCampos();
}