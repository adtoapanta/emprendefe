﻿$(document).ready(function () { administrar.inicio(); });

var administrar = {
    idConvocatoria:0,
    idEtapa:0,
    postulaciones: {},
    btnEjecutar: new BtnLoad('btnEjecutar'),
    btnExtenderPlazo: new BtnLoad('btnExtenderPlazo'),
    inicio: function () {
        var self = this;

        self.idEtapa = $("#txtIdEtapa").val();
        self.idConvocatoria = $("#txtIdConvocatoria").val();

        $("#btnEjecutar").click(function () {
            self.ejecutar();
        });
        $("#btnExtenderPlazo").click(function () {
            self.guardarExtenderPlazo();
        });
        
        self.getPostulantes();
        self.showAcciones();
    },

    ejecutar: function () {
        var accion = $("#slcAcciones").val();

        switch (accion) {
            case "extenderplazo":
                this.extender();
                break;
            case "cerrarinscripciones":
                this.cerrarInscripciones();
                break;
        }
    },
    extender: function () {
        $('#extenderPlazoModal').modal('show');
    },
    guardarExtenderPlazo: function () {
        var self = this;
        var fecha = $("#txtFechaPlazo").val();
        self.btnExtenderPlazo.Start();
        htclibjs.Ajax({
            url: "/Convocatoria/ExtenderPlazo",
            data: JSON.stringify({ idC: self.idConvocatoria, f:fecha }),
            success: function (r) {
                $('#extenderPlazoModal').modal('hide')
                $("#dvFechaCierre").html(getOutDate(new Date(r.data)));
                self.btnExtenderPlazo.Stop();
            },
            successError: function () {
                self.btnExtenderPlazo.Stop();
            },
            error: function () {
                self.btnExtenderPlazo.Stop();
            }
        });
    },
    cerrarInscripciones: function () {
        var self = this;
        if (confirm("Seguro de cerrar inscripciones?")) {
            self.btnEjecutar.Start();
            htclibjs.Ajax({
                url: "/Convocatoria/CerrarInscripciones",
                data: JSON.stringify({ idC: self.idConvocatoria }),
                success: function (r) {
                    window.location.href = SITEROOT + "/Convocatoria/Convocatorias/";
                    self.btnEjecutar.Stop();
                },
                successError: function () {
                    self.btnEjecutar.Stop();
                },
                error: function () {
                    self.btnEjecutar.Stop();
                },
            });

        }
    },
    getPostulantes: function () {
        var self = this;
        
        htclibjs.Ajax({
            url: "/Convocatoria/GetPostulaciones",
            data: JSON.stringify({ idC: self.idConvocatoria }),
            success: function (r) {
                self.postulaciones = r.data;
                self.showPostulaciones();
            }
        });
    },

    showAcciones: function () {
        var self = this;
        $("#slcAcciones option").remove();
        
        switch (parseInt(self.idEtapa)) {
            case 3://Inscripcion
                $("#slcAcciones").append(new Option("Extender plazo", "extenderplazo"));
                $("#slcAcciones").append(new Option("Cerrar inscripciones", "cerrarinscripciones"));
                break;
            case 4://Primer cierre
                $("#slcAcciones").append(new Option("Seleccione", ""));
                break;
            default:
                $("#slcAcciones").append(new Option("Seleccione", ""));
        } 

    },

    showPostulaciones: function () {
        var self = this;
        //Destruimos antes de acrgar nuevos
        if ($.fn.DataTable.isDataTable('#tblPostulaciones')) {
            $('#tblPostulaciones').DataTable().destroy();
        }
        $('#tblPostulaciones tbody').empty();

        $("#tblPostulaciones").DataTable({
            data: self.postulaciones,
            aaSorting: [],
            bDestroy: true,
            bFilter: true,
            bPaginate: true,
            bLengthChange: true,
            language: SPANISHDATATABLE,
            aoColumns: [
                {
                    width: '50%', mData: 'emprendimiento', title: 'Emprendimiento',
                    mRender: function (data, type, full) {
                        return '<a href="' + SITEROOT + '/Postulacion/Index/'+full.id+'">' + data + '</a>';
                    }
                },
                {
                    width: '25%', mData: 'emprendedor', title: 'Emprendedor',
                    mRender: function (data, type, full) {
                        return data;
                    }
                },
                {
                    width: '25%', mData: 'fecha', title: 'Fecha',
                    mRender: function (data, type, full) {
                        var f = new Date(data);
                        return getOutDate(f);
                    }
                },
            ],
        });
    }
}