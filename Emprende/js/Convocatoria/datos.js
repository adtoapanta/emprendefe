﻿//Se llama Modelo De Negocio
$(document).ready(function () { administrar.inicio(); });

var administrar = {
    idConvocatoria: 0,
    idEtapa: 0,
    postulaciones: {},
    btnEjecutar: new BtnLoad('btnEjecutar'),
    btnExtenderPlazo: new BtnLoad('btnExtenderPlazo'),
    inicio: function () {
        var self = this;

        self.idConvocatoria = $("#txtIdConvocatoria").val();
        //Boton de acciones
        $("#btnEjecutar").click(function () {
            self.ejecutar();
        });
        //boton de modal extender plazo
        $("#btnExtenderPlazo").click(function () {
            self.guardarExtenderPlazo();
        });

        var fecha = $("#dvFechaCierre").data("fechacierre");
        if (fecha === "") {
            $("#dvFechaCierre").hide();
        } else {
            $("#dvFechaCierre label").html("Fecha de cierre: "+fecha);
            $("#dvFechaCierre").show();
        }

        self.getPostulantes();
    },
    showFechaCierre: function () {
        var fecha = $("#dvFechaCierre").data("fechacierre");
        if (fecha === "") {
            $("#dvFechaCierre").hide();
        } else {
            $("#dvFechaCierre label").html("Fecha de cierre: " + fecha);
            $("#dvFechaCierre").show();
        }
    },
    ejecutar: function () {
        var accion = $("#slcAcciones").val();

        switch (accion) {
            case "extenderPlazo":
                this.extender();
                break;
            case "cerrarEtapa":
                this.cerrarEtapa();
                break;
        }
    },
    extender: function () {
        $('#extenderPlazoModal').modal('show');
    },
    guardarExtenderPlazo: function () {
        var self = this;
        var fecha = $("#txtFechaPlazo").val();
        self.btnExtenderPlazo.Start();
        htclibjs.Ajax({
            url: "/Convocatoria/ExtenderPlazoCierreDatos",
            data: JSON.stringify({ idC: self.idConvocatoria, f: fecha }),
            success: function (r) {
                self.btnExtenderPlazo.Stop();
                $('#extenderPlazoModal').modal('hide')
                $("#dvFechaCierre").data("fechacierre", getOutDate(new Date(r.data)));
                self.showFechaCierre();
            }
        });
    },
    cerrarEtapa: function () {
        var self = this;
        if (confirm("Seguro de cerrar la etapa?")) {
            self.btnEjecutar.Start();
            htclibjs.Ajax({
                url: "/Convocatoria/CerrarEtapa",
                data: JSON.stringify({ idC: self.idConvocatoria }),
                success: function (r) {
                    window.location.href = SITEROOT + "/Convocatoria/Convocatorias/";
                    self.btnEjecutar.Stop();
                },
                successError: function (r) {
                    self.btnEjecutar.Stop();
                },
                error: function (r) {
                    self.btnEjecutar.Stop();
                }
            });

        }
    },
    getPostulantes: function () {
        var self = this;

        htclibjs.Ajax({
            url: "/Convocatoria/GetPostulaciones",
            data: JSON.stringify({ idC: self.idConvocatoria }),
            success: function (r) {
                self.postulaciones = r.data;
                self.showPostulaciones();
            }
        });
    },

    showPostulaciones: function () {
        var self = this;
        //Destruimos antes de acrgar nuevos
        if ($.fn.DataTable.isDataTable('#tblPostulaciones')) {
            $('#tblPostulaciones').DataTable().destroy();
        }
        $('#tblPostulaciones tbody').empty();

        $("#tblPostulaciones").DataTable({
            data: self.postulaciones,
            aaSorting: [],
            bDestroy: true,
            bFilter: true,
            bPaginate: true,
            bLengthChange: true,
            language: SPANISHDATATABLE,
            aoColumns: [
                {
                    width: '40%', mData: 'emprendimiento', title: 'Emprendimiento',
                    mRender: function (data, type, full) {
                        return '<a href="' + SITEROOT + '/Postulacion/Index/' + full.id + '">' + data + '</a>';
                    }
                },
                {
                    width: '40%', mData: 'emprendedor', title: 'Emprendedor',
                    mRender: function (data, type, full) {
                        return data;
                    }
                },
                /*{
                    width: '25%', mData: 'revisadoEtapa', title: 'Revisado', className: "text-center",
                    mRender: function (data, type, full) {
                        //no controlo nulos, no hay
                        var ht = '<i id="icn' + full.id + '" class="' + (full.revisadoEtapa ? "fas fa-check" : "fas fa-times") + '" style="font-size:25px; color:' + (full.revisadoEtapa ? "green" : "red") + ';"></i>';
                        return ht;
                    }
                },*/
                {
                    width: '20%', mData: 'apruebaEtapa', title: 'Completo', className: "text-center",
                    mRender: function (data, type, full) {
                        //no controlo nulos, no hay
                        var ht = '<i id="icn' + full.id + '" class="' + (full.apruebaEtapa ? "fas fa-check" : "fas fa-times") + '" style="font-size:25px; color:' + (full.apruebaEtapa ? "green" : "red") + ';"></i>';
                        return ht;
                    }
                },
            ],
        });
    }
}