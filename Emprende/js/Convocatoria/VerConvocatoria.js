﻿$(document).ready(function () { initVerConvocatoria(); });

function initVerConvocatoria() {
    console.log('init ver Convocatoria')
    var html = $('#txtHTML').val();
    $('#btnRegistrar').click(function (e) {
        registrar();
    });

    $('#divCriteriosConvocatoria').html(htmlDecode(html));

    $('#btnAbrirRegistro').click(function (e) {
        console.log('clic');
        $('#modalIniciarSesion').modal('hide')
    });
    $('#btnRegresar').click(function (e) {
        console.log('clic');
        $('#modalRegistrarUsuario').modal('hide')
    });

    $('#btnPostularConEmprendimiento').click(function (e) {
        postular();
    });

    $('#btnPostularSinUsuario').click(function (e) {
        window.location.href = SITEROOT;
    });

    $("#slcEmprendimientosPersona").change(function () {
        $('#txtIdEmprendimiento').val($('#slcEmprendimientosPersona').val());
    });

    consultarIdPublico();
    $('#btnPostularSinEmprendimiento').click(function (e) {
        consultarEmprendimientosCompletos();
    });


}

function postular() {

    var idConvocatoria = $('#txtIdConvocatoria').val();
    var idEmprendimiento = $('#txtIdEmprendimiento').val();

    if ($("#chckCondiciones").is(':checked')) {
        htclibjs.Ajax({
            url: "/Postulacion/postular",
            data: JSON.stringify({
                idConvocatoria: idConvocatoria, idEmprendimiento: idEmprendimiento
            }),
            success: function (r) {

                toastr.success('', 'Se ha registrado su inscripción. Se comunicarán los siguientes pasos', { positionClass: "toast-bottom-right" });
                var url = SITEROOT + '/Persona/Dashboard'
                window.location.href = url;

            }
        });
    }
    else {

        toastr.error('', 'Primero debe aceptar las condiciones de la convocatoria', { positionClass: "toast-bottom-right" });     

    }



}

function consultarEmprendimientosCompletos() {



    htclibjs.Ajax({
        url: "/Emprendimiento/consultarEmprendimientosCompletosPorPersona",
        data: JSON.stringify({

        }),
        success: function (r) {

            if (r.data == 0) {

                $('#modalEmprendimientosPersona').modal('hide');
                alert('No tiene emprendimientos listos para postular, favor complete un emprendimiento');
                var url = SITEROOT + '/Persona/Dashboard'
                window.location.href = url;

            }
            else {
                console.log('con emprendimientos');
                var data = r.data;
                console.log(data);
                $('#slcEmprendimientosPersona').empty();
                $('#slcEmprendimientosPersona').append(new Option('Seleccione', '', true));

                $.each(data, function (i, item) {

                    $('#slcEmprendimientosPersona').append(new Option(item.nombre, item.id, true));

                });

            }
        }
    });

}

function htmlDecode(input) {

    if (input != '') {
        var e = document.createElement('div');
        e.innerHTML = input;
        return e.childNodes[0].nodeValue;
    }
}

function consultarIdPublico() {

    var idConvocatoria = currentSession.selectedConvocatoria;
    if (idConvocatoria == 0) {
        /*var url = window.location.href.toString();
        var urlSplit = url.split("/");
        idConvocatoria = urlSplit[urlSplit.length - 1];
        currentSession.selectedConvocatoria = idConvocatoria;*/
        currentSession.selectedConvocatoria = parseInt($("#txtIdConvocatoria").val());
    }

    htclibjs.Ajax({
        url: "/Convocatoria/consultarIdPublico",
        data: JSON.stringify({
            idReferencia: currentSession.selectedConvocatoria
        }),
        success: function (r) {
            var url = SITEROOT + '/Convocatoria/Descarga/' + r.data;
            $('#btnDescargarBases').attr('href', url);

        }
    });

}

function registrar() {
    console.log('clic boton');
    var nombrePersona = $('#txtNombreUsuario').val();
    var apellidoPersona = $('#txtApellidoUsuario').val();
    var cedulaPersona = $('#txtCedulaUsuario').val();
    var telefonoPersona = $('#txtTelefonoUsuario').val();
    var correoPersona = $('#txtCorreoUsuario').val();
    var nacionalidadPersona = $('#txtNacionalidadUsuario').val();
    var ciudadPersona = $('#txtCiudadUsuario').val();
    var sexoPersona = $('#txtSexoUsuario').val();;

    var url = SITEROOT;
    htclibjs.Ajax({
        url: "/Usuario/crearPersona",
        data: JSON.stringify({
            nombre: nombrePersona, apellido: apellidoPersona, documento: cedulaPersona, telefono: telefonoPersona, sexo: sexoPersona, nacionalidad: nacionalidadPersona, correo: correoPersona, ciudad: ciudadPersona
        }),
        success: function (r) {
            currentSession.personaId = r.data
            url += '/Usuario/Dashboard/' + currentSession.personaId;
            window.location.href = url;

        }
    });

}
