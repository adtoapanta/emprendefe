﻿//Se llama Diagnóstico
$(document).ready(function () { verificacion.inicio(); });

var verificacion = {
    idPostulante: 0,
    documentos: {},
    documentosOpcionales: {},
    btnGuardarCerrarVerificacion: new BtnLoad('btnGuardarCerrarVerificacion'),
    btnAsignarDiagnostico: new BtnLoad('btnAsignarDiagnostico'),
    plantillaDocumentosPedir: '<div class="form-check"><input class="form-check-input" name="chkDocumentos" type= "checkbox" value= "__idDocumento__" id= "chk__idDocumento__" __check__ data-descripcion="__descripcion__"><label class="form-check-label" for="chk__idDocumento__">__nombre__</label></div>',
    permiso: null,
    inicio: function () {
        var self = this;
        self.idPostulante = $("#txtIdPostulante").val();
        self.idConvocatoria = $("#txtIdConvocatoria").val();
        self.getRequisitosPostulantes();
        self.getDocumentos();
        $("#addDocumento").hide();
        
        $("#btnCerrarEtapaVerificacion").click(function () {
            $('#verificacionModal').modal('show');
        });
        $("#btnCerrarEtapaVerificacion").hide();

        $("#btnGuardarCerrarVerificacion").click(function () {
            self.guardarDocumentos();
        });

        $("#btnAddBorderOpcional").click(function () {
            $("#addDocumento").show();
        });
        $("#btnAddOpcional").click(function () {
            self.addDocumentoPedir();
        });
        self.permiso = $("#txtPermiso").val();
        
        $(document).on('click', '.btnRequisitosVerificar', function (e) {
            e.preventDefault();
            var idRP = $(this).data("idrequisito");
            if (self.permiso == "Editar") {
                self.verificarRequisito(idRP);
            } else {
                toastr.warning('No tiene permiso para verificar.', '', { positionClass: "toast-bottom-right" });
            }
        });

        if ($("#btnAsignarResponsable").length > 0) {
            $("#btnAsignarResponsable").click(function () {
                self.showAsignarResponsable();
            });

            $("#btnAsignarDiagnostico").click(function () {
                self.asignarResponsable();
            });
        }
    },

    showAsignarResponsable: function () {
        $('#mdlAsignarResposable').modal('show');
    },

    asignarResponsable: function () {
        var self = this;
        var idUsuario = $("#slcUsuarioAsignar").val();
        var nombre = $("#slcUsuarioAsignar option:selected").text();

        if (idUsuario !== "0") {
            self.btnAsignarDiagnostico.Start();
            htclibjs.Ajax({
                url: "/Convocatoria/AsignarResponsableVerificacion",
                data: JSON.stringify({ idP: self.idPostulante, idA: idUsuario }),
                success: function (r) {
                    toastr.info('Guardado', '', { positionClass: "toast-bottom-right" });
                    self.btnAsignarDiagnostico.Stop();
                    $('#mdlAsignarResposable').modal('hide');
                    $('#spnNombreUsuarioAsignado').text(nombre);
                },
                error: function () {
                    self.btnAsignarDiagnostico.Stop();
                },
                errorSuccess: function () {
                    self.btnAsignarDiagnostico.Stop();
                }
            });
        } else {
            toastr.error('Seleccione un usuario a asignar', '', { positionClass: "toast-bottom-right" });
        }
    },

    addDocumentoPedir: function () {
        var self = this;
        var nombre = $("#txtNombreDocumentoOpcional").val();
        var idDocumento = $("#slcTipoDocumentoOpcional").val();

        if ($.trim(nombre) === "") {
            toastr.error('Ingrese el nombre del documento', '', { positionClass: "toast-bottom-right" });
            return false;
        }
        if (idDocumento === "0") {
            toastr.error('Seleccione el tipo de documento', '', { positionClass: "toast-bottom-right" });
            return false;
        }

        var ht = self.plantillaDocumentosPedir.replace(/\__idDocumento__/g, idDocumento).replace(/\__nombre__/g, nombre).replace(/\__check__/g, "checked").replace(/\__descripcion__/g, nombre);
        $("#dvArchivosNecesarios").append(ht);

        $("#txtNombreDocumentoOpcional").val("");
        $("#slcTipoDocumentoOpcional").val("0");
        $("#addDocumento").hide();
    },

    guardarDocumentos: function () {
        var self = this;
        var cargados = [];
        var cargado = {};
        $.each($("input[name='chkDocumentos']:checked"), function () {
            cargado = { idDocumento: $(this).val(), observaciones: $(this).data('descripcion') };//el resto de info la llena el server
            cargados.push(cargado);
        });

        self.btnGuardarCerrarVerificacion.Start();
        htclibjs.Ajax({
            url: "/Convocatoria/GuardarDocumentosPedir",
            data: JSON.stringify({ idP: self.idPostulante, docs: cargados }),
            success: function (r) {
                toastr.info('Guardado', '', { positionClass: "toast-bottom-right" });
                self.btnGuardarCerrarVerificacion.Stop();
                self.cerrar();
            }
        });

        
    },
    getRequisitosPostulantes: function () {
        var self = this;
        htclibjs.Ajax({
            url: "/Convocatoria/GetRequisitosPostulante",
            data: JSON.stringify({ idP: self.idPostulante }),
            success: function (r) {
                var postulantes = r.data;
                self.showRequisitosPostulantes(postulantes);
            }
        });

    },
    getDocumentos: function () {
        var self = this;
        htclibjs.Ajax({
            url: "/Convocatoria/GetDocumentosArchivos",
            data: null,
            success: function (r) {
                console.log(r.data);
                self.documentos = r.data.documentosNecesarios;
                self.documentosOpcionales = r.data.documentosOpcionales;
                self.showDocumentos();
                self.showDocumentosOpcionales();
            }
        });
    },
    showDocumentos: function () {
        var self = this;
        var ht = '<p> Documentos a pedir:</p>';
        $.each(self.documentos, function (index, doc) {
            var elDoc = doc.ARC_Documentos;
            ht += self.plantillaDocumentosPedir.replace(/\__idDocumento__/g, doc.idDocumento).replace(/\__nombre__/g, elDoc.nombre).replace("/\__check__/g", "").replace(/\__descripcion__/g, "");
        });
        $("#dvArchivosNecesarios").html(ht);
    },
    showDocumentosOpcionales: function () {
        var self = this;
        $("#slcTipoDocumentoOpcional").empty();
        $("#slcTipoDocumentoOpcional").append(new Option("Tipo de documento", 0));
        $.each(self.documentosOpcionales, function (index, doc) {
            $("#slcTipoDocumentoOpcional").append(new Option(doc.nombre, doc.id));
        });
    },

    cerrar: function () {
        var self = this;
        self.btnGuardarCerrarVerificacion.Start();
        htclibjs.Ajax({
            url: "/Convocatoria/CerrarEtapaPostulante",
            data: JSON.stringify({ idP: self.idPostulante }),
            success: function (r) {
                self.btnGuardarCerrarVerificacion.Stop();
                window.location.href = SITEROOT + "/Convocatoria/Postulantes/" + self.idConvocatoria;
            }
        });
    },

    verificarRequisito: function (idRp) {
        htclibjs.Ajax({
            url: "/Convocatoria/VerificarRequisitosPostulante",
            data: JSON.stringify({ idRP: idRp }),
            success: function (r) {
                $("#i" + idRp).removeClass('fa-times').removeClass('prohibido').addClass('fa-check').css('color', 'green');
                $("#btn" + idRp).remove();
                if ($(".prohibido").length == 0) $("#btnCerrarEtapaVerificacion").show();
            }
        });
    },

    showRequisitosPostulantes: function (requisitos) {
        var self = this;

        var plantilla = '<button type="button" id="btn__id__" class="btn btn-info btnRequisitosVerificar" data-idrequisito="__id__" ><i class="far fa-check-circle"></i></button>';
        if ($.fn.DataTable.isDataTable('#tblRequisitos')) {
            $('#tblRequisitos').DataTable().destroy();
        }
        $('#tblRequisitos tbody').empty();

        
        var count = requisitos.filter(x => x.verificado === false);
        if (count.length == 0) $("#btnCerrarEtapaVerificacion").show();

        $("#tblRequisitos").DataTable({
            data: requisitos,
            aaSorting: [],
            bDestroy: true,
            bFilter: true,
            bPaginate: true,
            bLengthChange: true,
            language: SPANISHDATATABLE,
            aoColumns: [
                {
                    width: '50%', mData: 'emprendimiento', title: 'Requisito',
                    mRender: function (data, type, full) {

                        return '<a href="' + SITEROOT + full.CON_requisitos.urlPath + '' + self.idPostulante + '/' + full.id + '" onclick="showLoading();">' + full.CON_requisitos.nombre + '</a>';
                    }
                },
                {
                    width: '20%', mData: 'completo', title: 'Completo', className: 'text-center',
                    mRender: function (data, type, full) {
                        return (data ? '<i class="fa fa-check" style="color:green;"></i>' : '<i id="i' + full.id + '" class="fa fa-times prohibido" style="color:red;"></i>');
                    }
                },
                {
                    width: '20%', mData: 'verificado', title: 'Verificado', className: 'text-center',
                    mRender: function (data, type, full) {
                        return (data ? '<i class="fa fa-check" style="color:green;"></i>' : '<i id="i'+full.id+'" class="fa fa-times prohibido" style="color:red;"></i>');
                    }
                },
                {
                    width: '10%', mData: 'id', title: 'Verificar', className: "text-center",
                    mRender: function (data, type, full) {
                        return (((full.verificado == null || full.verificado == false) && full.completo == true) ? plantilla.replace(/\__id__/g, data) : '');
                    }
                },
            ],
        });
    },
};