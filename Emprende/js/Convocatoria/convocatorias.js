﻿$(document).ready(function () { initConvocatoria(); });

function initConvocatoria() {
    $('#btnCrearConvocatoria').click(function (e) {
        crearConvocatoria();
    });
    consultarConvocatoria();

}


function crearConvocatoria() {
    var nombreCon = $("#txtNombreConvocatoria").val();

    if ($.trim(nombreCon) === '') {
        toastr.error('No se puede crear sin nombre', 'Error.', { positionClass: "toast-bottom-right" });
        return false;
    }

    var btnFiltrar = new BtnLoad("btnCrearConvocatoria");
    btnFiltrar.Start();
    htclibjs.Ajax({
        url: "/Convocatoria/crearConvocatoria",
        data: JSON.stringify({
            nombreConvocatoria: nombreCon
        }),
        success: function (r) {
            var id = r.data;
            btnFiltrar.Stop();
            window.location.href = SITEROOT + "/Convocatoria/Editar/" + id;

        },
        successError: function () {
            btnFiltrar.Stop();
        }
    });
}

function consultarConvocatoria() {

    htclibjs.Ajax({
        url: "/Convocatoria/cosultarConvocatorias",
        data: JSON.stringify({ }),
        success: function (r) {
            var data = r.data;
            var trHTML =
                '<thead>' +
                '<tr>' +                
                '<th>Nombre</th>' +
                '<th>Etapa</th>' +
                '<th>Apertura</th>' +
                '<th>Cierre</th>' +
                '<th>Descripción</th>' +
                '<th></th>' +
                '</tr> ' +
                '</thead> ' +
                '<tbody>';

            $.each(data, function (i, item) {
                var fechaApertura = getOutDate(new Date(item.fechaApertura));
                var fechaCierre = getOutDate(new Date(item.fechaCierre));

                trHTML += '<tr>' +
                    '<td>' + item.nombre + '</td>' +
                    '<td>' + item.etapa + '</td>' +
                    '<td id="tdFechaAperturaConvocatoria' + item.id + '">' + fechaApertura + '</td>' +
                    '<td id="tdFechaCierreConvocatoria' + item.id + '">' + fechaCierre + '</td>' +
                    '<td id="tdDescripcionConvocatoria' + item.id + '">' + (item.descripcion === null ? "" : item.descripcion) + '</td>' +
                    '<td class="text-center"> <a href="' + SITEROOT + '/Convocatoria/Etapa/' + item.id +'" class="btn btn-primary btn-sm" ><i class="fa fa-cog"></i></a> </td>' +
                    '</tr> ';
            });
            trHTML += '</tbody>'

            $('#tblConvocatorias').html('');
            $('#tblConvocatorias').append(trHTML);
            $('#tblConvocatorias').DataTable({
                aaSorting: [], language: SPANISHDATATABLE, dom: 'Bfrtip', buttons: [{ extend: 'excel', className: 'btn btn-primary', text: 'Descargar excel' }]});

        }
    });

}

