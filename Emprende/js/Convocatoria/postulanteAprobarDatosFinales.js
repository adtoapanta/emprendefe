﻿$(document).ready(function () { postulanteADF.inicio(); });

var postulanteADF = {
    idPostulante: 0,
    documentos: [],
    archivoSubir: {},
    btnSubirArchivo: new BtnLoad('btnSubirArchivo'),
    btnCerrarEtapa: new BtnLoad('btnCerrarEtapa'),
    inicio: function () {
        var self = this;
        self.idPostulante = $("#txtIdPostulante").val();
        self.idConvocatoria = $("#txtIdConvocatoria").val();
        self.getDocumentosPostulante();
        $("#btnCerrarEtapa").hide();

        $("#btnCerrarEtapa").click(function () {
            self.cerrarEtapa();
        });

        $(document).on('click', '.btnVerificar', function (e) {
            e.preventDefault();
            var idD = $(this).data("id");
            self.verificarDocumento(idD);
        });

        $(document).on('click', '.btnSubirDocumento', function (e) {
            e.preventDefault();
            var idD = $(this).data("id");
            self.modalSubirDocumento(idD);
        });

        $('#fleArchivo').on('change', function (e) {
            var files = $(this).prop("files");
            var names = $.map(files, function (val) { return val.name; });

            $.each(names, function (index, archivo) {
                $("#lblListaArchivo").html(archivo);
            });

            self.archivoSubir = e.target.files[0];
        });
        $("#btnSubirArchivo").click(function () {
            var idDC = $("#mdlSubirDocumento").data("iddoccargado");
            self.subirArchivo(idDC);
        });
    },

    subirArchivo: function (idDC) {
        var self = this;

        if (self.archivoSubir == null) {
            toastr.error('No ha subido archivo solicitado', '', { positionClass: "toast-bottom-right" });
            return false;
        }
        self.btnSubirArchivo.Start();
        var data = new FormData();
        var desc = $("#txtObservacionArchivo").val();

        data.append("idAC", idDC)
        data.append("descripcionArchivo", desc);
        data.append("idP", self.idPostulante);
        data.append("descripcion", "Subida de archivo por parte del administrador");
        data.append("archivo", self.archivoSubir);

        $.ajax({
            url: SITEROOT + "/Postulacion/SubirArchivoDocumentos",
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (r, textStatus, jqXHR) {
                if (typeof r.error === 'undefined') {
                    // Success so call function to process the form
                    if (r.exitoso) {
                        toastr.info('Guardado', '', { positionClass: "toast-bottom-right" });
                        $('#mdlSubirDocumento').modal('hide');
                    } else {
                        toastr.error(r.data, 'Operación incompleta', { positionClass: "toast-bottom-right" });
                    }
                }
                else {
                    // Handle errors here
                    toastr.error(r.data, 'Error', { positionClass: "toast-bottom-right" });
                }
                $('#mdlSubirDocumento').modal('hide');
                self.btnSubirArchivo.Stop();
                self.cleanModal();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Handle errors here
                toastr.error(textStatus, 'Error', { positionClass: "toast-bottom-right" });
                $('#mdlSubirDocumento').modal('hide');
                self.btnSubirArchivo.Stop();
                self.cleanModal();
            }
        });
    },

    cleanModal: function () {
        var self = this;
        self.archivoSubir = null;
        $("#fleArchivo").val("");
        $("#txtObservacionArchivo").val("");
    },

    verificarDocumento: function (idDoc) {
        var self = this;
        if (confirm("¿Seguro de confirmar?")) {

            htclibjs.Ajax({
                url: "/Convocatoria/VerificarDocumento",
                data: JSON.stringify({ idP: self.idPostulante, idD:idDoc }),
                success: function (r) {
                    $("#iVrf" + idDoc).removeClass("fa-times").addClass("fa-check").css("color", "green");
                    
                    var d = self.documentos.find(x => x.id === idDoc);
                    d.estaVerificado = true;
                    self.showDocumentosPostulantes();
                },
                error: function (e) {
                    //self.showDocumentosPostulantes();
                },
            });

        }

    },


    modalSubirDocumento: function (idDocCargado) {
        $('#mdlSubirDocumento').data("iddoccargado", idDocCargado);
        $('#mdlSubirDocumento').modal('show');
    },

    cerrarEtapa: function () {
        var self = this;
        if (confirm("Seguro de cerrar la etapa?")) {
            self.btnCerrarEtapa.Start();
            htclibjs.Ajax({
                url: "/Convocatoria/CerrarEtapaPostulante",
                data: JSON.stringify({ idP: self.idPostulante }),
                success: function (r) {
                    self.btnCerrarEtapa.Stop();
                    window.location.href = SITEROOT + "/Convocatoria/Postulantes/" + self.idConvocatoria;
                },
                successError: function (r) {
                    self.btnCerrarEtapa.Stop();
                },
                error: function (e) {
                    self.btnCerrarEtapa.Stop();
                },
            });

        }
    },

    getDocumentosPostulante: function () {
        var self = this;
        htclibjs.Ajax({
            url: "/Convocatoria/GetDocumentosCargados",
            data: JSON.stringify({ idP: self.idPostulante }),
            success: function (r) {
                var req = r.data;
                self.documentos = req;
                self.showDocumentosPostulantes();
            }
        });
    },

    showDocumentosPostulantes: function () {
        var self = this;
        var count = self.documentos.filter(x => x.estaVerificado === null || x.estaVerificado === false);
       
        if (count.length === 0) $("#btnCerrarEtapa").show();

        var plantilla = '<div id="dvDocCargados__id__" class="btn-group">' +
            '<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i></button>' +
            '<div class="dropdown-menu"><a class="dropdown-item btnVerificar" data-id="__id__" href="#">Verificar</a><a class="dropdown-item btnSubirDocumento" data-id="__id__" href="#">Subir documento</a></div>' +
            '</div>';//hacer

        if ($.fn.DataTable.isDataTable('#tblDocumentosAprobar')) {
            $('#tblDocumentosAprobar').DataTable().destroy();
        }
        $('#tblDocumentosAprobar tbody').empty();

        $("#tblDocumentosAprobar").DataTable({
            data: self.documentos,
            aaSorting: [],
            bDestroy: true,
            bFilter: true,
            bPaginate: true,
            bLengthChange: true,
            language: SPANISHDATATABLE,
            aoColumns: [
                {
                    width: '40%', mData: 'emprendimiento', title: 'Requisito',
                    mRender: function (data, type, full) {
                        var doc;
                        if (full.ARC_Archivos !== null) {
                            doc = '<a href="' + SITEROOT + '/Convocatoria/Descarga/' + full.ARC_Archivos.idPublico + '" target="_blank">' + full.ARC_Documentos.nombre + '</a>';
                        } else {
                            doc = full.ARC_Documentos.nombre;
                        }

                        return doc;
                    }
                },
                {
                    width: '40%', mData: 'observaciones', title: 'Detalle', className: 'text-left',
                    mRender: function (data, type, full) {
                        return data;
                    }
                },
                {
                    width: '20%', mData: 'estaEntregado', title: 'Cargado', className: 'text-center',
                    mRender: function (data, type, full) {
                        return (data ? '<i class="fa fa-check" style="color:green;"></i>' : '<i class="fa fa-times" style="color:red;"></i>');
                    }
                },
                {
                    width: '20%', mData: 'estaVerificado', title: 'Verificado', className: 'text-center',
                    mRender: function (data, type, full) {
                        return (data ? '<i id="iVrf' + full.id + '" class="fa fa-check" style="color:green;"></i>' : '<i id="iVrf' + full.id + '" class="fa fa-times" style="color:red;"></i>');
                    }
                },
                {
                    width: '20%', mData: 'id', title: '', className: 'text-center',
                    mRender: function (data, type, full) {
                        return plantilla.replace(/\__id__/g, data);
                    }
                },
            ],
        });
    },



};