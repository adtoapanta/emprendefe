﻿$(document).ready(function () { postulanteDF.inicio(); });

var postulanteDF = {
    idPostulante: 0,
    documentos: [],
    inicio: function () {
        var self = this;
        self.idPostulante = $("#txtIdPostulante").val();
        self.idConvocatoria = $("#txtIdConvocatoria").val();
        self.getDocumentosPostulante();
    },

    getDocumentosPostulante: function () {
        var self = this;
        htclibjs.Ajax({
            url: "/Convocatoria/GetDocumentosCargados",
            data: JSON.stringify({ idP: self.idPostulante }),
            success: function (r) {
                var req = r.data;
                self.documentos = req;
                self.showDocumentosPostulantes();
            }
        });
    },

    showDocumentosPostulantes: function () {
        var self = this;

        if ($.fn.DataTable.isDataTable('#tblDocumentos')) {
            $('#tblDocumentos').DataTable().destroy();
        }
        $('#tblDocumentos tbody').empty();

        $("#tblDocumentos").DataTable({
            data: self.documentos,
            aaSorting: [],
            bDestroy: true,
            bFilter: true,
            bPaginate: true,
            bLengthChange: true,
            language: SPANISHDATATABLE,
            aoColumns: [
                {
                    width: '40%', mData: 'emprendimiento', title: 'Requisito',
                    mRender: function (data, type, full) {
                        var doc;
                        if (full.ARC_Archivos !== null) {
                            doc = '<a href="' + SITEROOT + '/Convocatoria/Descarga/' + full.ARC_Archivos.idPublico+'" target="_blank">' + full.ARC_Documentos.nombre+'</a>';
                        } else {
                            doc = full.ARC_Documentos.nombre;
                        }

                        return doc;
                    }
                },
                {
                    width: '40%', mData: 'observaciones', title: 'Detalle', className: 'text-left',
                    mRender: function (data, type, full) {
                        return data;
                    }
                },
                {
                    width: '20%', mData: 'estaEntregado', title: 'Cargado', className: 'text-center',
                    mRender: function (data, type, full) {
                        return (data ? '<i id="iVrf' + full.id + '" class="fa fa-check" style="color:green;"></i>' : '<i id="iVrf' + full.id + '" class="fa fa-times" style="color:red;"></i>');
                    }
                },
            ],
        });
    },


    
};