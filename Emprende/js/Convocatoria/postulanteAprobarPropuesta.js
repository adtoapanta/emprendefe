﻿$(document).ready(function () { aprobarPropuesta.inicio(); });

var aprobarPropuesta = {
    idPostulacion: 0,
    idConvocatoria: 0,
    btnAprobarPropuesta: new BtnLoad('btnAprobarPropuesta'),
    inicio: function () {
        var self = this;

        self.idPostulacion = $("#txtIdPostulante").val();
        self.idConvocatoria = $("#txtIdConvocatoria").val();
        $("#btnAprobarPropuesta").click(function () {
            self.aprobar();
        });
    },
    aprobar: function () {
        var self = this;
        self.btnAprobarPropuesta.Start();
        htclibjs.Ajax({
            url: "/Convocatoria/AprobarPropuesta",
            data: JSON.stringify({ idP: self.idPostulacion}),
            success: function (r) {
                self.btnAprobarPropuesta.Stop();
                window.location.href = SITEROOT + "/Convocatoria/Postulantes/" + self.idConvocatoria;
            },
            successError: function () {
                self.btnAprobarPropuesta.Stop();
            }
        });
    }
};
