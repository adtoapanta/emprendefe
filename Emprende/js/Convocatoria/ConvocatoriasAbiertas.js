﻿var _convocatorias = [];

$(document).ready(function () { initConvocatoriasAbiertas(); });

function initConvocatoriasAbiertas() {
    console.log('init convocatorias abiertas');
    getConvocatoriasAbiertas();
}

function getConvocatoriasAbiertas() {
    htclibjs.Ajax({
        url: "/Convocatoria/cosultarConvocatoriasAbiertas",
        data: JSON.stringify({}),
        success: function (r) {
            _convocatorias = r.data;
            mostrarConvocatorias();

            $('.seleccionarConvocatoria').on('click', function (e) {
                selecionarConvocatoria($(this));
            });
        }
    });
}

function mostrarConvocatorias() {
    let html = '';

    $.each(_convocatorias, function (i, item) {
        let id = item.id;
        let nombreConv = item.nombre;
        let fechaApertura = getOutDate(new Date(item.fechaApertura));
        let fechaCierre = getOutDate(new Date(item.fechaCierre));
        let descripcion = item.descripcion === null ? "" : item.descripcion;

        html += '<div class="row">';
        html += '	<div class="col-10 offset-1">';
        html += '		<div class="card">';
        html += '			<div class="card-header">';
        html += '				<div class="row">';
        html += '					<div class="col-10">';
        html += `						<h5 class="card-title">${nombreConv}</h5>`;
        html += '					</div>';
        html += '					<div class="col-2 text-right">';
        html += `                       <button id="${id}" class="btn btn-primary btn-sm seleccionarConvocatoria" >Ver detalles</button >`;
        html += '					</div>';
        html += '				</div>';
        html += '			</div>';
        html += '			<div class="card-body">';
        html += '				<div class="row">';
        html += '					<div class="col-4">';
        html += '						<div class="form-group row">';
        html += '							<label for="txtApertura" class="col-sm-4 col-form-label"><b>Apertura: </b></label>';
        html += '							<div class="col-sm-7">';
        html += `								<input type="text" class="form-control" id="txtApertura" disabled value="${fechaApertura}">`;
        html += '							</div>';
        html += '						</div>';
        html += '					</div>';
        html += '					<div class="col-4">';
        html += '						<div class="form-group row">';
        html += '							<label for="txtApertura" class="col-sm-3 col-form-label"><b>Cierre: </b></label>';
        html += '							<div class="col-sm-7">';
        html += `								<input type="text" class="form-control" id="txtApertura" disabled value="${fechaCierre}">`;
        html += '							</div>';
        html += '						</div>';
        html += '					</div>';
        html += '				</div>';
        html += '				<br />';
        html += '				<div class="row">';
        html += '					<div class="col-12 text-justify">';
        html += '						<p class="card-text">';
        html += `							${descripcion}`;
        html += '						</p>';
        html += '					</div>';
        html += '				</div>';
        html += '			</div>';
        html += '		</div>';
        html += '	</div>';
        html += '</div>';
        html += '<br />';

    });

    $('#divConvocatorias').html('');
    $('#divConvocatorias').append(html);
}

function selecionarConvocatoria(boton) {
    var convocatoriaId = boton.attr('id');
    currentSession.selectedConvocatoria = convocatoriaId;

    var url = SITEROOT + '/Convocatoria/VerConvocatoria/' + convocatoriaId;

    var win = window.open(url, '_blank');
    if (win) {
        win.focus();
    } else {
        alert('Para continuar permita acceso a popups para este sitio');
    }
}