﻿$(document).ready(function () {
    agendarComite.iniciar();
});

var agendarComite = {
    idConvocatoria: 0,
    btnAgendarComite: new BtnLoad('btnAgendarComite'),
    btnCerrarEtapa: new BtnLoad('btnCerrarEtapa'),
    btnAplicarMiembros: new BtnLoad('btnAplicarMiembros'),
    btnLimpiarMiembros: new BtnLoad('btnLimpiarMiembros'),
    iniciar: function () {
        var self = this;
        this.idConvocatoria = $("#txtIdConvocatoria").val();
        this.getPostulantes();

        $('#txtHoraComite').datetimepicker({
            format: 'LT', icons: {
                time: "fa fa-clock",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
        });

        $("#btnAplicarMiembros").click(function () {
            self.aplicarMiembros();
        });

        $("#btnLimpiarMiembros").click(function () {
            self.limpiarMiembros();
        });
        

        $(document).on('click', '.btnAgendarComite', function (e) {
            e.preventDefault();
            var self = this;
            $("#btnAgendarComite").data("idpostulante", $(this).data('id'));//guardo en en boton q agenda
            $('#agendarComiteModal').modal('show');
        });

        $(document).on('click', '.btnDescartar', function (e) {
            e.preventDefault();
            var idPsotulante = $(this).data('id');
            if (confirm('¿Seguro de descartar postulante?')) {
                self.descartarpostulante(idPsotulante);
            }
        });

        $("#btnAgendarComite").click(function () {
            var idpostulante = $(this).data("idpostulante");
            self.agendarComite(parseInt(idpostulante));
        });

        $("#btnCerrarEtapa").click(function () {
            self.cerrarEtapa();
        });
    },
    getPostulantesSeleccionados: function () {
        var ids = [];

        $(".chkSeleccionado:checkbox").each(function () {
            var $this = $(this);
            if ($this.is(":checked")) {
                ids.push($this.val());
            }
        });

        return ids;
    },

    verificarSeleccion: function () {
        //verificamos si ya estan seleccionados
        var mcName = $("#slcMiembrosComite option:selected").text();
        var proceder = true;
        $(".chkSeleccionado:checkbox").each(function () {
            var $this = $(this);
            var c = "";
            if ($this.is(":checked")) {
                c = $this.data('comite');
                if (c !== null && c.indexOf(mcName) >= 0) {
                    $this.parent().parent().css('background-color', '#ca5e59');
                    proceder = false;
                }
            }
        });

        return proceder;
    },

    aplicarMiembros: function () {
        var self = this;
        var ids = [];

        var mc = $("#slcMiembrosComite").val();

        if (!self.verificarSeleccion()) {
            toastr.error('Existen postulaciones ya asignadas al miembro del comite', '', { positionClass: "toast-bottom-right" });
            return false;
        }

        ids = self.getPostulantesSeleccionados();
        if (ids.length > 0) {
            self.btnAplicarMiembros.Start();
            htclibjs.Ajax({
                url: "/Convocatoria/AgregarMiembrosComitePostulacion",
                data: JSON.stringify({ idPs: ids, idMC: mc }),
                success: function (r) {
                    self.getPostulantes();
                    self.btnAplicarMiembros.Stop();
                    toastr.info('Guardado', '', { positionClass: "toast-bottom-right" });
                },
                errorSuccess: function () {
                    self.getPostulantes();
                },
                error: function (e) {
                    self.btnAplicarMiembros.Stop();
                    self.getPostulantes();
                },
            });
        } else {
            toastr.error('No se ha seleccionado postulaciones', '', { positionClass: "toast-bottom-right" });
        }
    },

    limpiarMiembros: function () {
        var self = this;
        var ids = [];

        if (confirm("Va a eliminar los miembros de comite de los postulantes seleccionados. ¿Proceder?")) {
            ids = self.getPostulantesSeleccionados();

            if (ids.length > 0) {
                self.btnLimpiarMiembros.Start();
                htclibjs.Ajax({
                    url: "/Convocatoria/LimpiarMiembrosComitePostulacion",
                    data: JSON.stringify({ idPs: ids}),
                    success: function (r) {
                        self.btnLimpiarMiembros.Stop();
                        toastr.info('Guardado', '', { positionClass: "toast-bottom-right" });
                        self.getPostulantes();
                    },
                    errorSuccess: function () {
                        self.getPostulantes();
                        self.btnLimpiarMiembros.Stop();
                    },
                    error: function (e) {
                        self.getPostulantes();
                        self.btnLimpiarMiembros.Stop();
                    },
                    
                });
            } else {
                toastr.error('No se ha seleccionado postulaciones', '', { positionClass: "toast-bottom-right" });
            }
        }
        
    },

    cerrarEtapa: function () {
        var self = this;
        if (confirm("Seguro de cerrar la etapa?")) {

            self.btnCerrarEtapa.Start();
            htclibjs.Ajax({
                url: "/Convocatoria/CerrarEtapa",
                data: JSON.stringify({ idC: self.idConvocatoria }),
                success: function (r) {
                    self.btnCerrarEtapa.Stop();
                    window.location.href = SITEROOT + "/Convocatoria/Convocatorias/";
                },
                error: function (e) {
                    self.btnCerrarEtapa.Stop();
                },
            });

        }
    },
    descartarpostulante: function (idPostulante) {

        htclibjs.Ajax({
            url: "/Convocatoria/DescartarPostulante",
            data: JSON.stringify({ idP: idPostulante }),
            success: function (r) {
                $("#icn" + idPostulante).removeClass("fa-check").addClass("fa-times").css('color', 'red');
            }
        });
    },

    agendarComite: function (idPostulante) {
        var self = this;
        var fecha = $("#txtFechaComite").val();
        var hora = $("#txtHoraComite").val();
        var lugar = $("#txtLugar").val();

        if (fecha.trim() !== '' && hora.trim() !== '' && lugar.trim() !== '') {
            self.btnAgendarComite.Start();
            htclibjs.Ajax({
                url: "/Convocatoria/AgendarComitePostulante",
                data: JSON.stringify({ idP: idPostulante, f: fecha, h: hora, l:lugar }),
                success: function (r) {
                    $('#agendarComiteModal').modal('hide');
                    $("#btnAgendarComite").data("idpostulante", "");
                    $("#icn" + idPostulante).removeClass("fa-times").addClass("fa-check").css('color', 'green');
                    //poner fecha en datatable
                    $("#lblFechaA" + idPostulante).html(getOutDateTime(new Date(r.data)));
                    self.btnAgendarComite.Stop();
                },
                error: function (r) {
                    self.btnAgendarComite.Stop();
                }
            });
        } else {
            toastr.error('Ingrese fecha, hora y lugar', 'No guardado.', { positionClass: "toast-bottom-right" });
        }


    },

    getPostulantes: function () {
        var self = this;

        htclibjs.Ajax({
            url: "/Convocatoria/GetPostulaciones",
            data: JSON.stringify({ idC: self.idConvocatoria }),
            success: function (r) {
                self.showPostulaciones(r.data);
            }
        });
    },
    showPostulaciones: function (postulaciones) {
        var self = this;
        var plantilla = '<div id="dvPostulanteAction__id__" class="btn-group">' +
            '<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i></button>' +
            '<div class="dropdown-menu"><a class="dropdown-item btnAgendarComite" data-id="__id__" href="#">Agendar comite</a><a class="dropdown-item btnDescartar" data-id="__id__" href="#">Descartar</a></div>' +
            '</div>';

        //Destruimos antes de acrgar nuevos
        if ($.fn.DataTable.isDataTable('#tblPostulaciones')) {
            $('#tblPostulaciones').DataTable().destroy();
        }
        $('#tblPostulaciones tbody').empty();

        $("#tblPostulaciones").DataTable({
            data: postulaciones,
            aaSorting: [],
            bDestroy: true,
            bFilter: true,
            bPaginate: true,
            bLengthChange: true,
            language: SPANISHDATATABLE,
            aoColumns: [
                {
                    width: '*', mData: 'emprendimiento', title: 'Asignar comité',
                    mRender: function (data, type, full) {
                        return '<input type="checkbox" id="chkE'+full.id+'" class="chkSeleccionado" value="'+full.id+'" data-comite="'+full.comite+'" />';
                    }
                },
                {
                    width: '*', mData: 'emprendimiento', title: 'Emprendimiento',
                    mRender: function (data, type, full) {
                        return '<a href="' + SITEROOT + '/Postulacion/Index/' + full.id + '">' + data + '</a>';
                    }
                },
                {
                    width: '*', mData: 'emprendedor', title: 'Emprendedor',
                    mRender: function (data, type, full) {
                        return data;
                    }
                },
                {
                    width: '*', mData: 'comite', title: 'Comité',
                    mRender: function (data, type, full) {
                        return data;
                    }
                },
                {
                    width: '*', mData: 'fechaAgendarComite', title: 'Fecha agendada',
                    mRender: function (data, type, full) {
                        var f = new Date(data);
                        return '<label id="lblFechaA' + full.id + '">' + getOutDateTime(f) + '</label>';
                    }
                },
                {
                    width: '*', mData: 'apruebaEtapa', title: 'Estado', className: "text-center",
                    mRender: function (data, type, full) {
                        var ht = '<i id="icn' + full.id + '" class="fas" style="font-size:25px;"></i>';
                        if (full.revisadoEtapa) {
                            ht = '<i id="icn' + full.id + '" class="' + (full.apruebaEtapa ? "fas fa-check" : "fas fa-times") + '" style="font-size:25px; color:' + (full.apruebaEtapa ? "green" : "red") + ';"></i>';
                        }
                        return ht;
                    }
                },
                {
                    width: '5%', mData: 'id', title: '', className: "text-center",
                    mRender: function (data, type, full) {
                        return plantilla.replace(/\__id__/g, data);
                    }
                },
            ],
        });
    }
};