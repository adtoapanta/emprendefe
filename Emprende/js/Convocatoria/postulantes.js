﻿$(document).ready(function () { finales.inicio(); });

var finales = {
    idConvocatoria: 0,
    inicio: function () {
        var self = this;
        
        self.idConvocatoria = $("#txtIdConvocatoria").val();
        self.getPostulantes();

        $(document).on('click', '.btnVerPostulante', function (e) {
            e.preventDefault();
            var idP = $(this).data("idpostulante");
            window.location.href = SITEROOT+"/Convocatoria/Postulante/"+idP;
        });
    },

    getPostulantes: function () {
        var self = this;

        htclibjs.Ajax({
            url: "/Convocatoria/GetPostulaciones",
            data: JSON.stringify({ idC: self.idConvocatoria }),
            success: function (r) {
                self.showPostulaciones(r.data);
            }
        });
    },

    showPostulaciones: function (postulaciones) {
        var self = this;
        var plantilla = '<button type="button" class="btn btn-info btnVerPostulante" data-idpostulante="__id__" ><i class="fas fa-flask"></i></button>';
        //Destruimos antes de acrgar nuevos
        if ($.fn.DataTable.isDataTable('#tblPostulaciones')) {
            $('#tblPostulaciones').DataTable().destroy();
        }
        $('#tblPostulaciones tbody').empty();

        $("#tblPostulaciones").DataTable({
            data: postulaciones,
            aaSorting: [],
            bDestroy: true,
            bFilter: true,
            bPaginate: true,
            bLengthChange: true,
            language: SPANISHDATATABLE,
            aoColumns: [
                {
                    width: '50%', mData: 'emprendimiento', title: 'Emprendimiento',
                    mRender: function (data, type, full) {
                        return '<a href="' + SITEROOT + '/Postulacion/Index/' + full.id + '">' + data + '</a>';
                    }
                },
                {
                    width: '20%', mData: 'emprendedor', title: 'Emprendedor',
                    mRender: function (data, type, full) {
                        return data;
                    }
                },
                {
                    width: '20%', mData: 'etapaPostulacion', title: 'Etapa',
                    mRender: function (data, type, full) {
                        return data;
                    }
                },
                {
                    width: '10%', mData: 'id', title: '', className: "text-center",
                    mRender: function (data, type, full) {
                        if (full.etapaPostulacion === 'Finalizado') {
                            return '<i class="fa fa-check" style="color:green;"></i>';
                        }
                        
                        return plantilla.replace(/\__id__/g, data);
                    }
                },
            ],
        });
    }

};