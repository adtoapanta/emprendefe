﻿$(document).ready(function () { contratoP.init(); });

var contratoP = {
    idPostulacion: 0,
    idConvocatoria:0,
    archivoSubir: null,
    init: function () {
        var self = this;


        self.idPostulacion = $("#txtIdPostulante").val();
        self.idConvocatoria = $("#txtIdConvocatoria").val();
        $("#btnSubirFinalizar").click(function () {
            self.subirFinalizar();
        });
        $("#btnDownloadContrato").click(function () {
            self.downloadContrato();
        });

        $('#fleArchivoContrato').on('change', function (e) {
            self.archivoSubir = e.target.files[0];
        })
        console.log(self.idPostulacion);
    },

    downloadContrato: function () {
        var self = this;

        console.log('download contrato');
        location.href = SITEROOT + "/Postulacion/DescargarContrato?idPostulacion=" + self.idPostulacion;
    },

    subirFinalizar: function () {
        var self = this;

        if (self.archivoSubir == null) {
            toastr.error("Es obligatoria subir un documento", '', { positionClass: "toast-bottom-right" });
            return false;
        }
        if (confirm('Seguro de finalizar?')) {
            
            var data = new FormData();
            data.append("archivo", self.archivoSubir);
            data.append("idpostulacion", self.idPostulacion);

            $.ajax({
                url: SITEROOT + "/Convocatoria/AceptarContrato",
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function (r, textStatus, jqXHR) {
                    if (typeof r.error === 'undefined') {
                        // Success so call function to process the form
                        if (r.exitoso) {
                            toastr.info('Guardado', '', { positionClass: "toast-bottom-right" });
                            //REDIRECCION
                            window.location.href = SITEROOT + "/Convocatoria/Postulantes/" + self.idConvocatoria;
                        } else {
                            toastr.error(r.data, 'Operación incompleta', { positionClass: "toast-bottom-right" });
                        }
                    }
                    else {
                        // Handle errors here
                        toastr.error(r.data, 'Error', { positionClass: "toast-bottom-right" });
                    }
                    self.archivoSubir = null;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // Handle errors here
                    toastr.error(textStatus, 'Error', { positionClass: "toast-bottom-right" });
                    self.archivoSubir = null;
                }
            });

        }
    }
    
};