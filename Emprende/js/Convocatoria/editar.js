﻿$(document).ready(function () {
    editarConvocatoria.iniciar();
});

var editarConvocatoria = {
    btnGuardarEdicion: new BtnLoad("btnGuardarEdicion"),
    btnAbrirInscripcion: new BtnLoad("btnAbrirInscripcion"),
    iniciar: function () {
        var self = this;
        $("#txtCriterios").Editor({
            'texteffects': true,
            'aligneffects': true,
            'textformats': true,
            'fonteffects': true,
            'actions': true,
            'insertoptions': true,
            'extraeffects': true,
            'advancedoptions': true,
            'screeneffects': true,
            'bold': true,
            'italics': true,
            'underline': true,
            'ol': true,
            'ul': true,
            'undo': true,
            'redo': true,
            'l_align': true,
            'r_align': true,
            'c_align': true,
            'justify': true,
            'insert_link': true,
            'unlink': true,
            'insert_img': true,
            'hr_line': true,
            'block_quote': true,
            'source': true,
            'strikeout': true,
            'indent': true,
            'outdent': true,
            //'fonts': fonts,
            //'styles': styles,
            'print': true,
            'rm_format': true,
            'status_bar': true,
            //'font_size': fontsizes,
            //'color': colors,
            //'splchars': specialchars,
            'insert_table': true,
            'select_all': true,
            'togglescreen': true
        });
        var htmlEncode = $("#txtEditorCriterios").text();
        $("#txtCriterios").Editor('setText', (jQuery('<div />').html(htmlEncode).text()));

        $("#btnAbrirInscripcion").click(function () {
            self.btnAbrirInscripcion.Start();
            self.abrirInscripcion();
        });

        $("#btnGuardarEdicion").click(function (e) {
            e.preventDefault();
            
            var text = jQuery('<div />').text($('#txtCriterios').Editor("getText")).html()
            $('#txtEditorCriterios').text(text);
            self.btnGuardarEdicion.Start();

            if ($('#chkRequisitos2').is(':checked') && $.trim($("#txtCapacitacion").val()) === '') {
                toastr.error("No se han ingresado textos de capacitación.", '', { positionClass: "toast-bottom-right" });
                self.btnGuardarEdicion.Stop();
                return false;
            }

            $("#frmEditar").submit();
        });

        
        $(".chkRequisitos").change(function () {
            if ($(this).data("nombre") == 'Capacitación') {
                if (this.checked) {
                    $("#txtCapacitacion").show();
                } else {
                    $("#txtCapacitacion").hide();
                    $("#txtCapacitacion").val("");
                }
            }
            
        });
    },
    abrirInscripcion: function () {
        var self = this;
        var idConvocatoria = $("#txtIdConvocatoria").val();
        htclibjs.Ajax({
            url: "/Convocatoria/AbrirInscripciones",
            data: JSON.stringify({idC:idConvocatoria}),
            success: function (r) {
                $("#btnAbrirInscripcion").hide();
                window.location.href = SITEROOT + "/Convocatoria/Convocatorias/";
                //self.btnAbrirInscripcion.Stop();
            },
            successError: function () {
                self.btnAbrirInscripcion.Stop();
            },
            error: function () {
                self.btnAbrirInscripcion.Stop();
            },
        });
    }

};