﻿$(document).ready(function () {
    prefactibilidad.iniciar();
});


var prefactibilidad = {
    idConvocatoria: 0,
    btnGuardarPrefactibilidad: new BtnLoad("btnGuardarPrefactibilidad"),
    btnCerrarEtapa: new BtnLoad('btnCerrarEtapa'),
    archivosSubir: {},
    iniciar: function () {
        var self = this;
        this.idConvocatoria = $("#txtIdConvocatoria").val();
        self.getPostulantes();


        $(document).on('click', '.btnAbrirModal', function (e) {
            e.preventDefault();
            //Limpio las opciones
            $("#dvListaArchivos").html("");
            $("#fileArchivosSoporte").val("");
            $("#txtObservacionesPrefactibilidad").val("");
            $("#rdoEstadoAprueba").prop("checked", true);
            $("#dvCalificaciones").html("");
            //-
            var idP = $(this).data('idpostulante');
            $("#btnGuardarPrefactibilidad").data("idpostulante", idP);//guardo en en boton q agenda
            //$('#prefactibilidadModal').modal('show');
            self.cargarCalificacionPrefactibilidad(idP);
        });

        $('#fileArchivosSoporte').on('change', function (e) {
            var ht='';
            var files = $(this).prop("files");
            var names = $.map(files, function (val) { return val.name; });

            self.archivosSubir.files = e.target.files;

            ht = '<ul style="list-style-type: none;">';
            $.each(names, function (index, archivo) {
                ht += '<li><i class="far fa-file"></i> '+archivo+'</li>';
            });
            ht += '</ul>';
            $("#dvListaArchivos").html(ht);
        })

        $("#btnGuardarPrefactibilidad").click(function () {
            var idPostulante = $(this).data("idpostulante");
            self.guardarPrefactibilidad(idPostulante);
        });

        $("#btnCerrarEtapa").click(function () {
            self.cerrarEtapa();
        });
    },

    cargarCalificacionPrefactibilidad: function (idPostulacion) {
        var self = this;
        htclibjs.Ajax({
            url: "/Comite/GetCalificacionPrefactibilidad",
            data: JSON.stringify({ idPos: idPostulacion }),
            success: function (r) {
                var c = r.data;
                var html = '';
                if (c.aprueba) {
                    $('#rdoEstadoAprueba').prop('checked', true);
                } else {
                    $('#rdoEstadoNoAprueba').prop('checked', true);
                }

                html = 'Calificaciones:';
                html += '<ul class="list-group">';
                $.each(c.misCalificaciones, function (index, calificacion) {
                    html += '<li class="list-group-item d-flex justify-content-between align-items-center">';
                    html += '<span>' + calificacion.nombre + ' <a style="margin-left:30px;" target="_blank" href="' + SITEROOT + '/Postulacion/DescargarComitePostulacion?idComitePostulacion=' + calificacion.idComitePostulacion + '"><i class="fa fa-download"></i></a></span> ';
                    html += '<span class="badge badge-primary badge-pill">' + calificacion.calificacion + ' </span>';
                    html += '</li>';
                });
                html += '</ul>';
                $("#dvCalificaciones").html(html);

                $("#spnTotal").html(c.calificacion);

                $('#prefactibilidadModal').modal('show');
            },
            error: function (e) {

            },
        });
    },

    guardarPrefactibilidad: function (idP) {
        var self = this;
        var obs = $('#txtObservacionesPrefactibilidad').val();
        var aprueba = $("input[name='rdoEstado']:checked").val()=="1"?true:false;
        self.btnGuardarPrefactibilidad.Start();

        var data = new FormData();
        var c = 1;
        $.each(self.archivosSubir.files, function (key, value) {
            data.append("archivo"+c, value);
            c += 1;
        });
        data.append("idC", self.idConvocatoria);
        data.append("idP",idP);
        data.append("obs", obs);
        data.append("aprueba", aprueba);

        $.ajax({
            url: SITEROOT + "/Convocatoria/GuardarPrefactibilidad",
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (r, textStatus, jqXHR) {
                if (typeof r.error === 'undefined') {
                    // Success so call function to process the form
                    if (r.exitoso) {
                        if (aprueba) {
                            $("#icn" + idP).removeClass("fa-times").addClass("fa-check").css('color', 'green');
                        } else {
                            $("#icn" + idP).removeClass("fa-check").addClass("fa-times").css('color', 'red');
                        }
                        toastr.info('Guardado','', { positionClass: "toast-bottom-right" });
                    } else {
                        toastr.error(r.data, 'Operación incompleta', { positionClass: "toast-bottom-right" });
                    }
                }
                else {
                    // Handle errors here
                    toastr.error(r.data, 'Error', { positionClass: "toast-bottom-right" });
                }
                $('#prefactibilidadModal').modal('hide');
                self.btnGuardarPrefactibilidad.Stop();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Handle errors here
                toastr.error(textStatus, 'Error', { positionClass: "toast-bottom-right" });
                $('#prefactibilidadModal').modal('hide');
                self.btnGuardarPrefactibilidad.Stop();
            }
        });
    },
    cerrarEtapa: function () {
        var self = this;
        if (confirm("Seguro de cerrar la etapa?")) {
            self.btnCerrarEtapa.Start();
            htclibjs.Ajax({
                url: "/Convocatoria/CerrarEtapa",
                data: JSON.stringify({ idC: self.idConvocatoria }),
                success: function (r) {
                    self.btnCerrarEtapa.Stop();
                    window.location.href = SITEROOT + "/Convocatoria/Convocatorias/";
                },
                error: function (e) {
                    self.btnCerrarEtapa.Stop();
                },
            });

        }
    },
    getPostulantes: function () {
        var self = this;

        htclibjs.Ajax({
            url: "/Convocatoria/GetPostulaciones",
            data: JSON.stringify({ idC: self.idConvocatoria }),
            success: function (r) {
                self.showPostulaciones(r.data);
            }
        });
    },
    showPostulaciones: function (postulaciones) {
        var self = this;
        var plantilla = '<button type="button" class="btn btn-info btnAbrirModal" data-idpostulante="__id__" ><i class="fa fa-cog"></i></button>';

        //Destruimos antes de acrgar nuevos
        if ($.fn.DataTable.isDataTable('#tblPostulaciones')) {
            $('#tblPostulaciones').DataTable().destroy();
        }
        $('#tblPostulaciones tbody').empty();

        $("#tblPostulaciones").DataTable({
            data: postulaciones,
            aaSorting: [],
            bDestroy: true,
            bFilter: true,
            bPaginate: true,
            bLengthChange: true,
            language: SPANISHDATATABLE,
            aoColumns: [
                {
                    width: '*', mData: 'emprendimiento', title: 'Emprendimiento',
                    mRender: function (data, type, full) {
                        return '<a href="' + SITEROOT + '/Postulacion/Index/' + full.id + '">' + data + '</a>';
                    }
                },
                {
                    width: '*', mData: 'emprendedor', title: 'Emprendedor',
                    mRender: function (data, type, full) {
                        return data;
                    }
                },
                {
                    width: '*', mData: 'apruebaEtapa', title: 'Estado', className: "text-center",
                    mRender: function (data, type, full) {
                        var ht = '<i id="icn' + full.id + '" class="fas" style="font-size:25px;"></i>';
                        if (full.revisadoEtapa) {
                            ht = '<i id="icn' + full.id + '" class="' + (full.apruebaEtapa ? "fas fa-check" : "fas fa-times") + '" style="font-size:25px; color:' + (full.apruebaEtapa ? "green" : "red") + ';"></i>';
                        }
                        return ht;
                    }
                },
                {
                    width: '5%', mData: 'id', title: '', className: "text-center",
                    mRender: function (data, type, full) {
                        return plantilla.replace(/\__id__/g, data);
                    }
                },
            ],
        });
    }
}