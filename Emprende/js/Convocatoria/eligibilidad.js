﻿$(document).ready(function () { eligibilidad.inicio(); });

var eligibilidad = {
    idConvocatoria: 0,
    preguntas: {},
    btnFiltrar: new BtnLoad("btnFiltrar"),
    btnEjecutarPrefactibilidad: new BtnLoad("btnEjecutarPrefactibilidad"),
    inicio: function () {
        var self = this;

        self.idConvocatoria = $("#txtIdConvocatoria").val();
        var p = $("#txtPreguntas").val();
        self.preguntas = $.parseJSON(p);
        self.cargarPreguntas();
        self.cargarPreguntaRespuesta();

        
        $("#slcPregunta").change(function () {
            self.cargarRespuestas();
        });
        $("#btnAddPregunta").click(function () {
            var idP = $("#slcPregunta").val();
            var checkeds = $("input[name='chkRespuestas[]']:checked");
            if (checkeds.length > 0) {
                $.each(checkeds, function () {
                    self.agregarPreguntaRespuesta(idP, $(this).val());
                });
                self.cargarPreguntas();
                $("#dvRespuestas").html("");
                $('#modalAddPreguntas').modal('hide');
                self.nuevoFiltrado();
            } else {
                alert("Elija una respuesta");
            }
        });

        $(".dvAplicable").hide();
        $("#btnAplicarCerrar").click(function () {
            var fechaCierreDatos = $("#txtFechaCierreDatos").val();
            if (fechaCierreDatos.trim() !== '') {
                $('#mdlTextoCorreos').modal('show');    
            } else {
                toastr.error('Es obligatorio una fecha de cierre de datos', 'No se procede.', { positionClass: "toast-bottom-right" });
            }
        });

        
        $("#btnEjecutarPrefactibilidad").click(function () {
            self.aplicar();
        });

        $("#btnFiltrar").click(function () {
            self.filtrarSeleccion();
        });
        

        $(document).on('click', '.btnEliminarDeFiltro', function (e) {
            e.preventDefault();
            var idP = $(this).data("idpregunta");
            var idR = $(this).data("idrespuesta");
            self.deseleccionarPreguntaRespuesta(idP,idR);
            var ob = $(this).closest('.dvPreguntaRespuesta');
            ob.remove();
        });

    },
    aplicar: function () {
        var self = this;
        var fechaCierreDatos = $("#txtFechaCierreDatos").val();
        var textoAprueba = $("#txtClasificados").val();
        var textoNoAprueba = $("#txtNoClasificados").val();
        self.btnEjecutarPrefactibilidad.Start();

        if (fechaCierreDatos.trim() !== '') {
            htclibjs.Ajax({
                url: "/Convocatoria/AplicarPostulantes",
                data: JSON.stringify({ idC: self.idConvocatoria, f: fechaCierreDatos, ap: textoAprueba, nap:textoNoAprueba }),
                success: function (r) {
                    self.btnEjecutarPrefactibilidad.Stop();
                    window.location.href = SITEROOT + "/Convocatoria/Convocatorias/";
                }
            });
        } else {
            toastr.error('Es obligatorio una fecha de cierre de datos', 'Error', { positionClass: "toast-bottom-right" });
            self.btnEjecutarPrefactibilidad.Stop();
        }
        
        
    },
    nuevoFiltrado: function () {
        this.clearPostulaciones();
        $(".dvAplicable").hide();
    },
    deseleccionarPreguntaRespuesta: function (idP, idR) {
        var self = this;
        var p = self.preguntas.find(x => x.id === parseInt(idP));
        p._seleccionado = false;
        var r = p.SCO_PosiblesRespuestas.find(x => x.id === parseInt(idR));
        r._seleccionado = false;
        self.cargarPreguntas();
        self.nuevoFiltrado();
    },

    cargarPreguntas: function () {
        var self = this;
        $("#slcPregunta option").remove();
        $("#slcPregunta").append(new Option("Seleccione", 0,true,true));
        $.each(self.preguntas, function (index, p) {
            var r = $.grep(p.SCO_PosiblesRespuestas, function (e) { return e._seleccionado === false; });
            if (r.length > 0) {
                $("#slcPregunta").append(new Option(p.nombre, p.id));
            }
        });
    },

    cargarPreguntaRespuesta: function () {
        var self = this;
        $.each(self.preguntas, function (index, p) {
            if (p._seleccionado == true) {
                var r = p.SCO_PosiblesRespuestas.find(x => x._seleccionado === true);
                self.agregarPreguntaRespuesta(p.id, r.id);
            }
        });
    },

    cargarRespuestas() {
        var self = this;
        var idP = $("#slcPregunta").val();
        //var plantilla = '<div class="custom-control custom-radio"><input type="radio" value="__id__" id="customRadio__id__" name="customRadioRespuestas" class="custom-control-input"><label class="custom-control-label" for="customRadio__id__">__txtRespuesta__</label></div>';
        var plantilla = '<div class="form-check"><input class="form-check-input" type="checkbox" value="__id__" id="chkRespuestas__id__" name="chkRespuestas[]"><label class="form-check-label" for="chkRespuestas__id__">__txtRespuesta__</label></div>';

        var html = "";
        $("#dvRespuestas").html("");
        
        if (idP != 0) {
            var pr = self.preguntas.find(x => x.id === parseInt(idP));
            $.each(pr.SCO_PosiblesRespuestas, function (index, p) {
                if (!p._seleccionado) {
                    html += plantilla.replace(/\__id__/g, p.id).replace("__txtRespuesta__", p.nombre);
                }
            });
            if (pr.SCO_PosiblesRespuestas.length > 0) $("#dvRespuestas").html(html);
        }
    },

    agregarPreguntaRespuesta: function (idP, idR) {
        var self = this;
        
        var plantilla = '<div class="row border-bottom dvPreguntaRespuesta top-space5"><div class="col-4"> __pregunta__</div>'+
                '<div class="col"><div class="form-check"><i class="far fa-check-square dataRespuestaPregunta" data-idpregunta="__idpregunta__" data-idrespuesta="__idrespuesta__"></i>'+
                '<label class="form-check-label"> __respuesta__</label></div></div><div class="col-1 text-right">'+
                '<button type="button" data-idpregunta="__idpregunta__" data-idrespuesta="__idrespuesta__" class="btn btn-warning btn-sm btnEliminarDeFiltro"><i class="fa fa-minus-circle"></i></button></div>'+
                '</div >';

        var pregunta = self.preguntas.find(x => x.id === parseInt(idP));
        var respuesta = pregunta.SCO_PosiblesRespuestas.find(x => x.id === parseInt(idR));
        pregunta._seleccionado = true;
        respuesta._seleccionado = true;
        var html = plantilla.replace("__pregunta__", pregunta.nombre).replace("__respuesta__", respuesta.nombre).replace(/\__idpregunta__/g, pregunta.id).replace(/\__idrespuesta__/g, respuesta.id);
        $("#filtroPreguntasRespuestas").append(html);

    },
    

    filtrarSeleccion: function () {
        var self = this;
        var criterios = [];
        
        $(".dataRespuestaPregunta").each(function () {
            var idP = $(this).data("idpregunta");
            var idR = $(this).data("idrespuesta");
            var criterio = { idCuestionario: 1, idConvocatoria: self.idConvocatoria, idPregunta: idP, idPosibleRespuesta: idR };
            criterios.push(criterio);
        });

        self.btnFiltrar.Start();
        htclibjs.Ajax({
            url: "/Convocatoria/SetCriteriosGetPostulantes",
            data: JSON.stringify({ idC: self.idConvocatoria, ctrs: criterios }),
            success: function (r) {
                var postulantes = r.data;
                self.showPostulaciones(postulantes);
                self.btnFiltrar.Stop();
                $(".dvAplicable").show();
            }
        });
    },

    clearPostulaciones: function () {
        if ($.fn.DataTable.isDataTable('#tblPostulaciones')) {
            $('#tblPostulaciones').DataTable().destroy();
        }
        $('#tblPostulaciones tbody').empty();
    },
    showPostulaciones: function (postulaciones) {
        var self = this;

        //Destruimos antes de acrgar nuevos
        self.clearPostulaciones();

        $("#tblPostulaciones").DataTable({
            data: postulaciones,
            //aaSorting: [],
            bDestroy: true,
            bFilter: true,
            bPaginate: true,
            bLengthChange: true,
            language: SPANISHDATATABLE,
            dom: 'Bfrtip', buttons: [{
                extend: 'excel', className: 'btn btnPrimary', text: 'Descargar excel', exportOptions: {
                    columns: [0, 1,2,3,4]
                }
            }],
            aoColumns: [
                {
                    width: '50%', mData: 'emprendimiento', title: 'Emprendimiento',
                    mRender: function (data, type, full) {
                        return '<a href="' + SITEROOT + '/Postulacion/Index/' + full.id + '">' + data + '</a>';
                    }
                },
                {
                    width: '25%', mData: 'emprendedor', title: 'Emprendedor',
                    mRender: function (data, type, full) {
                        return data;
                    }
                },
                {
                    width: '25%', mData: 'fecha', title: 'Fecha', 
                    mRender: function (data, type, full) {
                        var f = new Date(data);
                        return getOutDate(f);
                    }
                },
                {
                    width: '0%', mData: 'telefono', title: 'Telefono', visible: false,
                    mRender: function (data, type, full) {
                        return data;
                    }
                },
                {
                    width: '0%', mData: 'celular', title: 'Celular', visible: false,
                    mRender: function (data, type, full) {
                        return data;
                    }
                },
            ],
        });
    },
};