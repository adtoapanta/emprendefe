﻿$(document).ready(function () { propuesta.inicio(); });

var propuesta = {
    objetivos: [],
    idPostulante: 0,
    idConvocatoria:0,
    propuestaCambiada: true,
    archivoSubir: null,
    btnArchivoYPropuesta: new BtnLoad('btnArchivoYPropuesta'),
    inicio: function () {
        var self = this;

        self.idPostulante = $("#txtIdPostulante").val();
        self.idConvocatoria = $("#txtIdConvocatoria").val();

        $("#txtVentaEstimada").keyup(function () {
            self.cambioTotal();
            self.propuestaCambiada = true;
            self.verificarBotones();
        });

        $("#btnAgregarRecurso").click(function () {
            self.agregarRecurso();
        });

        $("#btnAddObjetivoModal").click(function () {
            self.showModalObjetivo();
        });

        $("#btnAgregarObjetivoRecurso").click(function () {
            self.obtenerObjetivo();
            self.propuestaCambiada = true;
            self.verificarBotones();
        });

        $("#btnGuardarPropuesta").click(function () {
            self.guardarPropuesta();
        });

        $("#btnAceptarPropuesta").click(function () {
            $('#mdlAceptarPropuestaArchivo').modal('show');
        });

        $("#btnArchivoYPropuesta").click(function () {
            self.aceptarPropuesta();
        });


        $("#btnCalculo").click(function () {
            self.cambioTotal();
            self.propuestaCambiada = true;
            self.verificarBotones();
        });


        $(document).on('click', '.btnRemoveRecurso', function (e) {
            e.preventDefault();
            $(this).closest('.trRecurso').remove();
        });

        $(document).on('click', '.editObjetivo', function (e) {
            e.preventDefault();
            var id = $(this).data("id");
            self.editOpcion(id);
        });


        var objeetivos = $("#txtPropuestasObjetivos").val();
        if (isJSON(objeetivos) !== false) {
            self.objetivos = $.parseJSON(objeetivos);
            $.each(self.objetivos, function (index, obj) {
                obj.recursos = $.parseJSON(obj.recursos);
                obj.CON_propuestas = null;
                obj.mes = obj.mes + "";
                obj.totalObjetivo = null;
            });
        }

        if (self.objetivos.length > 0) self.propuestaCambiada = false;
        self.verificarBotones();

        $("#btnMedioAprobacion").click(function (e) {
            e.preventDefault();

            location.href = SITEROOT + "/Postulacion/DescargarMedioDeAprobacion/?idPostulacion=" + self.idPostulante;
        });

        $('#fliArchivoPropuesta').on('change', function (e) {
            var files = $(this).prop("files");
            var names = $.map(files, function (val) { return val.name; });

            $.each(names, function (index, archivo) {
                $("#lblFliArchivoPropuesta").html(archivo);
            });

            self.archivoSubir = e.target.files[0];
        })
    },

    editOpcion: function (idO) {
        var self = this;
        var seleccionado = self.objetivos.find(x => x.id === idO);

        $("#txtObjetivoRecursos").val(seleccionado.nombre);
        $("#txtMes").val(seleccionado.mes);
        $("#tblRecursosModal").html("");
        $.each(seleccionado.recursos, function (index, recurso) {
            $("#txtRecursoObjetivo").val(recurso.nombre);
            $("#txtDesembolsoObjetivo").val(recurso.valor);
            self.agregarRecurso();
        });
        $('#objetivoModal').data("id", idO);
        $('#objetivoModal').modal('show');

    },

    verificarBotones: function () {
        var self = this;
        if (self.propuestaCambiada) {
            $("#btnAceptarPropuesta").hide();
            $("#btnImpPropuesta").hide();
            $("#btnImpResumen").hide();
            
            $("#btnMedioAprobacion").hide();
            $("#btnGuardarPropuesta").show();
        } else {
            $("#btnAceptarPropuesta").show();
            $("#btnImpPropuesta").show();
            $("#btnImpResumen").show();
            $("#btnMedioAprobacion").show();
            $("#btnGuardarPropuesta").hide();
        }
    },
    aceptarPropuesta: function () {
        var self = this;
        var guardar = true;

        if (self.propuestaCambiada) {
            if (!confirm("La propuesta actual no ha sido guardada, va a activar la anterior. Proceder?")) return false;
        }
        if (self.archivoSubir == null) {
            toastr.error('No se ha subido el archivo de propuesta', '', { positionClass: "toast-bottom-right" });
            return false;
        }


        var data = new FormData();
        data.append("archivo", self.archivoSubir);
        data.append("idpostulacion", self.idPostulante);

        self.btnArchivoYPropuesta.Start();
        $.ajax({
            url: SITEROOT + "/Convocatoria/AceptarPropuesta",
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (r, textStatus, jqXHR) {
                if (typeof r.error === 'undefined') {
                    // Success so call function to process the form
                    if (r.exitoso) {
                        toastr.info('Guardado', '', { positionClass: "toast-bottom-right" });
                        //REDIRECCION
                        window.location.href = SITEROOT + "/Convocatoria/Postulantes/"+self.idConvocatoria;
                    } else {
                        toastr.error(r.data, 'Operación incompleta', { positionClass: "toast-bottom-right" });
                    }
                }
                else {
                    // Handle errors here
                    toastr.error(r.data, 'Error', { positionClass: "toast-bottom-right" });
                }
                self.archivoSubir = null;
                self.btnArchivoYPropuesta.Stop();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Handle errors here
                toastr.error(textStatus, 'Error', { positionClass: "toast-bottom-right" });
                $('#mdlAceptarPropuestaArchivo').modal('hide');
                self.archivoSubir = null;
                self.btnArchivoYPropuesta.Stop();
            }
        });


    },

    guardarPropuesta: function () {
        var self = this;
        //Convertimos a objetos que entienda el server
        var propuestasObjetivos = [];
        var sum = 0;

        if (self.objetivos.length == 0) {
            toastr.error('No se ha ingresado objetivos', '', { positionClass: "toast-bottom-right" });
            return false;
        }

        $.each(self.objetivos, function (index, obj) {
            sum = 0;
            $.each(obj.recursos, function () { sum += parseFloat(this.valor) || 0; });
            propuestasObjetivos.push({ nombre: obj.nombre, mes: obj.mes, recursos: JSON.stringify(obj.recursos), totalObjetivo: sum })
        });
        var plazo = $("#txtPlazo").val();
        var gracia = $("#txtGracias").val();
        var condiciones = $("#txtCondiciones").val();
        var notas = $("#txtNotas").val();
        var observaciones = $("#txtObservaciones").val();
        var vEstimada = $("#txtVentaEstimada").val();
        var totalPropuesta = $("#txtTotalPropuesta").val();
        var tasa = $("#txtTasa").val();
        var coutaMensual = $("#txtCuotaMensual").val();
        var puntoEquilibrio = $("#txtPuntoEquilibrio").val();

        if ($.trim(plazo) === '' || $.trim(gracia) === '' || $.trim(tasa) === '' || $.trim(vEstimada) === '') {
            toastr.error('No se ha ingresado toda la información solicitada', '', { positionClass: "toast-bottom-right" });
            return false;
        }

        htclibjs.Ajax({
            url: "/Convocatoria/GuardarPropuesta",
            data: JSON.stringify({ idP: self.idPostulante, objs: propuestasObjetivos, t: totalPropuesta, ts: tasa, p: plazo, g: gracia, v: vEstimada, ctm: coutaMensual, pe: puntoEquilibrio, c: condiciones, n: notas, obs: observaciones }),
            success: function (r) {
                //me quedo en la pagina
                toastr.info('Propuesta guardada', '', { positionClass: "toast-bottom-right" });
                self.propuestaCambiada = false;
                self.verificarBotones();
            }
        });
    },

    obtenerObjetivo: function () {
        var objetivo;

        var obj = $("#txtObjetivoRecursos").val();
        var m = $("#txtMes").val();

        if ($.trim(obj) === "") return false;

        var filas = $("#tblRecursosModal tr");
        if ($.trim(obj) === "" || $.trim(m) === "") {
            toastr.error('No se ha ingresado información del objetivo', 'Información incompleta', { positionClass: "toast-bottom-right" });
            return false;
        }
        if (filas.length === 0) {
            toastr.error('No se han ingresado recursos', 'Información incompleta', { positionClass: "toast-bottom-right" });
            return false;
        }

        var rcrss = [];
        $.each(filas, function (index, fila) {
            var r = $(fila).find("td:eq(0)").text();
            var rvalor = $(fila).find("td:eq(1)").text().replace("$", "");
            rcrss.push({ nombre: r, valor: parseFloat(rvalor) });
        });
        var idObjetivo = $('#objetivoModal').data("id");
        if (idObjetivo == "0") {
            objetivo = { id: Math.random().toString(36).substring(7), nombre: obj, mes: m, recursos: rcrss };
            this.objetivos.push(objetivo);
        } else {
            objetivo = this.objetivos.find(x => x.id === idObjetivo);
            objetivo.nombre = obj;
            objetivo.mes = m;
            objetivo.recursos = rcrss;
        }

        $('#objetivoModal').modal('hide');
        this.imprimirObjetivos();
        this.cambioTotal();
    },

    limpiarModalObjetivos: function () {
        $("#txtObjetivoRecursos").val("");
        $("#txtMes").val("");
        $("#txtRecursoObjetivo").val("");
        $("#txtDesembolsoObjetivo").val("");
        $("#tblRecursosModal").html("");
        $('#objetivoModal').data("id", "0");
    },

    getValoresObjetivo: function (objetivo) {
        var html = '<tr><td rowspan="' + objetivo.recursos.length + '">' + objetivo.mes + '</td><td rowspan="' + objetivo.recursos.length + '"> <a href="#" class="editObjetivo" data-id="' + objetivo.id + '">' + objetivo.nombre + '</a></td>';
        var contador = 0;
        var total = 0;
        $.each(objetivo.recursos, function (index, recurso) {
            contador += 1;
            if (contador === 1) {
                html += '<td>' + recurso.nombre + '</td><td>$' + recurso.valor + '</td></tr>';
            }
            else {
                html += '<tr><td>' + recurso.nombre + '</td><td>$' + recurso.valor + '</td></tr>';
            }
            total += recurso.valor;
        });
        return { html: html, total: total };
    },

    imprimirObjetivos: function () {
        var self = this;
        var total = 0;
        $("#tblObjetivoFijo tbody").html("");

        $.each(self.objetivos, function (index, obj) {
            var cal = self.getValoresObjetivo(obj);
            $("#tblObjetivoFijo tbody").append(cal.html);
            total += cal.total;
        });
        $("#txtTotalPropuesta").val(total);
        $("#tblObjetivoFijo tbody").append('<tr><td>Total</td><td></td><td></td><td>$' + total + '</td></tr>');
    },

    agregarRecurso: function () {
        var plantilla = '<tr class="trRecurso"><td style= "width:60%;" >__recurso__</td><td>$__desembolso__</td><td class="text-center"><button class="btn btn-outline-danger btnRemoveRecurso btn-sm" type="button"><i class="fa fa-minus"></i></button></td></tr>';
        var recurso = $("#txtRecursoObjetivo").val();
        var desembolso = $("#txtDesembolsoObjetivo").val();

        if ($.trim(recurso) === "" || !$.isNumeric(desembolso)) {
            toastr.error('Información ingresada incompleta', 'Operación incompleta', { positionClass: "toast-bottom-right" });
            return false;
        }

        var html = plantilla.replace('__recurso__', recurso).replace('__desembolso__', desembolso);

        $('#tblRecursosModal').append(html);
        $('#txtRecursoObjetivo').val("");
        $('#txtDesembolsoObjetivo').val("");
    },

    showModalObjetivo: function () {
        //limpio el html
        this.limpiarModalObjetivos();
        $('#objetivoModal').modal('show');
    },

    cambioTotal: function () {
        this.calcularCapital();
        this.generarTablaAmortizacion();
        this.getDatosPropuesta();
    },

    getDatosPropuesta: function () {
        var self = this;
        var inversion = $("#txtTotalPropuesta").val();
        var ventaPromedio = $("#txtVentaEstimada").val();

        if ($.trim(ventaPromedio) !== "") {
            htclibjs.Ajax({
                url: "/Convocatoria/GetDatosPropuesta",
                data: JSON.stringify({idP: self.idPostulante, inv: inversion, vp: ventaPromedio }),
                success: function (r) {
                    var dp = r.data;
                    $("#spValorProyecto").html(dp.ValorProyecto);
                    $("#spTpe").html(dp.Tpe);
                    $("#spInversionCrisfe").html(dp.InversionCrisfe);
                    $("#spSemilla").html(dp.Semilla);
                    $("#spInversion").html(dp.Inversion);
                    $("#spInversionSugerida").html(dp.InversionSugerida);
                    $("#spVentaEstimada").html(dp.InversionVentasEstimadasUnidades);
                    $("#spPE2Unidades").html(dp.PE2Unidades);
                    $("#spPE2Dolar").html(dp.PE2Dolar);
                    $("#spPE2Meses").html(dp.PE2Meses);
                    $("#spPrecioPonderado").html(dp.PrecioPonderado);
                    $("#spCostoPonderado").html(dp.CostoPonderado);
                    $("#spVentasMensuales").html(dp.VentasMensuales);
                    $("#spVentasEstimadasUnidades").html(dp.VentasEstimadasUnidades);
                    $("#spCostoFijo").html(dp.CostoFijo);

                    $("#txtPuntoEquilibrio").val(dp.PE2Meses);
                }
            });
        }

    },

    calcularCapital: function () {
        var self = this;
        var tiempo = $("#txtPlazo").val();
        var gracia_ = $("#txtGracias").val();
        var total_ = parseFloat($("#txtTotalPropuesta").val());
        var tasa_ = $("#txtTasa").val();

        if ($.isNumeric(tiempo) && $.isNumeric(gracia_) && $.isNumeric(total_) && $.isNumeric(tasa_) && parseInt(tiempo) != 0 && parseInt(gracia_) != 0 && parseInt(total_) != 0 && parseInt(tasa_) != 0) {
            htclibjs.Ajax({
                url: "/Convocatoria/GetDatosAmortizacion",
                data: JSON.stringify({ plazo: tiempo, gracia: gracia_, tasa: tasa_, total: total_ }),
                success: function (r) {
                    var datos = r.data;
                    $("#spApoyoEconomico").html(datos.ApoyoEconomico);
                    $("#spAporteFondoEmprendimiento").html(datos.AporteFondo);
                    $("#spTotalApoyo").html(datos.TotalApoyo);
                    $("#spTiempo").html(datos.Tiempo);
                    $("#spReembolso").html(-datos.Reembolso.toFixed(2));
                    $("#spFechaInicioPago").html(getOutDate(new Date(datos.FechaInicioPago)));

                    $("#txtCuotaMensual").val(datos.Reembolso);
                }
            });

        }
    },

    generarTablaAmortizacion: function () {
        var self = this;
        var tiempo = $("#txtPlazo").val();
        var gracia_ = $("#txtGracias").val();
        var total_ = parseFloat($("#txtTotalPropuesta").val());
        var tasa_ = $("#txtTasa").val();

        var cuerpo = $("#tblAmortizacion tbody");
        var plantilla = '<tr><td>_nro_</td><td>_fecha_</td><td>_saldo_</td><td>_aporte_</td><td>_capital_</td><td>_reembolso_</td></tr>';
        var html = '';

        if ($.isNumeric(tiempo) && $.isNumeric(gracia_) && $.isNumeric(total_) && $.isNumeric(tasa_) && parseInt(tiempo) != 0 && parseInt(gracia_) != 0 && total_ != 0 && parseInt(tasa_) != 0) {
            htclibjs.Ajax({
                url: "/Convocatoria/GetTablaAmortizacion",
                data: JSON.stringify({ plazo: tiempo, gracia: gracia_, tasa: tasa_, total: total_ }),
                success: function (r) {
                    var meses = r.data;
                    var fechaIni = null;
                    $.each(meses, function (index, mes) {
                        html += plantilla.replace('_nro_', mes.numero).replace('_fecha_', mes.fecha === null ? '' : getOutDate(new Date(mes.fecha))).replace('_saldo_', mes.saldo === null ? '' : mes.saldo).replace('_aporte_', mes.aporte === null ? '' : mes.aporte).replace('_capital_', mes.capital === null ? '' : mes.capital).replace('_reembolso_', mes.reembolso === null ? '' : mes.reembolso);
                        if (fechaIni === null && mes.fecha !== null) {
                            fechaIni = mes.fecha;
                        }
                    });
                    $(cuerpo).html(html);
                }
            });
        }


    },

};