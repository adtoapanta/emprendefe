﻿$(document).ready(function () { initAdministrarProveedores(); });

function initAdministrarProveedores() {
    console.log('init Administrar Proveedores ')


    $('#btnGuardarProveedor').click(function (e) {
        guardarOEditar();
    });
    $('#btnBorrarProveedor').click(function (e) {
        eliminarProveedor();
    });

    $('#modalProveedores').on('hidden.bs.modal', function () {
        limpiarCampos();
    });

    consultarProveedores();
}

function eliminarProveedor() {

    var idProveedor = $("#txtIdProveedor").val();

    if (idProveedor == 0) {
        toastr.error('', 'Primero debe seleccionar un proveedor antes de eliminar', { positionClass: "toast-bottom-right" });
    }

    else {

        var boton = new BtnLoad("btnBorrarProveedor");
        boton.Start();
        htclibjs.Ajax({
            url: "/ProveedoresCapacitacion/eliminarProveedor",
            data: JSON.stringify({
                idProveedor: idProveedor
            }),
            success: function (r) {

                consultarProveedores();
                $('#modalProveedores').modal('hide');
                boton.Stop();
                toastr.success('', 'Proveedor eliminado', { positionClass: "toast-bottom-right" });
            }
        });

    }

}

function guardarOEditar() {

    var idProveedor = $("#txtIdProveedor").val();

    if (idProveedor == 0)
        guardarProveedor();
    else
        editarProveedor();

}

function guardarProveedor() {

    var nombre = $("#txtNombreProveedor").val();
    var email = $("#txtCorreoProveedor").val();

    var boton = new BtnLoad("btnGuardarProveedor");
    boton.Start();
    htclibjs.Ajax({
        url: "/ProveedoresCapacitacion/guardarProveedor",
        data: JSON.stringify({
            nombre: nombre,
            correo: email
        }),
        success: function (r) {

            toastr.success('', 'Proveedor guardado', { positionClass: "toast-bottom-right" });
            consultarProveedores();
            limpiarCampos();
            boton.Stop();
        }
    });

}

function editarProveedor() {

    var idProveedor = $("#txtIdProveedor").val();
    var nombre = $("#txtNombreProveedor").val();
    var email = $("#txtCorreoProveedor").val();

    var boton = new BtnLoad("btnGuardarProveedor");
    boton.Start();
    htclibjs.Ajax({
        url: "/ProveedoresCapacitacion/editarProveedor",
        data: JSON.stringify({
            idProveedor: idProveedor,
            nombre: nombre,
            correo: email
        }),
        success: function (r) {

            toastr.success('', 'Proveedor actualizado', { positionClass: "toast-bottom-right" });
            consultarProveedores();
            $('#modalProveedores').modal('hide');
            boton.Stop();
        }
    });

}

function consultarProveedores() {

    htclibjs.Ajax({
        url: "/ProveedoresCapacitacion/consultarProveedores",
        data: JSON.stringify({

        }),
        success: function (r) {

            var trHTML =
                '<thead>' +
                '<tr>' +
                '<th>Nombre</th>' +
                '<th>Email</th>' +
                '<th></th>' + //fila vacia donde va el boton de editar
                '</tr > ' +
                '</thead> ' +

                '<tbody> '

                ;

            $.each(r.data, function (i, item) {

                trHTML +=

                    '<tr>' +
                    '<td id="tdNombreProveedor' + item.id + '" >' + item.nombre + '</td>' +
                    '<td id="tdEmailProveedor' + item.id + '" >' + item.correo + '</td>' +
                    //boton editar de la tabla
                    '<td id="tdIdProveedor' + item.id + '"> <button class="btnSeleccionarProveedor btn btn-primary" value="' + item.id + '" type="button" data-toggle="modal" data-target="#modalProveedores" >Editar</button> </td>' +
                    '</tr> ';


            });

            trHTML +=
                '</tbody>';

            $('#tblProveedores').html('');
            $('#tblProveedores').append(trHTML);

            //accion al hacer clic en boton de la tabla
            $('.btnSeleccionarProveedor').on('click', function (e) {

                selecionarProveedor($(this));


            });
        }
    });

}

function selecionarProveedor(boton) {

    var idProveedor = boton.val();
    var nombre = $('#tdNombreProveedor' + boton.val()).html();
    var email = $('#tdEmailProveedor' + boton.val()).html();

    $("#txtIdProveedor").val(idProveedor);
    $("#txtNombreProveedor").val(nombre);
    $("#txtCorreoProveedor").val(email);


}

function limpiarCampos() {

    $("#txtIdProveedor").val(0);
    $("#txtNombreProveedor").val('');
    $("#txtCorreoProveedor").val('');

}