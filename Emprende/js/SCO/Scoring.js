﻿//mover esto a emprendimiento.js

//$(document).ready(function () {
//    Scoring.MostrarTabla(2, 5, 2, "#dvQ", true, function () { alert('guardado')}) ;
//});

var Scoring = {
    handlerMostrarS: {},
    handlerCargarS: {},
    handlerGuardarS: {},
    div: '',
    showGuardar: true,
    establa: false, 
    idCuestionario: 0,

    //cambiar para recibir objeto con opciones..
    MostrarCuestionario: function (idT, idR, idC, div, showGuardar, handlerGuardar, handlerMostrar) {

        this.div = div;
        this.showGuardar = showGuardar;
        

        this.handlerMostrarS = handlerMostrar;
        this.handlerGuardarS = handlerGuardar;
        this.handlerCargarS = Scoring.creaCuestionario;

        this.idCuestionario = idC;

        Scoring.getCuestionario(idT, idR, idC);

    },

    MostrarTabla: function (idT, idR, idC, div, showGuardar, handlerGuardar, handlerMostrar) {
        this.div = div;
        this.showGuardar = showGuardar;


        this.handlerMostrarS = handlerMostrar;
        this.handlerGuardarS = handlerGuardar;
        this.handlerCargarS = Scoring.creaTablaCuestionario;

        this.idCuestionario = idC;
        this.establa = true;

        Scoring.getCuestionario(idT, idR, idC);
    },


    getCuestionario: function (idT, idR, idC, queHacer) {
       
        htclibjs.Ajax({
            url: "/Evaluacion/getEvaluacion",
            data: JSON.stringify({ idt:idT , idc:idC, idr:idR }),
            success: function (r) {
                if (Scoring.handlerCargarS) {
                    Scoring.handlerCargarS(r.data);
                }
                
            }
        });

    },
    
    //creaCuestionarioAnt: function (preguntas) {
    //    var html = "";
    //    var pr = {};
    //    var op = {};
    //    var sel = "";
    //    var obl = '';
    //    var obli = 0;

    //    for (var i = 0; i < preguntas.length; i++) {
    //        pr = preguntas[i];

    //        if (pr.obligatorio == true) { obl = '(*)'; obli = 1; } else { obl = ''; obli = 0;}

    //        html = html + pr.nombre + obl + ":";
            
    //        if (pr.idTipo == 1) {
    //            html = html + "<input class='SCO_prg' type='number' id='" + pr.idR + "'value='" + pr.respuesta + "'  data-obl='" + obli +"' />";
    //        }
    //        else {
    //            html = html + "<Select class='SCO_prg' id='" + pr.idR + "'  data-obl='" + obli + "' >";
    //            html = html + "<option value='0'> - Seleccione - </option>";
    //            for (var j = 0; j < pr.opciones.length; j++) {
    //                op = pr.opciones[j];

    //                if (op.id == pr.idPosiblerespuesta) {
    //                    sel = 'selected'
    //                } else {
    //                    sel = ''
    //                }

    //                html = html + "<option value='" + op.id + "' " + sel + " >" + op.nombre + "</option>";
    //            }
    //            html = html + "</Select>";
    //        }

    //        html = html + "<br/>";
    //    }

    //    if (Scoring.showGuardar == true) {
    //        html = html + "<input type='button' value='Guardar' id='btnGuarda' onclick='Scoring.Guardar();'/>";
    //    }
        
    //    html = html + "<br/>";


    //    $(Scoring.div).html(html);

    //    if (Scoring.handlerMostrarS) {
    //        Scoring.handlerMostrarS(preguntas);
    //    }
    //},

    creaCuestionario: function (preguntas) {
        let html = "";
        let pr = {};
        let op = {};
        let sel = "";
        let obl = '';
        let obli = 0;

        for (let i = 0; i < preguntas.length; i++) {
            pr = preguntas[i];

            if (pr.obligatorio == true) { obl = '(*)'; obli = 1; } else { obl = ''; obli = 0; }
            let textoPregunta = pr.nombre + obl + ":";
            
            html += '<div class="form-group row">';
            html += `   <label for="${pr.idR}" class="col-sm-7 col-form-label">${textoPregunta}</label>`;
            html += '   <div class="col-sm-5">';
            html += Scoring.InputRespuesta(pr, obli);
            html += '   </div>';
            html += '</div>';
        }

        if (Scoring.showGuardar == true) {
            html += '<div class="row">';
            html += '<div class="col text-right">';
            html = html + '<button type="button" class="btn btn-primary" id="btnGuarda" onclick="Scoring.Guardar(this);"><i class="fa fa-save"></i> Guardar</button>';
            html += '</div></div>';
        }

        $(Scoring.div).html(html);

        if (Scoring.handlerMostrarS) {
            Scoring.handlerMostrarS(preguntas);
        }
    },

    InputRespuesta: function (pr, obli) {
        let html = '';

        if (pr.idTipo == 1) {
            html += `<input type="number" class="form-control SCO_prg" id="${pr.idR}" value="${pr.respuesta}" data-obl="${obli}" />`;
        }
        else {
            html += `<select class="form-control SCO_prg" id="${pr.idR}" data-obl="${obli}">`;
            html += '    <option value="0"> - Seleccione - </option>';
                         for (let j = 0; j < pr.opciones.length; j++) 
                         {
                             op = pr.opciones[j];
                             if (op.id == pr.idPosiblerespuesta) { sel = 'selected'} else { sel = '' }
            html += `        <option value="${op.id}" ${sel}>${op.nombre}</option>`;
                         }
            html += '</select>';
        }
        return html;
    },

    creaTablaCuestionario: function (preguntas) {

        var html = "";
        var pr = {};
        var op = {};
        var sel = "";
        var obl = '';
        var obli = 0;


        html = html + "<table id='frmQ' border=1>";

        //Modelado para Qs de emprndefe

        if (Scoring.idCuestionario == 3 || Scoring.idCuestionario == 4) {
            html = html + "<tr><td></td><td>&nbsp;&nbsp;Sí&nbsp;&nbsp;</td><td>&nbsp;&nbsp;No&nbsp;&nbsp;</td><td>Comentarios</td></tr>";
        } else {
            html = html + "<tr><td></td><td>&nbsp;&nbsp;0&nbsp;&nbsp;</td><td>&nbsp;&nbsp;1&nbsp;&nbsp;</td><td>&nbsp;&nbsp;2&nbsp;&nbsp;</td> <td>&nbsp;&nbsp;3&nbsp;&nbsp;</td><td>&nbsp;&nbsp;4&nbsp;&nbsp;</td><td>&nbsp;&nbsp;5&nbsp;&nbsp;</td><td>&nbsp;&nbsp;6&nbsp;&nbsp;</td></tr>";            
        }

        

        for (var i = 0; i < preguntas.length; i++) {
            pr = preguntas[i];

            if (pr.obligatorio == true) { obl = '(*)'; obli = 1; } else { obl = ''; obli = 0; }


            html = html + "<tr><td>";
            html = html + pr.nombre ;
            html = html + "</td>";

                for (var j = 0; j < pr.opciones.length; j++) {
                    op = pr.opciones[j];

                    if (op.id == pr.idPosiblerespuesta) {
                        sel = 'checked'
                    } else {
                        sel = ''
                    }

                    html = html + "<td>";
                    //html = html + "<label><input type='radio' name=' " + pr.idR + "' value='" + op.id + "'" + sel + ">" + op.nombre + "</label>";
                    html = html + "<label><input type='radio' class='SCO_prg' name='" + pr.idR + "' value='" + op.id + "' " + sel + "></label>";
                    html = html + "</td>";
                }            

                if (Scoring.idCuestionario == 3 || Scoring.idCuestionario == 4) {
                    html += `<td><input type="text" class="form-control " id="Ob${pr.idR}"/></td>`;                    
                }

                html = html + "</tr>";

        }

        html = html + "</table>";

        html = html + "<br/>";
        if (Scoring.showGuardar == true) {
            html += '<div class="row">';
            html += '<div class="col text-right">';
            html = html + '<button type="button" class="btn btn-primary" onclick="Scoring.Guardar(this);"><i class="fa fa-save"></i> Guardar</button>';
            html += '</div></div>';
        }

        html = html + "<br/>";


        $(Scoring.div).html(html);

        if (Scoring.handlerMostrarS) {
            Scoring.handlerMostrarS(preguntas);
        }

    },

    Guardar: function (obj) {
        var c = $(obj).children().attr('class');
        $(obj).children().removeClass()
        $(obj).children().addClass("fa fa-spinner fa-spin")
        $(obj).attr("disabled", true);

        if (Scoring.Validar() == true) {

            if (Scoring.establa == true) {
                var respuestas = Scoring.getRespsTabla();
            } else {
                var respuestas = Scoring.getResps();
            }


            htclibjs.Ajax({
                url: "/Evaluacion/responder",
                data: JSON.stringify({ resps: respuestas }),
                success: function (r) {
                    if (Scoring.handlerGuardarS) {
                        Scoring.handlerGuardarS(r.data);
                    } 
                    $(obj).children().addClass(c);
                    $(obj).attr("disabled", false);
                },
                successError: function (r) {
                    $(obj).children().addClass(c);
                    $(obj).attr("disabled", false);
                },
                error: function () {
                    $(obj).children().addClass(c);
                    $(obj).attr("disabled", false);
                }
            });
        }
    },

    Validar: function () {

        //alert("quitar esto");
        //return true;

        var valida = true;

        //para tablas
        $("#frmQ input[type = radio]").each(function () {
            var name = $(this).attr("name");
            if ($("#frmQ input:radio[name=" + name + "]:checked").length == 0) {

                alert("debe completar todos los campos obligatorios");

                valida = false;
                //break;
                return false;
            }
        });
       


        $('.SCO_prg').each(function (i, obj) {

            if ($(this).data('obl') === 1 && ($(this).val() === '0' || $(this).val() === '')) {
                alert("debe completar todos los campos obligatorios");
                valida = false;
                //break;
                return false;
            }            
        });

        return valida;
        //return true;
    },

    getResps: function () { 
        var resps = [];
        var idp = 1;

        $('.SCO_prg').each(function (i, obj) {

            var resp = {
                id: obj.id,
                idOpcion: $(this).val(),
                valor: $(this).val(),
                obs: '' //$("#Ob" + obj.id).val()
            }

            resps.push(resp);
        });


        return resps;
    },

    getRespsTabla: function () {
        var resps = [];
        var idp = 1;

        $("#frmQ input[type=radio]:checked").each(function () {
            var valor = $(this).val();
            var idR = $(this).attr("name");

            var resp = {
                id: idR,
                idOpcion: valor,
                valor: valor,
                obs: $("#Ob" + idR).val()
            }

            resps.push(resp);

        });

        return resps;
    },


    GuardarExt: function (handler) {
        Scoring.handlerGuardarS = handler;
        Scoring.Guardar()
    }
}