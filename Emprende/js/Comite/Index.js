﻿$(document).ready(function () {
    comiteEval.iniciar();
});


var comiteEval = {
    idComPos: 0,    

    btnCerrarEval: new BtnLoad('btnCerrarEval'),
    iniciar: function () {
        var self = this;

        $('#tblPostulacionesComite').DataTable({
            aaSorting: [], language: SPANISHDATATABLE
        });

        
        $(document).on('click', '.btnEvaluar', function (e) {
            e.preventDefault();
            var idCP = $(this).data('idcp');
            var idT = $(this).data('tipo');

            self.evaluar(idCP,idT);
        });

    },

    getPostulacionesParaMiembro: function () {
        var self = this;
        var idPublico = $("#txtIdPublico").val();
        htclibjs.Ajax({
            url: "/Comite/PostulacionesMiembroComite",
            data: JSON.stringify({ idP: idPublico }),
            success: function (r) {
                self.showPostulantes(r.data);
                toastr.info('Actualizado', '', { positionClass: "toast-bottom-right" });
            },
            errorSuccess: function (r) {
            },
            error: function (r) {
            }
        });
    },
    
    showPostulantes: function (postulantes) {
        var plantilla = '<button type="button" class="btn btn-primary btnEvaluar" data-idcp="__idCP__" data-tipo="__idTipo__" ><i class="fas fa-pencil-alt"></i></button>';
        //Destruimos antes de acrgar nuevos
        if ($.fn.DataTable.isDataTable('#tblPostulacionesComite')) {
            $('#tblPostulacionesComite').DataTable().destroy();
        }
        $('#tblPostulacionesComite tbody').empty();

        $("#tblPostulacionesComite").DataTable({
            data: postulantes,
            aaSorting: [],
            bDestroy: true,
            bFilter: true,
            bPaginate: true,
            bLengthChange: true,
            language: SPANISHDATATABLE,
            aoColumns: [
                {
                    width: '*', mData: 'convocatoria', title: 'Convocatoria',
                    mRender: function (data, type, full) {
                        return data;
                    }
                },
                {
                    width: '*', mData: 'nombre', title: 'Emprendimiento',
                    mRender: function (data, type, full) {
                        return '<a href="' + SITEROOT + '/Postulacion/DescargarBriefEmprendedor?idPostulacion=' + full.idPostulacion + '">' + data + '</a>';
                    }
                },
                {
                    width: '*', mData: 'id', title: '', className:'text-center',
                    mRender: function (data, type, full) {
                        return plantilla.replace("__idCP__", full.idComitePostulacion).replace("__idTipo__", full.idTipo);
                    }
                },
            ],
        });

    },

    guardarEvaluacion: function (puntaje) {
        var self = comiteEval;
        var calif = puntaje;
        var obs1 = $("#txtObs").val();

        htclibjs.Ajax({
            url: "/Comite/Calificar",
            data: JSON.stringify({ idCP: comiteEval.idComPos, cal: calif, obs:obs1 }),
            success: function (r) {
                toastr.info('Guardado', '', { positionClass: "toast-bottom-right" });
                self.getPostulacionesParaMiembro();
                $('#mdlPreguntas').modal('hide');
            },
            errorSuccess: function (r) {
            },
            error: function (r) {
            }
        });

    },

    evaluar: function (idComitePostulacion, tipo) {
        comiteEval.idComPos = idComitePostulacion;

        let idQ = parseInt(tipo) === 2 ? 3 : 4;

        Scoring.MostrarTabla(3, comiteEval.idComPos, idQ, "#divEval", true, comiteEval.guardarEvaluacion);
        $('#mdlPreguntas').modal('show')
    }



}