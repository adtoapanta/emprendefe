﻿$(document).ready(function () { initAdministrarComite(); });

function initAdministrarComite() {
    console.log('init Administrar Comite ')


    $('#btnGuardarComite').click(function (e) {
        guardarOEditar();
    });
    $('#btnBorrarComite').click(function (e) {
        eliminarMiembroComite();
    });    

    $('#modalMiembroComite').on('hidden.bs.modal', function () {
        limpiarCampos();
    });

    consultarMiembrosComite();
}

function eliminarMiembroComite(){
    
    var idComite = $("#txtIdMiembroComite").val();

    if (idComite == 0) {
        toastr.error('', 'Primero debe seleccionar un miembro de comite antes de eliminar', { positionClass: "toast-bottom-right" });
    }

    else {

        var boton = new BtnLoad("btnBorrarComite");
        boton.Start();
        htclibjs.Ajax({
            url: "/Comite/eliminarMiembroComite",
            data: JSON.stringify({
                idComite: idComite
            }),
            success: function (r) {
                
                consultarMiembrosComite();
                $('#modalMiembroComite').modal('hide');
                boton.Stop();
                toastr.success('', 'Comite eliminado', { positionClass: "toast-bottom-right" });
            }
        });

    }    

}



function guardarOEditar() {

    var idComite = $("#txtIdMiembroComite").val();

    if (idComite == 0)
        guardarMiembroComite();
    else
        editarMiembroComite();

}

function guardarMiembroComite() {

    console.log("Ingreso guardar Comite");
    var nombre = $("#txtNombreComite").val();
    var email = $("#txtEmailComite").val();
    var telefono = $("#txtTelefonoComite").val();
    var institucion = $("#txtInstitucionComite").val();


    var boton = new BtnLoad("btnGuardarComite");
    boton.Start();
    htclibjs.Ajax({
        url: "/Comite/CrearMiembroComite",
        data: JSON.stringify({
            name: nombre,
            email: email,
            telefono: telefono,
            institucion: institucion
        }),
        success: function (r) {

            toastr.success('', 'Comite guardado', { positionClass: "toast-bottom-right" });
            consultarMiembrosComite();
            limpiarCampos();
            boton.Stop();
        }
    });

}

function editarMiembroComite() {

    var idComite = $("#txtIdMiembroComite").val();
    var nombre = $("#txtNombreComite").val();
    var email = $("#txtEmailComite").val();
    var telefono = $("#txtTelefonoComite").val();
    var institucion = $("#txtInstitucionComite").val();


    var boton = new BtnLoad("btnGuardarComite");
    boton.Start();
    htclibjs.Ajax({
        url: "/Comite/editarMiembroComite",
        data: JSON.stringify({
            idComite: idComite,
            nombre: nombre,
            telefono: telefono,
            correo: email,
            institucion: institucion
        }),
        success: function (r) {

            toastr.success('', 'Comite actualizado', { positionClass: "toast-bottom-right" });
            consultarMiembrosComite();
            $('#modalMiembroComite').modal('hide');            
            boton.Stop();
        }
    });

}

function consultarMiembrosComite() {

    console.log("Ingreso consultar Comite");

    htclibjs.Ajax({
        url: "/Comite/consultarMiembrosComite",
        data: JSON.stringify({

        }),
        success: function (r) {

            var trHTML =
                '<thead>' +
                '<tr>' +
                '<th>Nombre</th>' +
                '<th>Email</th>' +
                '<th>Teléfono</th>' +
                '<th>Institución</th>' +
                '<th></th>' + //fila vacia donde va el boton de editar
                '</tr > ' +
                '</thead> ' +

                '<tbody> '

                ;

            $.each(r.data, function (i, item) {

                trHTML +=

                    '<tr>' +
                    '<td id="tdNombreComite' + item.id + '" >' + item.nombre + '</td>' +
                    '<td id="tdEmailComite' + item.id + '" >' + item.email + '</td>' +
                    '<td id="tdTelefonoComite' + item.id + '">' + item.telefono + '</td>' +
                    '<td id="tdInstitucionComite' + item.id + '" ">' + item.institucion + '</td>' +
                    //boton editar de la tabla
                    '<td id="tdIdComite' + item.id + '"> <button class="btnSeleccionarComite btn btn-primary" value="' + item.id + '" type="button" data-toggle="modal" data-target="#modalMiembroComite" >Editar</button> </td>' +
                    '</tr> ';


            });

            trHTML +=
                '</tbody>';

            $('#tblMiembrosComite').html('');
            $('#tblMiembrosComite').append(trHTML);

            //accion al hacer clic en boton de la tabla
            $('.btnSeleccionarComite').on('click', function (e) {

                selecionarMiembroComite($(this));


            });
        }
    });

}

function selecionarMiembroComite(boton) {
    
    var idComite = boton.val();
    var nombre = $('#tdNombreComite' + boton.val()).html();
    var email = $('#tdEmailComite' + boton.val()).html();
    var telefono = $('#tdTelefonoComite' + boton.val()).html();
    var institucion = $('#tdInstitucionComite' + boton.val()).html();    

    $("#txtIdMiembroComite").val(idComite);
    $("#txtNombreComite").val(nombre);
    $("#txtEmailComite").val(email);
    $("#txtTelefonoComite").val(telefono);
    $("#txtInstitucionComite").val(institucion);
    
}

function limpiarCampos() {

    $("#txtIdMiembroComite").val(0);
    $("#txtNombreComite").val('');
    $("#txtEmailComite").val('');
    $("#txtTelefonoComite").val('');
    $("#txtInstitucionComite").val('');

}