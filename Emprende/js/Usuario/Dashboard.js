﻿$(document).ready(function () { initDashboard(); });

function initDashboard() {
    console.log('init Dashboard ')
    consultarEmprendimientos();
    consultarPostulaciones();
    cargarConvocatorias();
    $('#btnPostularAConvocatoria').click(function (e) {
        postular();
    });
    $('#btnIniciarSension').click(function (e) {
        iniciarSesion();
    });
    $('#btnAgregarEmprendimiento').click(function (e) {

        //var url = SITEROOT + '/Emprendimiento/CrearEmprendimiento';
        //window.location.href = url;
    });

    $('#btnLgCrearEmprendimiento').click(function (e) {

        //var url = SITEROOT + '/Emprendimiento/CrearEmprendimiento';
        //window.location.href = url;
    });




}

function iniciarSesion() {

    $('form[name=test]').submit();

}


function consultarEmprendimientos() {

    
    htclibjs.Ajax({
        url: "/Emprendimiento/consultarEmprendimientosPorPersona",
        data: JSON.stringify({
            
        }),
        success: function (r) {

            if (r.data == 0) {

                //crearEmprendimiento
                $('#crearEmprendimiento').show();

            }
            else {
                console.log('con emprendimientos');
                $('#misEmprendimientos').show();
                var data = r.data;
                
                var trHTML =
                    '<thead>' +
                    '<tr>' +
                    '<th>Nombre</th>' +                    
                    '<th></th>' + //espacio vacio para boton
                    '<th></th>' + //espacio vacio para boton
                    '</tr> ' +
                    '</thead> ' +
                    '<tbody>';

                $.each(data, function (i, item) {

                    var botonPostular = '#btnPostular';

                    trHTML +=
                        '<tr>' +
                        '<td id="tdNombreEmprendimiento' + item.id + '">' + item.nombre + '</td>' +                        
                        // boton de seleccion
                    '<td class="text-center"> <button id="' + item.id + '" class="btn btn-primary btn-sm seleccionarEmprendimiento"  ><i>Editar</i></button> </td>' +
                    '<td class="text-center"> <button value="' + item.completo + '" data-idemprendimiento = "' + item.id + '" class="btn btn-primary btn-sm postularEmprendimiento"  ><i>Postular</i></button> </td>' +
                        '</tr> ';

                    
                });
                trHTML += '</tbody>';

                $('#tblMisEmprendimientos').html('');
                $('#tblMisEmprendimientos').append(trHTML);                

                //accion al hacer clic en boton de la tabla
                $('.seleccionarEmprendimiento').on('click', function (e) {

                    selecionarEmprendimiento($(this));
                    

                });

                $('.postularEmprendimiento').on('click', function (e) {

                    postularEmprendimiento($(this));                   

                });
            }
        }
    });

}

function postularEmprendimiento(boton) {

    var completo = boton.val();
    var idEmprendimiento = boton.data("idemprendimiento");
    if (completo == 'true') {

        $('#txtIdEmprendimiento').val(idEmprendimiento);
        $('#modalPostular').modal('show');        
        
    }
    else {

        alert('Debe completar el registro de su emprendimiento antes de postular');

    }
}


function selecionarEmprendimiento(boton) {

    var emprendimientoId = boton.attr('id');
    //$('#txtIdEmprendimiento').val(emprendimientoId);

    var url = SITEROOT + '/Emprendimiento/EditarEmprendimiento/' + emprendimientoId;
    window.location.href = url;
}

function consultarPostulaciones() {

    var idPersona = currentSession.personaId;
    if (idPersona == 0) {
        var url = window.location.href.toString();
        var urlSplit = url.split("/");
        idPersona = urlSplit[urlSplit.length - 1];
        currentSession.personaId = idPersona;
    }
    htclibjs.Ajax({
        url: "/Postulacion/consultarPostulacionesPersona",
        data: JSON.stringify({
            idPersona: idPersona
        }),
        success: function (r) {
            if (r.data == 0) {

                console.log('sin Postulaciones');


            }
            else {
                $('#misPostulaciones').show();
                var data = r.data;                
                var trHTML =
                    '<thead>' +
                    '<tr>' +
                    '<th>Convocatoria</th>' +
                    '<th>Emprendimiento</th>' +
                    '<th>Etapa</th>' +
                    //espacio vacio para boton
                    '<th></th>' +
                    '</tr> ' +
                    '</thead> ' +
                    '<tbody>';

                $.each(data, function (i, item) {

                    trHTML +=
                        '<tr>' +
                        '<td >' + item.nombreConvocatoria + '</td>' +
                        '<td >' + item.nombreEmprendimiento + '</td>' +
                        '<td >' + item.nombreEtapa + '</td>' +
                        // boton de seleccion
                        '<td class="text-center"> <a href="' + SITEROOT + '/Postulacion/Etapa/' + item.idPostulacion + '" class="btn btn-primary btn-sm" ><i>Ver Postulación</i></a> </td>' +
                        '</tr> ';
                });
                trHTML += '</tbody>';
                    

                $('#tblMisPostulaciones').html('');
                $('#tblMisPostulaciones').append(trHTML);
                // convierte a datatable 
                //$('#tblConvocatoriasAbiertas').DataTable({ aaSorting: [], language: SPANISHDATATABLE });

            }
        }
    });

}

function cargarConvocatorias() {

    $("#slcConvocatoriasAbiertas").empty();
    $("#slcConvocatoriasAbiertas").append(new Option("Seleccione", "", true, true));

    htclibjs.Ajax({
        url: "/Convocatoria/cosultarConvocatoriasAbiertas",
        data: JSON.stringify({}),
        success: function (r) {

            var data = r.data;

            if (data == 0) {

                $(".sinConvocatorias").show();

            }

            else {

                $.each(data, function (i, item) {

                    $(".conConvocatorias").show();
                    $("#slcConvocatoriasAbiertas").append(new Option(item.nombre, item.id));
                });
            }

        }
    });

}

function postular() {

    var convocatoriaId = $("#slcConvocatoriasAbiertas").val();
    var emprendimientoId = $("#txtIdEmprendimiento").val();
    var url = SITEROOT;

    url += '/Convocatoria/verConvocatoria/' + convocatoriaId + '/' + emprendimientoId;
    window.location.href = url;    

}