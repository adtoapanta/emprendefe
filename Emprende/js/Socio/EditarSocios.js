﻿$(document).ready(function () { initMatrizInversion(); });

function initMatrizInversion() {
    console.log('init Socios ')

    $('#btnAgregarSocio').click(function (e) {
        limpiarCampos();
    });

    $('#btnGuardarSocio').click(function (e) {
        guardarOEditar();
    });
    $('#btnBorrarSocio').click(function (e) {
        eliminarSocio();
    });

    $('#modalSocio').on('hidden.bs.modal', function () {
        limpiarCampos();
    });
    
    consultarSociosEmprendimiento();
}

function eliminarSocio() {

    var idSocio = $("#txtIdSocio").val();

    if (idSocio == 0) {

        alert('Seleccione un socio antes de eliminar');

    }
    else {
        htclibjs.Ajax({
            url: "/Socio/eliminarSocio",
            data: JSON.stringify({
                idSocio: idSocio
            }),
            success: function (r) {

                alert("Socio Eliminado");
                consultarSociosEmprendimiento();
                limpiarCampos();
            }
        });
    }



}

function guardarOEditar() {

    var idSocio = $("#txtIdSocio").val();
    if (idSocio == 0) {

        guardarSocio();

    }
    else {

        editarSocio();

    }
}

function guardarSocio() {

    console.log("Ingreso guardar socio");
    var idEmprendimiento = $("#txtIdEmprendimiento").val();
    var idSocio = $("#txtIdSocio").val();
    var nombreSocio = $("#txtnombreSocio").val();
    var telefonoSocio = $("#txtTelefonoSocio").val();
    var cargoSocio = $("#txtCargoSocio").val();
    var aporteSocio = $("#txtAporteSocio").val();
    var porcentajeSocio = $("#txtPorcentajeSocio").val();

    var boton = new BtnLoad("btnGuardarSocio");
    boton.Start();
    htclibjs.Ajax({
        url: "/Socio/agregarSocioEmprendimiento",
        data: JSON.stringify({
            idEmprendimiento: idEmprendimiento, nombre: nombreSocio, telefono: telefonoSocio, cargo: cargoSocio, aporte: aporteSocio, porcentaje: porcentajeSocio
        }),
        success: function (r) {

            alert("Socio guardado");
            consultarSociosEmprendimiento();
            limpiarCampos();
            boton.Stop();
        }
    });

    
}

function editarSocio() {
           
    var idSocio = $("#txtIdSocio").val();
    var nombreSocio = $("#txtnombreSocio").val();
    var telefonoSocio = $("#txtTelefonoSocio").val();
    var cargoSocio = $("#txtCargoSocio").val();
    var aporteSocio = $("#txtAporteSocio").val();
    var porcentajeSocio = $("#txtPorcentajeSocio").val();

    var boton = new BtnLoad("btnGuardarSocio");
    boton.Start();
    htclibjs.Ajax({
        url: "/Socio/editarSocioEmprendimiento",
        data: JSON.stringify({
            idSocio: idSocio, nombre: nombreSocio, telefono: telefonoSocio, cargo: cargoSocio, aporte: aporteSocio, porcentaje: porcentajeSocio
        }),
        success: function (r) {

            alert("Socio guardado");
            consultarSociosEmprendimiento();
            $('#modalSocio').modal('hide');
            limpiarCampos();
            boton.Stop();
        }
    });

    
}

function consultarSociosEmprendimiento() {

    console.log("Ingreso consultar Socios");
    var idEmprendimiento = $("#txtIdEmprendimiento").val();
    htclibjs.Ajax({
        url: "/Socio/consultarSociosEmprendimiento",
        data: JSON.stringify({
            idEmprendimiento: idEmprendimiento
        }),
        success: function (r) {

            var trHTML =
                '<thead>' +
                '<tr>' +
                '<th>Nombre</th>' +
                '<th>Telefono</th>' +
                '<th>Cargo/Rol</th>' +
                '<th>Aporte $</th>' +
                '<th>Participación []%</th>' +
                '<th></th>' + //fila vacia donde va el boton de editar
                '</tr > ' +
                '</thead> ' +

                '<tbody> '

                ;

            $.each(r.data, function (i, item) {

                trHTML +=

                    '<tr>' +
                    '<td id="tdSocioNombre' + item.id + '" style="text-align: right">' + item.nombre + '</td>' +
                    '<td id="tdSocioTelefono' + item.id + '" >' + item.telefono + '</td>' +
                    '<td id="tdSocioCargo' + item.id + '">' + item.cargo + '</td>' +
                    '<td id="tdSocioAporte' + item.id + '" style="text-align: right">' + item.aporte + '</td>' +
                    '<td id="tdSocioPorcentaje' + item.id + '" style="text-align: right">' + item.porcentaje + '</td>' +
                    '<td id="tdIdSocio' + item.id + '"> <button class="btnSeleccionarSocio btn btn-primary" value="' + item.id + '" type="button" data-toggle="modal" data-target="#modalSocio" >Editar</button> </td>' + //boton editar de la tabla
                    '</tr> ';


            });

            trHTML +=
                '</tbody>';

            $('#tblSocios').html('');
            $('#tblSocios').append(trHTML);

            //accion al hacer clic en boton de la tabla
            $('.btnSeleccionarSocio').on('click', function (e) {

                selecionarSocio($(this));


            });
        }
    });

}

function selecionarSocio(boton) {

    console.log("Intento de seleccionar socio");
    var idSocio = boton.val();
    var nombre = $('#tdSocioNombre' + boton.val()).html();
    var telefono = $('#tdSocioTelefono' + boton.val()).html();
    var cargo = $('#tdSocioCargo' + boton.val()).html();
    var aporte = $('#tdSocioAporte' + boton.val()).html();
    var porcentaje = $('#tdSocioPorcentaje' + boton.val()).html();

    $("#txtIdSocio").val(idSocio);
    $("#txtnombreSocio").val(nombre);
    $("#txtTelefonoSocio").val(telefono);
    $("#txtCargoSocio").val(cargo);
    $("#txtAporteSocio").val(aporte);
    $("#txtPorcentajeSocio").val(porcentaje);
    

}

function limpiarCampos() {

    $("#txtIdSocio").val(0);
    $("#txtnombreSocio").val('');
    $("#txtTelefonoSocio").val('');
    $("#txtCargoSocio").val('');
    $("#txtAporteSocio").val('');
    $("#txtPorcentajeSocio").val('');

}