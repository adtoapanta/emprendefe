﻿$(document).ready(function () { initMatrizInversion(); });

$(window).bind('beforeunload', function () { guardarResumenInversion(); });

var editor;

var totalTotal = 0;
var totalAportePropio = 0;
var totalAporteInversion = 0;

function initMatrizInversion() {
    console.log('init Matriz inversion ')

    $('#btnGuardarItemInversion').click(function (e) {
        guardarOEditar();
    });
    $('#borrarItemInversion').click(function (e) {
        eliminarItemInversion();
    });
    $('#btnTerminarMatrizInversion').click(function (e) {
        terminarRequisito();
    });

    $('#slsTipoItem').change(function (e) {
        var idTipo = $('#slsTipoItem').val();
        if (idTipo == 1) {
            $('.noPosee').show();

        }
        else {
            $('.noPosee').hide();
            $("#txtPrioridad").val('');
            $("#txtJustificacion").val('');
        }
    });

    $('#modalItemInversion').on('hidden.bs.modal', function () {
        limpiarCampos();
    });

    consultarMatrizInversion();
    consultarResumenInversion();
}

function consultarResumenInversion() {

    var idPostulacion = $("#txtIdPostulacion").val();

    htclibjs.Ajax({
        url: "/Postulacion/consultarResumenInversion",
        data: JSON.stringify({
            idPostulacion: idPostulacion
        }),
        success: function (r) {

            $("#txtResumenInversion").val(r.data);

        }
    });

}

function guardarResumenInversion() {

    var idPostulacion = $("#txtIdPostulacion").val();
    var resumen = $("#txtResumenInversion").val();

    htclibjs.Ajax({
        url: "/MatrizInversion/guardarResumenInversion",
        data: JSON.stringify({
            idPostulacion: idPostulacion, resumen: resumen, totalInversion: totalAporteInversion
        }),
        success: function (r) {


        }
    });

}

function terminarRequisito() {

    var idRequisito = $('#txtIdRequisito').val();

    location.href = SITEROOT + '/RequisitosPostulante/completarRequisito/' + idRequisito;



}

function guardarOEditar() {

    var idItem = $("#txtIdItem").val();
    if (idItem == 0)
        guardarItemInversion();
    else
        editarItemInversion();

}

function guardarItemInversion() {

    var tipoItem = $("#slsTipoItem").val();

    console.log("Ingreso a la funcion Agregar Pago");
    var idPostulacion = $("#txtIdPostulacion").val();
    var prioridad = $("#txtPrioridad").val();
    var recurso = $("#txtRecurso").val();
    var justificacion = $("#txtJustificacion").val();
    var precioUnitario = $("#txtPrecioUnitario").val();
    var cantidad = $("#txtCantidad").val();
    var total = precioUnitario * cantidad;
    var idTipoInversion = $("#slsTipoInversion").val();

    if (tipoItem == 0) {
        var aportePropio = total;
        var aporteInversion = 0;
    }
    else {
        var aportePropio = 0
        var aporteInversion = total;
    }

    htclibjs.Ajax({
        url: "/MatrizInversion/agregarItemInversion",
        data: JSON.stringify({
            idPostulacion: idPostulacion, prioridad: prioridad, recurso: recurso, justificacion: justificacion, precioUnitario: precioUnitario, cantidad: cantidad, total: total, aportePropio: aportePropio, aporteInversion: aporteInversion, idTipoInversion: idTipoInversion
        }),
        success: function (r) {

            alert("Item Guardado");
            consultarMatrizInversion();
            limpiarCampos();
        }
    });

    
}

function editarItemInversion() {

    var tipoItem = $("#slsTipoItem").val();

    
    var idItem = $("#txtIdItem").val();    
    var prioridad = $("#txtPrioridad").val();
    var recurso = $("#txtRecurso").val();
    var justificacion = $("#txtJustificacion").val();
    var precioUnitario = $("#txtPrecioUnitario").val();
    var cantidad = $("#txtCantidad").val();
    var total = precioUnitario * cantidad;
    var idTipoInversion = $("#slsTipoInversion").val();

    if (tipoItem == 0) {
        var aportePropio = total;
        var aporteInversion = 0;
    }
    else {
        var aportePropio = 0
        var aporteInversion = total;
    }

    htclibjs.Ajax({
        url: "/MatrizInversion/editarItemInversion",
        data: JSON.stringify({
            idItem: idItem, prioridad: prioridad, recurso: recurso, justificacion: justificacion, precioUnitario: precioUnitario, cantidad: cantidad, total: total, aportePropio: aportePropio, aporteInversion: aporteInversion, idTipoInversion: idTipoInversion
        }),
        success: function (r) {

            alert("Item Guardado");
            consultarMatrizInversion();
            limpiarCampos();
        }
    });


}

function eliminarItemInversion() {

    var idItem = $("#txtIdItem").val();

    if (idItem == 0) {

        alert('Seleccione un item antes de eliminar');

    }
    else {
        htclibjs.Ajax({
            url: "/MatrizInversion/eliminarItemInversion",
            data: JSON.stringify({
                idItemInversion: idItem
            }),
            success: function (r) {

                alert("Item Eliminado");
                consultarMatrizInversion();
                limpiarCampos();
            }
        });
    }
}

function consultarMatrizInversion() {

    console.log("Ingreso consultar Matriz Inversion");
    var idPostulacion = $("#txtIdPostulacion").val();
    htclibjs.Ajax({
        url: "/MatrizInversion/consultarMatrizInversion",
        data: JSON.stringify({
            idPostulacion: idPostulacion
        }),
        success: function (r) {
            totalTotal = 0;
            totalAportePropio = 0;
            totalAporteInversion = 0;

            var trHTML =
                '<thead>' +
                '<tr>' +
                '<th>Prio.</th>' +
                '<th>Recurso</th>' +
                '<th>Justificación</th>' +
                '<th>Precio Unitario</th>' +
                '<th>Cant.</th>' +
                '<th>Total</th>' +
                '<th>Aporte Propio</th>' +
                '<th>Aporte Solicitado</th>' +
                '<th></th>' + //fila vacia donde va el boton de editar
                '</tr > ' +
                '</thead> ' +

                '<tbody> '

                ;

            $.each(r.data, function (i, item) {

                trHTML +=

                    '<tr>' +
                    '<td id="tdItemPrioridad' + item.id + '" style="text-align: right">' + item.prioridad + '</td>' +
                    '<td id="tdItemRecurso' + item.id + '" >' + item.recurso + '</td>' +
                    '<td id="tdItemJustificacion' + item.id + '">' + item.justificacion + '</td>' +
                    '<td id="tdItemPrecioUnitario' + item.id + '" style="text-align: right">$' + item.precioUnitario + '</td>' +
                    '<td id="tdItemCantidad' + item.id + '" style="text-align: right">' + item.cantidad + '</td>' +
                    '<td id="tdItemTotal' + item.id + '" style="text-align: right">$' + item.total + '</td>' +
                    '<td id="tdItemAportePropio' + item.id + '" style="text-align: right">$' + item.aportePropio + '</td>' +
                    '<td id="tdItemAporteInversion' + item.id + '" style="text-align: right">$' + item.aporteInversion + '</td>' +
                    '<td id="tdIdItem' + item.id + '"> <button class="btnSeleccionarItemInversion btn btn-primary" value="' + item.id + '" type="button" data-toggle="modal" data-target="#modalItemInversion" >Editar</button> </td>' + //boton editar de la tabla
                    '</tr> ';
                totalTotal += item.total;
                totalAportePropio += item.aportePropio;
                totalAporteInversion += item.aporteInversion;

            });

            trHTML +=
                '</tbody>' +
                '<tfoot>' +
                '<tr>' +
                '<th>Total</th>' +
                '<th></th>' +
                '<th></th>' +
                '<th></th>' +
                '<th></th>' +
                '<th style="text-align: right" >$' + addCommas(totalTotal.toFixed(2)) + '</th>' +
                '<th style="text-align: right" >$' + addCommas(totalAportePropio.toFixed(2)) + '</th>' +
                '<th style="text-align: right" >$' + addCommas(totalAporteInversion.toFixed(2)) + '</th>' +
                '</tr>' +
                '</tfoot> ';

            $('#tblMatrizInversion').html('');
            $('#tblMatrizInversion').append(trHTML);

            //accion al hacer clic en boton de la tabla
            $('.btnSeleccionarItemInversion').on('click', function (e) {

                selecionarItemInversion($(this));

            });
        }
    });

}

function selecionarItemInversion(boton) {


    var idItem = boton.val();

    htclibjs.Ajax({
        url: "/MatrizInversion/consultarItemInversion",
        data: JSON.stringify({
            idItemInversion: idItem
        }),
        success: function (r) {

            var item = r.data;
            $("#txtPrioridad").val(item.prioridad);
            $("#txtRecurso").val(item.recurso);
            $("#txtJustificacion").val(item.justificacion);
            $("#txtPrecioUnitario").val(item.precioUnitario);
            $("#txtCantidad").val(item.cantidad);
            $("#txtTotal").val(item.total);
            $("#txtAportePropio").val(item.aportePropio);
            $("#txtAporteInversion").val(item.aporteInversion);
            $("#txtIdItem").val(item.id);
            if (item.aporteInversion == 0) {

                $('#slsTipoItem').val(0);

            }
            else
                $('#slsTipoItem').val(1);
        }
    });



}

function limpiarCampos() {

    $("#txtPrioridad").val('0');
    $("#txtRecurso").val('');
    $("#txtJustificacion").val('');
    $("#txtPrecioUnitario").val('');
    $("#txtCantidad").val('');
    $("#txtTotal").val('');
    $("#txtValor").val('');
    $("#txtAporteInversion").val('');
    $("#txtIdItem").val(0);

}