﻿$(document).ready(function () { documentos.init(); });

var documentos = {
    idPostulante: 0,
    cargados: [],
    archivoSubir: {},
    btnSubirArchivo: new BtnLoad('btnSubirArchivo'),
    btnTerminarDocumentos: new BtnLoad('btnTerminarDocumentos'),
    init: function () {
        var self = this;
        self.idPostulante = $("#txtIdPostulante").val();
        self.getDocumentosCargados();
        $("#btnTerminarDocumentos").hide();
        $('#fleArchivo').on('change', function (e) {
            var files = $(this).prop("files");
            var names = $.map(files, function (val) { return val.name; });

            $.each(names, function (index, archivo) {
                $("#lblListaArchivo").html(archivo);
            });

            self.archivoSubir = e.target.files[0];
        })

        $("#btnSubirArchivo").click(function () {
            var idA = $("#mdlUploadArchivo").data("id");
            var desc = $("#mdlUploadArchivo").data("descripcion");
            self.subirArchivo(idA,desc);
        });

        $("#btnTerminarDocumentos").click(function () {
            self.terminarEtapa();
        });

        $(document).on('click', '.btnUpload', function (e) {
            e.preventDefault();
            var id = $(this).data("id");
            var desc = $(this).data("descripcion");
            $('#mdlUploadArchivo').data("id", id);
            $('#mdlUploadArchivo').data("descripcion", desc);
            $('#mdlUploadArchivo').modal('show')
        });

        $(document).on('click', '.btnDownload', function (e) {
            e.preventDefault();
            var idPbl = $(this).data("id");
            if (idPbl !== '') {
                window.open(SITEROOT+'/Convocatoria/Descarga/'+idPbl, '_blank'); 
            } else {
                toastr.warning('No existe archivo a descargar', '', { positionClass: "toast-bottom-right" });
            }
        });
    },
    cleanModal: function () {
        $("#txtObservacionArchivo").val("");
        $("#fleArchivo").val("");
        $("#lblListaArchivo").html("Escoja el archivo");
    },
    subirArchivo: function (idA,des) {
        var self = this;

        if (self.archivoSubir == null){
            toastr.error('No ha subido archivo solicitado', '', { positionClass: "toast-bottom-right" });
            return false;
        }
        self.btnSubirArchivo.Start();
        var data = new FormData();
        var desc = $("#txtObservacionArchivo").val();

        data.append("idAC", idA)
        data.append("descripcionArchivo", des);
        data.append("idP", self.idPostulante);
        data.append("descripcion", desc);
        data.append("archivo", self.archivoSubir);

        $.ajax({
            url: SITEROOT + "/Postulacion/SubirArchivoDocumentos",
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (r, textStatus, jqXHR) {
                if (typeof r.error === 'undefined') {
                    // Success so call function to process the form
                    if (r.exitoso) {
                        self.getDocumentosCargados();
                        toastr.info('Guardado', '', { positionClass: "toast-bottom-right" });
                        $('#mdlUploadArchivo').modal('hide');
                    } else {
                        toastr.error(r.data, 'Operación incompleta', { positionClass: "toast-bottom-right" });
                    }
                }
                else {
                    // Handle errors here
                    toastr.error(r.data, 'Error', { positionClass: "toast-bottom-right" });
                }
                $('#mdlUploadArchivo').modal('hide');
                self.btnSubirArchivo.Stop();
                self.archivoSubir = null;
                self.cleanModal();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Handle errors here
                toastr.error(textStatus, 'Error', { positionClass: "toast-bottom-right" });
                $('#mdlUploadArchivo').modal('hide');
                self.btnSubirArchivo.Stop();
                self.archivoSubir = null;
                self.cleanModal();
            }
        });
    },

    getDocumentosCargados: function () {
        var self = this;
        htclibjs.Ajax({
            url: "/Convocatoria/GetDocumentosCargados",
            data: JSON.stringify({idP: self.idPostulante}),
            success: function (r) {
                self.cargados = r.data;
                self.showDocumentosCargados();
            }
        });
    },
    showDocumentosCargados: function () {
        var self = this;
        //Destruimos antes de acrgar nuevos
        if ($.fn.DataTable.isDataTable('#tblDocumentos')) {
            $('#tblDocumentos').DataTable().destroy();
        }
        $('#tblDocumentos tbody').empty();

        var count = self.cargados.filter(x => x.estaEntregado === false);
        if (count.length == 0) $("#btnTerminarDocumentos").show();

        $("#tblDocumentos").DataTable({
            data: self.cargados,
            aaSorting: [],
            bDestroy: true,
            bFilter: true,
            bPaginate: false,
            bLengthChange: true,
            language: SPANISHDATATABLE,
            aoColumns: [
                {
                    width: '30%', mData: 'id', title: 'Documento',
                    mRender: function (data, type, full) {
                        return full.ARC_Documentos.nombre;
                    }
                },
                {
                    width: '40%', mData: 'observaciones', title: 'Descripción',
                    mRender: function (data, type, full) {
                        return data;
                    }
                },
                {
                    width: '10%', mData: 'estaEntregado', title: 'Entregado', className: "text-center",
                    mRender: function (data, type, full) {
                        
                        var ht = '<i id="icn' + full.id + '" class="' + (data ? "fas fa-check" : "fas fa-times") + '" style="font-size:25px; color:' + (data ? "green" : "red") + ';"></i>';
                        return ht;
                    }
                },
                {
                    width: '10%', mData: 'id', title: 'Cargar', className: "text-center",
                    mRender: function (data, type, full) {
                        var ht = '<button type="button" class="btn btn-success btn-sm btnUpload" data-id="' + data + '" data-descripcion="' + full.ARC_Documentos.nombre + ' ' + (full.observaciones == null ? '' : full.observaciones) + '"><i class="fas fa-cloud-upload-alt"></i></button>';
                        return ht;
                    }
                },
                {
                    width: '10%', mData: 'id', title: 'Descargar', className: "text-center",
                    mRender: function (data, type, full) {
                        var publ = "";
                        if (full.ARC_Archivos != null) 
                            publ = full.ARC_Archivos.idPublico;
                        
                        return publ=="" ? "" :'<button type= "button" class="btn btn-primary btn-sm btnDownload" data-id="'+publ+'"><i class="fas fa-cloud-download-alt"></i></button>';
                    }
                },
                
            ],
        });
    },
    terminarEtapa: function () {
        var self = this;
        var cumple = $("#slcCumpleCapacitacion").val() == "1";
        var proveedor = $("#slcProveedorCapacitacion").val();

        if (proveedor == "0") {
            toastr.error('No ha seleccionado un proveedor', '', { positionClass: "toast-bottom-right" });
            return false;
        }

        if (confirm("¿Seguro de terminar?")) {
            console.log(cumple);
            console.log(proveedor);
            self.btnTerminarDocumentos.Start();
            htclibjs.Ajax({
                url: "/Convocatoria/CerrarEtapaDocumentosPostulante",
                data: JSON.stringify({ 'idP': self.idPostulante, 'p': proveedor, 'c': cumple}),
                success: function (r) {
                    self.btnTerminarDocumentos.Stop();
                    window.location.href = SITEROOT + "/Usuario/Dashboard/";
                },
                successError: function () {
                    self.btnTerminarDocumentos.Stop();
                },
                error: function (e) {
                    self.btnTerminarDocumentos.Stop();
                },
            });

        }
    },
};