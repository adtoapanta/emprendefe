﻿/// <reference path="../RequisitosPostulante/tablaRequisitos.js" />

$(document).ready(function () { initDatos(); });

function initDatos() {
    console.log('init ingreso datos')

    $('#btnEnviarDatos').click(function (e) {
        terminarEtapa();
    });

    consultarRequisitosPostulacion(); //está en el js tablaRequisitos.js
}

function terminarEtapa() {
    console.log('boton clic');
    var idPostulacion = $('#txtIdPostulacion').val();
    var btnEnviarDatos = new BtnLoad("btnEnviarDatos");

    btnEnviarDatos.Start();
    htclibjs.Ajax({
        url: "/Postulacion/terminarEtapaPostulacion",
        data: JSON.stringify({
            idPostulacion: idPostulacion
        }),
        success: function (r) {
            var url = SITEROOT + '/Persona/Dashboard'
            window.location.href = url;
            btnEnviarDatos.Stop();
        },
        successError: function () {
            btnEnviarDatos.Stop();
        },
        error: function () {
            btnEnviarDatos.Stop();
        }
    });
}

