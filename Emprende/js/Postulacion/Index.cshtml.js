﻿/// <reference path="../RequisitosPostulante/tablaRequisitos.js" />

$(document).ready(function () { init(); });

function init() {
    console.log('init');
    consultarRequisitosPostulacion(); //está en el js tablaRequisitos.js

    $("#btnMedioAprobacion").click(function (e) {
        e.preventDefault();

        let idPostulacion = $('#txtIdPostulacion').val();
        
        //location.href = SITEROOT + "/Postulacion/DescargarMedioDeAprobacion?idPostulacion=" + idPostulacion;
        var win = window.open(SITEROOT + '/General/Descarga/101/' + idPostulacion, '_blank');
        if (win) {
            win.focus();
        } else {
            toastr.warning('Permita abrir tabs para este sitio', '', { positionClass: "toast-bottom-right" });
        }
    });

    $("#btnContrato").click(function (e) {
        e.preventDefault();

        let idPostulacion = $('#txtIdPostulacion').val();

        location.href = SITEROOT + "/Postulacion/DescargarContrato?idPostulacion=" + idPostulacion;
    });

    $("#btnDiagnostico").click(function (e) {
        e.preventDefault();

        let idPostulacion = $('#txtIdPostulacion').val();

        location.href = SITEROOT + "/Postulacion/DescargarDiagnosticoEmprendedor?idPostulacion=" + idPostulacion;
    });

    $("#btnBrief").click(function (e) {
        e.preventDefault();

        let idPostulacion = $('#txtIdPostulacion').val();

        location.href = SITEROOT + "/Postulacion/DescargarBriefEmprendedor?idPostulacion=" + idPostulacion;
    });

    $("#btnComiteEvaluacion").click(function (e) {
        e.preventDefault();

        console.log('btnComiteEvaluacion');

        let idComitePostulacion = $('#txtIdComitePostulacion').val();

        location.href = SITEROOT + "/Postulacion/DescargarComitePostulacion?idComitePostulacion=" + idComitePostulacion;
    });

    getIndicadores();
    getAllArchivos();
}

function getAllArchivos() {
    var idPostulacion = $('#txtIdPostulacion').val();
    htclibjs.Ajax({
        url: "/Postulacion/GetAllArchivos",
        data: JSON.stringify({ idP: idPostulacion }),
        success: function (r) {
            var archivos = r.data;
            llenarTablaArchivos(archivos);
        }
    });
}

function llenarTablaArchivos(archivos) {
    var html = "";
    var contador = 1;
    var plantilla = '<div class="col-3"><div class="card" style= "width: 18rem; min-height:160px;" ><div class="card-body"><h5 class="card-title">__titulo__</h5><p class="card-text">__descripcion__</p><a href="__link__" target="_blank" class="btn btn-primary"><i class="fa fa-file-download"></i> Descargar</a></div></div></div>';
    html = '<div class="row top-space30">';
    $.each(archivos, function (index, archivo) {
        html += plantilla.replace('__titulo__', archivo.nombre).replace('__descripcion__', archivo.descripcion).replace('__link__', SITEROOT +'/General/Descarga/'+ archivo.idPublico);
        if ((contador % 4) == 0) {
            html += '</div>';
            html += '<div class="row top-space30">';
        }
        contador += 1;
    });
    html += '</div>';
    $("#dvDocumentos").append(html);
}

function getIndicadores() {

    console.log('getIndicadores');

    var idPostulacion = $('#txtIdPostulacion').val();

    console.log('idPostulacion');
    console.log(idPostulacion);

    htclibjs.Ajax({
        url: "/Postulacion/getIndicadores",
        data: JSON.stringify({ idPostulacion: idPostulacion }),
        success: function (r) {
            _indicadores = r.data;
            console.log('_indicadores');
            console.log(_indicadores);

            llenaTablaIndicadores();
        }
    });
}
function llenaTablaIndicadores() {

    //todo poner formato en moneda o decimales

    $("#tblIndicadores").DataTable({
        data: _indicadores,
        "dom": 'Brt',
        buttons: [ 'copy', 'excel' ],
        aoColumns: [
            {
                mData: 'fecha', title: 'Fecha', mRender: function (data, type, full) {
                    var f = new Date(data);
                    return getOutDate(f);
                }
            },
            {
                mData: 'numeroTrabajadores', title: 'Numero de trabajadores', mRender: function (data, type, full) {
                    return data;
                }
            },
            {
                mData: 'ventasDolares', title: 'Ventas en dolares', mRender: function (data, type, full) {
                    return data;
                }
            },
            {
                mData: 'ventasVolumen', title: 'Volumen de ventas', mRender: function (data, type, full) {
                    return data;
                }
            },
            {
                mData: 'costoVariable', title: 'Costo variable', mRender: function (data, type, full) {
                    return data;
                }
            },
            {
                mData: 'costoFijo', title: 'Costo fijo', mRender: function (data, type, full) {
                    return data;
                }
            },
            {
                mData: 'utilidadOperativa', title: 'Utilidad operativa', mRender: function (data, type, full) {
                    return data;
                }
            },
            {
                mData: 'margenContribucion', title: 'Margen de contribucion', mRender: function (data, type, full) {
                    return data;
                }
            },
            {
                mData: 'utilidadNeta', title: 'Utilidad Neta', mRender: function (data, type, full) {
                    return data;
                }
            },
            {
                mData: 'capitalTrabajo', title: 'Capital de trabajo', mRender: function (data, type, full) {
                    return data;
                }
            }
        ],
    });
}