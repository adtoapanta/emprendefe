﻿$(document).ready(function () { postulaciones.inicio(); });

var postulaciones = {
    postulaciones: {},
    inicio: function () {
        var self = this;
        self.postulaciones = $("#txtPostulaciones").val();
        self.showPostulaciones();
        $("#slcEtapaPostulacion").change(function () {
            self.filtrarPostulaciones();
        });

    },

    filtrarPostulaciones: function () {
        var self = this;
        var idEtapa = $("#slcEtapaPostulacion").val();
        var idConvocatoria = $("#txtIdConvocatoria").val();
        htclibjs.Ajax({
            url: "/Postulacion/FiltrarPostulaciones",
            data: JSON.stringify({idC:idConvocatoria,idE:idEtapa}),
            success: function (r) {
                self.postulaciones = r.data;
                self.showPostulaciones();
            }
        });
    },

    showPostulaciones: function () {
        var self = this;
        //Destruimos antes de acrgar nuevos
        if ($.fn.DataTable.isDataTable('#tblPostulaciones')) {
            $('#tblPostulaciones').DataTable().destroy();
        }
        $('#tblPostulaciones tbody').empty();

        console.log(self.postulaciones);
        $("#tblPostulaciones").DataTable({
            data: jQuery.parseJSON(self.postulaciones),
            //aaSorting: [],
            bDestroy: true,
            bFilter: true,
            bPaginate: true,
            bLengthChange: true,
            aoColumns: [
                {
                    width: '25%', mData: 'emprendimiento', title: 'Emprendimiento',
                    mRender: function (data, type, full) {
                        return data;
                    }
                },
                {
                    width: '25%', mData: 'emprendedor', title: 'Emprendedor',
                    mRender: function (data, type, full) {
                        return data;
                    }
                },
                {
                    width: '25%', mData: 'idEtapa', title: 'Etapa',
                    mRender: function (data, type, full) {
                        return data;
                    }
                },
                {
                    width: '25%', mData: 'fecha', title: 'Fecha',
                    mRender: function (data, type, full) {
                        var f = new Date(data);
                        return getOutDate(f);
                    }
                },
            ],
        });
    }
}