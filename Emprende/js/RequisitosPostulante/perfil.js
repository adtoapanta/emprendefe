﻿$(document).ready(function () {

    var idPost = $('#txtIdPostulacion').val();
    console.log(idPost);
    //Scoring.MostrarCuestionario(1, idPost, 2, "#divQ2", false);
    Scoring.MostrarTabla(2, idPost, 2, "#divQ2", false);
});


function guardarPerfil() {

    var btnGuardarPerfil = new BtnLoad("btnGuardarPerfil");
    btnGuardarPerfil.Start();

    Scoring.establa = true;

    if (Scoring.Validar() === true) {
        Scoring.GuardarExt(function () {
            var idRequisito = $('#txtIdRequisito').val();
            location.href = SITEROOT + '/RequisitosPostulante/completarRequisito/' + idRequisito;
        });
    } else {
        btnGuardarPerfil.Stop();
    }
}