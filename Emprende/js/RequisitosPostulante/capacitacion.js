﻿$(document).ready(function () {
    capacitacion.iniciar();
});


var capacitacion = {
    idPostulacion: 0,
    archivosSubir: {},
    btnTerminar: new BtnLoad("btnTerminar"),
    btnSubirArchivos: new BtnLoad("btnSubirArchivos"),
    iniciar: function () {
        var self = this;

        self.idPostulacion = $("#txtIdPostulacion").val();
        self.cargarArchivos();

        $('#btnTerminar').hide();
        $('#btnTerminar').click(function () {
            self.btnTerminar.Start();
            var idPostulacion = $('#txtIdPostulacion').val();
            var idRequisito = $('#txtIdRequisito').val();
            location.href = SITEROOT + '/RequisitosPostulante/completarRequisito/' + idRequisito;
            self.btnTerminar.Stop();
        }); 

        $('#fileArchivosCapacitacion').on('change', function (e) {
            var ht = '';
            var files = $(this).prop("files");
            var names = $.map(files, function (val) { return val.name; });

            self.archivosSubir.files = e.target.files;

            ht = '<ul style="list-style-type: none;">';
            $.each(names, function (index, archivo) {
                ht += '<li><i class="far fa-file"></i> ' + archivo + '</li>';
            });
            ht += '</ul>';
            $("#dvListaArchivos").html(ht);
        })

        $('#btnSubirArchivos').click(function () {
            self.subirArchivos();
        });
        $('#dvInfoEmpty').hide();

        $(document).on('click', '.btnDownload', function (e) {
            e.preventDefault();
            var id = $(this).data("id");
            self.download(id);
        });
        $(document).on('click', '.btnDelete', function (e) {
            e.preventDefault();
            var id = $(this).data("id");
            self.delete(id);
        });
    },

    download: function (idP) {
        var win = window.open(SITEROOT +'/Convocatoria/Descarga/'+idP, '_blank');
        if (win) {
            //Browser has allowed it to be opened
            win.focus();
        } else {
            //Browser has blocked it
            alert('Habilite los popups para este sitio.');
        }
    },
    delete: function (idP) {
        var self = this;
        htclibjs.Ajax({
            url: "/RequisitosPostulante/DeleteArchivosCapacitacion",
            data: JSON.stringify({ idp: idP }),
            success: function (r) {
                self.cargarArchivos();
            }
        });
    },
    subirArchivos: function () {
        var self = this;
        var data = new FormData();
        var c = 1;
        var obs = $("#txtObservacionesArchivos").val();
        $.each(self.archivosSubir.files, function (key, value) {
            data.append("archivo" + c, value);
            c += 1;
        });

        data.append("idP", self.idPostulacion);
        data.append("observaciones", obs);

        self.btnSubirArchivos.Start();
        $.ajax({
            url: SITEROOT + "/RequisitosPostulante/GuardarArchivos",
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (r, textStatus, jqXHR) {
                if (typeof r.error === 'undefined') {
                    // Success so call function to process the form
                    if (r.exitoso) {
                        self.cargarArchivos();
                        toastr.info('Archivos guardados', '', { positionClass: "toast-bottom-right" });
                    } else {
                        toastr.error(r.data, 'Operación incompleta', { positionClass: "toast-bottom-right" });
                    }
                }
                else {
                    // Handle errors here
                    toastr.error(r.data, 'Error', { positionClass: "toast-bottom-right" });
                }
                $('#mdlSubirArchivos').modal('hide');
                $("#dvListaArchivos").html('');
                self.btnSubirArchivos.Stop();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Handle errors here
                toastr.error(textStatus, 'Error', { positionClass: "toast-bottom-right" });
                $('#mdlSubirArchivos').modal('hide');
                $("#dvListaArchivos").html('');
                self.btnSubirArchivos.Stop();
            }
        });
    },
    cargarArchivos: function () {
        var self = this;
        htclibjs.Ajax({
            url: "/RequisitosPostulante/GetArchivosCapacitacion",
            data: JSON.stringify({ idp: self.idPostulacion }),
            success: function (r) {
                console.log(r.data);
                self.showArchivos(r.data);
            }
        });

    },
    showArchivos: function (archivos) {
        //

        //Destruimos antes de acrgar nuevos
        if ($.fn.DataTable.isDataTable('#tblArchivosCapacitacion')) {
            $('#tblArchivosCapacitacion').DataTable().destroy();
        }
        $('#tblArchivosCapacitacion tbody').empty();


        if (archivos.length > 0) {
            $('#dvInfoEmpty').hide();
            $('#btnTerminar').show();

            var plantillaDown = '<button type="button" class="btn btn-info btnDownload" data-id="__id__" ><i class="fa fa-download"></i></button>';
            var plantillaDel = '<button type="button" class="btn btn-danger btnDelete" data-id="__id__" ><i class="fas fa-trash"></i></button>';

            $("#tblArchivosCapacitacion").DataTable({
                data: archivos,
                aaSorting: [],
                bDestroy: true,
                bFilter: false,
                bPaginate: false,
                bLengthChange: true,
                language: SPANISHDATATABLE,
                aoColumns: [
                    {
                        width: '50%', mData: 'nombreArchivo', title: 'Archivo',
                        mRender: function (data, type, full) {
                            return data;
                        }
                    },
                    {
                        width: '25%', mData: 'idPublico', title: 'Descargar', className: "text-center",
                        mRender: function (data, type, full) {
                            return plantillaDown.replace('__id__',data);
                        }
                    },
                    {
                        width: '25%', mData: 'id', title: 'Borrar', className: "text-center",
                        mRender: function (data, type, full) {
                            return plantillaDel.replace('__id__', data);
                        }
                    },
                ],
            });
        } else {
            $('#dvInfoEmpty').show();
        }
        
    },

    
};