﻿var contador = 0;
function consultarRequisitosPostulacion() {
    var idPostulacion = $('#txtIdPostulacion').val();
    htclibjs.Ajax({
        url: "/RequisitosPostulante/consultarRequisitosPostulante",
        data: JSON.stringify({
            idPostulacion: idPostulacion
        }),
        success: function (r) {

            console.log(idPostulacion);

            var data = r.data;
            var trHTML =
                '<thead>' +
                '<tr>' +
                '<th>Descripción</th>' +
                '<th>Estado</th>' +
                '</tr> ' +
                '</thead> ' +
                '<tbody>';

            $.each(data, function (i, item) {
                var url = SITEROOT + item.CON_requisitos.urlPath;
                url += idPostulacion + '/';
                url += item.id;
                trHTML +=
                    '<tr>' +
                    '<td id="tdNombreEmprendimiento' + item.id + '"><a href="' + url + '">' + item.CON_requisitos.nombre + '</a></td>' +
                    '<td id="tdDescripcionEmprendimiento' + item.id + '"  >' + getEstado(item.completo) + ' </td>' +
                    // boton de seleccion
                    //'<td class="text-center"> <button id="' + item.id + '" class="btn btn-primary btn-sm seleccionarEmprendimiento" data-toggle="modal" data-target="#modalPostular" ><i>Postular</i></button> </td>' +
                    '</tr> ';

            });
            trHTML += '</tbody>';

            $('#requisitosPostulacion').html('');
            $('#requisitosPostulacion').append(trHTML);


            if (contador < data.length) {
                $('#btnEnviarDatos').attr('disabled', true);
            }

        }
    });
}
function getEstado(estado) {
    var icon = '';
    if (estado) {
        icon = '<a class="fa fa-check" > Terminado </a>';
        contador++;
    }
    else
        icon = '<a class="fa fa-times" > Incompleto </a>';

    return icon;
}