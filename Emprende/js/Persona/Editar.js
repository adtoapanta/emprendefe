﻿var provincias = [];
var cedulaValida = false;
var persona;
var direccion;
var conyuge;
var btnGuardarPersona = new BtnLoad("btnGuardarPersona");

$(document).ready(function () { initEditarPersona(); });

function initEditarPersona() {
    console.log('init EditarPersona ')

    $('#btnGuardarPersona').click(function (e) {
        completarDatosGenerales();
    });

    $('#btnGuardarReferencia').click(function (e) {
        guardarOEditar();
    });

    $('#borrarReferencia').click(function (e) {
        eliminarReferencia();
    });


    consultarProvincias();

    $("#txtProvincia").change(function () {
        cargarCiudades();
    });

    $("#txtCiudad").change(function () {
        cargarParroquias();
    });


    $('#txtCedulaPersona').on('input', function (e) {
        var v = validarDocumentos();
        if (v == true) {
            toastr.success('', 'Documento valido', { positionClass: "toast-bottom-right" });
        }
    });

    $('#modalReferencia').on('hidden.bs.modal', function () {
        limpiarCampos();
    });

    consultarPaises();
    consultarReferenciasPersona();
    consultarPersonaCompleta();
}

function validarDocumentos() {
    var cedula = $("#txtCedulaPersona").val();
    var idTipoDocumento = $("#txtTipoDocumento").val();

    if (idTipoDocumento == 1)
        cedulaValida = validarCedula(cedula);
    else if (idTipoDocumento == 2)
        cedulaValida = validarRUC(cedula);
    else if (idTipoDocumento == 3)
        cedulaValida = validarPasaporte(cedula);

    return cedulaValida;
}

function consultarReferenciasPersona() {

    console.log("Ingreso consultar Referencia");

    htclibjs.Ajax({
        url: "/Referencia/consultarReferenciasPersona",
        data: JSON.stringify({

        }),
        success: function (r) {

            var trHTML =
                '<thead>' +
                '<tr>' +
                //'<th>Parentesco</th>' +
                '<th>Nombre</th>' +
                '<th>Teléfono</th>' +
                '<th>Dirección</th>' +
                '<th></th>' + //fila vacia donde va el boton de editar
                '</tr > ' +
                '</thead> ' +

                '<tbody> '

                ;

            $.each(r.data, function (i, item) {

                trHTML +=

                    '<tr>' +
                    //'<td>' + item.parentesco + '</td>' +
                    '<td>' + item.nombre + '</td>' +
                    '<td>' + item.telefono + '</td>' +
                    '<td>' + item.direccion + '</td>' +
                    '<td> <button class="btnSeleccionarReferencia btn btn-primary" value="' + item.id + '" type="button" data-toggle="modal" data-target="#modalReferencia">Editar</button> </td>' + //boton editar de la tabla
                    '</tr> ';

            });

            trHTML +=
                '</tbody>';

            $('#tblReferencias').html('');
            $('#tblReferencias').append(trHTML);

            //accion al hacer clic en boton de la tabla
            $('.btnSeleccionarReferencia').on('click', function (e) {

                selecionarReferencia($(this));


            });
        }
    });

}

function selecionarReferencia(boton) {

    var id = boton.val();

    htclibjs.Ajax({
        url: "/Referencia/consultarReferencia",
        data: JSON.stringify({
            id: id
        }),
        success: function (r) {

            $('#txtIdReferencia').val(r.data.id);
            $('#txtParentescoReferencia').val(r.data.parentesco);
            $('#txtNombreReferencia').val(r.data.nombre);
            $('#txtDireccionReferencia').val(r.data.direccion);
            $('#txtTelefonoReferencia').val(r.data.telefono);

        }
    });

}

function eliminarReferencia() {

    var id = $('#txtIdReferencia').val();

    if (id == 0) {

        toastr.error('', 'Primero debe seleccionar una referencia antes de eliminar', { positionClass: "toast-bottom-right" });

    }
    else {
        htclibjs.Ajax({
            url: "/Referencia/eliminarReferencia",
            data: JSON.stringify({
                id: id
            }),
            success: function (r) {

                toastr.success('', 'Referencia eliminada', { positionClass: "toast-bottom-right" });
                consultarReferenciasPersona();
            }
        });
    }
}

function guardarOEditar() {

    var idReferencia = $('#txtIdReferencia').val();


    if (idReferencia == 0)
        guardarReferencia();

    else
        editarReferencia();
}

function guardarReferencia() {

    var parentesco = $('#txtParentescoReferencia').val();
    var nombre = $('#txtNombreReferencia').val();
    var direccion = $('#txtDireccionReferencia').val();
    var telefono = $('#txtTelefonoReferencia').val();

    htclibjs.Ajax({
        url: "/Referencia/guardarReferenciaPersona",
        data: JSON.stringify({
            idParentesco: parentesco, nombre: nombre, direccion: direccion, telefono: telefono
        }),
        success: function (r) {

            toastr.success('', 'Referencia Guardada', { positionClass: "toast-bottom-right" });
            consultarReferenciasPersona();
            limpiarCampos();
        }
    });

}

function editarReferencia() {

    var idReferencia = $('#txtIdReferencia').val();
    var parentesco = $('#txtParentescoReferencia').val();
    var nombre = $('#txtNombreReferencia').val();
    var direccion = $('#txtDireccionReferencia').val();
    var telefono = $('#txtTelefonoReferencia').val();

    htclibjs.Ajax({
        url: "/Referencia/editarReferenciaPersona",
        data: JSON.stringify({
            idReferencia: idReferencia, idParentesco: parentesco, nombre: nombre, direccion: direccion, telefono: telefono
        }),
        success: function (r) {

            toastr.success('', 'Referencia Actualizada', { positionClass: "toast-bottom-right" });
            consultarReferenciasPersona();
            $('#modalReferencia').modal('hide');
            limpiarCampos();
        }
    });

}

function limpiarCampos() {

    $('#txtIdReferencia').val(0);

    $('#txtNombreReferencia').val('');
    $('#txtDireccionReferencia').val('');
    $('#txtTelefonoReferencia').val('');

}

function completarDatosGenerales() {

    //datos persona
    var nombre1 = $("#txtNombre1Persona").val();
    var nombre2 = $("#txtNombre2Persona").val();
    var apellido1 = $("#txtApellido1Persona").val();
    var apellido2 = $("#txtApellido2Persona").val();
    var nacionalidad = $("#txtNacionalidadPersona").val();
    var cedula = $("#txtCedulaPersona").val();
    var telefono = $("#txtTelefonoPersona").val();
    var celular = $("#txtCelularPersona").val();
    var genero = $("#txtGeneroPersona").val();
    var idTipoDocumento = $("#txtTipoDocumento").val();
    var fechaNacimiento = $("#txtFechaNacimientoPersona").val();
    var estadoCivil = $("#txtEstadoCivil").val();;
    var nivelEducacion = $("#txtNivelEducacion").val();;

    // datos direcion
    var idTipoLugar = $('#txtTipoLugar').val();
    var idRef = 0; //parametro se guarda el real en controlador
    var idTipo = 0; //parametro se guarda el real en controlador
    var linea1 = $('#txtCallePrincipal').val();
    var linea2 = $('#txtCalleSecundaria').val();
    var numero = $('#txtNumero').val();
    //var nombre = $('#txtNombre').val();
    var referencia = $('#txtReferencia').val();
    var barrio = $('#txtBarrio').val();
    var idParroquia = $('#txtParroquia').val();
    var direccion = new direccionPersona(idTipoLugar, idRef, idTipo, linea1, linea2, numero, "Hogar", referencia, barrio, idParroquia);
    //datos conyuge 


    if (persona.datosCompletos == true && (persona.idEstadoCivil == 2 || persona.idEstadoCivil == 6)) {
        var idConyuge = $("#txtIdConyuge").val();
        var nombre1Conyuge = $("#txtNombre1Conyuge").val();
        var nombre2Conyuge = $("#txtNombre2Conyuge").val();
        var apellido1Conyuge = $("#txtApellido1Conyuge").val();
        var apellido2Conyuge = $("#txtApellido2Conyuge").val();
        var nacionalidadConyuge = $("#txtNacionalidadConyuge").val();
        var cedulaConyuge = $("#txtCedulaConyuge").val();
        var fechaNacimientoConyuge = $("#txtFechaNacimientoConyuge").val();
    }
    else {
        var idConyuge = '0';
        var nombre1Conyuge = '';
        var nombre2Conyuge = '';
        var apellido1Conyuge = '';
        var apellido2Conyuge = '';
        var nacionalidadConyuge = 1;
        var cedulaConyuge = '';
        var fechaNacimientoConyuge = '01-01-1999';
    }

    var url = SITEROOT;

    var validoD = validarDocumentos();
    if (validoD == true) {
        btnGuardarPersona.Start();
        htclibjs.Ajax({
            url: "/Persona/completarDatosGenerales",
            data: JSON.stringify({
                cedula: cedula, genero: genero, nombre1: nombre1, nombre2: nombre2, apellido1: apellido1, apellido2: apellido2
                , telefono: telefono, celular: celular, nacionalidad: nacionalidad, estadoCivil: estadoCivil, direccion: direccion, idTipoDocumento: idTipoDocumento, fechaNacimiento: fechaNacimiento,
                idNivelEducacion: nivelEducacion,
                nombre1Conyuge: nombre1Conyuge, nombre2Conyuge: nombre2Conyuge, apellido1Conyuge: apellido1Conyuge, apellido2Conyuge: apellido2Conyuge, idNacionalidadConyuge: nacionalidadConyuge, fechaNacimientoConyuge: fechaNacimientoConyuge, documentoConyuge: cedulaConyuge,
                idConyuge: idConyuge
            }),
            success: function (r) {
                alert('Datos Actualizados');
                url += '/Persona/Dashboard';
                window.location.href = url;
                //btnGuardarPersona.Stop();
            },
            successError: function () {
                btnGuardarPersona.Stop();
            },
            error: function () {
                btnGuardarPersona.Stop();
            }
        });

    }
    else {

        toastr.error('', 'No de documento no válido', { positionClass: "toast-bottom-right" });

    }

}

function consultarProvincias() {

    htclibjs.Ajax({
        url: "/Provincia/consultarPronviciasInclude",
        data: JSON.stringify({

        }),
        success: function (r) {

            $.each(r.data, function (i, item) {

                provincias.push(item);

            });
            cargarProvincias();
        }
    });
}

function cargarProvincias() {

    provincias.sort(compare);
    $('#txtProvincia').append(new Option('seleccione', 0, true));
    for (var i = 0; i < provincias.length; i++) {

        $('#txtProvincia').append(new Option(provincias[i].nombre, provincias[i].id));

    }

}

function cargarCiudades() {

    var provinciaId = $('#txtProvincia').val();

    $("#txtCiudad").empty();
    $('#txtCiudad').append(new Option('seleccione', 0, true));

    for (var i = 0; i < provincias.length; i++) {

        if (provincias[i].id == provinciaId) {

            provincias[i].Ciudades.sort(compare);
            for (var j = 0; j < provincias[i].Ciudades.length; j++) {

                $('#txtCiudad').append(new Option(provincias[i].Ciudades[j].nombre, provincias[i].Ciudades[j].id));
            }
        }

    }

}

function cargarParroquias() {

    var idProvincia = $("#txtProvincia").val();
    var idCiudad = $("#txtCiudad").val();

    var provincia = provincias.find(x => x.id == idProvincia);
    var ciudad = provincia.Ciudades.find(x => x.id == idCiudad);

    $('#txtParroquia').empty();
    $('#txtParroquia').append(new Option('seleccione', 0, true));

    ciudad.Parroquias.sort(compare);

    for (var i = 0; i < ciudad.Parroquias.length; i++) {

        $('#txtParroquia').append(new Option(ciudad.Parroquias[i].nombre, ciudad.Parroquias[i].id));

    }
}

function consultarPersonaCompleta() {

    htclibjs.Ajax({
        url: "/Persona/consultarPersonaCompleta",
        data: JSON.stringify({

        }),
        success: function (r) {

            persona = r.data;

            if (persona.datosCompletos == true) {
                direccion = persona.MIS_Direcciones;
                conyuge = persona.MIS_Personas2;

                //carga datos personales 
                var fecha = persona.fechaNacimiento.split('T');

                $("#txtNombre1Persona").val(persona.nombre);
                $("#txtNombre2Persona").val(persona.nombre2);
                $("#txtApellido1Persona").val(persona.apellido);
                $("#txtApellido2Persona").val(persona.apellido2);
                $("#txtNacionalidadPersona").val(persona.idPais);
                $("#txtCedulaPersona").val(persona.cedula);
                $("#txtTelefonoPersona").val(persona.telefono);
                $("#txtCelularPersona").val(persona.celular);
                $("#txtGeneroPersona").val(persona.sexo);
                $("#txtTipoDocumento").val(persona.idTipoDocumento);
                $("#txtFechaNacimientoPersona").val(fecha[0]);
                $("#txtEstadoCivil").val(persona.idEstadoCivil);;
                $("#txtNivelEducacion").val(persona.idNivelEducacion);;

                //carga datos direccion 
                $('#txtIdDireccion').val(direccion.id);
                $('#txtTipoLugar').val(direccion.idTipoLugar);
                $('#txtCallePrincipal').val(direccion.linea1);
                $('#txtCalleSecundaria').val(direccion.linea2);
                $('#txtNumero').val(direccion.numero);
                //var nombre = $('#txtNombre').val();
                $('#txtReferencia').val(direccion.referencia);
                $('#txtBarrio').val(direccion.barrio);
                $('#txtProvincia').val(direccion.Parroquias.Ciudades.Provincias.id);
                cargarCiudades();
                $('#txtCiudad').val(direccion.Parroquias.Ciudades.id);
                cargarParroquias();
                $('#txtParroquia').val(direccion.idParroquia);

                if (conyuge != null) {
                    //carga datos conyuge
                    $("#txtIdConyuge").val(conyuge.id);
                    $("#txtNombre1Conyuge").val(conyuge.nombre);
                    $("#txtNombre2Conyuge").val(conyuge.nombre2);
                    $("#txtApellido1Conyuge").val(conyuge.apellido);
                    $("#txtApellido2Conyuge").val(conyuge.apellido2);
                    $("#txtNacionalidadConyuge").val(conyuge.idPais);
                    $("#txtCedulaConyuge").val(conyuge.cedula);
                    fecha = conyuge.fechaNacimiento.split('T');
                    $("#txtFechaNacimientoConyuge").val(fecha[0]);
                }
            }
        }
    });

}

function direccionPersona(idTipoLugar, idRef, idTipo, linea1, linea2,
    numero, nombre, referencia, barrio, idParroquia) {

    var direccion =
    {
        id: $('#txtIdDireccion').val(),
        idTipoLugar: idTipoLugar,
        idRef: idRef,
        idTipo: idTipo,
        linea1: linea1,
        linea2: linea2,
        numero: numero,
        nombre: nombre,
        referencia: referencia,
        barrio: barrio,
        idParroquia: idParroquia
    }

    return direccion;

}

function validarCedula(ced) {
    var cTemp = ced;
    var cedula = cTemp.replace("-", "");

    array = cedula.split("");
    num = array.length;
    if (num == 10) {
        total = 0;
        digito = (array[9] * 1);
        for (i = 0; i < (num - 1); i++) {
            mult = 0;
            if ((i % 2) != 0) {
                total = total + (array[i] * 1);
            }
            else {
                mult = array[i] * 2;
                if (mult > 9)
                    total = total + (mult - 9);
                else
                    total = total + mult;
            }
        }
        decena = total / 10;
        decena = Math.floor(decena);
        decena = (decena + 1) * 10;
        final = (decena - total);
        if ((final == 10 && digito == 0) || (final == digito)) {
            //   $(lblMensajeCedula).html("<span style='color:green'> válida</span>");
            //  $(btnContinuar).prop('disabled', false);
            return true;
        }
        else {
            //   $(lblMensajeCedula).html("<span style='color:red'> NO válida</span>");
            //  $(btnContinuar).prop('disabled', true);
            return false;
        }
    }
    else {
        // $(lblMensajeCedula).html("<span style='color:red'> Deben ser 10 dígitos</span>");
        //  $(btnContinuar).prop('disabled', true);
        return false;
    }
    return true;
}

function validarRUC(ru) {
    var cTemp = ru;
    var ruc = cTemp.replace("-", "");
    //todo: ver este bug: replace solo reemplaza una vez   por tanto  175659739-7-001 no es válido cuando sí lo es en verdad. Solución: meter replace dentro de un ciclo
    var tresDigitosRUC = false;  // va a ver si los 3 dígitos del RUC están bien

    array = ruc.split("");
    num = array.length;
    if (num == 13) {
        total = 0;
        digito = (array[9] * 1);
        for (i = 0; i < (num - 4); i++) {
            mult = 0;
            if ((i % 2) != 0) {
                total = total + (array[i] * 1);
            }
            else {
                mult = array[i] * 2;
                if (mult > 9)
                    total = total + (mult - 9);
                else
                    total = total + mult;
            }
        }
        decena = total / 10;
        decena = Math.floor(decena);
        decena = (decena + 1) * 10;
        final = (decena - total);
        for (i = 10; i < (num); i++) {
            if (!isNaN(parseFloat(array[i])) && isFinite(array[i]))
                tresDigitosRUC = true;
            else {
                tresDigitosRUC = false;
                break;
            }
        }

        if (((final == 10 && digito == 0) || (final == digito)) && tresDigitosRUC)
            return true;
        else
            return false;
    }
    else
        return false;

    return true;
}

function validarPasaporte(pas) {
    if (pas.charAt(0) == 'P') {
        return true
    }
    else {
        return false;
    }
}

function consultarPaises() {

    $('#txtNacionalidadPersona').empty();
    $('#txtNacionalidadConyuge').empty();

    htclibjs.Ajax({
        url: "/Pais/consultarPaises",
        data: JSON.stringify({

        }),
        success: function (r) {

            var paises = r.data;
            paises.sort(compare);

            $.each(paises, function (i, item) {

                $('#txtNacionalidadPersona').append(new Option(item.nombre, item.id));
                $('#txtNacionalidadConyuge').append(new Option(item.nombre, item.id));

            });

            $('#txtNacionalidadPersona').val(1);
            $('#txtNacionalidadConyuge').val(1);

        }
    });

}

function compare(a, b) {
    
    var textA = a.nombre.toUpperCase();
    var textB = b.nombre.toUpperCase();
    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
}