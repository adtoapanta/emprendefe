﻿$(document).ready(function () { initReporteConsolidado(); });

function initReporteConsolidado() {
    console.log("init");

    //$('#btnConsultarPagos').click(function (e) {
    //    console.log("boton clic");
    //    consultarTodosLosPagosParametros();
    //});

    consultarReporteConsolidado();


}

function consultarReporteConsolidado() {

    htclibjs.Ajax({
        url: "/Reportes/consultarReporteConsolidado",
        data: JSON.stringify({

        }),
        success: function (r) {

            var trHTML =
                '<thead>' +
                '<tr>' +
                '<th>Convocatoria</th>' +
                '<th>Tipo</th>' +
                '<th>No. Documento</th>' +
                '<th>Emprendedor</th>' +
                '<th>Emprendimiento</th>' +
                '<th>Etapa P.</th>' +
                '<th>Correo</th>' +
                '<th>Telefono</th>' +
                '<th>Celular</th>' +
                '<th>Direccion Emprendedor</th>' +
                '<th>Direccion Emprendimiento</th>' +
                //'<th></th>' + //fila vacia donde va el boton de editar
                '</tr > ' +
                '</thead> ' +

                '<tbody> '

                ;

            $.each(r.data, function (i, item) {

                var fecha = getDateFromJson(item.fecha);

                trHTML +=

                    '<tr>' +
                    '<td >' + item.nombreConvocatoria + '</td>' +
                    '<td >' + item.tipoConvocatoria + '</td>' +
                    '<td >' + item.documentoEmprendedor + '</td>' +
                    '<td >' + item.nombreEmprendedor + '</td>' +
                    '<td >' + item.nombreEmprendimiento + '</td>' +
                    '<td >' + item.etapaPostulacion + '</td>' +
                    '<td >' + item.correoEmprendedor + '</td>' +
                    '<td >' + item.telefonoEmprendedor + '</td>' +
                    '<td >' + item.celularEmprendedor + '</td>' +
                    '<td >' + item.direccionPersona + '</td>' +
                    '<td >' + item.direccionEmprendimiento + '</td>' +
                    //'<td id="tdIdDeuda' + item.id + '"> <button class="btnSeleccionarDeuda btn btn-primary" value="' + item.id + '" type="button"   >Detalles</button> </td>' + //boton editar de la tabla
                    '</tr> ';


            });

            trHTML +=
                '</tbody>';


            $('#tblReporteConsolidado').html('');
            $('#tblReporteConsolidado').append(trHTML);


            exportTable();

            //accion al hacer clic en boton de la tabla
            //$('.btnSeleccionarDeuda').on('click', function (e) {

            //    selecionarDeuda($(this));
            //    //$('#modalEditarConvocatoria').modal('show'); 

            //});
        }
    });

}

function exportTable() {

    var table = $('#tblReporteConsolidado').DataTable({
        lengthChange: false,
        //buttons: ['copy', 'excel', 'pdf'],
        language: SPANISHDATATABLE,
        dom: 'Bfrtip', buttons: [{
            extend: 'excel', className: 'btn btn-primary', text: 'Descargar excel'
        }]
    });

    //table.buttons().container()
    //    .appendTo('#tblReporteConsolidado_wrapper .col-md-6:eq(0)');

}