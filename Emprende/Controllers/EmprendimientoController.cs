﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emprende.Models;
using Emprende.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;
namespace Emprende.Controllers
{
    public class EmprendimientoController : Controller
    {
        // GET: Emprendimiento
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CrearEmprendimiento()
        {
            return View();
        }
        public ActionResult EditarEmprendimiento()
        {
            int idEmprendimiento = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"]);

            ViewBag.emprendimiento = EmprendimientoExtension.consultarEmprendimientoId(idEmprendimiento);

            return View();
        }
        public JsonResult guardarEmprendimiento(string nombre)
        {
            Resultado resultado;
            try
            {
                var context2 = new ApplicationDbContext();
                var user = context2.Users.Where(u => u.UserName == User.Identity.Name).First();


                PersonasExtension persona = new PersonasExtension();
                ViewBag.persona = persona.consultarPersonaIdUsuario(user.Id, user.Email);
                EmprendimientoExtension emprendimiento = new EmprendimientoExtension();
                resultado = new Resultado(true, emprendimiento.crearEmprendimiento(ViewBag.persona.id, nombre).id);
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult completarEmprendimiento(int idEmprendimiento, string descripcion, string ruc, Boolean tieneProducto, int? tieneVentas, int? tiempoVentas, MIS_Direcciones direccion, int bandera, bool estaConstituida, int idMedioInformacion)
        {
            Resultado resultado;
            try
            {
                resultado = new Resultado(true, EmprendimientoExtension.completarEmprendimiento(idEmprendimiento, descripcion, ruc, tieneProducto, tieneVentas, tiempoVentas??0, direccion, bandera, estaConstituida,idMedioInformacion));                
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Emprendmiento" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult consultarEmprendimientosPorPersona()
        {
            Resultado resultado;
            try
            {
                var context2 = new ApplicationDbContext();
                var user = context2.Users.Where(u => u.UserName == User.Identity.Name).First();

                PersonasExtension persona = new PersonasExtension();
                ViewBag.persona = persona.consultarPersonaIdUsuario(user.Id, user.Email);

                List<Emprendimientos> emprendimientos = new List<Emprendimientos>();
                emprendimientos = EmprendimientoExtension.consultarEmprendimientosPorPersona(ViewBag.persona.id);
                if (emprendimientos.Count == 0)
                    resultado = new Resultado(true, 0);
                else
                    resultado = new Resultado(true, emprendimientos);
            }
            catch (Exception e)
            {
                Logger.Write("Error en consultar emprendimientos por persona" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }
        public JsonResult consultarEmprendimientosCompletosPorPersona()
        {
            Resultado resultado;
            try
            {
                var context2 = new ApplicationDbContext();
                var user = context2.Users.Where(u => u.UserName == User.Identity.Name).First();

                PersonasExtension persona = new PersonasExtension();
                ViewBag.persona = persona.consultarPersonaIdUsuario(user.Id, user.Email);

                List<Emprendimientos> emprendimientos = new List<Emprendimientos>();
                emprendimientos = EmprendimientoExtension.consultarEmprendimientosCompletosPorPersona(ViewBag.persona.id);
                if (emprendimientos.Count == 0)
                    resultado = new Resultado(true, 0);
                else
                    resultado = new Resultado(true, emprendimientos);
            }
            catch (Exception e)
            {
                Logger.Write("Error en consultar emprendimientos por persona" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult consultarEmprendimientoId(int idEmprendimiento)
        {
            Resultado resultado;
            try
            {
                resultado = new Resultado(true, EmprendimientoExtension.consultarEmprendimientoId(idEmprendimiento));
            }
            catch (Exception e)
            {
                Logger.Write("Error en consultar emprendimiento Completo" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }
    }
}