﻿using Emprende.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emprende.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;
namespace Emprende.Controllers
{
    public class ReportesController : Controller
    {
        // GET: Reportes
        public ActionResult Index()
        {
            return View();
        }

        // GET: Reportes
        public ActionResult Consolidado()
        {
            if (User.IsInRole("Admin") || User.IsInRole("SuperAdmin"))
            {
                return View();
            }
            else
            {
                return Redirect(Url.Content("~/"));
            }
        }

        public JsonResult consultarReporteConsolidado()
        {
            Resultado resultado;
            try
            {
                resultado = new Resultado(true, ReporteConsolidadoExtension.consultarReporteConsolidado());
            }
            catch (Exception e)
            {
                Logger.Write("Error en consultar Reporte Consolidado" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public ActionResult Indicadores()
        {
            if (User.IsInRole("Admin") || User.IsInRole("SuperAdmin"))
            {
                return View();
            }
            else
            {
                return Redirect(Url.Content("~/"));
            }
        }

        [HttpPost]
        public ActionResult UploadIndicadores(HttpPostedFileBase file)
        {
            try
            {

                DateTime fecha = Convert.ToDateTime(Request["txtFecha"].ToString());

                if (file != null && file.ContentLength > 0)
                {
                    //var fileName = Path.GetFileName(file.FileName);
                    System.IO.Stream stream = file.InputStream;
                    ExcelManager excelManager = new ExcelManager(stream);
                    excelManager.recorrerArchivo(fecha);
                }

                return RedirectToAction("Indicadores");
            }
            catch (Exception e)
            {
                ViewBag.error = e.Message;
                return RedirectToAction("Indicadores");
            }
        }
    }
}