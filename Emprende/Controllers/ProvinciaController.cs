﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emprende.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace Emprende.Controllers
{
    public class ProvinciaController : Controller
    {
        // GET: Provincia
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult consultarPronviciasInclude()
        {
            Resultado resultado;
            try
            {
                
                resultado = new Resultado(true, ProvinciaExtension.consultarPronviciasInclude());
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Persona" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}