﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emprende.Models;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Emprende.Extensions;
namespace Emprende.Controllers
{
    public class ProveedoresCapacitacionController : Controller
    {
        // GET: ProveedoresCapacitacion
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Administrar()
        {
            //if (User.IsInRole("Admin") || User.IsInRole("SuperAdmin"))
            //{
                return View();
            //}
            //else
            //{
            //    return Redirect(Url.Content("~/"));
            //}
        }

        public JsonResult guardarProveedor(string nombre, string correo)
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, ProveedoresCapacitacionExtension.guardarProveedor(nombre,correo));

            }
            catch (Exception e)
            {
                Logger.Write("Error " + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            
            return Json(resultado);
        }

        public JsonResult consultarProveedores()
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, ProveedoresCapacitacionExtension.consultarProveedores());

            }
            catch (Exception e)
            {
                Logger.Write("Error " + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult editarProveedor(int idProveedor, string nombre, string correo)
        {
            Resultado resultado;
            try
            {
                ProveedoresCapacitacionExtension.editarProveedor(idProveedor, nombre, correo);
                resultado = new Resultado(true, true);

            }
            catch (Exception e)
            {
                Logger.Write("Error " + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult eliminarProveedor(int idProveedor)
        {
            Resultado resultado;
            try
            {
                ProveedoresCapacitacionExtension.eliminarProveedor(idProveedor);
                resultado = new Resultado(true, true);

            }
            catch (Exception e)
            {
                Logger.Write("Error " + e.Message);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado);
        }

    }
}