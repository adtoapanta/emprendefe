﻿using Emprende.Extensions;
using Emprende.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Emprende.Controllers
{
    public class FinanzasController : Controller
    {
        // GET: Finanzas
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Productos()
        {
            int currentUserId = User.Identity.GetUserId<int>();
            //TODO por ahora pongo uno específico
            currentUserId = 2; //ADMIN
            //currentUserId = 3; // user

            if (currentUserId > 0)
            {
                int idPostulacion = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"].ToString());
                int idRequisito = Convert.ToInt32(Url.RequestContext.RouteData.Values["id2"].ToString());
                ViewBag.postulacion = PostulacionExtension.consultarPostulacionesId(idPostulacion);
                ViewBag.idRequisito = idRequisito;

                ViewBag.paginaAnterior = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : "";
                return View();
            }
            else
            {
                //hasta definir que se va a hacer 
                throw new Exception("Usuario no logeado");
            }
        }
        public JsonResult getProductos(int pIdEmp)
        {
            Resultado resultado;
            try
            {
                int idPersona = Convert.ToInt32(System.Web.HttpContext.Current.Session["idPersona"]);

                using (var db = new emprendeEntities())
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    List<Productos> productos = db.Productos.Include("Insumos").Where(p => p.idEmprendimiento == pIdEmp).ToList();
                    resultado = new Resultado(true, productos);
                }
            }
            catch (Exception e)
            {
                Logger.Write("Error al obtener productos" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }
        public JsonResult eliminarProducto(int id)
        {
            Resultado resultado;
            try
            {

                using (var db = new emprendeEntities())
                {
                    Productos producto = db.Productos.Where(p => p.id == id).First();
                    ProductoExt productoExt = new ProductoExt(db, producto);
                    productoExt.eliminar();

                    resultado = new Resultado(true, true);
                }
            }
            catch (Exception e)
            {
                Logger.Write("Error al eliminar insumo" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }
        public JsonResult guardarProducto(Productos producto)
        {
            Resultado resultado;
            try
            {
                int idPersona = Convert.ToInt32(System.Web.HttpContext.Current.Session["idPersona"]);

                using (var db = new emprendeEntities())
                {
                    ProductoExt productoExt = new ProductoExt(db, producto);
                    productoExt.guardar();

                    resultado = new Resultado(true, true);
                }
            }
            catch (Exception e)
            {
                Logger.Write("Error al obtener productos" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);

        }
        public JsonResult guardarProducto2(int id, int idEmprendimiento, string nombre, decimal Precio, int unidadesActual, int unidadesProyectado, decimal costo, List<Insumos> Insumos)
        {
            Resultado resultado;
            try
            {
                int idPersona = Convert.ToInt32(System.Web.HttpContext.Current.Session["idPersona"]);

                using (var db = new emprendeEntities())
                {
                    ProductoExt productoExt = new ProductoExt(db, id, idEmprendimiento, nombre, Precio, unidadesActual, unidadesProyectado, costo, Insumos);
                    productoExt.guardar();

                    resultado = new Resultado(true, true);
                }
            }
            catch (Exception e)
            {
                Logger.Write("Error al obtener productos" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);

        }
        public JsonResult eliminarInsumo(int id)
        {
            Resultado resultado;
            try
            {
                using (var db = new emprendeEntities())
                {
                    Insumos insumo = db.Insumos.Where(i => i.id == id).First();
                    db.Insumos.Remove(insumo);
                    db.SaveChanges();

                    resultado = new Resultado(true, true);
                }
            }
            catch (Exception e)
            {
                Logger.Write("Error al eliminar insumo" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);

        }

        public ActionResult Balance()
        {
            CON_postulaciones postulacion;
            int idPostulacion = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"].ToString());
            int idRequisito = Convert.ToInt32(Url.RequestContext.RouteData.Values["id2"].ToString());
            
            using (var db = new emprendeEntities())
            {               
                postulacion = db.CON_postulaciones.Include("Emprendimientos").Include("CON_convocatorias").Where(p => p.id == idPostulacion).First();   
            }

            ViewBag.postulacion = postulacion;
            ViewBag.emprendimiento = postulacion.Emprendimientos;
            ViewBag.idPostulacion = idPostulacion;
            ViewBag.idRequisito = idRequisito;
            ViewBag.esAdmin = !User.IsInRole("Usuario");
            ViewBag.paginaAnterior = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : "";

            return View();
        }
        public JsonResult getDatosBalances(int idPostulacion)
        {
            Resultado resultado;

            try
            {
                CON_postulaciones postulacion;

                using (var db = new emprendeEntities())
                {
                    db.Configuration.LazyLoadingEnabled = true;
                    db.Configuration.ProxyCreationEnabled = false;
                    postulacion = db.CON_postulaciones.Include("Emprendimientos").Include("CON_convocatorias").Where(p => p.id == idPostulacion).First();
                }

                //abro la conexión de balances
                string ConexionBalance = ConfigurationManager.AppSettings["ConexionBalance"];
                Balances.interfaz.init(ConexionBalance);

                BalanceExt balanceExt = new BalanceExt(postulacion);
                balanceExt.calculaVentasBalance(false);
                balanceExt.calculaVentasBalance(true);

                //cierro la conexion de balances
                Balances.interfaz.fin();

                ViewBag.balanceExt = balanceExt;

                resultado = new Resultado(true, balanceExt);
            }
            catch (Exception e)
            {
                Logger.Write("getDatosBalances: " + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }
        [HttpPost]
        public ActionResult guardarBalance()
        {
            try
            {
                int idBalanceAct = Convert.ToInt32(Request["txt_idBalanceAct"].ToString());
                int idBalanceProy = Convert.ToInt32(Request["txt_idBalanceProy"].ToString());

                int idRequisito = Convert.ToInt32(Request["txtIdRequisito"].ToString());
                int idPostulacion = Convert.ToInt32(Request["txtIdPostulacion"].ToString());
                
                string ConexionBalance = ConfigurationManager.AppSettings["ConexionBalance"];
                Balances.interfaz.init(ConexionBalance);

                decimal valorCampo;

                for (int i = 1; i <= 35; i++)
                {
                    var val = Request["txt_" + i];
                    if (val == null) val = "0";
                    valorCampo = Convert.ToDecimal(val);
                    BalanceExt.guardaDatoBalance(idBalanceAct, i, valorCampo);

                    if (i > 6)
                    {
                        var valP = Request["txt_" + i + "_P"];
                        if (valP == null) valP = "0";
                        valorCampo = Convert.ToDecimal(valP);
                        BalanceExt.guardaDatoBalance(idBalanceProy, i, valorCampo);
                    }
                }

                Balances.interfaz.fin();

                return RedirectToAction("completarRequisito", "RequisitosPostulante", new { id = idRequisito });

            }
            catch (Exception e)
            {
                throw new Exception("Error al guardarBalance: " + e.Message);
            }
        }
    }
}