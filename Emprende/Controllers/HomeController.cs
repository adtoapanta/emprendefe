﻿using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Emprende.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        //[Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            try {
                //DO

            }
            catch (Exception e) {
                Extensions.Funciones.WriteLog(e);
            }
            return View();
        }

        public ActionResult TestListaArchivos()
        {
            try
            {
                //DO
                string oment = "";
                VtcArchivos.Funciones.GetListaArchivos("SELECT * FROM ARC_Archivos", "http://www.google.com", "/download", ref oment);
            }
            catch (Exception e)
            {
                Logger.Write("Error en Home/TestListaArchivos" + e.Message);
            }
            return View();
        }


        public ActionResult Submit()
        {
            var file = Request.Files["avatar"];
            string coment = "";

            VtcArchivos.Funciones.SubirArchivo(file, 1,"Archivo de prueba", "Es un archivo", ref coment);

            return View();
        }

        public FileResult Descarga()
        {
            string coment = "";
            return VtcArchivos.Funciones.DescargaArchivoConPermiso("a0981cc7-3506-4e06-9ffa-9495a596536a",2,null,ref coment, "prueba de descarga");
        }

    }
}