﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Emprende.Controllers
{
    public class GeneralController : Controller
    {
        // GET: General
        public ActionResult Index()
        {
            return View();
        }

        public FileResult Descarga()
        {
            string idArchivo, coment = string.Empty;
            string idReferencia;
            FileResult archivo;

            idArchivo = Convert.ToString(Url.RequestContext.RouteData.Values["id"] ?? "");
            idReferencia = Convert.ToString(Url.RequestContext.RouteData.Values["id2"] ?? "");
            if (!string.IsNullOrEmpty(idReferencia))
            {
                archivo = VtcArchivos.Funciones.DescargaArchivo(Convert.ToInt32(idArchivo), Convert.ToInt32(idReferencia), ref coment);
            }
            else {
                archivo = VtcArchivos.Funciones.DescargaArchivo(idArchivo, ref coment);
            }
            

            if (!string.IsNullOrEmpty(coment))
            {
                //Hay q mejorar la manera de mostrar el error
                throw new Exception(coment);
            }
            return archivo;
        }

    }

}