﻿using Emprende.Extensions;
using Emprende.Models;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Emprende.Controllers
{
    public class ComiteController : Controller
    {
        // GET: Comite
        public ActionResult Index()
        {
            string idPublico;
            CON_comiteMiembros comite;
            try {
                idPublico = Url.RequestContext.RouteData.Values["id"].ToString();
                using (var baseE = new Models.emprendeEntities())
                {
                    comite = baseE.CON_comiteMiembros.Where(x => x.idPublico == idPublico).FirstOrDefault();
                }

                ViewBag.postulaciones = GetPostulacionesMiembroComite(idPublico);
                ViewBag.idPublico = idPublico;
                ViewBag.nombre = comite.nombre;
            }
            catch (Exception e) {
                ViewBag.error = e.Message;
            }
            
            return View();
        }

        public ActionResult Administrar()
        {
            if (User.IsInRole("Admin") || User.IsInRole("SuperAdmin"))
            {
                return View();
            }
            else
            {
                return Redirect(Url.Content("~/"));
            }
        }

        public JsonResult PostulacionesMiembroComite(string idP) {
            Resultado resultado;
            try
            {
                resultado = new Resultado(true, GetPostulacionesMiembroComite(idP));
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado);
        }

        private List<SP_PostulacionesMiembroComite> GetPostulacionesMiembroComite(string idPublico) {
            List<SP_PostulacionesMiembroComite> post;
            using (var baseE = new Models.emprendeEntities())
            {
                post = baseE.Database.SqlQuery<Models.SP_PostulacionesMiembroComite>("SP_PostulacionesMiembroComite @idPublico", new System.Data.SqlClient.SqlParameter("idPublico", idPublico)).ToList();
            }

            return post;
        }


        //guarda la calificacion obtenida en la tabla comitePostulacion
        public JsonResult calificar(int idCP, int cal, string obs)
        {
            Resultado resultado;
            CON_comitePostulaciones cp;

            int minAp = Convert.ToInt32(ConfigurationManager.AppSettings["CalificacionMinComite"]);

            try
            {
                using (var baseE = new Models.emprendeEntities())
                {

                    cp = baseE.CON_comitePostulaciones.Where(x => x.id == idCP).FirstOrDefault();
                    cp.esCalificado = true;
                    cp.calificacion = cal;
                    cp.observaciones = obs;
                    cp.aprueba = cal >= minAp ? true : false;

                    baseE.Entry(cp).State = System.Data.Entity.EntityState.Modified;
                    baseE.SaveChanges();
                }               

                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Logger.Write("Error " + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado);
        }


        //idPostulacion
        public JsonResult GetCalificacionComite(int idPos)
        {
            Resultado resultado;
            ComiteEvaluacion ce;
            try
            {
                ce = new ComiteEvaluacion(idPos,(int)TiposComite.Comite);                
                resultado = new Resultado(true, ce);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult GetCalificacionPrefactibilidad(int idPos)
        {
            Resultado resultado;
            ComiteEvaluacion ce;
            try
            {
                ce = new ComiteEvaluacion(idPos, (int)TiposComite.Prefactibilidad);
                resultado = new Resultado(true, ce);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        /// <summary>
        /// Funcion para cuando se crea un miembro
        /// </summary>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public JsonResult CrearMiembroComite(string name, string email, string telefono, string institucion)
        {
            Resultado resultado;
            CON_comiteMiembros cm;
            try
            {
                using (var baseE = new Models.emprendeEntities()) {
                    cm = new CON_comiteMiembros()
                    {
                        nombre = name,
                        email = email,
                        telefono = telefono,
                        institucion = institucion,
                        idPublico = Guid.NewGuid().ToString()
                    };
                    baseE.CON_comiteMiembros.Add(cm);
                    baseE.SaveChanges();
                }
                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Logger.Write("Error " + e.Message);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado);
        }

        public JsonResult consultarMiembrosComite()
        {
            Resultado resultado;            
            try
            {
                
                resultado = new Resultado(true, ComiteExtension.consultarMiembrosComite());

            }
            catch (Exception e)
            {
                Logger.Write("Error " + e.Message);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado);
        }

        public JsonResult editarMiembroComite(int idComite, string nombre, string telefono, string correo, string institucion)
        {
            Resultado resultado;
            try
            {
                ComiteExtension.editarMiembroComite(idComite, nombre, telefono, correo, institucion);
                resultado = new Resultado(true, true);

            }
            catch (Exception e)
            {
                Logger.Write("Error " + e.Message);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado);
        }

        public JsonResult eliminarMiembroComite(int idComite)
        {
            Resultado resultado;
            try
            {
                ComiteExtension.eliminarMiembroComite(idComite);
                resultado = new Resultado(true, true);

            }
            catch (Exception e)
            {
                Logger.Write("Error " + e.Message);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado);
        }

    }

}