﻿using Emprende.Extensions;
using Emprende.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Emprende.Controllers
{
    public class RequisitosPostulanteController : Controller
    {
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        // GET: RequisitosPostulante
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult consultarRequisitosPostulante(int idPostulacion)
        {
            Resultado resultado;
            List<PostulacionExtension> postulaciones = new List<PostulacionExtension>();
            try
            {
                resultado = new Resultado(true, RequisitosPostulanteExtension.consultarRequisitosPostulante(idPostulacion, 5));
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public ActionResult completarRequisito(int id)
        {
            try
            {
                using (var db = new emprendeEntities())
                {
                    RequisitosPostulanteExtension requisito = new RequisitosPostulanteExtension(db, id);
                    requisito.completarRequisito();

                    //TODO verificar que pasa cuando el usuario no está logeado 
                    if (User.IsInRole("Admin") || User.IsInRole("SuperAdmin"))
                    {
                        return RedirectToAction("Postulante", "Convocatoria", new { id = requisito.idPostulacion });
                    }
                    else
                    {
                        //cuando es admin sólo hace el redirect porque ya están finalizados 
                        return RedirectToAction("Etapa", "Postulacion", new { id = requisito.idPostulacion });
                    }
                }
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                throw new Exception("Error: " + e.Message);
            }
        }


        #region "Capacitacion"
        public ActionResult Capacitacion()
        {
            int idPostulacion = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"].ToString());
            int idRequisito = Convert.ToInt32(Url.RequestContext.RouteData.Values["id2"].ToString());

            ViewBag.postulacion = PostulacionExtension.consultarPostulacionesId(idPostulacion);
            ViewBag.idRequisito = idRequisito;

            ViewBag.paginaAnterior = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : "";

            return View();
        }

        public JsonResult GetArchivosCapacitacion(int idP)
        {
            Resultado resultado;
            try
            {
                string msg = string.Empty;
                List<VtcArchivos.Model.ARC_Archivos> archivos;

                archivos = VtcArchivos.Funciones.GetListaArchivos(idP, (int)TipoArchivos.ArchivoCapacitacion, ref msg);

                if (msg != string.Empty)
                {
                    throw new Exception(msg);
                }
                resultado = new Resultado(true, archivos);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GuardarArchivos()
        {
            HttpPostedFileBase file;
            string comment = string.Empty;
            int idPostulante;
            string observacion;
            Resultado rs;
            try
            {
                idPostulante = Convert.ToInt32(Request.Form["idP"]);
                observacion = Request.Form["observaciones"];
                foreach (string fileName in Request.Files)
                {
                    file = Request.Files[fileName];
                    VtcArchivos.Funciones.SubirArchivo(file, idPostulante,observacion, "Archivo de capacitación subido por el usuario", ref comment, idTipo: (int)TipoArchivos.ArchivoCapacitacion);
                    if (comment != string.Empty) throw new Exception(comment);
                }

                rs = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                rs = new Resultado(false, e.Message);
            }

            return Json(rs);
        }

        public JsonResult DeleteArchivosCapacitacion(int idp)
        {
            Resultado rs;
            string msg = string.Empty;
            try
            {
                VtcArchivos.Funciones.EliminarArchivo(idp, ref msg);
                if (msg != string.Empty) throw new Exception(msg);
                rs = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                rs = new Resultado(false, e.Message);
            }

            return Json(rs);
        }
        #endregion

        public ActionResult Entrevista()
        {
            int idPostulacion = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"].ToString());
            ViewBag.postulacion = PostulacionExtension.consultarPostulacionesId(idPostulacion);
            ViewBag.paginaAnterior = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : "";
            return View();
        }

        public ActionResult ModeloNegocio()
        {
            int idPostulacion = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"].ToString());
            int idRequisito = Convert.ToInt32(Url.RequestContext.RouteData.Values["id2"].ToString());
            ViewBag.postulacion = PostulacionExtension.consultarPostulacionesId(idPostulacion);
            ViewBag.idRequisito = idRequisito;

            CON_postulaciones postulacion;
            using (var db = new emprendeEntities())
            {
                postulacion = db.CON_postulaciones.Include("Emprendimientos").Where(p => p.id == idPostulacion).First();
            }
            ViewBag.emprendimiento = postulacion.Emprendimientos;

            ViewBag.paginaAnterior = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : "";

            return View();
        }
        public JsonResult guardarModNegCamposEmprendimiento(int idPostulacion, int pNumClientes, int pNumConsumidores, int pNumEmpleados, int pNumProveedores, string brief)
        {
            Resultado resultado;

            try
            {
                CON_postulaciones postulacion;
                using (var db = new emprendeEntities())
                {
                    postulacion = db.CON_postulaciones.Include("Emprendimientos").Where(p => p.id == idPostulacion).First();
                    Emprendimientos emprendimiento = postulacion.Emprendimientos;
                    emprendimiento.NumClientes = pNumClientes;
                    emprendimiento.NumConsumidores = pNumConsumidores;
                    emprendimiento.NumEmpleados = pNumEmpleados;
                    emprendimiento.NumProveedores = pNumProveedores;
                    emprendimiento.brief = brief;

                    db.SaveChanges();
                }

                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PerfilEmprendedor()
        {
            int idPostulacion = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"].ToString());
            int idRequisito = Convert.ToInt32(Url.RequestContext.RouteData.Values["id2"].ToString());

            ViewBag.idRequisito = idRequisito;
            ViewBag.idPostulacion = idPostulacion;

            ViewBag.postulacion = PostulacionExtension.consultarPostulacionesId(idPostulacion);
            ViewBag.paginaAnterior = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : "";
            return View();
        }

        public ActionResult Prefactibilidad()
        {
            int idPostulacion = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"].ToString());
            ViewBag.postulacion = PostulacionExtension.consultarPostulacionesId(idPostulacion);
            ViewBag.paginaAnterior = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : "";
            return View();
        }

        public ActionResult MatrizInversion()
        {
            int idPostulacion = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"].ToString());
            int idRequisito = Convert.ToInt32(Url.RequestContext.RouteData.Values["id2"].ToString());
            ViewBag.postulacion = PostulacionExtension.consultarPostulacionesId(idPostulacion);
            ViewBag.idRequisito = idRequisito;
            ViewBag.paginaAnterior = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : "";
            return View();
        }
    }
}