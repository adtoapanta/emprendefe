﻿using Emprende.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Emprende.Models.Scoring
{
    public class EvaluacionController : Controller
    {
        // GET: Evaluacion
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult getEvaluacion(int idt, int idr, int idc)
        {
            Resultado resultado;

            try
            {
                htcLib.espacio.inicializa(System.Configuration.ConfigurationManager.ConnectionStrings["Database"].ConnectionString, htcLib.tiposConexion.Web);

                evaluacion ev = Scorian.getEvaluacion(idt, idr, idc);

                resultado = new Resultado(true, ev.getDatos() );
            }
            catch (Exception e)
            {
                Logger.Write("Error " + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            finally{
                htcLib.espacio.cerrar();
            }

            return Json(resultado);
        }


        public JsonResult responder( List<RespuestaEV> resps)
        {
            Resultado resultado;
            int calif = 0;

            try
            {
                htcLib.espacio.inicializa(ConfigurationManager.ConnectionStrings["Database"].ConnectionString, htcLib.tiposConexion.Web);

                calif = evaluacion.guardaRespuestas(resps, true);

                resultado = new Resultado(true, calif);
            }
            catch (Exception e)
            {
                Logger.Write("Error " + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            finally
            {
                htcLib.espacio.cerrar();
            }

            return Json(resultado);
        }

    }
}