﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emprende.Models;
using Emprende.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Globalization;
using Newtonsoft.Json;
using System.IO;
using Microsoft.AspNet.Identity;

namespace Emprende.Controllers
{
    public class ConvocatoriaController : Controller
    {
        public ActionResult Etapa()
        {
            CON_convocatorias con;
            int idConvocatoria, idEtapa;

            ViewBag.paginaAnterior = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : "";

            try {

                idConvocatoria = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"]);
                using (var baseE = new Models.emprendeEntities())
                {
                    con = baseE.CON_convocatorias.Where(x => x.id == idConvocatoria).FirstOrDefault();
                    if (con == null) throw new Exception("Convocatoria no seleccionada.");
                    idEtapa = con.idEtapa??((int)IndiceEtapasConvocatoria.Creacion);
                }
                switch (idEtapa) {
                    case (int)IndiceEtapasConvocatoria.Creacion:
                        return RedirectToAction("Editar", "Convocatoria",new {id=idConvocatoria });
                    case (int)IndiceEtapasConvocatoria.Inscripción:
                    case (int)IndiceEtapasConvocatoria.ModeloNegocio:
                    case (int)IndiceEtapasConvocatoria.AgendarPrefactibilidad:
                    case (int)IndiceEtapasConvocatoria.Prefactibilidad:
                    case (int)IndiceEtapasConvocatoria.AgendarComite:
                    case (int)IndiceEtapasConvocatoria.Comite:
                        return RedirectToAction("Administrar", "Convocatoria", new { id = idConvocatoria });
                    case (int)IndiceEtapasConvocatoria.Diagnóstico:
                    case (int)IndiceEtapasConvocatoria.Documentos:
                    case (int)IndiceEtapasConvocatoria.AprobarDocumentos:
                    case (int)IndiceEtapasConvocatoria.Propuesta:
                    case (int)IndiceEtapasConvocatoria.AprobarPropuesta:
                    case (int)IndiceEtapasConvocatoria.Contrato:
                        return RedirectToAction("Postulantes", "Convocatoria", new { id = idConvocatoria });
                    case (int)IndiceEtapasConvocatoria.Elegibilidad:
                        return RedirectToAction("Eligibilidad", "Convocatoria", new { id = idConvocatoria });
                    case (int)IndiceEtapasConvocatoria.Finalizado:
                        throw new Exception("No deberia llegar a esta ubicación.");
                }
            }
            catch (Exception e) {
                ViewBag.error = e.Message;
            }
            return View("Error");
        }

        #region "Lista de convocatorias"
        // GET: Convocatoria
        public ActionResult Convocatorias()
        {
            if (User.IsInRole("Admin") || User.IsInRole("SuperAdmin"))
            {
                return View();
            }
            else
            {
                return Redirect(Url.Content("~/"));
            }
        }

        public ActionResult ConvocatoriasAbiertas()
        {
            return View();
        }
        public ActionResult VerConvocatoria()
        {
            int idConvocatoria;
            idConvocatoria = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"].ToString());
            CON_convocatorias convocatoria = ConvocatoriaExtension.consultarConvocatoriaId(idConvocatoria);
            string[] fechaCierre;
            fechaCierre = convocatoria.fechaCierre.ToString().Split(' ');
            ViewBag.idConvocatoria = idConvocatoria;
            ViewBag.nombreConvocatoria = convocatoria.nombre;
            ViewBag.fechaCierre = fechaCierre[0];
            ViewBag.descripcionConvocatoria = convocatoria.descripcion;
            ViewBag.criterios = convocatoria.criterios;

            if (Url.RequestContext.RouteData.Values["id2"] != null)
            {
                int idEmprendimiento = Convert.ToInt32(Url.RequestContext.RouteData.Values["id2"].ToString());
                ViewBag.idEmprendimiento = idEmprendimiento;
            }
            else
            {
                ViewBag.idEmprendimiento = 0;
                int idUsuario = User.Identity.GetUserId<int>();
                string correo = User.Identity.GetUserName();

                if (idUsuario == 0) // no esta logeado
                {
                    ViewBag.idPersona = 0;
                }
                else // usuario loggeado
                {
                    PersonasExtension persona = new PersonasExtension();
                    ViewBag.IdPersona = persona.consultarPersonaIdUsuario(idUsuario, correo).id;
                }
            }
            return View();
        }

        public JsonResult CrearConvocatoria(string nombreConvocatoria)
        {
            Resultado resultado;
            try
            {
                ConvocatoriaExtension convocatoria = new ConvocatoriaExtension();
                convocatoria.CrearConvocatoria(nombreConvocatoria);
                resultado = new Resultado(true, convocatoria.GetConvocatoria().id);
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear convocatoria" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult cosultarConvocatorias()
        {
            Resultado resultado;
            try
            {
                resultado = new Resultado(true,ConvocatoriaExtension.ConsultarConvocatorias());
            }

            catch (Exception e)
            {
                Logger.Write("Error en consultar convocatorias" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado);
        }
        public JsonResult consultarIdPublico(int idReferencia)
        {
            Resultado resultado;
            try
            {
                ConvocatoriaExtension convocatoria = new ConvocatoriaExtension();
                resultado = new Resultado(true, convocatoria.consultarIdPublico(idReferencia, (int)TipoArchivos.ArchivoBase));
            }

            catch (Exception e)
            {
                Logger.Write("Error en consultar convocatorias" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult cosultarConvocatoriasAbiertas()
        {
            Resultado resultado;
            try
            {
                List<CON_convocatorias> convocatorias = ConvocatoriaExtension.consultarConvocatoriasAbiertas();
                if (convocatorias.Count == 0)
                {
                    resultado = new Resultado(true, 0);
                }
                else
                {
                    resultado = new Resultado(true, ConvocatoriaExtension.consultarConvocatoriasAbiertas());
                }
            }

            catch (Exception e)
            {
                Logger.Write("Error en consultar convocatorias Abiertas" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado);
        }

        #endregion

        #region Edición convocatorias
        public JsonResult AbrirInscripciones(int idC)
        {
            Resultado resultado;
            try
            {
                resultado = new Resultado(true, ConvocatoriaExtension.AbrirInscripcionConvocatoria(idC));
            }
            catch (Exception e)
            {
                Logger.Write("Error en consultar convocatorias" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Funcion para la edición de convocatoria
        /// </summary>
        /// <param name="btnSubmit"></param>
        /// <returns></returns>
        public ActionResult Editar()
        {
            if (User.IsInRole("Admin") || User.IsInRole("SuperAdmin"))
            {
                int idConvocatoria, tipo;
                DateTime fechaApertura, fechaCierre;
                ConvocatoriaExtension convE;
                string criterios, nombre, descripcion, des, capacitacion;
                bool bien = true;
                bool editada = true;
                try
                {

                    idConvocatoria = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"]);
                    convE = new ConvocatoriaExtension(idConvocatoria, ref editada, ref bien);
                    if (!bien) return RedirectToAction("Etapa", "Convocatoria", new { id = idConvocatoria });

                    ViewBag.convocatoria = convE.GetConvocatoria();
                    if (Request.Form.Count > 0)
                    {

                        var btn1 = Request.Form["btnSubmit"];
                        nombre = Request.Form["txtNombreConvocatoria"];
                        fechaApertura = DateTime.ParseExact(Request.Form["txtFechaApertura"], "yyyy-MM-dd", CultureInfo.InvariantCulture);
                        fechaCierre = DateTime.ParseExact(Request.Form["txtFechaCierre"], "yyyy-MM-dd", CultureInfo.InvariantCulture);
                        criterios = Request.Form["txtEditorCriterios"];
                        descripcion = Request.Form["txtDescripcion"];
                        capacitacion = Request.Form["txtCapacitacion"];
                        tipo = Convert.ToInt32(Request.Form["slcTipoConvocatoria"]);

                        convE.SetValuesConvocatoria(nombre, fechaApertura, fechaCierre, descripcion, criterios, tipo, capacitacion);

                        var file = Request.Files["txtArchivoBase"];
                        if (file != null && file.ContentLength > 0)
                        {
                            des = "Bases de convocatoria " + nombre;
                            convE.SubirArchivoBase(file, des);
                        }

                        foreach (var req in convE.GetConvocatoria()._requisitos)
                        {
                            req._seleccionado = (Request.Form["chkRequisitos" + req.id.ToString()] != null);
                        }

                        ViewBag.guardado = convE.SaveEditConvocatoria();
                        editada = true;
                    }
                    ViewBag.archivoBase = convE.GetArchivoBase();
                    ViewBag.tiposConvocatoria = convE.GetTiposConvocatoria();
                    ViewBag.editada = editada;

                    ViewBag.paginaAnterior = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : "";
                }
                catch (Exception e)
                {
                    Logger.Write("Error al editar convocatoria " + e.Message);
                    ViewBag.error = e.Message;
                }

                return View("Editar");
            }
            else
            {
                return Redirect(Url.Content("~/"));
            } 
        }

        public FileResult Descarga()
        {
            string idArchivo, coment = string.Empty;
            FileResult archivo;

            idArchivo = Convert.ToString(Url.RequestContext.RouteData.Values["id"] ?? "");
            archivo = VtcArchivos.Funciones.DescargaArchivo(idArchivo, ref coment);

            if (!string.IsNullOrEmpty(coment))
            {
                //Hay q mejorar la manera de mostrar el error
                throw new Exception(coment);
            }
            return archivo;
        }
        #endregion

        #region "Acciones convocatoria"
        public ActionResult Administrar() {
            int idConvocatoria;
            ConvocatoriaExtension cone;
            string vista = "Error";
            try {
                idConvocatoria = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"]);
                cone = new ConvocatoriaExtension(idConvocatoria);

                switch (cone.GetConvocatoria().idEtapa)
                {
                    case (int)IndiceEtapasConvocatoria.Inscripción:
                        vista = "Administrar";
                        break;
                    case (int)IndiceEtapasConvocatoria.ModeloNegocio:
                        vista = "Datos";
                        break;
                    case (int)IndiceEtapasConvocatoria.AgendarPrefactibilidad:
                        ViewBag.miembrosPrefactibilidad = ConvocatoriaExtension.GetMiembrosComitePrefactibilidad();
                        vista = "AgendarPrefactibilidad";
                        break;
                    case (int)IndiceEtapasConvocatoria.Prefactibilidad:
                        vista = "Prefactibilidad";
                        break;
                    case (int)IndiceEtapasConvocatoria.AgendarComite:
                        ViewBag.miembrosComite = ConvocatoriaExtension.GetMiembrosComitePrefactibilidad();
                        vista = "AgendarComite";
                        break;
                    case (int)IndiceEtapasConvocatoria.Comite:
                        vista = "Comite";
                        break;
                }
                ViewBag.convocatoria = cone.GetConvocatoria();
            }
            catch (Exception e){
                Funciones.WriteLog(e);
                ViewBag.error = e.Message;
            }

            ViewBag.paginaAnterior = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : "";

            return View(vista);
        }

        /// <summary>
        /// Retorna las postulaciones que pertenecen a la etapa de la convocatoria
        /// </summary>
        /// <returns></returns>
        public JsonResult GetPostulaciones(int idC) {
            Resultado r;
            ConvocatoriaExtension cone;
            try {
                cone = new ConvocatoriaExtension(idC);
                r = new Resultado(true, cone.GetPostulantes());
            }
            catch (Exception e) {
                Funciones.WriteLog(e);
                r = new Resultado(false,e.Message);
            }

            return Json(r,JsonRequestBehavior.AllowGet);
        }

        public JsonResult CerrarInscripciones(int idC) {
            Resultado rs;

            try {
                ConvocatoriaExtension.CerrarEtapa(idC);
                rs = new Resultado(true,true);
            }
            catch (Exception e) {
                Funciones.WriteLog(e);
                rs = new Resultado(false,e.Message);
            }
            return Json(rs);
        }

        public JsonResult ExtenderPlazo(int idC, DateTime f) {
            Resultado rs;

            try
            {
                ConvocatoriaExtension.ExtenderPlazoCierre(idC,f);
                rs = new Resultado(true, f);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                rs = new Resultado(false, e.Message);
            }
            return Json(rs);
        }
        #endregion

        #region "Eligibilidad"
        public ActionResult Eligibilidad() {
            int idConvocatoria;
            ConvocatoriaExtension cone;

            try
            {
                idConvocatoria = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"]);
                cone = new ConvocatoriaExtension(idConvocatoria);

                ViewBag.convocatoria = cone.GetConvocatoria();
                ViewBag.preguntas = Funciones.ToJson(cone.GetPreguntasRespuestas());
                ViewBag.mailAprobados = cone.GetTextoMailEligibilidadAprobado();
                ViewBag.mailNoAprobados = cone.GetTextoMailEligibilidadNoAprobado();
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                ViewBag.error = e.Message;
            }

            ViewBag.paginaAnterior = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : "";
            return View();
        }


        public JsonResult SetCriteriosGetPostulantes(int idC,List<SCO_Criterios> ctrs) {
            Resultado r;
            ConvocatoriaExtension cone;
            List<PostulacionDetalle> pos;
            
            try
            {
                cone = new ConvocatoriaExtension(idC);
                cone.GuardarCriterios(ctrs);
                pos = cone.GetPostulantesFiltro();
                r = new Resultado(true, pos);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                r = new Resultado(false, e.Message);
            }
            return Json(r);
        }

        public JsonResult AplicarPostulantes(int idC, DateTime f,string ap, string nap) {
            Resultado r;
            ConvocatoriaExtension cone;
            try {
                cone = new ConvocatoriaExtension(idC);
                cone.GuardarTextosMailEligibilidad(ap, nap);
                cone.AplicarPostulantes();
                cone.CerrarEtapa();
                ConvocatoriaExtension.ExtenderPlazoCierreDatos(idC, f);//Establece una fecha a la convocatoria
                r = new Resultado(true,true);
            }
            catch (Exception e) {
                r = new Resultado(false, e.Message);
            }
            return Json(r);

        }

        #endregion

        #region "Datos"
        public JsonResult ExtenderPlazoCierreDatos(int idC,DateTime f) {
            Resultado rs;
            try
            {
                ConvocatoriaExtension.ExtenderPlazoCierreDatos(idC,f);
                rs = new Resultado(true, f);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                rs = new Resultado(false, e.Message);
            }
            return Json(rs);
        }
        #endregion

        #region "AgendarPrefactibilidad"
        public JsonResult AgendarPrefactibilidadPostulante(int idP, string f, string h,string l)
        {
            Resultado rs;
            DateTime fecha;
            try
            {
                fecha = DateTime.ParseExact(f+" "+h, "yyyy-MM-dd h:mm tt", CultureInfo.InvariantCulture);
                ConvocatoriaExtension.AgendarPrefactibilidadPostulante(idP, fecha,l);
                rs = new Resultado(true, fecha);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                rs = new Resultado(false, e.Message);
            }
            return Json(rs);
        }

        public JsonResult DescartarPostulante(int idP) {
            Resultado rs;

            try
            {
                ConvocatoriaExtension.DescartarPostulante(idP);
                rs = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                rs = new Resultado(false, e.Message);
            }
            return Json(rs);
        }
        #endregion

        #region "Prefactibilidad"
        public ActionResult GuardarPrefactibilidad()
        {
            Resultado rs;
            int idConvocatoria, idPostulante;
            bool aprobado;
            string obs;
            List<HttpPostedFileBase> arch;

            try
            {
                arch = new List<HttpPostedFileBase>();
                idConvocatoria = Convert.ToInt32(Request.Form["idC"]);
                idPostulante = Convert.ToInt32(Request.Form["idP"]);
                aprobado = Convert.ToBoolean(Request.Form["aprueba"]);
                obs = Request.Form["obs"];
                foreach (string fileName in Request.Files)
                {
                    arch.Add(Request.Files[fileName]);
                }
                ConvocatoriaExtension.GuardarPrefactibilidad(idPostulante, arch, aprobado, obs);

                rs = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                rs = new Resultado(false, e.Message);
            }
            return Json(rs);
        }
        #endregion

        #region "Agendar Comite"
        public JsonResult AgendarComitePostulante(int idP, string f, string h,string l)
        {
            Resultado rs;
            DateTime fecha;
            try
            {
                fecha = DateTime.ParseExact(f + " " + h, "yyyy-MM-dd h:mm tt", CultureInfo.InvariantCulture);
                ConvocatoriaExtension.AgendarComitePostulante(idP, fecha,l);
                rs = new Resultado(true, fecha);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                rs = new Resultado(false, e.Message);
            }
            return Json(rs);
        }
        #endregion


        public JsonResult AgregarMiembrosComitePostulacion(List<int> idPs, int idMC)
        {
            Resultado rs;
            try
            {
                ConvocatoriaExtension.AgregarMiembrosPrefactibilidadComitePostulacion(idPs, idMC,TiposComite.Comite);
                rs = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                rs = new Resultado(false, e.Message);
            }
            return Json(rs);
        }

        public JsonResult AgregarMiembrosPrefactibilidadPostulacion(List<int> idPs, int idMC)
        {
            Resultado rs;
            try
            {
                ConvocatoriaExtension.AgregarMiembrosPrefactibilidadComitePostulacion(idPs, idMC,TiposComite.Prefactibilidad);
                rs = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                rs = new Resultado(false, e.Message);
            }
            return Json(rs);
        }

        public JsonResult LimpiarMiembrosComitePostulacion(List<int> idPs)
        {
            Resultado rs;
            try
            {
                ConvocatoriaExtension.LimpiarMiembrosComitePostulacion(idPs);
                rs = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                rs = new Resultado(false, e.Message);
            }
            return Json(rs);
        }

        #region "Aprobar documentos finales"
        public JsonResult VerificarDocumento(int idP, int idD) {
            Resultado rs;
            string comm = "";
            try
            {
                VtcArchivos.Funciones.VerificarDocumento(null,null,idD,ref comm);
                if (comm != string.Empty) throw new Exception(comm);
                rs = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                rs = new Resultado(false, e.Message);
            }
            return Json(rs);
        }
#endregion

        public JsonResult GetDocumentosCargados(int idP) {
            List<VtcArchivos.Model.ARC_DocumentosCargados> documentosC;
            Resultado rs;
            string comm = "";
            try
            {
                documentosC = VtcArchivos.Funciones.GetDocumentosArchivosCargados(idP,(int)TipoDocumentos.Datos, ref comm);
                if (comm != string.Empty) throw new Exception(comm);
                //No necesita q sea referencia anidada hasta el final
                documentosC.ForEach(p => p.ARC_Documentos.ARC_DocumentosCargados = null);
                foreach (var dc in documentosC) {
                    if (dc.ARC_Archivos != null)
                        dc.ARC_Archivos.ARC_DocumentosCargados = null;
                }

                rs = new Resultado(true, documentosC, anidacionIndefinida: false);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                rs = new Resultado(false, e.Message);
            }
            return Json(rs);
        }

        #region "Comite"
        public JsonResult GetDocumentosArchivos()
        {
            Dictionary<string,object> ad;
            List<VtcArchivos.Model.ARC_DocumentosNecesarios> documentosN;
            List<VtcArchivos.Model.ARC_Documentos> documentosOpcionales;
            Resultado rs;
            string comm = "";
            try
            {
                ad = new Dictionary<string, object>();
                documentosN =VtcArchivos.Funciones.GetDocumentosNecesarios(0,ref comm);
                if (comm != string.Empty) throw new Exception(comm);
                documentosOpcionales = VtcArchivos.Funciones.GetDocumentos((int)CategoriasDocumentos.Opcionales, ref comm);
                if (comm != string.Empty) throw new Exception(comm);

                ad.Add("documentosNecesarios", documentosN);
                ad.Add("documentosOpcionales", documentosOpcionales);
                rs = new Resultado(true, ad);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                rs = new Resultado(false, e.Message);
            }
            return Json(rs);
        }

        public ActionResult GuardarComite()
        {
            Resultado rs;
            int idPostulante;
            bool aprobado;
            string obs;
            List<HttpPostedFileBase> arch;
            try
            {
                arch = new List<HttpPostedFileBase>();
                idPostulante = Convert.ToInt32(Request.Form["idP"]);
                aprobado = Convert.ToBoolean(Request.Form["aprueba"]);
                obs = Request.Form["obs"];
                foreach (string fileName in Request.Files)
                {
                    arch.Add(Request.Files[fileName]);
                }

                ConvocatoriaExtension.GuardarComite(idPostulante, arch, aprobado, obs);

                rs = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                rs = new Resultado(false, e.Message);
            }
            return Json(rs);
        }
        #endregion


        //sssssssssssssssss

        public JsonResult GuardarDocumentosPedir(int idP, List<VtcArchivos.Model.ARC_DocumentosCargados> docs) {
            Resultado rs;
            try
            {
                ConvocatoriaExtension.GuardarDocumentosAPedir(idP,docs);

                rs = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                rs = new Resultado(false, e.Message);
            }
            return Json(rs);
        }


        public ActionResult Postulantes() {
            int idConvocatoria;
            ConvocatoriaExtension cone;
            try {
                idConvocatoria = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"]);
                cone = new ConvocatoriaExtension(idConvocatoria);
                ViewBag.convocatoria = cone.GetConvocatoria();
                ViewBag.paginaAnterior = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : "";
            }
            catch (Exception e) {
                ViewBag.Error = e.Message;
            }
            return View("Postulantes");
        }

        public ActionResult Postulante() {
            int idPostulante;
            ConvocatoriaExtension con;
            PostulacionDetalle detalle;
            string vista= "";
            //string error="";
            try
            {
                idPostulante = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"]);
                detalle = ConvocatoriaExtension.GetPostulante(idPostulante);
                
                if (detalle == null) throw new Exception("Postulación no encontrada");
                con = new ConvocatoriaExtension(detalle.idConvocatoria);
                ViewBag.postulante = detalle;
                ViewBag.convocatoria = con.GetConvocatoria();

                
                switch (detalle.idEtapa) {
                    case (int)IndiceEtapasConvocatoria.Diagnóstico:
                        ViewBag.Administradores = User.IsInRole("SuperAdmin")? ConvocatoriaExtension.GetAdministradores():null;
                        var iden = User.Identity.GetUserId();
                        if (User.IsInRole("SuperAdmin"))
                        {
                            ViewBag.permiso = "Asignar";
                        }
                        else if (User.IsInRole("Admin") && detalle.idUsuarioAsignado == Convert.ToInt32(User.Identity.GetUserId())) {
                            ViewBag.permiso = "Editar";
                        }
                        vista = "PostulanteVerificacion";
                        break;
                    case (int)IndiceEtapasConvocatoria.Documentos:
                        vista = "PostulanteDatosFinales";
                        break;
                    case (int)IndiceEtapasConvocatoria.AprobarDocumentos:
                        vista = "PostulanteAprobarDatosFinales";
                        break;
                    case (int)IndiceEtapasConvocatoria.Propuesta:
                        ViewBag.InfoPropuesta = ConvocatoriaExtension.GetInformacionPropuesta(idPostulante);
                        vista = "PostulantePropuesta";
                        break;
                    case (int)IndiceEtapasConvocatoria.Contrato:
                        vista = "PostulanteContrato";
                        break;
                    case (int)IndiceEtapasConvocatoria.AprobarPropuesta:
                        ViewBag.InfoPropuesta = ConvocatoriaExtension.GetInformacionPropuesta(idPostulante);
                        ViewBag.IsSuperAdmin = User.IsInRole("SuperAdmin");
                        ViewBag.idPublico = con.consultarIdPublico(idPostulante, (int)TipoArchivos.ArchivoPropuestaAprobada);
                        vista = "PostulanteAprobarPropuesta";
                        break;
                    case (int)IndiceEtapasConvocatoria.Finalizado:
                        throw new Exception("Postulante en estado finalizado.");
                }
            }
            catch (Exception e)
            {
                ViewBag.Error = e.Message;
                vista = "Error";
            }

            return View(vista);
        }

        public ActionResult ImpresionPropuesta() {
            PostulacionDetalle detalle;
            int idPostulante = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"]);
            detalle = ConvocatoriaExtension.GetPostulante(idPostulante);
            if (detalle == null) throw new Exception("Postulación no encontrada");
            ViewBag.postulante = detalle;
            ViewBag.InfoPropuesta = ConvocatoriaExtension.GetInformacionPropuesta(idPostulante);
            return View("ImpresionPropuesta");
        }

        public JsonResult GetRequisitosPostulante(int idP) {
            List<CON_requisitosPostulante> req;
            Resultado res;
            try
            {
                req = ConvocatoriaExtension.GetRequisitosPostulanteEsVerificados(idP);
                res = new Resultado(true, req);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                res = new Resultado(false,e.Message);
            }
            return Json(res);
        }

        public JsonResult AsignarResponsableVerificacion(int idP, int idA) {
            Resultado res;
            try
            {
                res = new Resultado(true, ConvocatoriaExtension.AsignarResponsableVerificacio(idP, idA));
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                res = new Resultado(false, e.Message);
            }
            return Json(res);
        }

        public JsonResult VerificarRequisitosPostulante(int idRP) {
            Resultado res;
            try
            {
                res = new Resultado(true, ConvocatoriaExtension.VerificarRequisito(idRP));
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                res = new Resultado(false, e.Message);
            }
            return Json(res);
        }

        public JsonResult GuardarPropuesta(int idP, List<CON_propuestaObjetivos> objs,decimal t,int ts, int p, int g,decimal v,decimal ctm,decimal pe, string c, string n, string obs)
        {
            Resultado res;
            try
            {
                ConvocatoriaExtension.GuardarObjetivosPropuesta(idP, objs,t,p,g,ts,v,ctm,pe,c,n,obs);
                res = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                res = new Resultado(false, e.Message);
            }
            return Json(res);
        }

        #region "Propuesta"

        public JsonResult GetDatosPropuesta(int idP, decimal inv, decimal vp) {
            Resultado res;
            try
            {
                res = new Resultado(true, ConvocatoriaExtension.GetDatosPropuesta(idP,inv,vp));
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                res = new Resultado(false, e.Message);
            }
            return Json(res);
        }

        public JsonResult GetDatosAmortizacion(int plazo, int gracia, decimal tasa, decimal total) {
            Resultado res;
            try
            {
                res = new Resultado(true, ConvocatoriaExtension.GetDatosCalculadosAmortizacion(plazo, gracia, total, tasa, DateTime.Today));
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                res = new Resultado(false, e.Message);
            }
            return Json(res);
        }

        public JsonResult GetTablaAmortizacion(int plazo,int gracia,decimal tasa, decimal total) {
            Resultado res;
            try
            {
                res = new Resultado(true, ConvocatoriaExtension.GetDatosTablaAmortizacion(plazo, gracia, total, tasa, DateTime.Today));
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                res = new Resultado(false, e.Message);
            }
            return Json(res);
        }

        public JsonResult AceptarPropuesta() {//idPostulacion
            Resultado res;
            HttpPostedFileBase archivo = null;
            int idPostulacion;
            string msg = string.Empty;
            try
            {
                archivo = Request.Files["archivo"];
                idPostulacion = Convert.ToInt32(Request.Form["idpostulacion"]);
                VtcArchivos.Funciones.SubirArchivo(archivo, idPostulacion, "Proforma final","",ref msg,idTipo:(int)TipoArchivos.ArchivoPropuestaAprobada);
                if (msg != string.Empty) throw new Exception(msg);
                ConvocatoriaExtension.AceptarPropuesta(idPostulacion);
                ConvocatoriaExtension.CerrarEtapaPostulante(idPostulacion);

                res = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                res = new Resultado(false, e.Message);
            }
            return Json(res);
        }

        public JsonResult AceptarContrato() {
            Resultado res;
            HttpPostedFileBase archivo = null;
            int idPostulacion;
            string msg = string.Empty;
            try
            {
                archivo = Request.Files["archivo"];
                idPostulacion = Convert.ToInt32(Request.Form["idpostulacion"]);
                VtcArchivos.Funciones.SubirArchivo(archivo, idPostulacion, "Contrato aceptado", "", ref msg, idTipo: (int)TipoArchivos.ArchivoContratoAceptado);
                if (msg != string.Empty) throw new Exception(msg);
                ConvocatoriaExtension.AceptarContrato(idPostulacion);
                ConvocatoriaExtension.CerrarEtapaPostulante(idPostulacion);

                res = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                res = new Resultado(false, e.Message);
            }
            return Json(res);
        }

        public JsonResult AprobarPropuesta(int idP)
        {
            Resultado res;
            try
            {
                ConvocatoriaExtension.CerrarEtapaPostulante(idP);

                res = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                res = new Resultado(false, e.Message);
            }
            return Json(res);
        }

        #endregion

        public JsonResult CerrarEtapaPostulante(int idP) {
            Resultado res;
            try
            {
                ConvocatoriaExtension.CerrarEtapaPostulante(idP);
                res = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                res = new Resultado(false, e.Message);
            }
            return Json(res);
        }

        //No funciona polimorfismo
        public JsonResult CerrarEtapaDocumentosPostulante(int idP, int p, bool c)
        {
            Resultado res;
            try
            {
                ConvocatoriaExtension.CerrarEtapaPostulante(idP,c,p);
                res = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                res = new Resultado(false, e.Message);
            }
            return Json(res);
        }

        public JsonResult CerrarEtapa(int idC)
        {
            Resultado rs;
            try
            {
                ConvocatoriaExtension.CerrarEtapa(idC);
                rs = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                rs = new Resultado(false, e.Message);
            }
            return Json(rs);
        }

        public ActionResult Documentos() {
            return View("Documentos");
        }
    }
}