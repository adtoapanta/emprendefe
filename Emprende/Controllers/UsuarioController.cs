﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emprende.Models;
using Emprende.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Emprende.Controllers
{
    public class UsuarioController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        

        // GET: Usuario
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Administrar()
        {
            if (User.IsInRole("Admin") || User.IsInRole("SuperAdmin"))
            {
                return View();
            }
            else
            {
                return Redirect(Url.Content("~/"));
            }
        }

        public ActionResult Dashboard()
        {
            //int idPersona = Convert.ToInt32(System.Web.HttpContext.Current.Session["idPersona"]);


            //MIS_Personas persona = PersonasExtension.consultarPersonaPorID(idPersona);

            //ViewBag.nombrePersona = "Bienvenido: " + persona.nombre + " " + persona.apellido;

            int idUsuario = User.Identity.GetUserId<int>();
            string correo = User.Identity.GetUserName();
            PersonasExtension persona = new PersonasExtension();
            if (persona.consultarPersonaIdUsuario(idUsuario,correo).nombre == null)
            {
                return View("Editar", "Persona");
            }
            else 
                return View();
        }
        public async  Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = SignInManager.PasswordSignIn(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    {
                        UsuarioExtension usuario = new UsuarioExtension();
                        int idPersona = await UsuarioExtension.consultarIdPersona(model.Email);
                        System.Web.HttpContext.Current.Session["idPersona"] = idPersona;
                        return RedirectToLocal();
                    }

                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View("verConvocatoria","Convocatoria");
            }
        }
        private ActionResult RedirectToLocal()
        {
            
            return RedirectToAction("Dashboard" , "usuario");
        }
        public ActionResult crearPersona(string nombre, string apellido, string documento, string telefono, string sexo, string nacionalidad, string correo, string ciudad)
        {
            Resultado resultado;
            try
            {
                UsuarioExtension usuario = new UsuarioExtension();                
                resultado = new Resultado(true, Register(usuario.crearPersona(nombre, apellido, documento, telefono, sexo, nacionalidad, correo, ciudad)));
                
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear persona" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return RedirectToLocal();

        }

        public  int Register(RegisterViewModel model)
        {

            if (ModelState.IsValid)
            {

                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, idPersona = model.idPersona };
                var result =  UserManager.Create(user, "Shipobot1.");
                if (result.Succeeded)
                {
                     SignInManager.SignIn (user, isPersistent: false, rememberBrowser: false);

                     this.UserManager.AddToRole(user.Id, "Usuario");                    
                }              

                
            }

            System.Web.HttpContext.Current.Session["idPersona"] = model.idPersona;

            return model.idPersona;
            
            
        }
    }
}