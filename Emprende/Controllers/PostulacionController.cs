﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Emprende.Extensions;
using Emprende.Models;
namespace Emprende.Controllers
{
    public class PostulacionController : Controller
    {

        public ActionResult Index()
        {

            if (User.IsInRole("Admin") || User.IsInRole("SuperAdmin"))
            {
                int idPostulacion;
                try
                {
                    idPostulacion = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"]);
                    PostulacionCompleta postulacionCompleta;
                    using (var baseE = new Models.emprendeEntities())
                    {
                        postulacionCompleta = new PostulacionCompleta(baseE, idPostulacion);
                    }

                    ViewBag.postulacion = postulacionCompleta;
                    //ViewBag.postulacion = PostulacionExtension.GetPostulacion(idPostulacion);
                    ViewBag.paginaAnterior = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : "";

                }
                catch (Exception e)
                {
                    ViewBag.error = e.Message;
                }
                return View();
            }
            else
            {
                return Redirect(Url.Content("~/"));
            }
        }


        // GET: Postulacion
        public ActionResult Postulaciones()
        {
            int idConvocatoria;
            PostulacionExtension pe;
            try
            {

                idConvocatoria = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"]);
                pe = new PostulacionExtension(idConvocatoria);
                ViewBag.PE = pe;
            }
            catch (Exception e)
            {
                ViewBag.error = e.Message;
            }

            return View("Postulaciones");
        }
        public ActionResult Espera()
        {
            return View();
        }
        public ActionResult IngresoDatos()
        {
            return View();
        }
        public ActionResult TerminoProceso()
        {
            return View();
        }

        public ActionResult Etapa()
        {

            if (User.IsInRole("Usuario")  || User.IsInRole("Admin") || User.IsInRole("SuperAdmin"))
            {
                ViewBag.paginaAnterior = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : "";

                try
                {
                    int idPostulacion = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"]);
                    CON_postulaciones postulacion = new CON_postulaciones();
                    postulacion = PostulacionExtension.consultarPostulacionesId(idPostulacion);
                    string[] aux;
                    string fecha = "";
                    string hora = "";
                    string lugar = "";
                    string vista = "Espera";

                    if (postulacion.idEtapa < postulacion.CON_convocatorias.idEtapa)
                        vista = "TerminoProceso";

                    else if (postulacion.idEtapa >= postulacion.CON_convocatorias.idEtapa)
                    {

                        switch (postulacion.idEtapa)
                        {
                            case (int)IndiceEtapasConvocatoria.ModeloNegocio:
                                vista = "IngresoDatos";
                                break;
                            case (int)IndiceEtapasConvocatoria.Documentos:
                                ViewBag.Proveedores = PostulacionExtension.GetProveedoresCapacitacion();
                                vista = "Documentos";
                                break;
                        }
                    }
                    ViewBag.postulacion = postulacion;

                    if (postulacion.fechaAgendarComite != null)
                    {
                        aux = postulacion.fechaAgendarComite.ToString().Split(' ');
                        fecha = aux[0];
                        hora = aux[1];
                        lugar = postulacion.lugarComite;
                    }
                    ViewBag.fechaComite = fecha;
                    ViewBag.horaComite = hora;
                    ViewBag.lugarComite = lugar;

                    if (postulacion.fechaAgendaPrefactibilidad != null)
                    {
                        aux = postulacion.fechaAgendaPrefactibilidad.ToString().Split(' ');
                        fecha = aux[0];
                        hora = aux[1];
                    }
                    lugar = postulacion.lugarPrefactibilidad;
                    ViewBag.fechaPrefactibilidad = fecha;
                    ViewBag.horaPrefactibilidad = hora;
                    ViewBag.lugarPrefactibilidad = lugar;
                    ViewBag.idEtapa = postulacion.idEtapa;

                    return View(vista);
                }
                catch (Exception e)
                {
                    ViewBag.error = e.Message;
                }
                return View("Error");
            }
            else
            {
                return Redirect(Url.Content("~/"));
            }
        }

        public JsonResult FiltrarPostulaciones(int idC, int idE)
        {
            Resultado r;
            PostulacionExtension pe;
            try
            {
                pe = new PostulacionExtension(idC, idE);
                r = new Resultado(true, pe.GetJsonPostulaciones());
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                r = new Resultado(false, e.Message);
            }


            return Json(r, JsonRequestBehavior.AllowGet);
        }

        public JsonResult consultarPostulacionesPersona()
        {
            Resultado resultado;
            List<PostulacionExtension> postulaciones = new List<PostulacionExtension>();
            try
            {
                var context2 = new ApplicationDbContext();
                var user = context2.Users.Where(u => u.UserName == User.Identity.Name).First();

                PersonasExtension persona = new PersonasExtension();
                ViewBag.persona = persona.consultarPersonaIdUsuario(user.Id, user.Email);

                postulaciones = PostulacionExtension.consultarPostulacionesPersona(ViewBag.persona.id);
                if (postulaciones.Count == 0)
                    resultado = new Resultado(true, 0);
                else

                    resultado = new Resultado(true, postulaciones);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult postular(int idConvocatoria, int idEmprendimiento)
        {
            Resultado resultado;
            PostulacionExtension postulacion;
            try
            {
                postulacion = new PostulacionExtension();
                postulacion.postular(idConvocatoria, idEmprendimiento);
                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult consultarResumenInversion(int idPostulacion)
        {
            Resultado resultado;
            try
            {

                resultado = new Resultado(true, PostulacionExtension.consultarResumenInversion(idPostulacion));
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult terminarEtapaPostulacion(int idPostulacion)
        {
            Resultado resultado;

            try
            {
                PostulacionExtension.terminarEtapaPostulacion(idPostulacion);
                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubirArchivoDocumentos()
        {
            Resultado rs;
            int idPostulante, idArchivoCargado;
            string descripcion, descripcionArchivo;
            HttpPostedFileBase archivo = null;
            try
            {
                idArchivoCargado = Convert.ToInt32(Request.Form["idAC"]);
                descripcionArchivo = Request.Form["descripcionArchivo"];
                idPostulante = Convert.ToInt32(Request.Form["idP"]);
                descripcion = Request.Form["descripcion"];
                archivo = Request.Files["archivo"];

                PostulacionExtension.SubirArchivoDatos(idArchivoCargado, idPostulante, descripcionArchivo, descripcion, archivo);
                rs = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                rs = new Resultado(false, e.Message);
            }
            return Json(rs);
        }

        public JsonResult GetAllArchivos(int idP)
        {
            Resultado resultado;
            List<VtcArchivos.Model.ARC_Archivos> archivos;
            List<int> tipos;
            string msg = "";

            try
            {
                tipos = new List<int>() { (int)TipoArchivos.ArchivoPrefactibilidad, (int)TipoArchivos.ArchivoComite, (int)TipoArchivos.ArchivoCapacitacion, (int)TipoArchivos.ArchivoPropuestaAprobada, (int)TipoArchivos.ArchivoContratoAceptado, };
                archivos = VtcArchivos.Funciones.GetArchivos(idP, tipos, ref msg);
                if (msg != string.Empty) throw new Exception(msg);
                resultado = new Resultado(true, archivos);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        #region reportes
        public ActionResult DescargarMedioDeAprobacion(int idPostulacion)
        {
            try
            {
                var context = new ApplicationDbContext();
                var user = context.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();


                string pathDescarga = procesaReporte(idPostulacion, reportes.medioDeAprobacion, user);
                byte[] FileContents = System.IO.File.ReadAllBytes(pathDescarga);
                string wordContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

                return new FileContentResult(FileContents, wordContentType);
            }
            catch (Exception e)
            {
                return new HttpNotFoundResult(e.Message);
            }
        }
        public ActionResult DescargarContrato(int idPostulacion)
        {
            try
            {
                var context = new ApplicationDbContext();
                var user = context.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();

                string pathDescarga = procesaReporte(idPostulacion, reportes.Contrato, user);
                byte[] FileContents = System.IO.File.ReadAllBytes(pathDescarga);
                string wordContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

                return new FileContentResult(FileContents, wordContentType);
            }
            catch (Exception e)
            {
                return new HttpNotFoundResult(e.Message);
            }
        }
        public ActionResult DescargarDiagnosticoEmprendedor(int idPostulacion)
        {
            try
            {
                var context = new ApplicationDbContext();
                var user = context.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();

                string pathDescarga = procesaReporte(idPostulacion, reportes.diagnosticoEmprendedor, user);
                byte[] FileContents = System.IO.File.ReadAllBytes(pathDescarga);
                string wordContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

                return new FileContentResult(FileContents, wordContentType);

            }
            catch (Exception e)
            {
                return new HttpNotFoundResult(e.Message);
            }
        }
        public ActionResult DescargarBriefEmprendedor(int idPostulacion)
        {
            try
            {
                var context = new ApplicationDbContext();
                var user = context.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();

                string pathDescarga = procesaReporte(idPostulacion, reportes.brief, user);
                byte[] FileContents = System.IO.File.ReadAllBytes(pathDescarga);
                string wordContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

                return new FileContentResult(FileContents, wordContentType);
            }
            catch (Exception e)
            {
                return new HttpNotFoundResult(e.Message);
            }
        }
        public ActionResult DescargarComitePostulacion(int idComitePostulacion)
        {
            try
            {
                var context = new ApplicationDbContext();
                var user = context.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();

                string pathDescarga = procesaReporte(0, reportes.comite, user, idComitePostulacion);
                byte[] FileContents = System.IO.File.ReadAllBytes(pathDescarga);
                string wordContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

                return new FileContentResult(FileContents, wordContentType);
            }
            catch (Exception e)
            {
                return new HttpNotFoundResult(e.Message);
            }
        }
        
        string procesaReporte(int idPostulacion, reportes tipoReporte, ApplicationUser user, int idComitePostulacion = 0)
        {
            try
            {
                using (var db = new emprendeEntities())
                {
                    db.Configuration.LazyLoadingEnabled = true;

                    MIS_Personas ascesor = null;
                    if (user != null) ascesor = db.MIS_Personas.Where(a => a.id == user.idPersona).FirstOrDefault();

                    WordManager wordManager = new WordManager(db, idPostulacion, tipoReporte, ascesor, idComitePostulacion);
                    try
                    {
                        wordManager.procesaReporte();

                        wordManager.guardarCopy();
                        wordManager.cerrarDocXml();
                        return wordManager.pathDescarga;
                    }
                    catch (Exception e)
                    {
                        wordManager.cerrarDocXml();
                        throw new Exception("Error: " + e.Message);
                    }
                }
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                throw new Exception("procesa Reporte : " + e.Message);
            }
        }
        #endregion

        public JsonResult getIndicadores(int idPostulacion)
        {
            Resultado resultado;

            try
            {
                using (var db = new emprendeEntities())
                {
                    db.Configuration.LazyLoadingEnabled = true;

                    string sql = string.Format("CON_SP_getIndicadoresPostulacion {0}", idPostulacion);
                    List<SEG_IndicadoresEmprende> indicadores = db.Database.SqlQuery<SEG_IndicadoresEmprende>(sql).ToList();
                    resultado = new Resultado(true, indicadores);
                }
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}