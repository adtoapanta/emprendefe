﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emprende.Extensions;

namespace Emprende.Controllers
{
    public class DireccionesController : Controller
    {
        // GET: Direcciones
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult agregarLocalizacionIdPostulacion(int idPostulacion, decimal latitud, decimal longitud)
        {
            Resultado resultado;            
            try
            {
                DireccionesExtension.agregarLocalizacionIdPostulacion(idPostulacion, latitud, longitud);
                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }


    }
}