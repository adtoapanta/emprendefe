﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emprende.Extensions;

namespace Emprende.Controllers
{
    public class MatrizInversionController : Controller
    {
        // GET: MatrizInversion
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult agregarItemInversion(int idPostulacion, int prioridad, string recurso, string justificacion, decimal precioUnitario, int cantidad, decimal total, decimal aportePropio, decimal aporteInversion, int idTipoInversion)
        {
            Resultado resultado;
            MatrizInversionExtension item;
            try
            {
                item = new MatrizInversionExtension();
                item.agregarItemInversion(idPostulacion, prioridad, recurso, justificacion, precioUnitario, cantidad, total, aportePropio,  aporteInversion, idTipoInversion);
                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult editarItemInversion(int idItem, int prioridad, string recurso, string justificacion, decimal precioUnitario, int cantidad, decimal total, decimal aportePropio, decimal aporteInversion, int idTipoInversion)
        {
            Resultado resultado;
            MatrizInversionExtension item;
            try
            {
                item = new MatrizInversionExtension();
                item.editarItemInversion(idItem, prioridad, recurso, justificacion, precioUnitario, cantidad, total, aportePropio, aporteInversion, idTipoInversion);
                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult eliminarItemInversion(int idItemInversion)
        {
            Resultado resultado;
            MatrizInversionExtension item;
            try
            {
                item = new MatrizInversionExtension();
                item.eliminarItemInversion(idItemInversion);
                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult consultarMatrizInversion(int idPostulacion)
        {
            Resultado resultado;
            
            try
            {                
                resultado = new Resultado(true, MatrizInversionExtension.consultarMatrizInversion(idPostulacion));
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult consultarItemInversion(int idItemInversion)
        {
            Resultado resultado;

            try
            {
                resultado = new Resultado(true, MatrizInversionExtension.consultarItemInversion(idItemInversion));
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult guardarResumenInversion(int idPostulacion, string resumen, decimal totalInversion)
        {
            Resultado resultado;

            try
            {
                MatrizInversionExtension.guardarResumenInversion(idPostulacion, resumen, totalInversion);
                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}