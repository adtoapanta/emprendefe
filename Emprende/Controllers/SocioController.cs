﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emprende.Extensions;

namespace Emprende.Controllers
{
    public class SocioController : Controller
    {
        // GET: Socio
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EditarSocios()
        {
            int idEmprendimiento = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"].ToString());
            ViewBag.idEmprendimiento = idEmprendimiento;
            return View();
        }

        public JsonResult agregarSocioEmprendimiento(int idEmprendimiento, string nombre, string telefono, string cargo, string aporte, int porcentaje)
        {
            Resultado resultado;
            SociosExtension socio;
            try
            {
                socio = new SociosExtension();
                socio.agregarSocioEmprendimiento(idEmprendimiento, nombre, telefono, cargo, aporte, porcentaje);
                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult editarSocioEmprendimiento(int idSocio, string nombre, string telefono, string cargo, string aporte, int porcentaje)
        {
            Resultado resultado;
            SociosExtension socio;
            try
            {
                socio = new SociosExtension();
                socio.editarSocioEmprendimiento(idSocio, nombre, telefono, cargo, aporte, porcentaje);
                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult eliminarSocio(int idSocio)
        {
            Resultado resultado;            
            try
            {
                SociosExtension.eliminarSocio(idSocio);
                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult consultarSociosEmprendimiento(int idEmprendimiento)
        {
            Resultado resultado;
            
            try
            {
                
                resultado = new Resultado(true, SociosExtension.consultarSociosEmprendimiento(idEmprendimiento));
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}