﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emprende.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Emprende.Models;

namespace Emprende.Controllers
{
    public class PersonaController : Controller
    {
        // GET: Persona
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Dashboard()
        {

            int idUsuario = User.Identity.GetUserId<int>();
            string correo = User.Identity.GetUserName();

            if (idUsuario == 0) // no esta logeado
            {
                return RedirectToAction("Login", "Account");
            }
            else // esta loggeado
            {
                PersonasExtension persona = new PersonasExtension();
                MIS_Personas p = persona.consultarPersonaIdUsuario(idUsuario, correo);
                //p.datosCompletos = true; //todo quitar esto parche Fer
                ViewBag.persona = p;

                if (!ViewBag.persona.datosCompletos)
                {
                    return View("Editar");
                }
                else
                {
                    ViewBag.encabezado = "Bienvenido: " + ViewBag.persona.nombre + " " + ViewBag.persona.apellido;
                    return View();
                }
            }


        }
        public ActionResult Editar()
        {
            int idUsuario = User.Identity.GetUserId<int>();
            string correo = User.Identity.GetUserName();
            PersonasExtension persona = new PersonasExtension();
            MIS_Personas p = persona.consultarPersonaIdUsuario(idUsuario, correo);
            //p.datosCompletos = true; //todo quitar esto parche Fer
            ViewBag.persona = p;

            return View();
        }
        public JsonResult crearPersona(int idUsuario, string correo)
        {
            Resultado resultado;
            try
            {
                PersonasExtension persona = new PersonasExtension();
                persona.CrearPersona(idUsuario, correo);
                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Persona" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }
        public JsonResult consultarPersonaCompleta()
        {
            Resultado resultado;
            try
            {
                var context2 = new ApplicationDbContext();
                var user = context2.Users.Where(u => u.UserName == User.Identity.Name).First();

                PersonasExtension persona = new PersonasExtension();
                ViewBag.persona = persona.consultarPersonaIdUsuario(user.Id, user.Email);
                resultado = new Resultado(true, PersonasExtension.consultarPersonaCompleta(ViewBag.persona.id));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Persona" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }
        public JsonResult completarDatosGenerales
            (string cedula, string genero, string nombre1, string nombre2, string apellido1, string apellido2,
            string telefono, string celular, int nacionalidad, int estadoCivil, MIS_Direcciones direccion, int idTipoDocumento, DateTime fechaNacimiento, int idNivelEducacion,
            string nombre1Conyuge, string nombre2Conyuge, string apellido1Conyuge, string apellido2Conyuge, int idNacionalidadConyuge, DateTime fechaNacimientoConyuge, string documentoConyuge,
            int idConyuge
            )
        {
            Resultado resultado;
            try
            {
                var context2 = new ApplicationDbContext();
                var user = context2.Users.Where(u => u.UserName == User.Identity.Name).First();

                PersonasExtension persona = new PersonasExtension();
                ViewBag.persona = persona.consultarPersonaIdUsuario(user.Id, user.Email);
                PersonasExtension.completarDatosGenerales(ViewBag.persona.id, cedula, genero, nombre1, nombre2, apellido1, apellido2, telefono, celular, nacionalidad, estadoCivil, direccion, idTipoDocumento, fechaNacimiento, idNivelEducacion, nombre1Conyuge, nombre2Conyuge, apellido1Conyuge, apellido2Conyuge, idNacionalidadConyuge, fechaNacimientoConyuge, documentoConyuge, idConyuge);

                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Persona" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }

        public JsonResult consultarDireccionPersona()
        {
            Resultado resultado;
            try
            {
                var context2 = new ApplicationDbContext();
                var user = context2.Users.Where(u => u.UserName == User.Identity.Name).First();

                PersonasExtension persona = new PersonasExtension();
                ViewBag.persona = persona.consultarPersonaIdUsuario(user.Id, user.Email);

                resultado = new Resultado(true, PersonasExtension.consultarDireccionPersona(ViewBag.persona.id));
            }
            catch (Exception e)
            {
                Logger.Write("Error en crear Persona" + e.Message);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }
    }
}