﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emprende.Extensions;

namespace Emprende.Controllers
{
    public class PaisController : Controller
    {
        // GET: Pais
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult consultarPaises()
        {
            Resultado resultado;

            try
            {
                resultado = new Resultado(true, PaisExtension.consultarPaises());
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}