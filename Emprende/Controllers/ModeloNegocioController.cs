﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emprende.Extensions;
namespace Emprende.Controllers
{
    public class ModeloNegocioController : Controller
    {
        // GET: ModeloNegocio
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult consultarPreguntasCanvas()
        {
            Resultado resultado;

            try
            {
                resultado = new Resultado(true, ModeloNegocioExtension.consultarPreguntasCanvas());
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult actualizaCanvasEmprendimiento(int idEmprendimiento, int idPregunta, string respuesta)
        {
            Resultado resultado;

            try
            {
                ModeloNegocioExtension.actualizaCanvasEmprendimiento(idEmprendimiento, idPregunta, respuesta);
                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult consultarCanvasEmprendimiento(int idEmprendimiento)
        {
            Resultado resultado;

            try
            {
                
                resultado = new Resultado(true, ModeloNegocioExtension.consultarCanvasEmprendimiento(idEmprendimiento));
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}