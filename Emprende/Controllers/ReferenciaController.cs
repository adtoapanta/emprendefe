﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emprende.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Emprende.Models;

namespace Emprende.Controllers
{
    public class ReferenciaController : Controller
    {
        // GET: Referencia
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult guardarReferenciaPersona(int idParentesco, string nombre, string direccion, string telefono)
        {
            Resultado resultado;            
            try
            {
                var context2 = new ApplicationDbContext();
                var user = context2.Users.Where(u => u.UserName == User.Identity.Name).First();

                PersonasExtension persona = new PersonasExtension();
                ViewBag.persona = persona.consultarPersonaIdUsuario(user.Id, user.Email);

                resultado = new Resultado(true, ReferenciasExtension.guardarReferenciaPersona(ViewBag.persona.id,idParentesco,nombre,direccion,telefono));
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }


            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult editarReferenciaPersona(int idReferencia, int idParentesco, string nombre, string direccion, string telefono)
        {
            Resultado resultado;
            try
            {
                
                resultado = new Resultado(true, ReferenciasExtension.editarReferenciaPersona(idReferencia, idParentesco, nombre, direccion, telefono));
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult consultarReferenciasPersona()
        {
            Resultado resultado;
            try
            {
                var context2 = new ApplicationDbContext();
                var user = context2.Users.Where(u => u.UserName == User.Identity.Name).First();
                PersonasExtension persona = new PersonasExtension();
                ViewBag.persona = persona.consultarPersonaIdUsuario(user.Id, user.Email);

                resultado = new Resultado(true, ReferenciasExtension.consultarReferenciasPersona(ViewBag.persona.id));
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult consultarReferencia(int id)
        {
            Resultado resultado;
            try
            {
                
                resultado = new Resultado(true, ReferenciasExtension.consultarReferencia(id));
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult eliminarReferencia(int id)
        {
            Resultado resultado;
            try
            {
                ReferenciasExtension.eliminarReferencia(id);
                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Funciones.WriteLog(e);
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}