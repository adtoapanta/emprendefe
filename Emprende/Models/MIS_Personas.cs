//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Emprende.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MIS_Personas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MIS_Personas()
        {
            this.Emprendimientos = new HashSet<Emprendimientos>();
            this.MIS_Personas1 = new HashSet<MIS_Personas>();
            this.Mis_referencias = new HashSet<Mis_referencias>();
        }
    
        public int id { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string cedula { get; set; }
        public string telefono { get; set; }
        public string sexo { get; set; }
        public string correo { get; set; }
        public string nombre2 { get; set; }
        public string apellido2 { get; set; }
        public string celular { get; set; }
        public Nullable<int> idPais { get; set; }
        public Nullable<int> idEstadoCivil { get; set; }
        public Nullable<int> idUsuario { get; set; }
        public Nullable<int> idConyuge { get; set; }
        public Nullable<System.DateTime> fechaNacimiento { get; set; }
        public Nullable<int> idInstruccionAcademica { get; set; }
        public string lugarTrabajo { get; set; }
        public Nullable<int> antiguedadTrabajo { get; set; }
        public string cargoTrabajo { get; set; }
        public Nullable<bool> datosCompletos { get; set; }
        public Nullable<int> idDireccion { get; set; }
        public Nullable<int> idTipoDocumento { get; set; }
        public Nullable<int> idNivelEducacion { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Emprendimientos> Emprendimientos { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MIS_Personas> MIS_Personas1 { get; set; }
        public virtual MIS_Personas MIS_Personas2 { get; set; }
        public virtual estadoCivil estadoCivil { get; set; }
        public virtual Instruccion Instruccion { get; set; }
        public virtual Pais Pais { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Mis_referencias> Mis_referencias { get; set; }
        public virtual MIS_Direcciones MIS_Direcciones { get; set; }
    }
}
