﻿using Spire.Xls;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace Emprende.Models
{
    public class ExcelManager
    {
        Worksheet sheet;

        public ExcelManager(System.IO.Stream stream)
        {
            //Create a new workbook
            Workbook workbook = new Workbook();
            //Load a file and imports its data
            workbook.LoadFromStream(stream);
            //Initialize worksheet
            sheet = workbook.Worksheets[0]; //tomo la primera fila
        }

        public void recorrerArchivo(DateTime fecha)
        {
            CellRange[] filas = sheet.Rows;
            int rowsCount = sheet.Rows.Count(); //este siempre debería ser 18
            bool puedoGuardar = true;

            try
            {
                using (var db = new emprendeEntities())
                {
                    for (int i = 8; i < rowsCount; i++) //desde la 8 porque inicia en 0?
                    {
                        CellRange filaActual = filas[i];

                        SEG_IndicadoresEmprende indicador = new SEG_IndicadoresEmprende();
                        indicador.fecha = fecha;

                        for (int j = 1; j < filaActual.ColumnCount; j++)
                        {
                            CellRange Celda = filaActual.Columns[j];
                            var valor = Celda.Value;

                            //si la cedula está vacía me salto la fila 
                            if (j == 1 && valor == "")
                            {
                                puedoGuardar = false;
                                break;
                            }

                            switch (j)
                            {
                                case 1:
                                    indicador.cedula = valor;
                                    break;
                                case 2:
                                    indicador.numeroTrabajadores = Convert.ToInt32(valor);
                                    break;
                                case 3:
                                    indicador.ventasDolares = Convert.ToDecimal(valor);
                                    break;
                                case 4:
                                    indicador.ventasVolumen = Convert.ToDecimal(valor);
                                    break;
                                case 5:
                                    indicador.costoVariable = Convert.ToDecimal(valor);
                                    break;
                                case 6:
                                    indicador.costoFijo = Convert.ToDecimal(valor);
                                    break;
                                case 7:
                                    indicador.utilidadOperativa = Convert.ToInt32(valor);
                                    break;
                                case 8:
                                    indicador.margenContribucion = Convert.ToDecimal(valor);
                                    break;
                                case 9:
                                    indicador.utilidadNeta = Convert.ToDecimal(valor);
                                    break;
                                case 10:
                                    indicador.capitalTrabajo = Convert.ToDecimal(valor);
                                    break;
                            }
                        }

                        if (puedoGuardar) db.SEG_IndicadoresEmprende.Add(indicador);
                        
                        puedoGuardar = true;
                    }

                    db.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex)
            {
                string errorCompleto = "";

                foreach (DbEntityValidationResult item in ex.EntityValidationErrors)
                {
                    // Get entry
                    DbEntityEntry entry = item.Entry;
                    string entityTypeName = entry.Entity.GetType().Name;

                    // Display or log error messages
                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",
                                 subItem.ErrorMessage, entityTypeName, subItem.PropertyName);
                        errorCompleto += "/n" + message;
                    }
                }

                throw new Exception("DbEntityValidationException al guardar: " + errorCompleto);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}