﻿using Emprende.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Emprende.Models
{
    #region Partials
    public partial class CON_convocatorias
    {
        [NotMapped]
        public List<CON_requisitos> _requisitos;

        public void CargarRequisitos(Models.emprendeEntities baseE)
        {
            CON_requisitos req;

            //baseE.CON_convocatorias.Attach(this);
            baseE.Entry(this).Collection(p => p.CON_requisitosConvocatoria).Load();
            _requisitos = baseE.CON_requisitos.OrderBy(r => r.orden).ToList();
            foreach (var r in this.CON_requisitosConvocatoria)
            {
                req = _requisitos.Where(x => x.id == (r.idRequisito ?? 0)).FirstOrDefault();
                req._seleccionado = req != null;
            }

        }

        public void SetRequisitos(Models.emprendeEntities baseE)
        {
            CON_requisitosConvocatoria rc;
            foreach (var req in _requisitos)
            {
                rc = baseE.CON_requisitosConvocatoria.Where(x => x.idConvocatoria == this.id && x.idRequisito == req.id).FirstOrDefault();
                if (rc == null && req._seleccionado)
                {
                    baseE.CON_requisitosConvocatoria.Add(new CON_requisitosConvocatoria() { idConvocatoria = this.id, idRequisito = req.id, MINIMOS = "" });
                }
                if (rc != null && !req._seleccionado)
                {
                    baseE.CON_requisitosConvocatoria.Remove(rc);
                }
            }
        }


        public void CambiarEtapa(Models.emprendeEntities baseE)
        {
            List<CON_postulaciones> pos;
            IndiceEtapasConvocatoria siguienteEtapa = Funciones.GetNextEtapa((int)this.idEtapa);
            BitacoraEtapasPostulacion bitacora;

            bool saltarPrefactibilidad = false;
            if (this.idEtapa == (int)IndiceEtapasConvocatoria.ModeloNegocio)
                saltarPrefactibilidad = baseE.CON_requisitosConvocatoria.Where(x => x.idConvocatoria == this.id && x.idRequisito == 7).Count() == 0;//7 = Prefactibilidad como requisito
            siguienteEtapa = Funciones.GetNextEtapa((int)this.idEtapa, saltarPrefactibilidad);

            //Casos especiales de paso de 
            switch (this.idEtapa)
            {
                case (int)IndiceEtapasConvocatoria.Elegibilidad:
                    Extensions.Mail.EligibilidadMail em;
                    em = new Extensions.Mail.EligibilidadMail(this.id,baseE);
                    em.EnviarCorreo();
                    break;
                case ((int)IndiceEtapasConvocatoria.AgendarPrefactibilidad):
                    //Envio de correos a los postulanyes
                    break;
            }

            //Cambiamos a todas las postulaciones de etapa
            pos = baseE.CON_postulaciones.Where(x => x.idConvocatoria == this.id && x.idEtapa == this.idEtapa && x.revisadoEtapa == true && x.apruebaEtapa == true).ToList();
            if (pos.Count > 0)
            {
                pos.All(c => { c.idEtapa = (int)siguienteEtapa; c.revisadoEtapa = false; c.apruebaEtapa = false; return true; });
                foreach (CON_postulaciones pstlcn in pos)
                {
                    bitacora = new BitacoraEtapasPostulacion() { idUsuario = 0, idEtapa = (int)pstlcn.idEtapa, fecha = DateTime.Now, idPostulacion = pstlcn.id, comentario = "Cambio de etapa" };
                    baseE.BitacoraEtapasPostulacion.Add(bitacora);
                }
            }

            this.Etapas = null;
            this.idEtapa = (int)siguienteEtapa;
            baseE.Entry(this).State = System.Data.Entity.EntityState.Modified;
        }
    }

    /// <summary>
    /// Se usa para mostrar los requisitos al editar la convocatoria
    /// </summary>
    public partial class CON_requisitos
    {
        [NotMapped]
        public bool _seleccionado;
    }

    public partial class SCO_Preguntas
    {
        [NotMapped]
        public bool _seleccionado;
    }
    public partial class SCO_PosiblesRespuestas
    {
        [NotMapped]
        public bool _seleccionado;
    }

    /// <summary>
    /// Se usa para mostrar la convocatoria con la etapa, se usa la vista VW_ConvocatoriasDetalle
    /// </summary>
    public class ConvocatoriaDetalle : CON_convocatorias
    {
        public string etapa { get; set; }

        public ConvocatoriaDetalle()
        {
        }
    }

    public partial class CON_propuestaObjetivos
    {
        public List<Recurso> GetRecursos()
        {
            List<Recurso> recursos_;
            if (string.IsNullOrEmpty(recursos))
                return new List<Recurso>();
            recursos_ = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Recurso>>(recursos);
            return recursos_;
        }
    }

    /// <summary>
    /// Para las funciones de costos y ventas actuales y proyectados 
    /// </summary>
    public partial class Emprendimientos
    {
        public decimal getTotalCostosAct()
        {
            return this.unidadesVentaActual ?? 0 * this.CostoPromedioPonderado ?? 0;
        }
        public decimal getTotalVentasAct()
        {
            return this.unidadesVentaActual ?? 0 * this.PrecioPromedioPonderado ?? 0;
        }
        public decimal getTotalCostosProy()
        {
            return this.unidadesVentaProy ?? 0 * this.CostoPromedioPonderado ?? 0;
        }
        public decimal getTotalVentasProy()
        {
            return this.unidadesVentaProy ?? 0 * this.PrecioPromedioPonderado ?? 0;
        }

        internal string nombreTipoLugar()
        {
            tiposLugar tipoLugar = (tiposLugar)Enum.Parse(typeof(tiposLugar), this.idTipoLugar.ToString());
            return tipoLugar.ToString();
        }

        internal MIS_Direcciones objDireccion()
        {
            MIS_Direcciones direccion = this.MIS_Direcciones;
            return direccion;
        }

        internal string direccion()
        {
            return objDireccion().nombre;
        }

        internal string barrio()
        {
            return objDireccion().barrio;
        }

        internal string sector()
        {
            return ""; //objDireccion().sector;
        }

        internal string ciudad()
        {
            return objDireccion().Parroquias.Ciudades.nombre;
        }
    }

    public partial class MIS_Personas
    {
        MIS_Direcciones _objDireccion;
        MIS_Personas _conyugue;

        public string getNombreCompleto()
        {
            return string.Format("{0} {1} {2} {3}", nombre, nombre2, apellido, apellido2);
        }

        public string getEstadoCivil()
        {
            if (this.idEstadoCivil != null)
            {
                enumEstadoCivilEmprendedor estadoCivil = (enumEstadoCivilEmprendedor)Enum.Parse(typeof(enumEstadoCivilEmprendedor), idEstadoCivil.ToString());
                return estadoCivil.ToString();
            }
            else return "";
        }

        public string getInstruccion()
        {
            if (this.idInstruccionAcademica != null)
            {
                enumInstruccion instruccion = (enumInstruccion)Enum.Parse(typeof(enumInstruccion), idInstruccionAcademica.ToString());
                return instruccion.ToString();
            }
            else return "";
        }

        public string getSexo()
        {
            if (this.sexo == "M") return "Masculino"; else return "Femenino";
        }

        public string getNacionalidad()
        {
            string nac = "";
            int idP = this.idPais ?? 0;

            switch (idP)
            {
                case 0:
                    nac = "No Definida";
                    break;
                case 1:
                    nac = "Ecuatoriana";
                    break;
                default:
                    nac = "Otra";
                    break;
            }
            return nac;
        }
        public MIS_Direcciones objDireccion()
        {
            if (_objDireccion == null)
            {
                _objDireccion = this.MIS_Direcciones;
            }

            return _objDireccion;
        }

        //conyuge
        public MIS_Personas objConyuge()
        {
            int idConyuge = this.idConyuge ?? 0;

            if (idConyuge != 0)
            {
                if (_conyugue == null)
                {
                    _conyugue = this.MIS_Personas1.First();
                }
            }

            // si idconyuge es 0, retorna null
            return _conyugue;
        }
        public string getNombreCompletoC()
        {
            string nombreConyuge = "";
            if (objConyuge() != null)
            {
                nombreConyuge = string.Format("{0} {1} {2} {3}", _conyugue.nombre, _conyugue.nombre2, _conyugue.apellido, _conyugue.apellido2);
            }

            return nombreConyuge;
        }
        public string getCiC()
        {
            string retorno = "";
            if (objConyuge() != null)
            {
                retorno = _conyugue.cedula;
            }
            return retorno;
        }
        public string getNacionalidadC()
        {
            string retorno = "";
            if (objConyuge() != null)
            {
                retorno = _conyugue.getNacionalidad();
            }
            return retorno;
        }
        public string gettxtFechaNacC()
        {
            string retorno = "";
            if (objConyuge() != null)
            {
                //todo aquí poner formato a la fecha 
                retorno = _conyugue.fechaNacimiento.ToString();
            }
            return retorno;
        }
        public string getTelefonoC()
        {
            string retorno = "";
            if (objConyuge() != null)
            {
                retorno = _conyugue.telefono;
            }
            return retorno;
        }
    }

    public partial class MIS_Direcciones
    {
        public string direccion()
        {
            string dir = "";
            if (linea2 != null)
            {
                dir = string.Format("{0} {1} y {2}", linea1, numero, linea2);
            }
            else
            {
                dir = string.Format("{0} {1}", linea1, numero);
            }

            return dir;
        }

        public string ciudad()
        {
            return Parroquias.Ciudades.nombre;
        }
        public string provincia()
        {
            return Parroquias.Ciudades.Provincias.nombre;
        }
    }

    public partial class CON_postulaciones
    {
        internal decimal PorcentajeAporteEmprendedor()
        {
            decimal invPropia = inversionPropia ?? 00;
            decimal totInversion = totalInversion ?? 00;
            decimal PorcentajeAporteEmp = 0;
            if (totInversion != 0) PorcentajeAporteEmp = invPropia / totInversion;

            return PorcentajeAporteEmp;
        }
    }

    public partial class CON_propuestas
    {
        public decimal aporteEmprendedor()
        {
            decimal tot = total ?? 0;
            decimal tas = tasa ?? 0;
            return (tot * tas) / 100;
        }
    }
    #endregion

    #region Otras
    /// <summary>
    /// Se usa para cargar VW_PostulantesDetalle
    /// </summary>
    public class PostulacionDetalle : CON_postulaciones
    {
        //Emprendimiento, emprendedor, estado, fechaEstado
        public string nombre { get; set; }
        public string emprendimiento { get; set; }
        public string emprendedor { get; set; }
        public string estadoEmprendimiento { get; set; }
        public string etapaPostulacion { get; set; }
        public string cedula { get; set; }
        public string correo { get; set; }
        public string telefono { get; set; }
        public string celular { get; set; }
        public string comite { get; set; }
        public string prefactibilidad { get; set; }
        public string usuarioAsignado{ get; set; }

        public PostulacionDetalle() { }
    }

    public class EtapasConvocatoria
    {
        public static string tabla = "VW_EtapasConvocatorias";

        public int id { get; set; }
        public string nombre { get; set; }

        public EtapasConvocatoria() { }
    }

    public class SP_PostulacionesMiembroComite
    {
        public int idPostulacion { get; set; }
        public int idComitePostulacion { get; set; }
        public string nombre { get; set; }
        public int idComiteMiembro { get; set; }
        public bool esCalificado { get; set; }
        public string convocatoria { get; set; }
        public int idConvocatoria { get; set; }
        public int idTipo { get; set; }

        public SP_PostulacionesMiembroComite() { }
    }

    public class MesAmortizacion
    {
        public int numero { get; set; }
        public DateTime? fecha { get; set; }
        public decimal saldo { get; set; }
        public decimal aporte { get; set; }
        public decimal capital { get; set; }
        public decimal reembolso { get; set; }

        public MesAmortizacion() { }
    }

    public class DatosAmortizacion
    {
        public decimal ApoyoEconomico { get; set; }
        public decimal AporteFondo { get; set; }
        public decimal TotalApoyo { get; set; }
        public int Tiempo { get; set; }
        public decimal Reembolso { get; set; }
        public DateTime FechaInicioPago { get; set; }
        public DatosAmortizacion() { }
    }

    public class DatosPropuesta
    {
        public decimal ValorProyecto { get; set; }
        public decimal Tpe { get; set; }
        public decimal InversionCrisfe { get; set; }
        public decimal Semilla { get; set; }
        public decimal Inversion { get; set; }
        public decimal InversionSugerida { get; set; }
        public decimal VentaEstimadaUnidades { get; set; }
        public decimal InversionVentasEstimadasUnidades { get; set; }
        public decimal PE2Unidades { get; set; }
        public decimal PE2Dolar { get; set; }
        public decimal PE2Meses { get; set; }
        public decimal PrecioPonderado { get; set; }
        public decimal CostoPonderado { get; set; }
        public decimal VentasMensuales { get; set; }
        public decimal VentasEstimadasUnidades { get; set; }
        public decimal CostoFijo { get; set; }
        public DatosPropuesta() { }
    }
    public class InformacionPropuesta
    {
        public DatosPropuesta DatosPropuesta { get; set; }
        public DatosAmortizacion AmortizacionCalculada { get; set; }
        public List<MesAmortizacion> ListaAmortizacion { get; set; }
        public List<CON_propuestaObjetivos> Objetivos { get; set; }
        public CON_propuestas Propuesta { get; set; }
        public string ObjetivosJson { get; set; }
        public InformacionPropuesta()
        {
        }
    }

    public class Recurso
    {
        public string nombre { get; set; }
        public decimal valor { get; set; }
        public Recurso() { }
    }

    public class ComiteCalificacion
    {
        public int idComitePostulacion { get; set; }
        public int calificacion { get; set; }
        public string nombre { get; set; }
        public string observacion { get; set; }

        public ComiteCalificacion() { }
    }
    public class ComiteEvaluacion
    {
        public decimal calificacion { get; set; }
        public bool aprueba { get; set; }
        public List<ComiteCalificacion> misCalificaciones { get; set; }

        private int idPostulacion { get; set; }
        private int idTipo { get; set; }
        private int minimoAprobacion { get; set; }
        //x debe ser un objeto que tiene nombre, calificacion

        public ComiteEvaluacion(int idPostulacion, int idTipo)
        {
            this.idPostulacion = idPostulacion;
            this.idTipo = idTipo;
            this.minimoAprobacion = Convert.ToInt32(ConfigurationManager.AppSettings["CalificacionMinComite"]);
            this.Cargar();
        }

        private void Cargar()
        {
            List<CON_comitePostulaciones> comitePostulaciones;
            int sum = 0;
            int cuenta = 0;

            //cargo mis calificaciones
            misCalificaciones = new List<ComiteCalificacion>();
            using (var baseE = new emprendeEntities())
            {
                comitePostulaciones = baseE.CON_comitePostulaciones.Include("CON_comiteMiembros").Where(x => x.idPostulacion == idPostulacion && x.idTipo == idTipo).ToList();
            }
            foreach (CON_comitePostulaciones cp in comitePostulaciones)
            {
                misCalificaciones.Add(new ComiteCalificacion() { calificacion = cp.calificacion ?? 0, nombre = cp.CON_comiteMiembros.nombre, idComitePostulacion = cp.id, observacion = cp.observaciones });
                sum += cp.calificacion ?? 0;
                cuenta += 1;
            }

            if (cuenta > 0)
            {
                this.calificacion = cuenta == 0 ? 0 : (sum / cuenta);
                this.calificacion = Math.Round(calificacion, 2);
                this.aprueba = this.calificacion > this.minimoAprobacion;
            }
            else
            {
                this.calificacion = 0;
                this.aprueba = false;
            }

        }

    }

    public class RespuestasScoring
    {
        public int idref { get; set; }
        public string p1 { get; set; }
        public string p2 { get; set; }
        public string p3 { get; set; }
        public string p5 { get; set; }
        public string p66 { get; set; }
    }

    public class PreguntasComiteEvaluador
    {
        public int id { get; set; }
        public string pregunta { get; set; }
        public int idTipo { get; set; }
        public bool obligatorio { get; set; }
        public int idR { get; set; }
        public int idEvaluacion { get; set; }
        public int idPosibleRespuesta { get; set; }
        public decimal respuestaT { get; set; }
        public string respuesta { get; set; }
        public string obs { get; set; }
    }

    public class VW_UsuariosAdministradores {
        public int id { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
    }
    #endregion
}