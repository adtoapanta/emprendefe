//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Emprende.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class socios
    {
        public int id { get; set; }
        public Nullable<int> idEmprendimiento { get; set; }
        public string nombre { get; set; }
        public string telefono { get; set; }
        public string cargo { get; set; }
        public string aporte { get; set; }
        public Nullable<int> porcentaje { get; set; }
    
        public virtual Emprendimientos Emprendimientos { get; set; }
    }
}
