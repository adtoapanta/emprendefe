//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Emprende.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CON_propuestaObjetivos
    {
        public int id { get; set; }
        public int idPropuesta { get; set; }
        public string nombre { get; set; }
        public string recursos { get; set; }
        public Nullable<decimal> totalObjetivo { get; set; }
        public Nullable<int> mes { get; set; }
    
        public virtual CON_propuestas CON_propuestas { get; set; }
    }
}
