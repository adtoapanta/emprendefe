﻿using Emprende.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Emprende.Models
{
    public class PostulacionCompleta
    {
        public MIS_Personas emprendedor;
        public Emprendimientos emprendimiento;
        public List<CON_propuestas> propuestas;
        public CON_propuestas propuestaAprobada;
        public CON_propuestas ultimaPropuesta;
        public CON_convocatorias convocatoria;
        public CON_postulaciones postulacion;
        public CON_canvas miCanvas;
        public List<Productos> productos;
        public ComiteEvaluacion ce;
        public RespuestasScoring respuestasScoring;
        public SCO_Evaluaciones evaluacionScoring;
        public List<inversion> matrizInversion;

        emprendeEntities db;

        public PostulacionCompleta(emprendeEntities db, int idPostulacion)
        {
            this.db = db;

            postulacion = db.CON_postulaciones.Include("Emprendimientos").Include("Emprendimientos.MIS_Personas").Include("CON_ProveedoresCapacitacion").Where(p => p.id == idPostulacion).First();
            
            this.emprendimiento = postulacion.Emprendimientos;
            this.emprendedor = emprendimiento.MIS_Personas;
            this.propuestas = db.CON_propuestas.Include("CON_propuestaObjetivos").OrderByDescending(p => p.id).ToList();
            this.convocatoria = postulacion.CON_convocatorias;
            this.ultimaPropuesta = propuestas.FirstOrDefault();
            this.miCanvas = emprendimiento.CON_canvas.FirstOrDefault();
            this.productos = db.Productos.Include("Insumos").Where(p => p.idEmprendimiento == emprendimiento.id).ToList();

            //propuesta aprobada
            foreach (CON_propuestas propuesta in propuestas)
            {
                if (propuesta.id == postulacion.idPropuesta)
                {
                    propuestaAprobada = propuesta;
                }
            }

            //comité evaluación
            //tabla calificaciones
            ce = new ComiteEvaluacion(this.postulacion.id, (int)TiposComite.Comite);

            //respuestas scoring 
            //string sql = string.Format("SELECT * FROM SCO_FN_RespuestasC1() WHERE idref = {0}", emprendimiento.id);
            string sql = string.Format("SCO_SP_Respuesta {0}", emprendimiento.id);
            this.respuestasScoring = db.Database.SqlQuery<RespuestasScoring>(sql).FirstOrDefault();

            //evaluacion scoring
            this.evaluacionScoring = db.SCO_Evaluaciones.Where(e => e.idRef == idPostulacion).Where(e => e.idTipoRef == 2).First();

            this.matrizInversion = MatrizInversionExtension.consultarMatrizInversion(idPostulacion);
        }
    }
}