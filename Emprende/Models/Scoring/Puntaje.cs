﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using htcLib;

namespace Emprende.Models.Scoring
{
    public class Puntaje
    {
        public static string mitabla = "SCO_Puntajes"; // nombre de la tabla
        public long id; // identificador de la tabla
        public long idCuestionario; // Referencia al cuestionario que tiene el rango
        public long idPregunta; // Referencia a la Pregunta que le corresponde la posiblerespuesta
        public long idPosibleRespuesta; // referencia a la posible respuesta
        public decimal puntaje; // valor de la respuesta
        public decimal desde; // valor minimo de rango
        public decimal hasta; // valor maximo de rango
        public bool prohibe; // con true cuestionario invalido


        /// <summary>
        ///     ''' Constructor de rango
        ///     ''' </summary>
        ///     ''' <remarks></remarks>
        public Puntaje()
        {
        }

        /// <summary>
        ///     ''' Constructor que asigna valores al objeto puntaje dependiendo del id
        ///     ''' </summary>
        ///     ''' <param name="id">id del puntaje</param>
        ///     ''' <remarks></remarks>
        public Puntaje(long id)
        {
            string sql;
            sql = string.Format("select * from {0} where id={1}", mitabla, id);

            object a = this;
            LM.cargaObjeto(sql, ref (a));
        }


        public Puntaje(long idC, long idP, long idR)
        {
            string sql;
            sql = string.Format("select * from {0} where idCuestionario={1} and idPregunta={2} and idPosibleRespuesta={3}", mitabla, idC, idP, idR);

            object a = this;

            LM.cargaObjeto(sql, ref(a));
        }

        /// <summary>
        ///     ''' Guarda los valores asignados al objeto puntaje
        ///     ''' </summary>
        ///     ''' <remarks></remarks>
        public void guardar()
        {
            LM.GuardaObjeto(this);
        }

        public void eliminar()
        {
            string sql;
            sql = string.Format("delete from {0} where id={1}", mitabla, this.id);
            htcLib.espacio.ManejadorBD.ejecuta(sql);
        }
    }

}