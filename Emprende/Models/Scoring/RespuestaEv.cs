﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Emprende.Models.Scoring
{
    public class RespuestaEV
    {
        public int id { get; set; }
        public int idOpcion { get; set; }
        public int valor { get; set; }
        public String obs { get; set; }

        public void guardar(){
            string SQL = string.Format("UPDATE [SCO_RespuestasEvaluacion] set idPosibleRespuesta={0}, respuesta={1}, obs='{3}' WHERE id={2} ", this.idOpcion, this.valor, this.id, "" + this.obs);
            htcLib.espacio.ManejadorBD.ejecuta(SQL);
        }
    }
}