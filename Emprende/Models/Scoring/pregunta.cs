﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emprende.Models.Scoring;
using htcLib;
using System.Collections;
using System.Data;

namespace Emprende.Models.Scoring
{
    public enum TipoControlPregunta
    {
        numerico = 1,
        seleccion = 2
    }
    /// <summary>

    /// ''' Objeto, pregunta que pertenece a cuestionario

    /// ''' </summary>

    /// ''' <remarks></remarks>
    public class Pregunta
    {
        public static string mitabla = "SCO_Preguntas"; // nombre de la tabla
        public long id; // id de la pregunta
        public long idTipoPregunta; // id del Tipopregunta, socieconomica
        public long idTipo; // seleccion o numeros
        public string nombre; // pregunta
        public string funcion; // funcion para responder la pregunta
        public string parametro;
        public bool obligatorio;

        public string explicacion;
        public string fuente;


        [AtGuardable(lmTipoGuardo.lmNno)]
        public ArrayList MisPosiblesRespuestas; // las posibles respuestas a la pregunta
        [AtGuardable(lmTipoGuardo.lmNno)]
        public ArrayList MisPuntajes; // los puntajes de las respuestas cuando son numericas
        [AtGuardable(lmTipoGuardo.lmNno)]
        public DataTable dtMisPuntajes;

        /// <summary>
        ///     ''' Constructor de pregunta
        ///     ''' </summary>
        ///     ''' <remarks></remarks>
        public Pregunta()
        {
        }

        /// <summary>
        ///     ''' Asigna valores al objeto pregunta dependiendo del id
        ///     ''' </summary>
        ///     ''' <param name="id"></param>
        ///     ''' <remarks></remarks>
        public Pregunta(long id)
        {
            string sql;
            sql = string.Format("select * from {0} where id={1}", mitabla, id);

            object a = this;
            htcLib.LM.cargaObjeto(sql, ref(a));
        }

        /// <summary>
        ///     ''' Guarda los valores asignados a pregunta
        ///     ''' </summary>
        ///     ''' <remarks></remarks>
        public void guardar()
        {
            htcLib.LM.GuardaObjeto(this);
        }

        /// <summary>
        ///     ''' Carga el ArrayList respuestas de pregunta con Objetos respuesta solo de las que tienen un puntaje asignado
        ///     ''' </summary>
        ///     ''' <param name="idC">Id del cuestionario al que pertenece la respuesta</param>
        ///     ''' <remarks></remarks>
        public void CargarPosiblesRespuestas(long idC = 0)
        {
            string sql;

            if (idC > 0)
                sql = string.Format("select PR.* from {0} as PR inner join {1} as P on PR.id=P.idPosibleRespuesta where PR.idPregunta={3} and P.idCuestionario={2} and puntaje is not null", PosibleRespuesta.mitabla, Puntaje.mitabla, idC, this.id);
            else
                sql = string.Format("select PR.* from {0} as PR where PR.idPregunta={1}", PosibleRespuesta.mitabla, this.id);

            this.MisPosiblesRespuestas = LM.CargaAL(sql, new PosibleRespuesta());
        }


        /// <summary>
        ///     ''' La pregunta tiene que cargar los puntajes que recibio
        ///     ''' </summary>
        ///     ''' <param name="idC"></param>
        ///     ''' <remarks></remarks>
        public void CargarMisPuntajes(long idC)
        {
            string sql;
            sql = string.Format("select * from {0} where idCuestionario={1} and idPregunta={2} order BY puntaje", Puntaje.mitabla, idC, this.id);
            this.MisPuntajes = LM.CargaAL(sql, new Puntaje());
        }

        public void CargarDTMisPunt(long idC)
        {
            string sql;
            sql = string.Format("SELECT dbo.SCO_PosiblesRespuestas.nombre, dbo.SCO_Puntajes.* FROM dbo.SCO_PosiblesRespuestas RIGHT OUTER JOIN  dbo.SCO_Puntajes ON dbo.SCO_PosiblesRespuestas.id = dbo.SCO_Puntajes.idPosibleRespuesta where idCuestionario={1} and dbo.SCO_Puntajes.idPregunta={2}", Puntaje.mitabla, idC, this.id);
            this.dtMisPuntajes = htcLib.espacio.ManejadorBD.traetabla(sql);
        }

        public DataTable cargaRespuestas()
        {
            string sql;

            sql = string.Format("select * from SCO_PosiblesRespuestas where idPregunta={0}", this.id);
            return htcLib.espacio.ManejadorBD.traetabla(sql);
        }

        public static DataTable getPreguntasXTipo(long idTipoPregunta)
        {
            string sql;

            sql = string.Format("select * from SCO_Preguntas where idTipoPregunta={0}", idTipoPregunta);
            return htcLib.espacio.ManejadorBD.traetabla(sql);
        }

        public long buscaRespuesta(string valor)
        {
            foreach (PosibleRespuesta pr in this.MisPosiblesRespuestas)
            {
                if ( pr.nombre.Trim(' ').ToLower() == valor.Trim(' ').ToLower())
                    return pr.id;
            }

            return -1;
        }
    }

}