﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using htcLib;

namespace Emprende.Models.Scoring
{

    public class RespuestaEvaluacion
    {
        public static string mitabla = "SCO_RespuestasEvaluacion"; // nombre de la tabla
        public long id; // id de respuestaevaluacion
        public long idEvaluacion; // id evaluacion a la que pertenece
        public long idPregunta; // id de la pregunta
        public long idPosibleRespuesta; // id de la respuesa
        public decimal respuesta;  // la respuesta
        public decimal puntajeObtenido; // valor por la respuesta
        [htcLib.AtGuardable(lmTipoGuardo.lmNno)]
        public bool SoloLectura;
        /// <summary>
        ///     ''' Constructor de respuestaevaluacion
        ///     ''' </summary>
        ///     ''' <remarks></remarks>
        public RespuestaEvaluacion()
        {
        }

        public void setRespuesta(decimal resp)
        {
            this.respuesta = resp;
            this.idPosibleRespuesta = -1;
        }

        /// <summary>
        ///     ''' Constructor que asigna valores a respuestaevaluacion dependiendo del id
        ///     ''' </summary>
        ///     ''' <param name="id">id de RespuestEvaluacion</param>
        ///     ''' <remarks></remarks>
        public RespuestaEvaluacion(long id)
        {
            string sql;
            sql = string.Format("select * from {0} where id={1}", mitabla, id);

            object a = this;
            htcLib.LM.cargaObjeto(sql, ref (a));            
        }

        /// <summary>
        ///     ''' Constructor que requiere del id de Evaluacion y de Pregunta
        ///     ''' </summary>
        ///     ''' <param name="idE">id de Evaluacion</param>
        ///     ''' <param name="idP">id de Pregunta</param>
        ///     ''' <remarks></remarks>
        public RespuestaEvaluacion(long idE, long idP)
        {
            string sql;
            sql = string.Format("select * from {0} where idEvaluacion={1} and idPregunta={2}", mitabla, idE, idP);

            object a = this;
            htcLib.LM.cargaObjeto(sql, ref (a));            
        }


        /// <summary>
        ///     ''' Guarda el objeto respuestaevaluacion
        ///     ''' </summary>
        ///     ''' <remarks></remarks>
        public void guardar()
        {
            htcLib.LM.GuardaObjeto(this);
        }
    }



}