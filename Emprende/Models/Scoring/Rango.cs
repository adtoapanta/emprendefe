﻿
using htcLib;
using System.Data;

namespace Emprende.Controllers.Scoring
{
    
    
    /// <summary>

    /// ''' Rango de cuestionario o evaluacion

    /// ''' </summary>

    /// ''' <remarks></remarks>
    public class Rango
    {
        public static string mitabla = "SCO_Rangos"; // nombre de la tabla
        public long id; // id de rango
        public long idCuestionario; // id del cuestionario del rango
        public string nombre; // nombre del rango
        public long desde; // valor minimo del rango
        public long hasta; // valor maximo del rango
        public string decision; // aprobado, negado, etc
        public string color; // caracteristico del rango
                             /// <summary>
                             ///     ''' Constructor de rango
                             ///     ''' </summary>
                             ///     ''' <remarks></remarks>
        public Rango()
        {
        }

        /// <summary>
        ///     ''' Constructor que asigna valores al objeto rango dependiendo del id
        ///     ''' </summary>
        ///     ''' <param name="id">Id del rango</param>
        ///     ''' <remarks></remarks>
        public Rango(long id)
        {
            string sql;
            sql = string.Format("select * from {0} where id={1}", mitabla, id);

            object a = this;
            LM.cargaObjeto(sql, ref (a));
        }

        /// <summary>
        ///     ''' Guarda los valores asignados al objeto rango
        ///     ''' </summary>
        ///     ''' <remarks></remarks>
        public void guardar()
        {
            LM.GuardaObjeto(this);
        }


        public Rango(long idCuestionario, decimal score)
        {
            string sql;

            sql = string.Format("SELECT * FROM {0} WHERE (desde <= {1} and hasta >= {1}) AND idCuestionario={2}", mitabla, score, idCuestionario);

            object a = this;
            LM.cargaObjeto(sql, ref (a));
        }

        public void eliminar()
        {
            string sql;

            sql = string.Format("delete from {0} where id={1}", mitabla, this.id);
            htcLib.espacio.ManejadorBD.ejecuta(sql);
        }
        public static DataTable getRangosScore(object idModelo)
        {
            string sql;

            sql = string.Format("select * from SCO_VIEW_Rangos where idCuestionario={0}", idModelo);
            return htcLib.espacio.ManejadorBD.traetabla(sql);
        }
    }

}