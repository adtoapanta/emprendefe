﻿using System;

using htcLib;
using System.Collections;
using Emprende.Controllers.Scoring;

namespace Emprende.Models.Scoring
{
    public class Cuestionario
    {
        public static string mitabla = "SCO_Cuestionarios"; // nombre de la tabla
        public long id; // id del cuestionario
        public long idCategoria; // id de la categoria a la que pertenece
        public string nombre; // nombre cuestionario
        public long constante; // valor constante
        public string observaciones; // obsevaciones
        public bool bloqueado; // control solo lectura
        public DateTime fechaRegistro; // creacion
        public DateTime fechaModificacion; // ultima modificacion
        public string descripcion;

        [AtGuardable(lmTipoGuardo.lmNno)]
        private ArrayList _MisPreguntas; // contiene las preguntas del cuestionario
        [AtGuardable(lmTipoGuardo.lmNno)]
        private ArrayList _MisRangos; // contiene los rangos que puede estar el cuestionario


        [AtGuardable(lmTipoGuardo.lmNno)]
        public ArrayList MisPreguntas
        {
            get
            {
                string sql;


                if (this._MisPreguntas == null)
                {
                    sql = string.Format("select {0}.* from {0} inner join {1} on {0}.id={1}.idPregunta where {1}.idCuestionario={2}", Pregunta.mitabla, "SCO_Secciones", this.id);
                    this._MisPreguntas = LM.CargaAL(sql, new Pregunta());

                    foreach (Pregunta pr in _MisPreguntas)
                        pr.CargarPosiblesRespuestas(this.id);
                }
                return this._MisPreguntas;
            }
        }



        [AtGuardable(lmTipoGuardo.lmNno)]
        public ArrayList MisRangos
        {
            get
            {
                string sql;


                if (this._MisRangos == null)
                {
                    sql = string.Format("select * from {0} where idCuestionario={1}", Rango.mitabla, this.id);
                    this._MisRangos = LM.CargaAL(sql, new Rango());
                }
                return this._MisRangos;
            }
        }




        /// <summary>
        ///     ''' Constructor de cuestionario
        ///     ''' </summary>
        ///     ''' <remarks></remarks>
        public Cuestionario()
        {
        }

        /// <summary>
        ///     ''' Constructor que asigna valores a cuestionario dependiendo del id
        ///     ''' </summary>
        ///     ''' <param name="id"></param>
        ///     ''' <remarks></remarks>
        public Cuestionario(long id)
        {
            string sql;
            sql = string.Format("select * from {0} where id={1}", mitabla, id);

            object a = this;
            LM.cargaObjeto(sql, ref (a));
            
        }




        /// <summary>
        ///     ''' Guarda los valores asignados a cuestionario
        ///     ''' </summary>
        ///     ''' <remarks></remarks>
        public void guardar()
        {
            LM.GuardaObjeto(this);
        }

        /// <summary>
        ///     ''' Califica la evaluacion dependiendo del rango en el que cayo la calificacion
        ///     ''' </summary>
        ///     ''' <remarks></remarks>
        public Rango EncontrarMiRango(decimal puntaje)
        {
            

            foreach (Rango rng in this.MisRangos)
            {
                if (puntaje >= rng.desde & puntaje <= rng.hasta)
                {
                    return rng;                    
                }
            }

            return null;
        }
    }

}