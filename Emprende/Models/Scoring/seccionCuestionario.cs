﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Emprende.Models.Scoring
{
    public class SeccionCuestionario
    {

        public static string mitabla = "SCO_Secciones";
        public long id;
        public long idCuestionario;
        public long peso;
        public long idPadre;
        public long Nivel;
        public long idPregunta;
        public string Nombre;

        [htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)]
        public ArrayList secciones; // hijos 
        [htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)]
        public preguntaRespuesta miPregunta;


        [htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)]
        public decimal calificacion;
        [htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)]
        public decimal calificacionPonderada;


        public SeccionCuestionario(DataRow dr)
        {

            object a = this;
            htcLib.LM.cargaObjeto(dr, ref (a));            

            if (this.idPregunta > 0)
                pasaPregunta(dr);

            this.secciones = new ArrayList();
        }

        public void pasaPregunta(DataRow dr)
        {
            this.miPregunta = new preguntaRespuesta();

            object a = this.miPregunta;            
            htcLib.LM.cargaObjeto(dr, ref(a));
        }

        public decimal califica()
        {
            
            decimal total = 0;

            if (this.idPregunta > 0)
                total = miPregunta.puntajeObtenido;
            else
                foreach (SeccionCuestionario s in this.secciones)
                {
                    s.califica();
                    total += s.calificacionPonderada;
                }

            this.calificacion = total;
            this.calificacionPonderada = this.calificacion * this.peso / (decimal)100;

            return total;
        }

        public bool ubica(SeccionCuestionario s)
        {
            
            bool encontre;

            if (s.idPadre == this.id)
            {
                this.secciones.Add(s);
                return true;
            }
            else
            {
                // no tengo hijos por definicion, asi que soy hoja, salgo con false
                encontre = false;

                foreach (SeccionCuestionario ss in this.secciones)
                {
                    encontre = ss.ubica(s);
                    if (encontre == true)
                        return true;
                }
            }

            return encontre;
        }




    }
}