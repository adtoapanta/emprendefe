﻿using Emprende.Controllers.Scoring;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Emprende.Models.Scoring
{

    public class evaluacion
    {
        public static string mitabla = "SCO_evaluaciones";

        public long id;
        public long idCuestionario;
        public long idTipoRef;
        public long idRef;
        public DateTime fecha;


        public long idRango;
        public decimal puntajeTotal;

        public bool completo;

        //constructores
        public evaluacion()
        {
        }

        public evaluacion(int id)
        {
            string sql;
            sql = string.Format("select * from {0} where id={1}", mitabla, id);

            object a = this;
            htcLib.LM.cargaObjeto(sql, ref (a));

        }

        public evaluacion(long miIdTipo, long miIdRef, int idCuestionario)
        {
            string sql = string.Format("SELECT * FROM {0} WHERE idTiporef = {1} AND idRef = {2} AND idCuestionario = {3}", mitabla, miIdTipo, miIdRef, idCuestionario);

            object a = this;
            htcLib.LM.cargaObjeto(sql, ref (a));

        }

        public void guardar()
        {
            Boolean nuevo = false;

            if (this.id == 0) { nuevo = true; };

            htcLib.LM.GuardaObjeto(this);

            if (nuevo == true) { this.creayGuardaRespuestasVacias(); }

        }

        private void creayGuardaRespuestasVacias()
        {
            htcLib.espacio.ManejadorBD.ejecuta(string.Format("SCO_SP_CreaRespuestasEvaluacion  {0}, {1}", this.id, this.idCuestionario));
        }

        public List<preguntaEV> getDatos()
        {

            List<preguntaEV> lista;
            List<PosibleRespuesta> respuestas;


            //Traigo las preguntas y si ha respondido
            string sql = string.Format("SCO_SP_preguntasEvaluacion {0}", this.id);
            lista = htcLib.LM.CargaAL(sql, new preguntaEV()).Cast<preguntaEV>().ToList();


            //Traigo las posibles Respuestas
            string sql2 = string.Format("SCO_SP_OpcionesCuestionario {0}", this.idCuestionario);
            respuestas = htcLib.LM.CargaAL(sql2, new PosibleRespuesta()).Cast<PosibleRespuesta>().ToList();


            foreach (preguntaEV pre in lista)
            {
                List<PosibleRespuesta> algo = (from a in respuestas where a.idPregunta == pre.id select a).ToList();
                pre.opciones = new List<PosibleRespuesta>();
                pre.opciones.AddRange(algo);

            }


            return lista;

        }

        public static int guardaRespuestas(List<RespuestaEV> resp, Boolean califica = false)
        {

            int idEvaluacion = 0;
            int Calif = 0;

            foreach (RespuestaEV item in resp)
            {
                item.guardar();
            }

            if (resp.Count > 0)
            {
                idEvaluacion = Convert.ToInt32(new RespuestaEvaluacion(resp[0].id).idEvaluacion);

                //respuestas adicionales, que vienen de los datos:
                htcLib.espacio.ManejadorBD.ejecuta(string.Format("SCO_Ajustes {0}", idEvaluacion));
            }


            if (califica = true && resp.Count > 0)
            {                               
                Calif = Convert.ToInt32( htcLib.espacio.ManejadorBD.ejecutaEscalar( string.Format("[SCO_SP_CalificaEvaluacion] {0}",idEvaluacion),htcLib.tiposdatos.entero));

                //ev.califica();
            }


            return Calif;

        }
    }
}



//        public class evaluacionA
//    {
//        public static string mitabla = "SCO_evaluaciones";

//        public long id;
//        public long idCuestionario;
//        public long idTipoRef;
//        public long idRef;

//        public long idRango;
//        public decimal puntajeTotal;
//        public string rating;

//        public DateTime fecha;

//        public bool completo; // si esta todo contestado

//        public SeccionCuestionario seccionRaiz;
//        public ArrayList Preguntas; // hijos 

//        private Cuestionario _miCuestionario;

//        private ArrayList MisRespuestasEvaluacion; // contiene la pregunta con su respuesta


//        public Cuestionario miCuestionario
//        {
//            get
//            {
//                if (this._miCuestionario == null)
//                    this._miCuestionario = new Cuestionario(this.idCuestionario);
//                return this._miCuestionario;
//            }
//        }


//        /// <summary>
//        ///     ''' Carga las respuestas elegidas por el cliente del cuestionario
//        ///     ''' </summary>
//        ///     ''' <remarks></remarks>
//        private void CargarMisRespuestasEvaluacion()
//        {
//            string sql;
//            // cargamos las respuestas de la evaluacion
//            sql = string.Format("SELECT * FROM {0} WHERE idEvaluacion={1}", RespuestaEvaluacion.mitabla, this.id);
//            this.MisRespuestasEvaluacion = htcLib.LM.CargaAL(sql, new RespuestaEvaluacion());
//            //FER: cambiar a EF
//        }

//        public evaluacionA)
//        {
//        }

//        public evaluacionAlong id)
//        {
//            string sql;
//            sql = string.Format("select * from {0} where id={1}", mitabla, id);

//            object a = this;
//            htcLib.LM.cargaObjeto(sql, ref (a));

//        }

//        public evaluacion(long miIdTipo, long miIdRef)
//        {
//            string sql = string.Format("SELECT * FROM {0} WHERE idTiporef = {1} AND idRef = {2}", mitabla, miIdTipo, miIdRef);

//            object a = this;
//            htcLib.LM.cargaObjeto(sql, ref (a));

//        }

//        public void guardar()
//        {
//            Boolean nuevo = false;

//            if (this.id == 0) { nuevo = true; };            

//            htcLib.LM.GuardaObjeto(this);

//            if (nuevo == true) {this.creayGuardaRespuestasVacias();}

//        }

//        public void cargar()
//        {
//            this.cargaSecciones();
//            this.califica();
//        }


//        public void cargaSecciones()
//        {
//            string sql;
//            DataTable dt;
//            DataRow dr;
//            SeccionCuestionario miSeccion;

//            // sql = String.Format("SELECT * FROM SCO_Secciones WHERE idCuestionario = {0} ORDER BY nivel", Me.idCuestionario)

//            sql = string.Format("SCO_SeccionesYrespuestas {0} , {1}", this.idCuestionario, this.id);


//            dt = htcLib.espacio.ManejadorBD.traetabla(sql);

//            if (dt.Rows.Count != 0)
//            {
//                dr = dt.Rows[0];
//                this.seccionRaiz = new SeccionCuestionario(dr);

//                for (int i = 1; i <= dt.Rows.Count - 1; i++)
//                {
//                    dr = dt.Rows[i];
//                    miSeccion = new SeccionCuestionario(dr);

//                    seccionRaiz.ubica(miSeccion);
//                }
//            }
//        }


//        public void califica()
//        {
//            Rango r;

//            if (this.seccionRaiz == null)
//                this.cargaSecciones();

//            this.puntajeTotal = this.seccionRaiz.califica();

//            r = this.miCuestionario.EncontrarMiRango(this.puntajeTotal);

//            this.idRango = r.id;
//            this.rating = r.decision;
//        }





//        /// <summary>
//        /// Crea las respuestas de la evaluacion, vacías para ser llenadas
//        /// Se podría cambiar a un SP
//        /// </summary>
//        public void creayGuardaRespuestasVacias()
//        {

//            RespuestaEvaluacion resp;

//            this.MisRespuestasEvaluacion = new ArrayList();

//            foreach (Pregunta pr in this.miCuestionario.MisPreguntas)
//            {
//                resp = new RespuestaEvaluacion();


//                resp.idEvaluacion = this.id;
//                resp.idPregunta = pr.id;
//                resp.idPosibleRespuesta = -100;   // -100 quiere decir que no esta respondida            
//                resp.guardar();


//                this.MisRespuestasEvaluacion.Add(resp);
//            }
//        }




//        /// <summary>
//        ///     ''' Retorna la respuesta de la evaluacion dependiendo de la pregunta
//        ///     ''' </summary>
//        ///     ''' <param name="idPregunta"></param>
//        ///     ''' <returns></returns>
//        ///     ''' <remarks></remarks>
//        public RespuestaEvaluacion GetRespuestaEvaluacion(long idPregunta)
//        {

//            RespuestaEvaluacion resultado;

//            resultado = new RespuestaEvaluacion(); // esto nos sirve cuando queremos crea una nueva evaluacion
//            resultado.idPregunta = idPregunta;
//            resultado.respuesta = -1;

//            foreach (RespuestaEvaluacion rpsEv in this.MisRespuestasEvaluacion)
//            {
//                if (rpsEv.idPregunta == idPregunta)
//                {
//                    resultado = rpsEv;
//                    break;
//                }
//            }
//            return resultado;
//        }

//        //HASTA AQUI ESTA OK


//        /// <summary>
//        ///     ''' Carga las el cuestionario, preguntas y respuestas seleccionadas
//        ///     ''' </summary>
//        ///     ''' <remarks></remarks>

//        public void CargarPreguntasYRespuestas()
//        {
//            this.CargarMisRespuestasEvaluacion();
//        }





//        /// <summary>
//        ///     ''' Realiza la calificación de la evaluacion respuesta por respuesta y crea un total a la evaluacion
//        ///     ''' </summary>
//        ///     ''' <remarks></remarks>
//        public void CalificarRespuestas()
//        {

//            Puntaje puntaje1 = new Puntaje();


//            decimal total;
//            bool encontre;

//            if (this.MisRespuestasEvaluacion == null)
//                this.CargarMisRespuestasEvaluacion();


//            // a el total de la evaluacion se debe añadir la base del cuestionario
//            total = this.miCuestionario.constante;
//            this.completo = true; // parto de que esta completo, pero si alguna pregunta no esta respondida se cambia a falso


//            foreach (Pregunta pregunta in miCuestionario.MisPreguntas) // recorremos por pregunta
//            {
//                pregunta.CargarMisPuntajes(this.idCuestionario); // cargamos los puntajes de las respuesta pertenecientes a las preguntas
//                foreach (RespuestaEvaluacion rspEv in this.MisRespuestasEvaluacion)
//                {
//                    if (pregunta.id == rspEv.idPregunta && rspEv.idPosibleRespuesta != -100)
//                    {
//                        // -100 quiere decir que no esta respondida
//                        encontre = false;
//                        switch (pregunta.idTipo)
//                        {
//                            case 2 // para de tipo seleccion
//                           :
//                                {
//                                    foreach (Puntaje puntaje in pregunta.MisPuntajes)
//                                    {
//                                        puntaje1 = puntaje;

//                                        if (puntaje.idPosibleRespuesta == rspEv.idPosibleRespuesta)
//                                        {
//                                            rspEv.puntajeObtenido = puntaje.puntaje; // asignamos el puntaje por su seleccion
//                                            total += puntaje.puntaje;
//                                            encontre = true;
//                                            break; // si no lo encuentra ni modo (osea no es necesario)
//                                        }
//                                    }

//                                    // LOGICA EEF:
//                                    // si no lo encuentro, tomo el último. Reviosar esta logica para otros proyectos
//                                    if (encontre == false)
//                                    {
//                                        rspEv.puntajeObtenido = puntaje1.puntaje; // asignamos el puntaje por su seleccion
//                                        total += puntaje1.puntaje;
//                                    }

//                                    break;
//                                }

//                            case 1:
//                                {

//                                    foreach (Puntaje puntaje in pregunta.MisPuntajes)
//                                    {
//                                        puntaje1 = puntaje;

//                                        if (rspEv.respuesta >= puntaje.desde & rspEv.respuesta <= puntaje.hasta)
//                                        {
//                                            rspEv.puntajeObtenido = puntaje.puntaje;
//                                            total += puntaje.puntaje;
//                                            encontre = true;
//                                            break;
//                                        }
//                                    }

//                                    // LOGICA EEF:
//                                    // si no lo encuentro, tomo el último. Reviosar esta logica para otros proyectos
//                                    if (encontre == false)
//                                    {
//                                        rspEv.puntajeObtenido = puntaje1.puntaje; // asignamos el puntaje por su seleccion
//                                        total += puntaje1.puntaje;
//                                    }

//                                    break;
//                                }
//                        }

//                        break;
//                    }
//                    else
//                       // si alguna pregunta esta en -100 , la eval no esta completa
//                       if (rspEv.idPosibleRespuesta == -100)
//                        this.completo = false;
//                }
//            }

//            this.puntajeTotal = total;
//        }




//        /// <summary>
//        ///     ''' Guarda la evaluacion en si, obtiene su id y guarda las respuestas de la evaluacion con el id de evaluacion
//        ///     ''' </summary>
//        ///     ''' <remarks></remarks>
//        public void guardarEvaluacionRespuestas()

//        {


//            this.fecha = DateTime.Today;

//            this.guardar();
//            foreach (RespuestaEvaluacion rsp in this.MisRespuestasEvaluacion)
//            {
//                rsp.idEvaluacion = this.id;
//                rsp.guardar();
//            }

//        }

//}


//}















