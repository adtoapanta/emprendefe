﻿using htcLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Emprende.Models.Scoring
{
    public class preguntaEV
    {
        public static string mitabla = "SCO_evaluaciones";


        //public int idTCategoria { get; set; } // clasificacion: socieconomica, etc
        public int id { get; set; }
        public int idTipo { get; set; }// seleccion o numeros
        public string nombre { get; set; }
        public string explicacion { get; set; }
        public string fuente { get; set; }
        public bool obligatorio { get; set; }

        //Respuesta, putaje
        public int idR { get; set; } //id de la respuesta ( para responderla)
        public int idPosiblerespuesta { get; set; }
        public decimal respuesta { get; set; }

        [AtGuardable(lmTipoGuardo.lmNno)]
        public List<PosibleRespuesta> opciones;


    }
}