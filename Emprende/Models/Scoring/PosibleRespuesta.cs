﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using htcLib;

namespace Emprende.Models.Scoring
{
    public class PosibleRespuesta
    {
        public static string mitabla = "SCO_PosiblesRespuestas"; // nombre de la tabla
        public long id; // id de la respuesta
        public long idPregunta; // Referencia a la pregunta
        public string nombre; // La Posible respuesta

        /// <summary>
        ///     ''' Constructor de respuesta
        ///     ''' </summary>
        ///     ''' <remarks></remarks>
        public PosibleRespuesta()
        {
        }

        /// <summary>
        ///     ''' Constructor que asigna valores a respuesta dependiendo del id
        ///     ''' </summary>
        ///     ''' <param name="id">Id de respuesta</param>
        ///     ''' <remarks></remarks>
        public PosibleRespuesta(long id)
        {
            string sql;
            sql = string.Format("select * from {0} where id={1}", mitabla, id);

            object a = this;
            LM.cargaObjeto(sql, ref (a));

        }

        /// <summary>
        ///     ''' Guarda los valores asignados a respuesta
        ///     ''' </summary>
        ///     ''' <remarks></remarks>
        public void guardar()
        {
            LM.GuardaObjeto(this);
        }

        public void eliminar()
        {
            string sql;

            sql = string.Format("delete from {0} where id={1}", mitabla, this.id);
            htcLib.espacio.ManejadorBD.ejecuta(sql);
        }
    }

}