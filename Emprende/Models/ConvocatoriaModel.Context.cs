﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Emprende.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class emprendeEntities : DbContext
    {
        public emprendeEntities()
            : base("name=emprendeEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<CON_convocatorias> CON_convocatorias { get; set; }
        public virtual DbSet<CON_criteriosConvocatoria> CON_criteriosConvocatoria { get; set; }
        public virtual DbSet<CON_postulaciones> CON_postulaciones { get; set; }
        public virtual DbSet<CON_requisitos> CON_requisitos { get; set; }
        public virtual DbSet<CON_requisitosConvocatoria> CON_requisitosConvocatoria { get; set; }
        public virtual DbSet<CON_requisitosPostulante> CON_requisitosPostulante { get; set; }
        public virtual DbSet<Catalogos> Catalogos { get; set; }
        public virtual DbSet<ItemsCatalogos> ItemsCatalogos { get; set; }
        public virtual DbSet<SCO_Criterios> SCO_Criterios { get; set; }
        public virtual DbSet<SCO_PosiblesRespuestas> SCO_PosiblesRespuestas { get; set; }
        public virtual DbSet<SCO_Preguntas> SCO_Preguntas { get; set; }
        public virtual DbSet<CON_propuestaObjetivos> CON_propuestaObjetivos { get; set; }
        public virtual DbSet<CON_propuestas> CON_propuestas { get; set; }
        public virtual DbSet<inversion> inversion { get; set; }
        public virtual DbSet<Insumos> Insumos { get; set; }
        public virtual DbSet<Productos> Productos { get; set; }
        public virtual DbSet<CON_canvasPreguntas> CON_canvasPreguntas { get; set; }
        public virtual DbSet<CON_canvas> CON_canvas { get; set; }
        public virtual DbSet<Emprendimientos> Emprendimientos { get; set; }
        public virtual DbSet<MIS_Personas> MIS_Personas { get; set; }
        public virtual DbSet<socios> socios { get; set; }
        public virtual DbSet<estadoCivil> estadoCivil { get; set; }
        public virtual DbSet<Instruccion> Instruccion { get; set; }
        public virtual DbSet<Pais> Pais { get; set; }
        public virtual DbSet<tiposLugar> tiposLugar { get; set; }
        public virtual DbSet<BitacoraEtapasPostulacion> BitacoraEtapasPostulacion { get; set; }
        public virtual DbSet<SCO_PreguntasxCuestionario> SCO_PreguntasxCuestionario { get; set; }
        public virtual DbSet<Mis_referencias> Mis_referencias { get; set; }
        public virtual DbSet<Ciudades> Ciudades { get; set; }
        public virtual DbSet<Parroquias> Parroquias { get; set; }
        public virtual DbSet<Provincias> Provincias { get; set; }
        public virtual DbSet<CON_comiteMiembros> CON_comiteMiembros { get; set; }
        public virtual DbSet<CON_comitePostulaciones> CON_comitePostulaciones { get; set; }
        public virtual DbSet<SEG_IndicadoresEmprende> SEG_IndicadoresEmprende { get; set; }
        public virtual DbSet<CON_ProveedoresCapacitacion> CON_ProveedoresCapacitacion { get; set; }
        public virtual DbSet<SCO_Evaluaciones> SCO_Evaluaciones { get; set; }
        public virtual DbSet<MIS_Direcciones> MIS_Direcciones { get; set; }
    }
}
