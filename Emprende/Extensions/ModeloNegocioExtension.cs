﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emprende.Models;

namespace Emprende.Extensions
{
    public class ModeloNegocioExtension
    {

        public static void crearCanvasEmprendimiento(int idEmprendimiento)
        {
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                CON_canvas canvas = new CON_canvas();
                canvas.idEmprendimiento = idEmprendimiento;
                canvas.idEstado = 1;
                canvas.estaTerminado = 0;
                canvas.propuesta = canvas.relaciones = canvas.canales = canvas.clientes = canvas.actividades = canvas.recursos = canvas.socios = canvas.ingresos = canvas.egresos = "";

                a.CON_canvas.Add(canvas);
                a.SaveChanges();
            }
        }

        public static List<CON_canvasPreguntas> consultarPreguntasCanvas()
        {
            using (var a = new Models.emprendeEntities())
            {
                List<CON_canvasPreguntas> preguntas = new List<CON_canvasPreguntas>();

                a.Configuration.LazyLoadingEnabled = false;

                preguntas = a.CON_canvasPreguntas.ToList();

                return preguntas;
            }


        }

        public static CON_canvas consultarCanvasEmprendimiento(int idEmprendimiento)
        {
            using (var a = new Models.emprendeEntities())
            {
                

                a.Configuration.LazyLoadingEnabled = false;

                CON_canvas canvas = new CON_canvas();

                canvas = a.CON_canvas.Where(x => x.idEmprendimiento == idEmprendimiento).FirstOrDefault();

                return canvas;
            }
        }

        public static void actualizaCanvasEmprendimiento(int idEmprendimiento, int idPregunta, string respuesta)
        {
            using (var a = new Models.emprendeEntities())
            {
                CON_canvas canvas = a.CON_canvas.Where(x => x.idEmprendimiento == idEmprendimiento).FirstOrDefault();

                switch (idPregunta)
                {
                    case 1:
                        canvas.propuesta = respuesta;
                        break;
                    case 2:
                        canvas.relaciones = respuesta;
                        break;
                    case 3:
                        canvas.canales = respuesta;
                        break;
                    case 4:
                        canvas.clientes= respuesta;
                        break;
                    case 5:
                        canvas.actividades = respuesta;
                        break;
                    case 6:
                        canvas.recursos = respuesta;
                        break;
                    case 7:
                        canvas.socios = respuesta;
                        break;
                    case 8:
                        canvas.ingresos = respuesta;
                        break;
                    case 9:
                        canvas.egresos = respuesta;
                        break;
                    default:
                        break;
                }

                a.SaveChanges();
            }
        }
    }
}