﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emprende.Models;

namespace Emprende.Extensions
{
    public class ProveedoresCapacitacionExtension
    {
        public static CON_ProveedoresCapacitacion guardarProveedor(string nombre, string correo)
        {

            using (var a = new Models.emprendeEntities())
            {

                a.Configuration.LazyLoadingEnabled = false;

                CON_ProveedoresCapacitacion proveedor = new CON_ProveedoresCapacitacion();
                proveedor.nombre = nombre;
                proveedor.correo = correo;

                a.CON_ProveedoresCapacitacion.Add(proveedor);
                a.SaveChanges();
                return proveedor;
            }

        }

        public static void editarProveedor(int idProveedor, string nombre, string correo)
        {

            using (var a = new Models.emprendeEntities())
            {

                a.Configuration.LazyLoadingEnabled = false;

                CON_ProveedoresCapacitacion proveedor = a.CON_ProveedoresCapacitacion.Where(x => x.id == idProveedor).FirstOrDefault();
                proveedor.nombre = nombre;
                proveedor.correo = correo;

                a.SaveChanges();
            }

        }

        public static void eliminarProveedor(int idProveedor)
        {

            using (var a = new Models.emprendeEntities())
            {

                a.Configuration.LazyLoadingEnabled = false;

                CON_ProveedoresCapacitacion proveedor = a.CON_ProveedoresCapacitacion.Where(x => x.id == idProveedor).FirstOrDefault();

                a.CON_ProveedoresCapacitacion.Remove(proveedor);
                a.SaveChanges();
            }

        }

        public static List<CON_ProveedoresCapacitacion> consultarProveedores()
        {

            using (var a = new Models.emprendeEntities())
            {

                a.Configuration.LazyLoadingEnabled = false;

                List<CON_ProveedoresCapacitacion> proveedores = a.CON_ProveedoresCapacitacion.ToList();

                return proveedores;
            }

        }

    }
}