﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emprende.Models;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace Emprende.Extensions
{
    public class ProductoExt
    {
        private emprendeEntities db;
        private Productos productoDb;
        private List<Insumos> insumos;

        /// <summary>
        /// para cargar un producto cuando quiero editarlo o eliminarlo 
        /// </summary>
        /// <param name="db">emprendeEntities</param>
        /// <param name="id">id del producto que voy a editar o eliminar (No debería ser 0)</param>
        public ProductoExt(emprendeEntities db, int id)
        {
            this.db = db;

            if (id > 0)
            {
                this.productoDb = db.Productos.Where(p => p.id == id).First();
            }
            else
            {
                this.productoDb = new Productos();
            }
        }

        /// <summary>
        /// Constructor para editar un producto que viene ya cargdo desde html
        /// </summary>
        /// <param name="db"></param>
        /// <param name="productoEnviado"></param>
        public ProductoExt(emprendeEntities db, Productos productoEnviado)
        {
            this.db = db;

            if (productoEnviado.id > 0)
            {
                this.productoDb = db.Productos.Where(p => p.id == productoEnviado.id).First();
            }
            else
            {
                this.productoDb = new Productos();
            }

            this.productoDb.costo = productoEnviado.costo;
            this.productoDb.idEmprendimiento = productoEnviado.idEmprendimiento;
            this.productoDb.nombre = productoEnviado.nombre;
            this.productoDb.Precio = productoEnviado.Precio;
            this.productoDb.unidadesActual = productoEnviado.unidadesActual;
            this.productoDb.unidadesProyectado = productoEnviado.unidadesProyectado;
        }

        public ProductoExt(emprendeEntities db, int id, int idEmprendimiento, string nombre, decimal precio, int unidadesActual, int unidadesProyectado, decimal costo, List<Insumos> insumos)
        {
            try
            {
                this.db = db;

                if (id > 0)
                {
                    this.productoDb = db.Productos.Where(p => p.id == id).First();
                }
                else
                {
                    this.productoDb = new Productos();
                    db.Productos.Add(this.productoDb);
                }

                this.productoDb.costo = costo;
                this.productoDb.idEmprendimiento = idEmprendimiento;
                this.productoDb.nombre = nombre;
                this.productoDb.Precio = precio;
                this.productoDb.unidadesActual = unidadesActual;
                this.productoDb.unidadesProyectado = unidadesProyectado;

                this.insumos = insumos;

                db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                string errorCompleto = "";

                foreach (DbEntityValidationResult item in ex.EntityValidationErrors)
                {
                    // Get entry
                    DbEntityEntry entry = item.Entry;
                    string entityTypeName = entry.Entity.GetType().Name;

                    // Display or log error messages
                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",
                                 subItem.ErrorMessage, entityTypeName, subItem.PropertyName);
                        errorCompleto += "/n" + message;
                    }
                }

                throw new Exception("DbEntityValidationException al guardar ProductoExt: " + errorCompleto);
            }
            catch (Exception e)
            {
                throw new Exception("guardar ProductoExt: " + e.Message);
            }
        }


        internal void guardarInsumos()
        {

            try
            {
                Insumos insumoDb;

                //string sql = "delete from insumos where idProducto = " + this.productoDb.id;
                //db.Database.ExecuteSqlCommand(sql);


                foreach (Insumos insumoActual in this.insumos)
                {
                    if (insumoActual.id > 0)
                    {
                        insumoDb = db.Insumos.Where(i => i.id == insumoActual.id).First();
                    }
                    else
                    {
                        insumoDb = new Insumos();
                        db.Insumos.Add(insumoDb);
                    }

                    insumoDb.idProducto = productoDb.id;
                    insumoDb.Nombre = insumoActual.Nombre;
                    insumoDb.precio = insumoActual.precio;
                    insumoDb.cantidad = insumoActual.cantidad;
                }

                db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                string errorCompleto = "";

                foreach (DbEntityValidationResult item in ex.EntityValidationErrors)
                {
                    // Get entry
                    DbEntityEntry entry = item.Entry;
                    string entityTypeName = entry.Entity.GetType().Name;

                    // Display or log error messages
                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",
                                 subItem.ErrorMessage, entityTypeName, subItem.PropertyName);
                        errorCompleto += "/n" + message;
                    }
                }

                throw new Exception("DbEntityValidationException al guardarInsumos: " + errorCompleto);
            }
            catch (Exception e)
            {
                throw new Exception("guardarInsumos: " + e.Message);
            }

        }

        internal void eliminar()
        {
            this.db.Productos.Remove(this.productoDb);

            //elimino los insumos del producto 
            string sql = "delete from insumos where idProducto = " + this.productoDb.id;
            this.db.Database.ExecuteSqlCommand(sql);
            this.db.SaveChanges();

            //recalculo los porcentajes
            SqlParameter pIdEmprendimiento = new SqlParameter("@idEmprendimiento", this.productoDb.idEmprendimiento);
            this.db.Database.ExecuteSqlCommand("SP_calcularProductos @idEmprendimiento", pIdEmprendimiento);
        }

        internal void guardar()
        {
            guardarInsumos();
            //db.SaveChanges();

            SqlParameter pIdEmprendimiento = new SqlParameter("@idEmprendimiento", this.productoDb.idEmprendimiento);
            db.Database.ExecuteSqlCommand("SP_calcularProductos @idEmprendimiento", pIdEmprendimiento);
        }
    }
}