﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emprende.Models;

namespace Emprende.Extensions
{
    public class ComiteExtension
    {

        public static List<CON_comiteMiembros> consultarMiembrosComite()
        {

            using (var a = new Models.emprendeEntities())
            {

                a.Configuration.LazyLoadingEnabled = false;

                List<CON_comiteMiembros> comite = a.CON_comiteMiembros.ToList();

                return comite;
            }

        }

        public static void editarMiembroComite(int idComite, string nombre, string telefono, string correo, string institucion )
        {

            using (var a = new Models.emprendeEntities())
            {

                a.Configuration.LazyLoadingEnabled = false;

                CON_comiteMiembros comite = a.CON_comiteMiembros.Where(x => x.id == idComite).FirstOrDefault();

                comite.nombre = nombre;
                comite.telefono = telefono;
                comite.email = correo;
                comite.institucion = institucion;

                a.SaveChanges();
            }

        }

        public static void eliminarMiembroComite(int idComite)
        {

            using (var a = new Models.emprendeEntities())
            {

                a.Configuration.LazyLoadingEnabled = false;

                CON_comiteMiembros comite = a.CON_comiteMiembros.Where(x => x.id == idComite).FirstOrDefault();

                a.CON_comiteMiembros.Remove(comite);
                a.SaveChanges();
            }

        }

    }
}