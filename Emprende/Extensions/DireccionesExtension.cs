﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emprende.Models;

namespace Emprende.Extensions
{
    public class DireccionesExtension
    {
        public static MIS_Direcciones agreagarDireccion(int idTipoLugar, int idRef, int idTipo, string linea1, string linea2,
            string numero, string nombre, string referencia, string barrio, int idParroquia)
        {
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                MIS_Direcciones direccion = new MIS_Direcciones();

                direccion.idTipoLugar = idTipoLugar;
                direccion.idRef = idRef;
                direccion.idTipo = idTipo;
                direccion.linea1 = linea1;
                direccion.linea2 = linea2;
                direccion.numero = numero;
                direccion.nombre = nombre;
                direccion.referencia = referencia;
                direccion.barrio = barrio;
                direccion.idParroquia = idParroquia;

                a.MIS_Direcciones.Add(direccion);
                a.SaveChanges();

                return direccion;
            }
        }
        public static MIS_Direcciones editarDireccion(int idDireccion, int idTipoLugar, string linea1, string linea2,
            string numero, string nombre, string referencia, string barrio, int idParroquia)
        {
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                MIS_Direcciones direccion = a.MIS_Direcciones.Where(x => x.id == idDireccion).FirstOrDefault();

                direccion.idTipoLugar = idTipoLugar;                
                direccion.linea1 = linea1;
                direccion.linea2 = linea2;
                direccion.numero = numero;
                direccion.nombre = nombre;
                direccion.referencia = referencia;
                direccion.barrio = barrio;
                direccion.idParroquia = idParroquia;
                
                a.SaveChanges();

                return direccion;
            }
        }

        public static void agregarLocalizacionIdPostulacion(int idPostulacion, decimal latitud, decimal longitud)
        {

            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                CON_postulaciones postulacion = a.CON_postulaciones.Include("Emprendimientos.MIS_Direcciones").Where(x => x.id == idPostulacion).FirstOrDefault();
                MIS_Direcciones direccion = postulacion.Emprendimientos.MIS_Direcciones;

                direccion.latitud = latitud;
                direccion.longitud = longitud;
                
                a.SaveChanges();

                
            }

        }

    }
}