﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emprende.Models;
namespace Emprende.Extensions
{
    public class ProvinciaExtension
    {
        public static List<Provincias> consultarPronviciasInclude()
        {
            
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                List<Provincias> provincias;

                provincias = a.Provincias.Include("Ciudades.Parroquias").ToList();

                return provincias;

            }
        }
    }
}