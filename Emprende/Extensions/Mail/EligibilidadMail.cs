﻿using Emprende.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Emprende.Extensions.Mail
{
    public class EligibilidadMail: Mailer
    {
        private int idConvocatoria { get; set; }
        private Models.emprendeEntities baseE { get; set; }
        public EligibilidadMail(int idConvocatoria, Models.emprendeEntities baseEm) {
            this.idConvocatoria = idConvocatoria;
            this.baseE = baseEm;
            this.esHTML = true;
        }

        public override void EnviarCorreo()
        {
            List<PostulacionDetalle> todos;
            List<PostulacionDetalle> aprobadas;
            List<PostulacionDetalle> noaprobadas;
            List<string> correosA;
            List<string> correosN;

            //obtenemos a este nivellos correos ya q es asincrona
            todos = baseE.Database.SqlQuery<Models.PostulacionDetalle>("SELECT * FROM VW_PostulantesDetalle WHERE idConvocatoria=@idConvocatoria", new System.Data.SqlClient.SqlParameter("idConvocatoria", idConvocatoria)).ToList();
            aprobadas = todos.Where(x => x.apruebaEtapa == true).ToList();
            noaprobadas = todos.Where(x => x.apruebaEtapa == false).ToList();
            correosA = aprobadas.Select(m => m.correo).ToList();
            correosN = noaprobadas.Select(m => m.correo).ToList();

            Task.Run(() => Ejecutar(correosA, correosN));
        }

        public async Task Ejecutar(List<string> correosA, List<string> correosN)
        {

            Mailer.BuildContenido elContenido = delegate () {
                    return File.ReadAllText(System.AppDomain.CurrentDomain.BaseDirectory + "/Plantillas/mailEligibilidadAprobados.txt");
            };
            await this.Enviar("Estado de su participación", correosA, true, elContenido);

            Mailer.BuildContenido elContenidoN = delegate () {
                return File.ReadAllText(System.AppDomain.CurrentDomain.BaseDirectory + "/Plantillas/mailEligibilidadRechazados.txt");
            };
            await this.Enviar("Estado de su participación", correosN, true, elContenidoN);

        }

    }
}