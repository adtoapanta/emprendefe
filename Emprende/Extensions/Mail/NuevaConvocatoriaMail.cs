﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Emprende.Extensions.Mail
{
    public class NuevaConvocatoriaMail : Mailer
    {
        private int idConvocatoria { get; set; }

        public NuevaConvocatoriaMail(int idConvocatoria) {
            this.idConvocatoria = idConvocatoria;
            this.esHTML = true;
        }

        public override void EnviarCorreo() {
            Task.Run(() => Ejecutar(idConvocatoria));
        }


        public async Task Ejecutar(int idConvocatoria) {
            List<string> correos;
            Emprende.Models.CON_convocatorias con;

            using (var baseE = new Models.emprendeEntities()) {
                correos = baseE.MIS_Personas.Where(p => p.idUsuario != null && p.correo != null).Select(c => c.correo).ToList();
                con = baseE.CON_convocatorias.Where(c => c.id == idConvocatoria).FirstOrDefault();
            }

            Mailer.BuildContenido elContenido = delegate ()
            {
                string contenido;
                contenido = LeerPlantilla(System.AppDomain.CurrentDomain.BaseDirectory + "/Plantillas/mailNuevaConvocatoria.txt");
                contenido = contenido.Replace("{{nombre}}", con.nombre);
                contenido = contenido.Replace("{{fechacierre}}", con.fechaCierre != null? Convert.ToDateTime(con.fechaCierre).ToString("dd/MM/yyyy"):"");//comprobación en caso de fallar
                
                return contenido;
            };
            await this.Enviar("Nueva convocatoria",correos,true, elContenido);
        }
    }
}