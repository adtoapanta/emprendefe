﻿using Emprende.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Emprende.Extensions.Mail
{
    public class PostulacionMail : Mailer
    {
        int idPostulacion;

        public PostulacionMail(int idPostulacion)
        {
            this.idPostulacion = idPostulacion;
            this.esHTML = true;
        }

        public override void EnviarCorreo()
        {
            Task.Run(Ejecutar);
        }

        public async Task Ejecutar()
        {
            PostulacionDetalle pos;


            pos = PostulacionExtension.GetPostulacion(idPostulacion);

            Mailer.BuildContenido elContenido = delegate () {
                string contenido;
                contenido = LeerPlantilla(System.AppDomain.CurrentDomain.BaseDirectory + "/Plantillas/mailPostulacion.txt");
                contenido = contenido.Replace("{{nombreEmprendedor}}", pos.nombre);
                return contenido;
            };
            await this.Enviar("Inscripción en Emprendefe", pos.correo, elContenido);

        }
    }
}