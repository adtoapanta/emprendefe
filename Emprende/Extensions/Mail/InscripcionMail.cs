﻿using Emprende.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Emprende.Extensions.Mail
{
    public class InscripcionMail : Mailer
    {
        string Email;
        string Nombres;

        public InscripcionMail(MIS_Personas persona) {
            this.Email = persona.correo;
            this.Nombres = persona.nombre + " " + persona.apellido;
            this.esHTML = true;
        }

        public override void EnviarCorreo()
        {
            Task.Run(Ejecutar);
        }

        public async Task Ejecutar()
        {

            Mailer.BuildContenido elContenido = delegate () {
                string contenido;
                contenido = LeerPlantilla(System.AppDomain.CurrentDomain.BaseDirectory + "/Plantillas/mailInscripcion.txt");
                contenido = contenido.Replace("{{nombreEmprendedor}}", Nombres);
                return contenido;
            };
            await this.Enviar("Inscripción en Emprendefe", Email,  elContenido);

        }
    }
}