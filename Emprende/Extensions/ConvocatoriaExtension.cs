﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emprende.Models;
using System.Data.Entity;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.IO;

namespace Emprende.Extensions
{
    public class ConvocatoriaExtension
    {
        private CON_convocatorias convocatoria;

        public string idPublico { get; set; }

        public ConvocatoriaExtension()
        {
        }

        /// <summary>
        /// Solo carga la convocatoria
        /// </summary>
        public ConvocatoriaExtension(int idConvocatoria)
        {
            using (var baseE = new Models.emprendeEntities())
            {
                this.convocatoria = baseE.CON_convocatorias.Include(y => y.Etapas).Where(x => x.id == idConvocatoria).FirstOrDefault();
                if (this.convocatoria == null) { throw new Exception("Convocatoria no existe"); }
            }
        }



        /// <summary>
        /// Trae solo los postulantes que coinciden con la etapa q se encuentra la convocatoria
        /// </summary>
        public List<PostulacionDetalle> GetPostulantes()
        {
            List<PostulacionDetalle> post;
            string sql;
            int etapa;

            
            if (this.convocatoria.idEtapa >= (int)IndiceEtapasConvocatoria.Diagnóstico) {
                sql = "SELECT * FROM VW_PostulantesDetalle WHERE idConvocatoria=@idConvocatoria AND idEtapa >= @idEtapa";
                etapa = (int)IndiceEtapasConvocatoria.Diagnóstico;
            }
            else {
                sql = "SELECT * FROM VW_PostulantesDetalle WHERE idConvocatoria=@idConvocatoria AND idEtapa=@idEtapa";
                etapa = (int)this.convocatoria.idEtapa;
            }
            using (var baseE = new Models.emprendeEntities())
            {
                post = baseE.Database.SqlQuery<Models.PostulacionDetalle>(sql, new System.Data.SqlClient.SqlParameter("idConvocatoria", convocatoria.id), new System.Data.SqlClient.SqlParameter("idEtapa", etapa)).ToList();
            }

            return post;
        }

        public CON_convocatorias GetConvocatoria()
        {
            return convocatoria;
        }


        #region "Eligibilidad"
        /// <summary>
        /// Retorna los postulantes despues de aplicar los criterios en la etapa Eligibilidad
        /// </summary>
        /// <returns></returns>
        public List<PostulacionDetalle> GetPostulantesFiltro()
        {
            List<PostulacionDetalle> post;

            using (var baseE = new Models.emprendeEntities())
            {
                post = baseE.Database.SqlQuery<Models.PostulacionDetalle>("SP_Elegibles @idConvocatoria,@idEtapa ", new System.Data.SqlClient.SqlParameter("idConvocatoria", convocatoria.id), new System.Data.SqlClient.SqlParameter("idEtapa", (int)IndiceEtapasConvocatoria.Elegibilidad)).ToList();
            }

            return post;
        }

        public List<SCO_Preguntas> GetPreguntasRespuestas()
        {
            List<SCO_Preguntas> preguntas;
            List<SCO_Criterios> criterios;
            int idConvocatoria = this.convocatoria.id;
            int idCuestionario = 1;
            SCO_Preguntas pregunta;
            SCO_PosiblesRespuestas respuesta;
            List<SCO_PreguntasxCuestionario> pc;

            preguntas = new List<SCO_Preguntas>();
            using (var baseE = new Models.emprendeEntities())
            {
                baseE.Configuration.LazyLoadingEnabled = false;
                pc = baseE.SCO_PreguntasxCuestionario.Include(y => y.SCO_Preguntas).Include(v=>v.SCO_Preguntas.SCO_PosiblesRespuestas).Where(x => x.idCuestionario == (int)IndiceCuestionario.Elegibilidad).ToList();
                foreach (var p in pc) {
                    preguntas.Add(p.SCO_Preguntas);
                }
                criterios = baseE.SCO_Criterios.Where(x => x.idConvocatoria == idConvocatoria && x.idCuestionario == idCuestionario).ToList();
                preguntas.All(c => { c.SCO_Criterios = null; return true; });// se pone null para evitar q se cargue
            }
            foreach (SCO_Criterios c in criterios)
            {
                pregunta = preguntas.Where(p => p.id == c.idPregunta).FirstOrDefault();
                respuesta = pregunta.SCO_PosiblesRespuestas.Where(r => r.id == c.idPosibleRespuesta).FirstOrDefault();
                pregunta._seleccionado = true;
                respuesta._seleccionado = true;
            }

            return preguntas;
        }

        public void AplicarPostulantes()
        {
            using (var baseE = new Models.emprendeEntities())
            {
                //Asumo que ya lo hizo
                baseE.Database.ExecuteSqlCommand("CON_SP_AplicarPostulantes @idConvocatoria,@idEtapa ", new SqlParameter("idConvocatoria", convocatoria.id), new SqlParameter("idEtapa", (int)IndiceEtapasConvocatoria.Elegibilidad));
            }
        }

        public void GuardarTextosMailEligibilidad(string aprobados, string retirados) {
            StreamWriter sw;
            
            sw= new StreamWriter(System.AppDomain.CurrentDomain.BaseDirectory+ "/Plantillas/mailEligibilidadAprobados.txt");
            sw.WriteLine(aprobados);
            sw.Close();

            sw = new StreamWriter(System.AppDomain.CurrentDomain.BaseDirectory + "/Plantillas/mailEligibilidadRechazados.txt");
            sw.WriteLine(retirados);
            sw.Close();
        }

        public string GetTextoMailEligibilidadAprobado() {
            return File.ReadAllText(System.AppDomain.CurrentDomain.BaseDirectory + "/Plantillas/mailEligibilidadAprobados.txt");
        }
        public string GetTextoMailEligibilidadNoAprobado()
        {
            return File.ReadAllText(System.AppDomain.CurrentDomain.BaseDirectory + "/Plantillas/mailEligibilidadRechazados.txt");
        }

        public void GuardarCriterios(List<SCO_Criterios> criterios)
        {
            List<SCO_Criterios> cDel;
            List<SCO_Criterios> cAdd = new List<SCO_Criterios>();
            int idCuestionario = 1;
            int idConvocatoria = this.convocatoria.id;

            foreach (SCO_Criterios sc in criterios)
            {
                cAdd.Add(new SCO_Criterios() { idCuestionario = sc.idCuestionario, idConvocatoria = sc.idConvocatoria, idPregunta = sc.idPregunta, idPosibleRespuesta = sc.idPosibleRespuesta });
            }

            using (var baseE = new Models.emprendeEntities())
            {
                //idCuestionario idConvocatoria
                cDel = baseE.SCO_Criterios.Where(x => x.idConvocatoria == idConvocatoria && x.idCuestionario == idCuestionario).ToList();
                baseE.SCO_Criterios.RemoveRange(cDel);
                baseE.SaveChanges();
                baseE.SCO_Criterios.AddRange(cAdd);
                baseE.SaveChanges();
            }
        }
        #endregion

        public static List<VW_UsuariosAdministradores> GetAdministradores() {
            List<VW_UsuariosAdministradores> adms;
            using (var baseE = new Models.emprendeEntities()) {
                adms = baseE.Database.SqlQuery<Models.VW_UsuariosAdministradores>("SELECT * FROM VW_UsuariosAdministradores ORDER BY nombre").ToList();
            }

                return adms;
        }

        public static bool AsignarResponsableVerificacio(int idPostulacion, int idUsuario) {
            CON_postulaciones pos;
            using (var baseE = new Models.emprendeEntities())
            {
                pos = baseE.CON_postulaciones.Where(x => x.id == idPostulacion).FirstOrDefault();
                pos.idUsuarioAsignado = idUsuario;
                baseE.Entry(pos).State = EntityState.Modified;
                baseE.SaveChanges();
            }
            return true;
        }

        #region EditarConvocatoria
        private VtcArchivos.Model.ARC_Archivos archivo;

        //se usa en edicion de convocatoria
        public ConvocatoriaExtension(int id, ref bool fueEditada, ref bool allFine) {

            string msg = string.Empty;
            fueEditada = true;

            using (var baseE = new Models.emprendeEntities())
            {
                convocatoria = baseE.CON_convocatorias.Where(x => x.id == id).FirstOrDefault();
                allFine = convocatoria.idEtapa == (int)IndiceEtapasConvocatoria.Creacion;
                if (convocatoria == null) throw new Exception("Convocatoria no existe.");
                if (convocatoria.fechaCierre == null) {
                    convocatoria.fechaCierre = DateTime.Today.AddMonths(1);
                    fueEditada = false;
                }
                convocatoria.CargarRequisitos(baseE);
            }
            archivo = VtcArchivos.Funciones.GetArchivo(convocatoria.id, (int)TipoArchivos.ArchivoBase, ref msg);

        }

        public List<ItemsCatalogos> GetTiposConvocatoria() {
            List<ItemsCatalogos> tipos;
            using (var baseE = new Models.emprendeEntities()) {
                tipos = baseE.ItemsCatalogos.Where(x => x.idCatalogo == 6).OrderBy(y => y.nombre).ToList();
            }
            return tipos;
        }

        public void SetValuesConvocatoria(string nombre, DateTime fechaApertura, DateTime fechaCierre, string descripcion, string criterios, int tipoConvocatoria, string capacitacion) {
            convocatoria.nombre = nombre;
            convocatoria.fechaApertura = fechaApertura;
            convocatoria.fechaCierre = fechaCierre;
            convocatoria.descripcion = descripcion;
            convocatoria.criterios = criterios;
            convocatoria.capacitacion = capacitacion;
            convocatoria.idTipo = tipoConvocatoria;
        }

        public void SubirArchivoBase(HttpPostedFileBase file, string nombre) {
            string coment = string.Empty;
            string msg = string.Empty;
            int idArchivo;

            VtcArchivos.Funciones.EliminarArchivo((int)TipoArchivos.ArchivoBase, convocatoria.id, ref msg);
            idArchivo = VtcArchivos.Funciones.SubirArchivo(file, convocatoria.id, nombre, "Archivo base convocatoria " + convocatoria.nombre, ref coment, idTipo: (int)TipoArchivos.ArchivoBase);
            if (coment != string.Empty) throw new Exception(coment);
            //Volvemos a leer ya q se ha subido un nuevo archivo
            archivo = VtcArchivos.Funciones.GetArchivo(idArchivo, ref coment);
        }


        public VtcArchivos.Model.ARC_Archivos GetArchivoBase() {
            return this.archivo;
        }

        public bool SaveEditConvocatoria()
        {
            using (var baseE = new Models.emprendeEntities())
            {
                baseE.CON_convocatorias.Attach(convocatoria);
                convocatoria.SetRequisitos(baseE);
                baseE.Entry(convocatoria).State = EntityState.Modified;
                baseE.SaveChanges();
            }
            return true;
        }
        #endregion


        public void CrearConvocatoria(string nombre)
        {
            using (var a = new Models.emprendeEntities())
            {
                convocatoria = new CON_convocatorias();
                convocatoria.nombre = nombre;
                convocatoria.idEtapa = 0;
                convocatoria.fechaApertura = DateTime.Today;
                convocatoria.tasa = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Tasa"]);
                convocatoria.idEtapa = (int)IndiceEtapasConvocatoria.Creacion;
                convocatoria.capacitacion= "";
                convocatoria.idTipo = 0;//no se define aca
                a.CON_convocatorias.Add(convocatoria);
                a.SaveChanges();
            }
        }


        public void CerrarEtapa() {
            using (var a = new Models.emprendeEntities())
            {
                convocatoria.CambiarEtapa(a);
                a.SaveChanges();
            }
        }

        public static List<ConvocatoriaDetalle> ConsultarConvocatorias()
        {
            List<ConvocatoriaDetalle> convocatorias;
            using (var a = new Models.emprendeEntities())
            {
                convocatorias = a.Database.SqlQuery<Models.ConvocatoriaDetalle>("SELECT * FROM VW_ConvocatoriasDetalle ORDER BY id DESC").ToList();
            }
            return convocatorias;
        }

        public static bool AbrirInscripcionConvocatoria(int idConvocatoria) {
            CON_convocatorias con;
            Mail.NuevaConvocatoriaMail conM;

            using (var a = new Models.emprendeEntities()) {
                con = a.CON_convocatorias.Where(x => x.id == idConvocatoria).FirstOrDefault();
                if (con.idEtapa == (int)IndiceEtapasConvocatoria.Creacion) {
                    con.idEtapa = (int)IndiceEtapasConvocatoria.Inscripción;
                    a.Entry(con).State = EntityState.Modified;
                    a.SaveChanges();
                }
            }

            conM = new Mail.NuevaConvocatoriaMail(con.id);
            conM.EnviarCorreo();

            return true;
        }

        public static bool CerrarEtapa(int idConvocatoria) {
            CON_convocatorias con;
            using (var a = new Models.emprendeEntities())
            {
                con = a.CON_convocatorias.Where(x => x.id == idConvocatoria).FirstOrDefault();
                con.CambiarEtapa(a);
                a.SaveChanges();
            }
            return true;
        }

        public static bool ExtenderPlazoCierre(int idConvocatoria, DateTime fecha)
        {
            CON_convocatorias con;
            using (var a = new Models.emprendeEntities())
            {
                con = a.CON_convocatorias.Where(x => x.id == idConvocatoria).FirstOrDefault();
                if (con.idEtapa == (int)IndiceEtapasConvocatoria.Inscripción)
                {
                    con.fechaCierre = fecha;
                    a.Entry(con).State = EntityState.Modified;
                    a.SaveChanges();
                }
                else {
                    throw new Exception("No se puede extender plazo de convocatoria si no esta en etapa de inscripciones");
                }
            }
            return true;
        }

        public static bool ExtenderPlazoCierreDatos(int idConvocatoria, DateTime fecha) {
            CON_convocatorias con;
            using (var baseE = new Models.emprendeEntities()) {
                con = baseE.CON_convocatorias.Where(x => x.id == idConvocatoria).FirstOrDefault();
                if (con.idEtapa == (int)IndiceEtapasConvocatoria.ModeloNegocio)
                {
                    con.fechaCierreDatos = fecha;
                    baseE.Entry(con).State = EntityState.Modified;
                    baseE.SaveChanges();
                }
                else
                {
                    throw new Exception("No se puede extender plazo de datos si no esta en etapa de inscripciones");
                }
            }
            return true;
        }

        public static bool AgendarPrefactibilidadPostulante(int idPropuesta, DateTime fecha, string lugar)
        {
            CON_postulaciones pos;
            using (var baseE = new Models.emprendeEntities())
            {
                pos = baseE.CON_postulaciones.Where(x => x.id == idPropuesta).FirstOrDefault();
                pos.fechaAgendaPrefactibilidad = fecha;
                pos.lugarPrefactibilidad = lugar;
                pos.revisadoEtapa = true;
                pos.apruebaEtapa = true;//Si se pone una fecha se encuentra aprobado, caso contrario lodescarta
                baseE.Entry(pos).State = EntityState.Modified;
                baseE.SaveChanges();
            }
            return true;
        }
        public static bool AgendarComitePostulante(int idPropuesta, DateTime fecha, string lugar)
        {
            CON_postulaciones pos;
            using (var baseE = new Models.emprendeEntities())
            {
                pos = baseE.CON_postulaciones.Where(x => x.id == idPropuesta).FirstOrDefault();
                pos.fechaAgendarComite = fecha;
                pos.revisadoEtapa = true;
                pos.lugarComite = lugar;
                pos.apruebaEtapa = true;//Si se pone una fecha se encuentra aprobado, caso contrario lodescarta
                baseE.Entry(pos).State = EntityState.Modified;
                baseE.SaveChanges();
            }
            return true;
        }


        public static bool GuardarPrefactibilidad(int idPostulante, List<HttpPostedFileBase> arch, bool aprobado, string obs)
        {
            string comment = "";
            CON_postulaciones pos;

            foreach (HttpPostedFileBase file in arch)
            {
                VtcArchivos.Funciones.SubirArchivo(file, idPostulante, "Archivo de prefactibilidad", obs, ref comment,idTipo:(int)TipoArchivos.ArchivoPrefactibilidad);
                if (comment != string.Empty) throw new Exception(comment);
            }

            using (var baseE = new Models.emprendeEntities())
            {
                pos = baseE.CON_postulaciones.Where(x => x.id == idPostulante).FirstOrDefault();
                pos.revisadoEtapa = true;
                pos.apruebaEtapa = aprobado;
                pos.observacionesPrefactibilidad = obs;
                baseE.Entry(pos).State = EntityState.Modified;
                baseE.SaveChanges();
            }
            return true;
        }



        public static bool GuardarComite(int idPostulante, List<HttpPostedFileBase> arch, bool aprobado, string obs)
        {
            string comment = "";
            CON_postulaciones pos;

            foreach (HttpPostedFileBase file in arch)
            {
                VtcArchivos.Funciones.SubirArchivo(file, idPostulante, "Archivo de comite", obs, ref comment, idTipo: (int)TipoArchivos.ArchivoComite);
                if (comment != string.Empty) throw new Exception(comment);
            }

            using (var baseE = new Models.emprendeEntities())
            {
                pos = baseE.CON_postulaciones.Where(x => x.id == idPostulante).FirstOrDefault();
                pos.revisadoEtapa = true;
                pos.apruebaEtapa = aprobado;
                pos.observacionesComite = obs;
                baseE.Entry(pos).State = EntityState.Modified;
                baseE.SaveChanges();
            }
            
            return true;
        }

        public static bool GuardarDocumentosAPedir(int idPostulante, List<VtcArchivos.Model.ARC_DocumentosCargados> documentos) {
            string msg;
            if (documentos.Count > 0)
            {
                msg = VtcArchivos.Funciones.CrearDocumentosCargados(documentos, idPostulante, (int)TipoDocumentos.Datos);
                if (msg != string.Empty) throw new Exception("Error al crear documentos a cargar: " + msg);
            }

            return true;
        }


        public static bool DescartarPostulante(int idPostulante) {
            CON_postulaciones pos;
            using (var baseE = new Models.emprendeEntities())
            {
                pos = baseE.CON_postulaciones.Where(x => x.id == idPostulante).FirstOrDefault();
                pos.apruebaEtapa = false;
                pos.revisadoEtapa = true;
                baseE.Entry(pos).State = EntityState.Modified;
                baseE.SaveChanges();
            }
            return true;
        }

        public static List<CON_convocatorias> consultarConvocatoriasAbiertas()
        {
            List<CON_convocatorias> convocatorias;
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                convocatorias = a.CON_convocatorias.Where(x => x.fechaCierre >= DateTime.Now && x.idEtapa == 3).OrderByDescending( x => x.fechaApertura ).ToList();
            }
            return convocatorias;
        }

        public static CON_convocatorias consultarConvocatoriaId(int id)
        {
            CON_convocatorias convocatoria;
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                convocatoria = a.CON_convocatorias.Where(x => x.id == id).FirstOrDefault();
            }
            return convocatoria;
        }

        public static PostulacionDetalle GetPostulante(int id)
        {
            PostulacionDetalle postulacion;
            using (var baseE = new Models.emprendeEntities())
            {
                postulacion = baseE.Database.SqlQuery<Models.PostulacionDetalle>("SELECT * FROM VW_PostulantesDetalle WHERE id=@idPostulante ", new System.Data.SqlClient.SqlParameter("idPostulante", id)).FirstOrDefault();
            }
            return postulacion;
        }

        public static InformacionPropuesta GetInformacionPropuesta(int idPostulacion) {
            CON_propuestas propuesta;
            InformacionPropuesta info;
            using (var baseE = new Models.emprendeEntities())
            {
                baseE.Configuration.LazyLoadingEnabled = false;
                propuesta = baseE.CON_propuestas.Include("CON_propuestaObjetivos").Where(x => x.idPostulacion==idPostulacion).OrderByDescending(y => y.id).FirstOrDefault();
            }
            info = new InformacionPropuesta();
            if (propuesta != null) {
                info.DatosPropuesta = GetDatosPropuesta(idPostulacion,(decimal)propuesta.total, (decimal)propuesta.ventaEstimada);
                info.AmortizacionCalculada = GetDatosCalculadosAmortizacion((int)propuesta.plazo, (int)propuesta.gracia, (decimal)propuesta.total, (decimal)propuesta.tasa, (DateTime)propuesta.fecha);
                info.ListaAmortizacion = GetDatosTablaAmortizacion((int)propuesta.plazo, (int)propuesta.gracia, (decimal)propuesta.total, (decimal)propuesta.tasa, (DateTime)propuesta.fecha);
                
                info.Objetivos = propuesta.CON_propuestaObjetivos.ToList();
                info.Objetivos.ForEach(b => b.CON_propuestas = null);//coloco null para que no haya relacion y que la conversi;on de json sea exitosa
                info.Propuesta = propuesta;
                info.ObjetivosJson = Funciones.ToJson(info.Objetivos);
            }
            return info;
        }

        public string consultarIdPublico( int idRefencia, int idTipo)
        {
            string idPublico;
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                idPublico = a.Database.SqlQuery<string>("ARC_SP_ConsultarIdPublico @idTipo, @idReferencia", new System.Data.SqlClient.SqlParameter("idTipo", idTipo), new System.Data.SqlClient.SqlParameter("idReferencia", idRefencia)).FirstOrDefault();
            }
            return idPublico;
        }
        public static List<CON_requisitosPostulante> GetRequisitosPostulanteEsVerificados(int idP) {
            List<CON_requisitosPostulante> requisitos;
            using (var baseE = new Models.emprendeEntities()) {
                baseE.Configuration.LazyLoadingEnabled = false;
                requisitos = baseE.CON_requisitosPostulante.Include(r => r.CON_requisitos).Where(x => x.idPostulante == idP && x.CON_requisitos.esVerificar == true).OrderBy(o => o.CON_requisitos.orden).ToList();
            }
            return requisitos;
        }
        public static bool GuardarObjetivosPropuesta(int idP, List<CON_propuestaObjetivos> objs,decimal totalPropuesta, int plazo, int gracia,int tasaAporte,decimal ventaEstimada,decimal coutaMensual,decimal puntoEquilibrio, string condiciones, string notas, string observaciones) {
            CON_propuestas propuestas;
            using (var baseE = new Models.emprendeEntities()) {
                propuestas = new CON_propuestas() {idPostulacion=idP,fecha=DateTime.Now,plazo=plazo,gracia=gracia,ventaEstimada=ventaEstimada,condiciones=condiciones,notas=notas,observaciones=observaciones,total=totalPropuesta,tasa= tasaAporte, coutaMensual=coutaMensual,puntoEquilibrio=puntoEquilibrio };
                foreach (CON_propuestaObjetivos obj in objs) {
                    propuestas.CON_propuestaObjetivos.Add(new CON_propuestaObjetivos() {nombre=obj.nombre,mes=obj.mes,recursos=obj.recursos,totalObjetivo=obj.totalObjetivo });
                }
                baseE.CON_propuestas.Add(propuestas);
                baseE.SaveChanges();
            }

            return true;
        }

        public static bool VerificarRequisito(int idRequisitoPostulante) {
            CON_requisitosPostulante rp;
            using (var baseE = new Models.emprendeEntities())
            {
                rp = baseE.CON_requisitosPostulante.Where(x => x.id == idRequisitoPostulante).FirstOrDefault();
                rp.verificado = true;
                baseE.Entry(rp).State = System.Data.Entity.EntityState.Modified;
                baseE.SaveChanges();
            }

            return true;
        }

        #region "Propuesta"
        public static DatosPropuesta GetDatosPropuesta(int idPostulacion,decimal inversion, decimal ventaPromedio) {
            DatosPropuesta propuesta;
            CON_postulaciones postulacion;
            Emprendimientos emp;
            decimal temp;
            propuesta = new DatosPropuesta();

            using (var baseE = new Models.emprendeEntities()) {
                postulacion = baseE.CON_postulaciones.Include(i => i.Emprendimientos).Where(x => x.id == idPostulacion).FirstOrDefault();
                emp = postulacion.Emprendimientos;
            }
            propuesta.ValorProyecto = postulacion.calificacion??100;
            propuesta.InversionCrisfe = inversion;
            propuesta.Semilla = inversion;
            propuesta.Inversion = 100;//siempre 100

            propuesta.InversionSugerida = inversion;//ok
            propuesta.InversionVentasEstimadasUnidades = emp.PrecioPromedioPonderado == null?0: Convert.ToDecimal(ventaPromedio / emp.PrecioPromedioPonderado);//Venta Promedio mensual/emprendimientos.PrecioPromedioPonderado
            temp = ((emp.PrecioPromedioPonderado ?? 0) - (emp.CostoPromedioPonderado ?? 0));
            propuesta.PE2Unidades = temp == 0 ? 0 : (inversion+ventaPromedio)/temp;
            propuesta.PE2Dolar = propuesta.PE2Unidades * (emp.CostoPromedioPonderado ?? 0);
            temp = emp.CostoPromedioPonderado == null ? 0 : Convert.ToDecimal(ventaPromedio / emp.CostoPromedioPonderado);
            propuesta.PE2Meses = temp == 0 ? 0 : (propuesta.PE2Unidades/temp);//punto de equilibrio//

            propuesta.PrecioPonderado =emp.PrecioPromedioPonderado??0;//emprendimientos->PrecioPromedioPonderado
            propuesta.CostoPonderado =emp.CostoPromedioPonderado??0;//emprendimientos->CostoPromedioPonderado
            propuesta.VentasMensuales = (emp.unidadesVentaActual ?? 0) * (emp.PrecioPromedioPonderado ?? 0);//Ventas mensuales promedio = unidadesVentaActual * PrecioPromedioPonderado
            propuesta.VentasEstimadasUnidades =emp.unidadesVentaActual??0;// VentaEstimadasEnUnidades->emprendimientos.unidadesVentaActual
            propuesta.CostoFijo =emp.costosFijos??0;//emprendimientos.costosFijos
            propuesta.Tpe = Math.Round(propuesta.PE2Meses,2);

            propuesta.PrecioPonderado = Math.Round(propuesta.PrecioPonderado,2);
            propuesta.CostoPonderado = Math.Round(propuesta.CostoPonderado, 2);
            propuesta.VentasMensuales = Math.Round(propuesta.VentasMensuales, 2);
            propuesta.InversionVentasEstimadasUnidades = Math.Round(propuesta.InversionVentasEstimadasUnidades,2);
            propuesta.PE2Unidades = Math.Round(propuesta.PE2Unidades,2);
            propuesta.PE2Dolar = Math.Round(propuesta.PE2Dolar, 2);
            propuesta.PE2Meses = Math.Round(propuesta.PE2Meses, 2);

            return propuesta;
        }

        public static DatosAmortizacion GetDatosCalculadosAmortizacion(int plazo, int mesesGracia, decimal deudaBase, decimal tasaPorcentaje, DateTime fecha, int diaPago = 15) {
            DateTime inicio;
            DatosAmortizacion datos = new DatosAmortizacion();
            datos.ApoyoEconomico = deudaBase;
            //datos.AporteFondo = Math.Round((deudaBase * (tasaPorcentaje / 100)) + deudaBase, 2);
            datos.TotalApoyo = Math.Round((deudaBase * (tasaPorcentaje / 100)) + deudaBase, 2); //datos.ApoyoEconomico + datos.AporteFondo;
            datos.AporteFondo = datos.TotalApoyo - datos.ApoyoEconomico;
            datos.Tiempo = plazo;
            datos.Reembolso= datos.TotalApoyo / plazo;
            if (fecha.Day > diaPago)
                fecha = fecha.AddMonths(1);
            inicio = new DateTime(fecha.Year, fecha.Month, diaPago);
            inicio = inicio.AddMonths(mesesGracia);
            datos.FechaInicioPago = inicio;

            return datos;
        }
        

        public static List<MesAmortizacion> GetDatosTablaAmortizacion(int plazo,int mesesGracia, decimal deudaBase,decimal tasaPorcentaje,DateTime fecha,int diaPago=15) {
            List<MesAmortizacion> amortizaciones;
            MesAmortizacion mes;
            DateTime inicio;
            decimal couta, deuda;
            decimal? aporte;
            decimal elAporte = 0;
            decimal elCapital = 0;
            DatosAmortizacion datos;

            amortizaciones = new List<MesAmortizacion>();
            datos = GetDatosCalculadosAmortizacion(plazo, mesesGracia, deudaBase, tasaPorcentaje, fecha, diaPago);
            deuda = datos.TotalApoyo;
            aporte = datos.AporteFondo;
            inicio = datos.FechaInicioPago;
            couta = datos.Reembolso;

            amortizaciones.Add(new MesAmortizacion() {numero=0,fecha=null,saldo=deuda,aporte=0,capital=0,reembolso=0 });
            for (int i = 1; i <= plazo; i++)
            {
                deuda = deuda - couta;
                aporte = aporte > 0 ? (aporte - couta) : null;

                elAporte = 0;
                if (aporte != null)
                    elAporte = (aporte > 0) ? couta : (couta + (decimal)aporte);

                elCapital = couta - elAporte;

                mes = new MesAmortizacion() { numero = i, fecha = inicio, saldo = Math.Round(deuda,2), aporte = (aporte != null ? Math.Round(elAporte,2) : 0), capital = (elCapital != 0 ? Math.Round(elCapital,2) : 0), reembolso = -Math.Round(couta,2) };
                amortizaciones.Add(mes);
                inicio = inicio.AddMonths(1);
            }

            return amortizaciones;
        }


        public static void AceptarPropuesta(int idPostulacion) {
            CON_propuestas propuesta;
            CON_postulaciones postulacion;
            //Acepto la última propuesta
            using (var baseE = new Models.emprendeEntities()) {
                propuesta = baseE.CON_propuestas.Where(x => x.idPostulacion == idPostulacion).OrderByDescending(y => y.id).FirstOrDefault();
                postulacion = baseE.CON_postulaciones.Where(x => x.id == idPostulacion).FirstOrDefault();
                postulacion.idPropuesta = propuesta.id;
                baseE.Entry(postulacion).State = System.Data.Entity.EntityState.Modified;
                baseE.SaveChanges();
            }
        }

        public static List<CON_comiteMiembros> GetMiembrosComitePrefactibilidad() {
            List<CON_comiteMiembros> mm;
            using (var baseE = new Models.emprendeEntities())
            {
                mm = baseE.CON_comiteMiembros.ToList();
            }

            return mm;
        }

        public static bool AgregarMiembrosPrefactibilidadComitePostulacion(List<int> idPostulantes, int idMiembroComite, TiposComite tipo) {
            CON_comitePostulaciones cp;
            using (var baseE = new Models.emprendeEntities())
            {
                foreach (int p in idPostulantes) {
                    cp = new CON_comitePostulaciones() { idPostulacion = p, idComiteMiembro = idMiembroComite, esCalificado = false, calificacion = 0, aprueba = false, idTipo = (int)tipo, idPublico = Guid.NewGuid().ToString() };
                    baseE.CON_comitePostulaciones.Add(cp);
                }
                baseE.SaveChanges();
            }

            return true;
        }


        public static bool LimpiarMiembrosComitePostulacion(List<int> idPostulantes) {
            using (var baseE = new Models.emprendeEntities())
            {
                baseE.CON_comitePostulaciones.RemoveRange(baseE.CON_comitePostulaciones.Where(x => idPostulantes.Contains(x.idPostulacion)));
                baseE.SaveChanges();
            }
            return true;

        }

        public static void AceptarContrato(int idPostulacion) {
            CON_postulaciones postulacion;
            using (var baseE = new Models.emprendeEntities()) {
                postulacion = baseE.CON_postulaciones.Where(x => x.id == idPostulacion).FirstOrDefault();
                postulacion.estaContratado= true;
                baseE.Entry(postulacion).State = System.Data.Entity.EntityState.Modified;
                baseE.SaveChanges();
            }
        }
        #endregion

        public static bool CerrarEtapaPostulante(int idP,bool? cumpleCapacitacion = null,int? idProveedorCapacitacion=null)
        {
            CON_postulaciones post;
            BitacoraEtapasPostulacion bitacora;

            int idNextEtapa;
            using (var baseE = new Models.emprendeEntities())
            {
                post = baseE.CON_postulaciones.Where(x => x.id == idP).FirstOrDefault();
                idNextEtapa = (int)Funciones.GetNextEtapa((int)post.idEtapa);
                post.idEtapa = idNextEtapa;
                post.revisadoEtapa = false;
                post.apruebaEtapa = false;
                if (cumpleCapacitacion != null && idProveedorCapacitacion != null) {//En la etapa de documentos sucede esto
                    post.cumpleCapacitacion = cumpleCapacitacion;
                    post.idProveedorCapacitacion = idProveedorCapacitacion;
                }
                baseE.Entry(post).State = System.Data.Entity.EntityState.Modified;
                bitacora = new BitacoraEtapasPostulacion() { idUsuario = 0, idEtapa = idNextEtapa, fecha = DateTime.Now, idPostulacion = post.id, comentario = "Cambio de etapa individual" };
                baseE.BitacoraEtapasPostulacion.Add(bitacora);
                baseE.SaveChanges();
            }
            //Cuando acaban todos se cierra la convocatoria?
            return true;
        }
        
    }
}