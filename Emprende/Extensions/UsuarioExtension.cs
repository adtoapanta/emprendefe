﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Emprende.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;

namespace Emprende.Extensions
{
    public class UsuarioExtension
    {
        MIS_Personas persona;
        public RegisterViewModel crearPersona(string nombre, string  apellido, string documento, string telefono, string sexo, string nacionalidad, string correo, string ciudad)
        {
            RegisterViewModel model = new RegisterViewModel();
            using (var a = new Models.emprendeEntities())
            {
                persona = new MIS_Personas();
                persona.nombre = nombre;
                persona.apellido = apellido;
                persona.cedula = documento;
                persona.telefono = telefono;
                persona.sexo = sexo;
                
                persona.correo = correo;
                
                a.MIS_Personas.Add(persona);
                a.SaveChanges();

                
                model.Email = correo;
                model.idPersona = persona.id;
            }
            return model;
        }
        public static async Task<int> consultarIdPersona(string userName)
        {
            int idPersona = 0;
            
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                idPersona = await a.Database.SqlQuery<int>("SP_ConsultarIdPersonaPorUsuario @userName", new SqlParameter("userName", userName)).FirstOrDefaultAsync(); ;
            }
            return idPersona;
        }
    }
}