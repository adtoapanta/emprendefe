﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emprende.Models;
using System.Data.Entity;
using Newtonsoft.Json;
using System.Data.SqlClient;

namespace Emprende.Extensions
{
    public class RequisitosPostulanteExtension
    {
        public int idPostulacion;
        CON_requisitosPostulante requisito;
        private emprendeEntities db;

        public RequisitosPostulanteExtension(emprendeEntities db, int id)
        {
            this.db = db;
            this.requisito = db.CON_requisitosPostulante.Where(x => x.id == id).First();
            this.idPostulacion = requisito.idPostulante ?? 0;
        }

        public void completarRequisito()
        {
            requisito.completo = true;

            //cuando cambio productos (requisito 10) pongo balance en no finalizado
            if (requisito.idRequisito == 10)
            {
                //busco el requisito 11 (BALANCE) para cambiar su estado
                CON_requisitosPostulante requisitoBalance = this.db.CON_requisitosPostulante.Where(r => r.idRequisito == 11).Where(r => r.idPostulante == idPostulacion).First();
                requisitoBalance.completo = false;
                requisitoBalance.verificado = false;
            }

            db.SaveChanges();
        }


        public static List<CON_requisitosPostulante> consultarRequisitosPostulante(int idPostulacion, int idEtapaPostulacion)
        {
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                List<CON_requisitosPostulante> requisitosPostulante;
                requisitosPostulante = new List<CON_requisitosPostulante>();

                requisitosPostulante = a.CON_requisitosPostulante.Include("CON_requisitos").Where(x => x.idPostulante == idPostulacion && x.CON_requisitos.idEtapa <= idEtapaPostulacion).ToList();
                return requisitosPostulante;
            }
        }
    }
}