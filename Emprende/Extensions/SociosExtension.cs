﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emprende.Models;

namespace Emprende.Extensions
{
    public class SociosExtension
    {
        public void agregarSocioEmprendimiento(int idEmprendimiento, string nombre, string telefono, string cargo, string aporte, int porcentaje)
        {
            using (var a = new Models.emprendeEntities())
            {
                socios socio = new socios();
                a.Configuration.LazyLoadingEnabled = false;

                socio.idEmprendimiento = idEmprendimiento;
                socio.nombre = nombre;
                socio.telefono = telefono;
                socio.cargo = cargo;
                socio.aporte = aporte;
                socio.porcentaje = porcentaje;

                a.socios.Add(socio);
                a.SaveChanges();
            }
        }

        public void editarSocioEmprendimiento(int idSocio, string nombre, string telefono, string cargo, string aporte, int porcentaje)
        {
            using (var a = new Models.emprendeEntities())
            {                
                a.Configuration.LazyLoadingEnabled = false;
                socios socio = a.socios.Where(x=>x.id == idSocio).FirstOrDefault();
                
                socio.nombre = nombre;
                socio.telefono = telefono;
                socio.cargo = cargo;
                socio.aporte = aporte;
                socio.porcentaje = porcentaje;
                
                a.SaveChanges();
            }
        }

        public static void eliminarSocio(int idSocio)
        {
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                socios socio = a.socios.Where(x => x.id == idSocio).FirstOrDefault();

                a.socios.Remove(socio);
                a.SaveChanges();
            }
        }

        public static List<socios> consultarSociosEmprendimiento(int idEmprendimiento)
        {
            
            using (var a = new Models.emprendeEntities())
            {
                
                a.Configuration.LazyLoadingEnabled = false;

                List<socios> socios = a.socios.Where(x => x.idEmprendimiento == idEmprendimiento).ToList();

                return socios;
            }
        }
    }
}