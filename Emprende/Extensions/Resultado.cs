﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Emprende.Extensions
{
    public class Resultado
    {

        public Boolean exitoso { get; set; }
        public string data;
        public int codigo { get; set; }

        public Resultado()
        {
        }

        public Resultado(Boolean exitoso, object data, int codigo = 0,bool anidacionIndefinida = true)
        {
            this.exitoso = exitoso;
            //Prefiero serializar con newtonsoftjson
            if (anidacionIndefinida)
            {
                this.data = JsonConvert.SerializeObject(data, new JsonSerializerSettings()
                {
                    PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                    Formatting = Formatting.Indented
                });
            }
            else
            {
                this.data = JsonConvert.SerializeObject(data, Formatting.Indented,new JsonSerializerSettings{ReferenceLoopHandling = ReferenceLoopHandling.Serialize});
            }


            this.codigo = codigo;
        }
        



    }
}