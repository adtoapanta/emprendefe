﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emprende.Models;
namespace Emprende.Extensions
{
    public class ReferenciasExtension
    {

        public static Mis_referencias guardarReferenciaPersona(int idPersona, int idParentesco, string nombre, string direccion, string telefono)
        {
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                Mis_referencias referencia = new Mis_referencias();
                referencia.idPersona = idPersona;
                referencia.parentesco = idParentesco;
                referencia.nombre = nombre;
                referencia.direccion = direccion;
                referencia.telefono = telefono;

                a.Mis_referencias.Add(referencia);
                a.SaveChanges();
                return referencia;

            }
        }

        public static Mis_referencias editarReferenciaPersona(int idReferencia, int idParentesco, string nombre, string direccion, string telefono)
        {
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                Mis_referencias referencia = a.Mis_referencias.Where(x => x.id == idReferencia).FirstOrDefault();
                
                referencia.parentesco = idParentesco;
                referencia.nombre = nombre;
                referencia.direccion = direccion;
                referencia.telefono = telefono;
                
                a.SaveChanges();
                return referencia;

            }
        }

        public static List<Mis_referencias> consultarReferenciasPersona(int idPersona)
        { 
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                List<Mis_referencias> referencias = a.Mis_referencias.Where(x => x.idPersona == idPersona).ToList();
                
                return referencias;

            }
        }

        public static Mis_referencias consultarReferencia(int id)
        {
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                Mis_referencias referencia = a.Mis_referencias.Where(x => x.id==id).FirstOrDefault();

                return referencia;

            }
        }

        public static void eliminarReferencia(int id)
        {
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                Mis_referencias referencia = a.Mis_referencias.Where(x => x.id == id).FirstOrDefault();

                a.Mis_referencias.Remove(referencia);
                a.SaveChanges();

            }
        }
    }
}