﻿using Emprende.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Emprende.Extensions
{
    public class BalanceExt
    {
        CON_postulaciones postulacion;
        public Emprendimientos emprendimiento;
        public Balances.Balance balanceAct;
        public Balances.Balance balanceProy;
        public decimal costoAct, ventasAct;
        public decimal costoProy, ventasProy;

        public BalanceExt(CON_postulaciones post)
        {
            this.postulacion = post;
            this.emprendimiento = this.postulacion.Emprendimientos;
            this.balanceAct = new Balances.Balance(this.emprendimiento.id, 1, false, false);
            this.balanceProy = new Balances.Balance(this.emprendimiento.id, 1, false, true);
        }

        public void calculaVentasBalance(bool esProyectado)
        {
            int unidadesAct, unidadesProy;
            decimal cpp, ppp;

            unidadesAct = emprendimiento.unidadesVentaActual ?? 0;
            unidadesProy = emprendimiento.unidadesVentaProy ?? 0;
            cpp = emprendimiento.CostoPromedioPonderado ?? 0;
            ppp = emprendimiento.PrecioPromedioPonderado ?? 0;

            if (esProyectado)
            {
                costoProy = cpp * unidadesProy;
                ventasProy = ppp * unidadesProy;
            }
            else
            {
                costoAct = cpp * unidadesAct;
                ventasAct = ppp * unidadesAct;
            }
        }

        public static void guardaDatoBalance(int idBal, int idCampo, decimal valor)
        {
            Balances.DatosBalance miDatoBalance;
            miDatoBalance = new Balances.DatosBalance(idCampo, idBal);

            miDatoBalance.idBalance = idBal;
            miDatoBalance.idCampo = idCampo;
            miDatoBalance.valor = valor;

            miDatoBalance.guardar();
        }
    }
}