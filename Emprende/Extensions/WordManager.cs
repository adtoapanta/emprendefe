﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emprende.Models;
using Spire.Doc;
using Spire.Doc.Documents;
using Spire.Doc.Fields;
using System.Drawing;

namespace Emprende.Extensions
{
    public class WordManager
    {
        Document document;
        string nombreArchivo;

        string pathArchivos;
        public PostulacionCompleta postulacionCompleta;
        private emprendeEntities db;
        private int idPostulacion;
        private reportes tipoReporte;
        internal string pathDescarga;
        internal MIS_Personas ascesor;
        tablasReportes tipoTablaActual;

        public CON_comitePostulaciones comitePostulacion;
        

        Table tableActual;
        String[] HeaderTablaActual;
        String[][] dataTablaActual;

        public WordManager(emprendeEntities db, int idPost, reportes tipoRep, MIS_Personas ascesor, int idComitePostulacion = 0)
        {
            this.db = db;

            this.idPostulacion = idPost;
            //cuando estoy sacando el reporte de comité, debo buscar el id de postulación 
            if (idComitePostulacion != 0)
            {
                comitePostulacion = db.CON_comitePostulaciones.Include("CON_ComiteMiembros").Where(cp => cp.id == idComitePostulacion).First();
                this.idPostulacion = comitePostulacion.idPostulacion;
            }

            this.tipoReporte = tipoRep;
            this.postulacionCompleta = new PostulacionCompleta(db, this.idPostulacion);
            this.ascesor = ascesor;

            //defino el nombre del archivo 
            switch (this.tipoReporte)
            {
                case reportes.medioDeAprobacion:
                    this.nombreArchivo = "ReporteMedioDeAprobacion";
                    break;
                case reportes.Contrato:
                    this.nombreArchivo = "ReporteContrato";
                    break;
                case reportes.diagnosticoEmprendedor:
                    this.nombreArchivo = "ReporteDiagnosticoEmprendedor";
                    break;
                case reportes.brief:
                    this.nombreArchivo = "ReporteBrief";
                    break;
                case reportes.comite:
                    this.nombreArchivo = "ReporteEvaluadorComite";
                    break;
            }

            this.pathArchivos = System.Web.HttpContext.Current.Server.MapPath("~/Archivos/");
            string pathDoc = pathArchivos + nombreArchivo + ".docx";

            document = new Document();
            document.LoadFromFile(pathDoc);
        }

        internal void procesaReporte()
        {
            switch (this.tipoReporte)
            {
                case reportes.medioDeAprobacion:
                    procesarMedioDeAprobacion();
                    break;
                case reportes.Contrato:
                    procesarConvenio();
                    break;
                case reportes.diagnosticoEmprendedor:
                    procesarDiagnosticoEmprendedor();
                    break;
                case reportes.brief:
                    procesarBrief();
                    break;
                case reportes.comite:
                    procesarComitePostulacion();
                    break;
            }
        }

        public void reemp(string textToReplace, string replacedText)
        {
            if (replacedText == null) replacedText = "";
            document.Replace(textToReplace, replacedText, false, true);
        }
        public void guardarCopy()
        {
            this.pathDescarga = pathArchivos + nombreArchivo + "-descargar.docx";
            document.SaveToFile(pathDescarga, FileFormat.Docx);
        }
        public void cerrarDocXml()
        {
            document.Close();
            document.Dispose();
        }

        void setTabla(string txtTabla)
        {
            //step 2
            Section section = document.Sections[0];
            TextSelection selection = document.FindString(txtTabla, true, true);

            if (selection != null)
            {
                TextRange range = selection.GetAsOneRange();
                Paragraph paragraph = range.OwnerParagraph;
                Body body = paragraph.OwnerTextBody;
                int index = body.ChildObjects.IndexOf(paragraph);

                generaTabla(section);

                //step 4
                body.ChildObjects.Remove(paragraph);
                body.ChildObjects.Insert(index, tableActual);
            }
            else
            {
                throw new Exception("no se encontró la selección a reemplazar");
            }
        }
        private void generaTabla(Section s)
        {
            //Create Table
            tableActual = s.AddTable(true);

            //Add Cells
            tableActual.ResetCells(dataTablaActual.Length + 1, HeaderTablaActual.Length);

            setTableHeader();
            setTableData();
        }
        void setTableHeader()
        {
            //Header Row
            TableRow FRow = tableActual.Rows[0];
            FRow.IsHeader = true;
            //Row Height
            FRow.Height = 8;
            //Header Format
            //FRow.RowFormat.BackColor = Color.AliceBlue;
            for (int i = 0; i < HeaderTablaActual.Length; i++)
            {
                //Cell Alignment
                Paragraph p = FRow.Cells[i].AddParagraph();
                p.Format.AfterSpacing = 0;
                FRow.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                p.Format.HorizontalAlignment = HorizontalAlignment.Center;
                //Data Format
                TextRange TR = p.AppendText(HeaderTablaActual[i]);
                TR.CharacterFormat.FontName = "Arial";
                TR.CharacterFormat.FontSize = 10;
                //TR.CharacterFormat.TextColor = Color.Teal;
                TR.CharacterFormat.Bold = true;
            }
        }
        void setTableData()
        {
            switch (tipoTablaActual)
            {
                case tablasReportes.objetivos:
                    setTableDataObjetivos();
                    break;
                case tablasReportes.productos:
                    setTableDataProductos();
                    break;
                case tablasReportes.CalificacionComite:
                    setTableDataCalifComite();
                    break;
                case tablasReportes.Matriz:
                    setTableDataMatriz();
                    break;
            }
        }

        private void reemplazarVarsGen()
        {
            string ciudad = "Quito";
            string fecha = DateTime.Today.ToString("yyyyMMdd");

            //todo buscar el usuario logeado
            string nombreAscesor = "";
            string apellidoAscesor = "";
            if (ascesor != null)
            {
                nombreAscesor = ascesor.nombre;
                apellidoAscesor = ascesor.apellido;
            }

            reemp("{Ciudad}", ciudad);
            reemp("{Fecha}", fecha);

            reemp("{NombreAs}", nombreAscesor);
            reemp("{ApellidoAs}", apellidoAscesor);
        }

        #region MedioDeAprobacion
        private void procesarMedioDeAprobacion()
        {
            reemplazarVarsGen();
            reemplazarVarsDatosPersonales();
            reemplazarVarsDatosEmprendimiento();
        }
        private void reemplazarVarsDatosPersonales()
        {
            MIS_Personas emprendedor = postulacionCompleta.emprendedor;
            MIS_Direcciones dirEmprendedor = emprendedor.objDireccion();

            string apellido1 = emprendedor.apellido;
            string apellido2 = emprendedor.apellido2;
            string nombre1 = emprendedor.nombre;
            string nombre2 = emprendedor.nombre2;
            string cedula = emprendedor.cedula;
            string celular = emprendedor.celular;
            string telFijo = emprendedor.telefono;
            string dirDomicilio = dirEmprendedor.direccion();
            string sector = dirEmprendedor.referencia; //todo confirmar que sector es referencia 

            //estadoCivil //todo
            string estadoCivilSoltero = "";
            string estadoCivilCasado = "";
            string estadoCivilDivorciado = "";
            string estadoCivilViudo = "";
            string estadoCivilSeparado = "";
            string estadoCivilUnion = "";

            enumEstadoCivilEmprendedor estadoCivilEmp = (enumEstadoCivilEmprendedor)emprendedor.idEstadoCivil;
            switch (estadoCivilEmp)
            {
                case enumEstadoCivilEmprendedor.soltero:
                    estadoCivilSoltero = "X";
                    break;
                case enumEstadoCivilEmprendedor.casado:
                    estadoCivilCasado = "X";
                    break;
                case enumEstadoCivilEmprendedor.divoricado:
                    estadoCivilDivorciado = "X";
                    break;
                case enumEstadoCivilEmprendedor.viudo:
                    estadoCivilViudo = "X";
                    break;
                case enumEstadoCivilEmprendedor.separado:
                    estadoCivilSeparado = "X";
                    break;
                case enumEstadoCivilEmprendedor.unionLibre:
                    estadoCivilUnion = "X";
                    break;
            }

            reemp("{Apellido1}", apellido1);
            reemp("{Apellido2}", apellido2);
            reemp("{Nombre1}", nombre1);
            reemp("{Nombre2}", nombre2);
            reemp("{cedula}", cedula);
            reemp("{celular}", celular);
            reemp("{telFijo}", telFijo);
            reemp("{dirDomicilio}", dirDomicilio);
            reemp("{sector}", sector);

            reemp("{s}", estadoCivilSoltero);
            reemp("{c}", estadoCivilCasado);
            reemp("{d}", estadoCivilDivorciado);
            reemp("{v}", estadoCivilViudo);
            reemp("{e}", estadoCivilSeparado);
            reemp("{u}", estadoCivilUnion);
        }
        private void reemplazarVarsDatosEmprendimiento()
        {
            Emprendimientos emprendimiento = postulacionCompleta.emprendimiento;
            MIS_Direcciones dirEmprendimiento = emprendimiento.objDireccion();

            string nombreEmp = emprendimiento.nombre;
            string direccion = dirEmprendimiento.direccion();

            string objetivoFin = postulacionCompleta.postulacion.objetivosFinanciar ?? "";
            string objetivoFin1 = "";
            string objetivoFin2 = "";
            if (objetivoFin.Length > 45) //todo verificar este valor (tratar de cortar en palabras?)
            {
                objetivoFin1 = objetivoFin.Substring(0, 45);
                objetivoFin2 = objetivoFin.Substring(46, 30);
            }
            else
            {
                objetivoFin1 = objetivoFin;
                objetivoFin2 = "";
            }
            decimal totalPropuesta = postulacionCompleta.ultimaPropuesta.total ?? 0;
            string valorSol = postulacionCompleta.postulacion.totalInversion.ToString();
            string valorSug = totalPropuesta.ToString();
            string ventasAct = emprendimiento.getTotalVentasAct().ToString();
            string ventasProg = emprendimiento.getTotalVentasProy().ToString();
            string valorDesembolso = totalPropuesta.ToString();

            //lugar actual
            string casaPropia = "";
            string casaFam = "";
            string alquilado = "";
            string otro = "";

            tiposLugar tipoLugarEmprendimiento = (tiposLugar)emprendimiento.MIS_Direcciones.idTipoLugar;
            switch (tipoLugarEmprendimiento)
            {
                case tiposLugar.casaPropia:
                    casaPropia = "X";
                    break;
                case tiposLugar.casaFamiliares:
                    casaFam = "X";
                    break;
                case tiposLugar.localAlquilado:
                    alquilado = "X";
                    break;
                case tiposLugar.otro:
                    otro = "X";
                    break;
            }

            //tabla
            decimal tiempoEq = postulacionCompleta.ultimaPropuesta.puntoEquilibrio ?? 0;
            string TiempoEquilibrio = Math.Ceiling(tiempoEq).ToString();

            decimal pae = postulacionCompleta.postulacion.PorcentajeAporteEmprendedor();
            string PorcentajeAporteEmprendedor = Math.Round(pae, 2).ToString() + " %";

            string Plazo = postulacionCompleta.ultimaPropuesta.plazo.ToString();
            string Reembolso = postulacionCompleta.ultimaPropuesta.coutaMensual.ToString();

            //reemplazos
            reemp("{nombreEmp}", nombreEmp);
            reemp("{cp}", casaPropia);
            reemp("{cf}", casaFam);
            reemp("{a}", alquilado);
            reemp("{o}", otro);
            reemp("{dirección}", direccion);
            reemp("{objetivoFin1}", objetivoFin1);
            reemp("{objetivoFin2}", objetivoFin2);
            reemp("{valorSol}", valorSol);
            reemp("{valorSug}", valorSug);
            reemp("{ventasAct}", ventasAct);
            reemp("{ventasProg}", ventasProg);
            reemp("{valorDesembolso}", valorDesembolso);

            setTablaObjetivos();

            reemp("{TiempoEquilibrio}", TiempoEquilibrio);
            reemp("{AporteEmp}", PorcentajeAporteEmprendedor);
            reemp("{Plazo}", Plazo);
            reemp("{Reembolso}", Reembolso);
        }

        void setTablaObjetivos()
        {
            tipoTablaActual = tablasReportes.objetivos;
            HeaderTablaActual = getHeaderObjetivos();
            dataTablaActual = getDataObjetivos();
            setTabla("{tablaObjetivos}");
        }
        private String[] getHeaderObjetivos()
        {
            //Create Header and Data
            String[] Header = { "Mes", "Objetivo", "Recursos", "Desembolso" };
            return Header;

        }
        private String[][] getDataObjetivos()
        {
            bool escribirObjetivo = true;
            string[] fila;
            String[][] tabla;
            int cont = -1;
            int contTotal = 0;

            if (this.postulacionCompleta.ultimaPropuesta != null)
            {
                List<CON_propuestaObjetivos> objetivosUltimaPropuesta = this.postulacionCompleta.ultimaPropuesta.CON_propuestaObjetivos.ToList();

                foreach (CON_propuestaObjetivos objetivo in objetivosUltimaPropuesta)
                {
                    List<Recurso> rec = objetivo.GetRecursos();
                    foreach (Recurso re in rec)
                    {
                        contTotal += 1;
                    }
                }

                tabla = new String[contTotal][];

                foreach (CON_propuestaObjetivos objetivo in objetivosUltimaPropuesta)
                {
                    escribirObjetivo = true;

                    List<Recurso> rec = objetivo.GetRecursos();
                    foreach (Recurso re in rec)
                    {
                        cont += 1;

                        if (escribirObjetivo)
                        {
                            fila = new String[] { objetivo.mes.ToString(), objetivo.nombre, re.nombre, re.valor.ToString() };
                        }
                        else
                        {
                            fila = new String[] { "", "", re.nombre, re.valor.ToString() };
                        }

                        tabla[cont] = fila;

                        escribirObjetivo = false;
                    }
                }

                return tabla;
            }
            else
            {
                return new String[0][];
            }

            //String[][] data = {
            //                      new String[]{"Spire.Doc for .NET",".NET Word Component","1","$799.00","$799.00"},
            //                      new String[]{"Spire.XLS for .NET",".NET Excel Component","2","$799.00","$1,598.00"},
            //                      new String[]{"Spire.Office for .NET",".NET Office Component","1","$1,899.00","$1,899.00"},
            //                      new String[]{"Spire.PDF for .NET",".NET PDFComponent","2","$599.00","$1,198.00"},
            //                  };

        }
        void setTableDataObjetivos()
        {
            //Data Row
            for (int r = 0; r < dataTablaActual.Length; r++)
            {
                TableRow DataRow = tableActual.Rows[r + 1];

                //Row Height
                DataRow.Height = 8;

                //C Represents Column.
                for (int c = 0; c < dataTablaActual[r].Length; c++)
                {
                    //Cell Alignment
                    //DataRow.Cells[c].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                    //Fill Data in Rows
                    Paragraph p2 = DataRow.Cells[c].AddParagraph();
                    p2.Format.AfterSpacing = 0;

                    TextRange TR2 = p2.AppendText("");
                    if (c == 3)
                    {
                        TR2 = p2.AppendText("$ " + dataTablaActual[r][c]);
                    }
                    else
                    {
                        TR2 = p2.AppendText(dataTablaActual[r][c]);
                    }

                    //Format Cells
                    if (c == 0 || c == 3)
                    {
                        p2.Format.HorizontalAlignment = HorizontalAlignment.Right;
                    }
                    TR2.CharacterFormat.FontName = "Arial";
                    TR2.CharacterFormat.FontSize = 8;
                    //TR2.CharacterFormat.TextColor = Color.Brown;
                }
            }
        }
        #endregion

        #region Convenio
        private void procesarConvenio()
        {
            MIS_Personas emprendedor = postulacionCompleta.emprendedor;
            Emprendimientos emprendimiento = postulacionCompleta.emprendimiento;
            CON_propuestas propuesta = postulacionCompleta.ultimaPropuesta;
            CON_postulaciones postulacion = postulacionCompleta.postulacion;
            MIS_Direcciones direccionEmprendedor = emprendedor.objDireccion();

            reemp("{nombreCEmp}", emprendedor.getNombreCompleto());
            reemp("{NOMBRECEMP}", emprendedor.getNombreCompleto());
            reemp("{estCivilEmp}", emprendedor.getEstadoCivil());
            reemp("{telefonoEmp}", emprendedor.telefono); //todo poner formato (+593) (9) (99070957)?
            reemp("{cedulaEmp}", emprendedor.cedula);
            reemp("{CEDULAEMP}", emprendedor.cedula);
            reemp("{emailEmp}", emprendedor.correo);
            reemp("{dirEmp}", direccionEmprendedor.direccion());
            reemp("{barrioEmp}", direccionEmprendedor.barrio);
            reemp("{sectorEmp}", direccionEmprendedor.referencia);
            reemp("{ciudadEmp}", direccionEmprendedor.ciudad());
            reemp("{provinciaEmp}", direccionEmprendedor.provincia());

            reemp("{convocatoria}", postulacionCompleta.convocatoria.nombre);
            reemp("{CONVOCATORIA}", postulacionCompleta.convocatoria.nombre);

            reemp("{actividadEconomicaEmp}", emprendimiento.descripcion);

            decimal total = propuesta.total ?? 0;
            reemp("{desembolso}", convertirNumeroALetrasDolares(total));
            reemp("{aporteEmp}", convertirNumeroALetrasDolares(propuesta.aporteEmprendedor()));

            decimal plazo = propuesta.plazo ?? 0;
            reemp("{plazo}", convertirNumeroALetras(plazo));

            string fecha = string.Format("{0} días de {1} de {2}", DateTime.Today.Day, DateTime.Today.ToString("MMMM"), DateTime.Today.Year);
            reemp("{fechaContrato}", fecha);
        }

        string convertirNumeroALetrasDolares(decimal numero)
        {
            string numeroS = numero.ToString();
            string numeroEnLetras = ConvertidorNumeroATexto.enletras(numeroS);
            return string.Format("{0} DÓLARES DE LOS ESTADOS UNIDOS DE AMERICA ($ {1})", numeroEnLetras, numeroS);
        }
        string convertirNumeroALetras(decimal numero)
        {
            string numeroS = numero.ToString();
            string numeroEnLetras = ConvertidorNumeroATexto.enletras(numeroS);
            return string.Format("{0} ({1})", numeroEnLetras, numeroS);
        }
        #endregion

        #region DiagnosticoEmprendedor
        private void procesarDiagnosticoEmprendedor()
        {
            reemplazarVarsGen();
        }
        #endregion

        #region Brief
        private void procesarBrief()
        {
            MIS_Personas emprendedor = postulacionCompleta.emprendedor;
            Emprendimientos emprendimiento = postulacionCompleta.emprendimiento;
            CON_canvas cv = postulacionCompleta.miCanvas;

            reemp("{{nombre}}", emprendedor.getNombreCompleto());
            reemp("{{nombreEmprendimiento}}", emprendimiento.nombre);
            reemp("{{emprendimiento.brief}}", emprendimiento.brief);

            reemp("{{Sector}}", postulacionCompleta.respuestasScoring.p1);

            //tabla productos
            setTablaProductos();

            //canvas
            reemp("{{propuesta}}", cv.propuesta);
            reemp("{{socios}}", cv.socios);
            reemp("{{actividades}}", cv.actividades);
            reemp("{{relaciones}}", cv.relaciones);
            reemp("{{clientes}}", cv.clientes);
            reemp("{{recursos}}", cv.recursos);
            reemp("{{canales}}", cv.canales);


            //Calificaciones         
            reemp("{{ScorePerfil}}", postulacionCompleta.evaluacionScoring.puntajeTotal.ToString());
            reemp("{{capacitadoPor}}", postulacionCompleta.postulacion.CON_ProveedoresCapacitacion.nombre);

            //tabla calificaciones comité
            setTablaCalificaciones();

            reemp("{{califTotal}}", this.postulacionCompleta.ce.calificacion.ToString());
            string aprueba = this.postulacionCompleta.ce.aprueba == true ? "Aprobado" : "Rechazado";
            reemp("{{resultadoCalif}}", aprueba); 

            //inversiones requeridas
            setTablaMatriz();

            reemp("{{puntoEqilibrio}}", postulacionCompleta.ultimaPropuesta.puntoEquilibrio.ToString());
            reemp("{{ExcedenteMensual}}", "0"); //Balance.neto
        }

        //tabla productos
        void setTablaProductos()
        {
            tipoTablaActual = tablasReportes.productos;
            HeaderTablaActual = getHeaderProductos();
            dataTablaActual = getDataProductos();
            setTabla("{{Tabla productos}}");
        }
        private String[] getHeaderProductos()
        {
            String[] Header = { "Nombre", "% Contrib.", "Costo", "PVP", "Unidades", "Ventas" };
            return Header;
        }
        private String[][] getDataProductos()
        {
            string[] fila;
            String[][] arrayData;
            int cont = -1;

            List<Productos> productos = this.postulacionCompleta.productos;

            if (productos != null)
            {
                arrayData = new String[productos.Count][];

                foreach (Productos producto in productos)
                {
                    cont += 1;

                    string nombre = producto.nombre;
                    decimal p = producto.porcentaje ?? 0;
                    string pContrib = Convert.ToString(Math.Round((p * 100), 2));
                    string costo = Convert.ToString(Math.Round(producto.costo, 2));
                    string precio = Convert.ToString(Math.Round(producto.Precio, 2));
                    string unidades = Convert.ToString(Math.Round(producto.unidadesActual, 2));
                    decimal v = producto.ventaActual ?? 0;
                    string ventas = Convert.ToString(Math.Round(v, 2));

                    //Nombre	% Contrib.	Costo	PVP	Unidades	Ventas	
                    fila = new String[] { nombre, pContrib, costo, precio, unidades, ventas };
                    arrayData[cont] = fila;
                }

                return arrayData;
            }
            else
            {
                return new String[0][];
            }
        }
        void setTableDataProductos()
        {
            //Data Row
            for (int r = 0; r < dataTablaActual.Length; r++)
            {
                TableRow DataRow = tableActual.Rows[r + 1];

                //Row Height
                DataRow.Height = 8;

                //C Represents Column.
                for (int c = 0; c < dataTablaActual[r].Length; c++)
                {
                    //Cell Alignment
                    //DataRow.Cells[c].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                    //Fill Data in Rows
                    Paragraph p2 = DataRow.Cells[c].AddParagraph();
                    p2.Format.AfterSpacing = 0;

                    TextRange TR = p2.AppendText("");

                    switch (c)
                    {
                        case 1:
                            //campos porcentaje
                            TR = p2.AppendText(dataTablaActual[r][c] + " %");
                            break;

                        case 2:
                        case 3:
                        case 5:
                            //campos dolares
                            TR = p2.AppendText("$ " + dataTablaActual[r][c]);
                            break;

                        default:
                            TR = p2.AppendText(dataTablaActual[r][c]);
                            break;
                    }

                    //Format Cells
                    if (c != 0)
                    {
                        p2.Format.HorizontalAlignment = HorizontalAlignment.Right;
                    }
                    TR.CharacterFormat.FontName = "Arial";
                    TR.CharacterFormat.FontSize = 8;
                    //TR2.CharacterFormat.TextColor = Color.Brown;
                }
            }
        }

        //tabla calificaciones
        void setTablaCalificaciones()
        {
            tipoTablaActual = tablasReportes.CalificacionComite;
            HeaderTablaActual = getHeaderCalifComite();
            dataTablaActual = getDataCalifComite();
            setTabla("{{Tabla calificaciones}}");
        }
        private string[] getHeaderCalifComite()
        {
            String[] Header = { "Miembro", "Calificación", "Obs/Recomendaciones" };
            return Header;
        }
        private string[][] getDataCalifComite()
        {
            string[] fila;
            String[][] arrayData;
            int cont = -1;

            List<ComiteCalificacion> calificaciones = this.postulacionCompleta.ce.misCalificaciones;

            if (calificaciones != null)
            {
                arrayData = new String[calificaciones.Count][];

                foreach (ComiteCalificacion item in calificaciones)
                {
                    cont += 1;

                    string miembro = item.nombre;
                    string calificacion = item.calificacion.ToString();
                    string Obs = item.observacion;

                    fila = new String[] { miembro, calificacion, Obs };
                    arrayData[cont] = fila;
                }

                return arrayData;
            }
            else
            {
                return new String[0][];
            }
        }
        void setTableDataCalifComite()
        {
            //Data Row
            for (int r = 0; r < dataTablaActual.Length; r++)
            {
                TableRow DataRow = tableActual.Rows[r + 1];

                //Row Height
                DataRow.Height = 8;

                //C Represents Column.
                for (int c = 0; c < dataTablaActual[r].Length; c++)
                {
                    //Cell Alignment
                    //DataRow.Cells[c].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                    //Fill Data in Rows
                    Paragraph p2 = DataRow.Cells[c].AddParagraph();
                    p2.Format.AfterSpacing = 0;

                    TextRange TR = p2.AppendText("");

                    //"Miembro", "Calificación", "Obs/Recomendaciones"
                    TR = p2.AppendText(dataTablaActual[r][c]);

                    //Format Cells
                    //if (c != 0)
                    //{
                    //    p2.Format.HorizontalAlignment = HorizontalAlignment.Right;
                    //}
                    TR.CharacterFormat.FontName = "Arial";
                    TR.CharacterFormat.FontSize = 8;
                    //TR2.CharacterFormat.TextColor = Color.Brown;
                }
            }
        }

        //tabla matriz
        void setTablaMatriz()
        {
            tipoTablaActual = tablasReportes.Matriz;
            HeaderTablaActual = getHeaderMatriz();
            dataTablaActual = getDataMatriz();
            setTabla("{{TablaMatriz}}");
        }
        private string[] getHeaderMatriz()
        {
            String[] Header = { "Prioridad", "Recurso", "Justificacion", "Precio Unitario", "Cantidad", "Total", "Aporte Propio", "Aporte Solicitado" };
            return Header;
        }
        private string[][] getDataMatriz()
        {
            string[] fila;
            String[][] arrayData;
            int cont = -1;

            List<inversion> matrizInversion = this.postulacionCompleta.matrizInversion;

            if (matrizInversion != null)
            {
                arrayData = new String[matrizInversion.Count][];

                foreach (inversion inversionObj in matrizInversion)
                {
                    cont += 1;

                    string prioridad = inversionObj.prioridad.ToString();
                    string recurso = inversionObj.recurso;
                    string justificacion = inversionObj.justificacion;
                    string precioUnitario = inversionObj.precioUnitario.ToString();
                    string cantidad = inversionObj.cantidad.ToString();
                    string total = inversionObj.total.ToString();
                    string aportePropio = inversionObj.aportePropio.ToString();
                    string aporteSolicitado = inversionObj.aporteInversion.ToString();
                    
                    fila = new String[] { prioridad, recurso, justificacion, precioUnitario,cantidad,total, aportePropio, aporteSolicitado };
                    arrayData[cont] = fila;
                }

                return arrayData;
            }
            else
            {
                return new String[0][];
            }
        }
        void setTableDataMatriz()
        {
            //Data Row
            for (int r = 0; r < dataTablaActual.Length; r++)
            {
                TableRow DataRow = tableActual.Rows[r + 1];

                //Row Height
                DataRow.Height = 8;

                //C Represents Column.
                for (int c = 0; c < dataTablaActual[r].Length; c++)
                {
                    //Cell Alignment
                    //DataRow.Cells[c].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                    //Fill Data in Rows
                    Paragraph p2 = DataRow.Cells[c].AddParagraph();
                    p2.Format.AfterSpacing = 0;

                    TextRange TR = p2.AppendText("");

                    //Format Cells
                    switch (c)
                    {
                        case 0:
                        case 4:
                            //enteros
                            TR = p2.AppendText(dataTablaActual[r][c]);
                            p2.Format.HorizontalAlignment = HorizontalAlignment.Right;
                            break;
                        case 1:
                        case 2:
                            //texto
                            TR = p2.AppendText(dataTablaActual[r][c]);
                            break;
                        default:
                            //3,5,6,7
                            // money
                            TR = p2.AppendText("$ " + dataTablaActual[r][c]);
                            p2.Format.HorizontalAlignment = HorizontalAlignment.Right;
                            break;
                    }

                    TR.CharacterFormat.FontName = "Arial";
                    TR.CharacterFormat.FontSize = 8;
                    //TR2.CharacterFormat.TextColor = Color.Brown;
                }
            }
        }
        #endregion

        #region ComitePostulacion
        private void procesarComitePostulacion()
        {
            MIS_Personas emprendedor = postulacionCompleta.emprendedor;
            Emprendimientos emprendimiento = postulacionCompleta.emprendimiento;

            reemp("{{Nombre}}", emprendedor.getNombreCompleto());
            reemp("{{emprendimiento}}", emprendimiento.nombre);

            //del idcomitéMiembro buscar en con_comiteMiembros 
            //obtener el nombre y la institución 
            CON_comiteMiembros comiteMiembro = comitePostulacion.CON_comiteMiembros;
            reemp("{{cp.miembro.nombre}}", comiteMiembro.nombre); 
            reemp("{{cp.miembro.Institucion}}", comiteMiembro.institucion);

            //tabla preguntas Comité
            setTablaPreguntasComite();

            reemp("{{cp.observaciones}}", comitePostulacion.observaciones);
        }

        void setTablaPreguntasComite()
        {
            tipoTablaActual = tablasReportes.Matriz;
            HeaderTablaActual = getHeaderPreguntasComite();
            dataTablaActual = getDataPreguntasComite();
            setTabla("{{tablaPreguntas}}");
        }
        private string[] getHeaderPreguntasComite()
        {
            String[] Header = {"Pregunta", "Respuesta", "Recomendaciones"};
            return Header;
        }
        private string[][] getDataPreguntasComite()
        {
            string[] fila;
            String[][] arrayData;
            int cont = -1;

            //"Pregunta", "Respuesta", "Recomendaciones"
            int idQ = comitePostulacion.idTipo == 2 ? 3 : 4;
            string sql = string.Format("SCO_SP_RespuestasEvaluacion {0}, {1}, {2}", 3, comitePostulacion.id, idQ );
            List<PreguntasComiteEvaluador> preguntasComiteEvaluador = db.Database.SqlQuery<PreguntasComiteEvaluador>(sql).ToList();
            

            if (preguntasComiteEvaluador != null)
            {
                arrayData = new String[preguntasComiteEvaluador.Count][];

                foreach (PreguntasComiteEvaluador preguntaComite in preguntasComiteEvaluador)
                {
                    cont += 1;

                    string pregunta = preguntaComite.pregunta;
                    string respuesta = preguntaComite.respuesta;
                    string obs = preguntaComite.obs;

                    fila = new String[] { pregunta, respuesta, obs };
                    arrayData[cont] = fila;
                }

                return arrayData;
            }
            else
            {
                return new String[0][];
            }
        }
        void setTableDataPreguntasComite()
        {
            //Data Row
            for (int r = 0; r < dataTablaActual.Length; r++)
            {
                TableRow DataRow = tableActual.Rows[r + 1];

                //Row Height
                DataRow.Height = 8;

                //C Represents Column.
                for (int c = 0; c < dataTablaActual[r].Length; c++)
                {
                    //Cell Alignment
                    //DataRow.Cells[c].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                    //Fill Data in Rows
                    Paragraph p2 = DataRow.Cells[c].AddParagraph();
                    p2.Format.AfterSpacing = 0;

                    TextRange TR = p2.AppendText("");
                    TR = p2.AppendText(dataTablaActual[r][c]);

                    TR.CharacterFormat.FontName = "Arial";
                    TR.CharacterFormat.FontSize = 8;
                    //TR2.CharacterFormat.TextColor = Color.Brown;
                }
            }
        }

        #endregion

        //todo borrar después, es ejemplo de tablas con spire
        private Table getSpireTable(Section s)
        {
            //Create Table
            Table table = s.AddTable(true);

            //Create Header and Data
            String[] Header = { "Item", "Description", "Qty", "Unit Price", "Price" };
            String[][] data = {
                                  new String[]{"Spire.Doc for .NET",".NET Word Component","1","$799.00","$799.00"},
                                  new String[]{"Spire.XLS for .NET",".NET Excel Component","2","$799.00","$1,598.00"},
                                  new String[]{"Spire.Office for .NET",".NET Office Component","1","$1,899.00","$1,899.00"},
                                  new String[]{"Spire.PDF for .NET",".NET PDFComponent","2","$599.00","$1,198.00"},
                              };
            //Add Cells
            table.ResetCells(data.Length + 1, Header.Length);

            //Header Row
            TableRow FRow = table.Rows[0];
            FRow.IsHeader = true;
            //Row Height
            FRow.Height = 23;
            //Header Format
            FRow.RowFormat.BackColor = Color.AliceBlue;
            for (int i = 0; i < Header.Length; i++)
            {
                //Cell Alignment
                Paragraph p = FRow.Cells[i].AddParagraph();
                FRow.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                p.Format.HorizontalAlignment = HorizontalAlignment.Center;
                //Data Format
                TextRange TR = p.AppendText(Header[i]);
                TR.CharacterFormat.FontName = "Calibri";
                TR.CharacterFormat.FontSize = 14;
                TR.CharacterFormat.TextColor = Color.Teal;
                TR.CharacterFormat.Bold = true;
            }

            //Data Row
            for (int r = 0; r < data.Length; r++)
            {
                TableRow DataRow = table.Rows[r + 1];

                //Row Height
                DataRow.Height = 20;

                //C Represents Column.
                for (int c = 0; c < data[r].Length; c++)
                {
                    //Cell Alignment
                    DataRow.Cells[c].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                    //Fill Data in Rows
                    Paragraph p2 = DataRow.Cells[c].AddParagraph();
                    TextRange TR2 = p2.AppendText(data[r][c]);
                    //Format Cells
                    p2.Format.HorizontalAlignment = HorizontalAlignment.Center;
                    TR2.CharacterFormat.FontName = "Calibri";
                    TR2.CharacterFormat.FontSize = 12;
                    TR2.CharacterFormat.TextColor = Color.Brown;
                }
            }

            return table;
        }
    }

    public enum tablasReportes
    {
        objetivos,
        productos,
        CalificacionComite,
        Matriz
    }
}