﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emprende.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
namespace Emprende.Extensions
{
    public class PersonasExtension
    {
        private MIS_Personas persona;

        public MIS_Personas CrearPersona(int idUsuario, string correo)
        {
            using (var a = new Models.emprendeEntities())
            {
                persona = new MIS_Personas();
                persona.idUsuario = idUsuario;
                persona.correo = correo;
                persona.datosCompletos = false;

                a.MIS_Personas.Add(persona);
                a.SaveChanges();

                return persona;
            }
        }
        public static MIS_Personas consultarPersonaPorID(int idPersona)
        {
            MIS_Personas persona = new MIS_Personas();
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                persona = a.MIS_Personas.Where(x => x.id == idPersona).FirstOrDefault();
                return persona;
            }
        }

        public static MIS_Personas consultarPersonaCompleta(int idPersona)
        {
            MIS_Personas persona = new MIS_Personas();
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                persona = a.MIS_Personas.Include("MIS_Direcciones.Parroquias.Ciudades.Provincias").Include("MIS_Personas1").Include("MIS_Personas2").Where(x => x.id == idPersona).FirstOrDefault();
                return persona;
            }
        }

        public MIS_Personas consultarPersonaIdUsuario(int idUsuario, string correo)
        {
            MIS_Personas persona;
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                persona = a.MIS_Personas.Where(x => x.idUsuario == idUsuario).FirstOrDefault();

                if (persona == null)
                {
                    persona = CrearPersona(idUsuario, correo);
                }

                return persona;
            }
        }

        public static MIS_Direcciones consultarDireccionPersona(int idPersona)
        {

            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                MIS_Personas persona = a.MIS_Personas.Include("MIS_Direcciones").Where(x => x.id == idPersona).First();

                return persona.MIS_Direcciones;

            }
        }

        public static void completarDatosGenerales(int id, string cedula, string genero, string nombre1, string nombre2, string apellido1, string apellido2,
            string telefono, string celular, int nacionalidad, int estadoCivil, MIS_Direcciones direccion, int idTipoDocumento, DateTime fechaNacimiento, int idNivelEducacion,
            string nombre1Conyuge, string nombre2Conyuge, string apellido1Conyuge, string apellido2Conyuge, int idNacionalidadConyuge, DateTime fechaNacimientoConyuge, string documentoConyuge,
            int idConyuge
            )
        {
            MIS_Personas persona;
            Extensions.Mail.InscripcionMail mail;
            bool enviarMailInscripcion = false;
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                persona = a.MIS_Personas.Where(x => x.id == id).FirstOrDefault();
                enviarMailInscripcion = !(persona.datosCompletos ?? false);
                persona.cedula = cedula;
                persona.sexo = genero;
                persona.nombre = nombre1;
                persona.nombre2 = nombre2;
                persona.apellido = apellido1;
                persona.apellido2 = apellido2;
                persona.telefono = telefono;
                persona.celular = celular;
                persona.idPais = nacionalidad;
                persona.idEstadoCivil = estadoCivil;
                persona.idTipoDocumento = idTipoDocumento;
                persona.fechaNacimiento = fechaNacimiento;
                persona.idNivelEducacion = idNivelEducacion;

                if (estadoCivil == 1 || estadoCivil == 3 || estadoCivil == 4 || estadoCivil == 5) // no tiene conyuge
                    persona.idConyuge = null;
                else // tiene conyuge 
                {
                    if (persona.datosCompletos == true && persona.idConyuge != null)
                        persona.idConyuge = editarConyuge(idConyuge,documentoConyuge, nombre1Conyuge, nombre2Conyuge, apellido1Conyuge, apellido2Conyuge, idNacionalidadConyuge, fechaNacimientoConyuge);
                    else
                        persona.idConyuge = guardarConyuge("", "", "", "", "",1, DateTime.Now); // crea conyuge vacio sin datos
                }

                if (direccion.id == 0) //creacion de direccion de la persona 
                {

                    persona.idDireccion = DireccionesExtension.agreagarDireccion(direccion.idTipoLugar, persona.id, 1, direccion.linea1, direccion.linea2,
                        direccion.numero, "Hogar", direccion.referencia, direccion.barrio, (int)direccion.idParroquia).id;

                }
                else // actualiza direccion
                {
                    persona.idDireccion = DireccionesExtension.editarDireccion(direccion.id ,direccion.idTipoLugar, direccion.linea1, direccion.linea2,
                        direccion.numero, "Hogar", direccion.referencia, direccion.barrio, (int)direccion.idParroquia).id;
                }
                                

                persona.datosCompletos = true;
                a.SaveChanges();

            }

            if (enviarMailInscripcion) {
                mail = new Extensions.Mail.InscripcionMail(persona);
                mail.EnviarCorreo();
            }

        }

        public static int guardarConyuge(string documento, string nombre1, string nombre2, string apellido1, string apellido2, int idNacionalidad, DateTime fechaNacimiento)
        {
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                MIS_Personas persona = new MIS_Personas();

                persona.cedula = documento;
                persona.nombre = nombre1;
                persona.nombre2 = nombre2;
                persona.apellido = apellido1;
                persona.apellido2 = apellido2;
                persona.fechaNacimiento = fechaNacimiento;
                persona.idPais = idNacionalidad;

                a.MIS_Personas.Add(persona);
                a.SaveChanges();

                return persona.id;

            }
        }

        public static int editarConyuge(int idConyuge, string documento, string nombre1, string nombre2, string apellido1, string apellido2, int idNacionalidad, DateTime fechaNacimiento)
        {
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                MIS_Personas persona = a.MIS_Personas.Where(x => x.id == idConyuge).FirstOrDefault();

                persona.cedula = documento;
                persona.nombre = nombre1;
                persona.nombre2 = nombre2;
                persona.apellido = apellido1;
                persona.apellido2 = apellido2;
                persona.fechaNacimiento = fechaNacimiento;
                persona.idPais = idNacionalidad;
                                
                a.SaveChanges();

                return persona.id;

            }
        }

    }
}