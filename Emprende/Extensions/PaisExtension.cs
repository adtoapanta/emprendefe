﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emprende.Models;

namespace Emprende.Extensions
{
    public class PaisExtension
    {
        public static List<Pais> consultarPaises()
        {
            using (var a = new Models.emprendeEntities())
            {                
                a.Configuration.LazyLoadingEnabled = false;

                List<Pais> paises = a.Pais.OrderBy(x => x.nombre).ToList();                

                return paises;
            }


        }

    }
}