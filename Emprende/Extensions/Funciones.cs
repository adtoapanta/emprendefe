﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Core.Objects;
using Newtonsoft.Json;

namespace Emprende.Extensions
{
    public class Funciones
    {
        public static void WriteLog(string error)
        {
            Logger.Write(error);
        }
        public static void WriteLog(Exception e)
        {
            string m = "";
            if (e.StackTrace != null)
                m = e.StackTrace;

            Logger.Write(m + " | " + e.Message);
        }

        public static string ToJson(object o)
        {
            return JsonConvert.SerializeObject(o, new JsonSerializerSettings()
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                Formatting = Formatting.Indented
            });
        }

        public static IndiceEtapasConvocatoria GetNextEtapa(int idEtapa_,bool NoPrefactibilidad =false)
        {
            switch ((IndiceEtapasConvocatoria)idEtapa_)
            {
                case IndiceEtapasConvocatoria.Creacion:
                    return IndiceEtapasConvocatoria.Inscripción;
                case IndiceEtapasConvocatoria.Inscripción:
                    return IndiceEtapasConvocatoria.Elegibilidad;
                case IndiceEtapasConvocatoria.Elegibilidad:
                    return IndiceEtapasConvocatoria.ModeloNegocio;
                case IndiceEtapasConvocatoria.ModeloNegocio:
                    if(NoPrefactibilidad) return IndiceEtapasConvocatoria.AgendarComite;
                    return IndiceEtapasConvocatoria.AgendarPrefactibilidad;
                case IndiceEtapasConvocatoria.AgendarPrefactibilidad:
                    return IndiceEtapasConvocatoria.Prefactibilidad;
                case IndiceEtapasConvocatoria.Prefactibilidad:
                    return IndiceEtapasConvocatoria.AgendarComite;
                case IndiceEtapasConvocatoria.AgendarComite:
                    return IndiceEtapasConvocatoria.Comite;
                case IndiceEtapasConvocatoria.Comite:
                    return IndiceEtapasConvocatoria.Diagnóstico;
                case IndiceEtapasConvocatoria.Diagnóstico:
                    return IndiceEtapasConvocatoria.Documentos;
                case IndiceEtapasConvocatoria.Documentos:
                    return IndiceEtapasConvocatoria.AprobarDocumentos;
                case IndiceEtapasConvocatoria.AprobarDocumentos:
                    return IndiceEtapasConvocatoria.Propuesta;
                case IndiceEtapasConvocatoria.Propuesta:
                    return IndiceEtapasConvocatoria.AprobarPropuesta;
                case IndiceEtapasConvocatoria.AprobarPropuesta:
                    return IndiceEtapasConvocatoria.Contrato;
                case IndiceEtapasConvocatoria.Contrato:
                    return IndiceEtapasConvocatoria.Finalizado;
            }
            return 0;
        }

    }

    public enum TipoArchivos {
        ArchivoBase =1,//Convocatoria
        ArchivoPrefactibilidad=7,//idPostulacion
        ArchivoComite =9,//idPostulacion
        ArchivoDatos=11,//usado para archivos
        ArchivoCapacitacion = 100,//idPostulante
        ArchivoPropuestaAprobada = 101,//idPostulacion
        ArchivoContratoAceptado = 102//idPostulacion
    }

    public enum TipoDocumentos
    {
        Datos = 11
    }
    public enum CategoriasDocumentos
    {
        Normal = 1,
        Opcionales = 2
    }

    public enum IndiceCuestionario {
        Elegibilidad =1,
    }

    public enum IndiceEtapasConvocatoria
    {
        Creacion = 1,
        Inscripción = 3,
        Elegibilidad = 4,
        ModeloNegocio = 5,
        AgendarPrefactibilidad = 6,
        Prefactibilidad = 7,
        AgendarComite = 8,
        Comite = 9,
        Diagnóstico = 10,
        Documentos = 11,
        AprobarDocumentos = 17,
        Propuesta =18,
        AprobarPropuesta = 19,
        Contrato = 26,
        Finalizado = 27
    }

    public enum TiposComite {
        Comite=2,
        Prefactibilidad=1
    }

    public enum IdiceEstadosEmprendimiento
    {
        Ideacion = 20,
        Prototipado = 21,
        PrimerasVentas = 22
    }

    public enum enumEstadoCivilEmprendedor
    {
        soltero = 1,
        casado = 2,
        divoricado = 3,
        viudo = 4,
        separado = 5,
        unionLibre = 6
    }

    public enum enumInstruccion
    {
        ninguna = 1,
        primaria = 2,
        secundaria = 3,
        superior = 4,
        tecnica = 5,
        otra = 6
    }

    public enum tiposLugar
    {
        casaPropia = 1,
        casaFamiliares = 2,
        localAlquilado = 3,
        otro = 4
    }

    public enum reportes
    {
        medioDeAprobacion,
        Contrato,
        diagnosticoEmprendedor,
        brief,
        comite
    }
}