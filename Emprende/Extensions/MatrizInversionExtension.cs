﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emprende.Models;

namespace Emprende.Extensions
{
    public class MatrizInversionExtension
    {
        public void agregarItemInversion(int idPostulacion, int prioridad, string recurso, string justificacion, decimal precioUnitario, int cantidad, decimal total, decimal aportePropio, decimal aporteInversion, int idTipoInversion)
        {
            using (var a = new Models.emprendeEntities())
            {
                inversion item = new inversion();                
                a.Configuration.LazyLoadingEnabled = false;

                item.idPostulacion = idPostulacion;
                item.prioridad = prioridad;
                item.recurso = recurso;
                item.justificacion = justificacion;
                item.precioUnitario = precioUnitario;
                item.cantidad = cantidad;
                item.total = total;
                item.aportePropio = aportePropio;
                item.aporteInversion = aporteInversion;
                item.idTipoInversion = idTipoInversion;

                //actualiza campos totales inversion en postulacion
                PostulacionExtension.actualizaTotalesPostulacion(idPostulacion, total, aportePropio);

                a.inversion.Add(item);                
                a.SaveChanges();

            }
        }

        public void editarItemInversion(int idItem, int prioridad, string recurso, string justificacion, decimal precioUnitario, int cantidad, decimal total, decimal aportePropio, decimal aporteInversion, int idTipoInversion)
        {
            using (var a = new Models.emprendeEntities())
            {                
                a.Configuration.LazyLoadingEnabled = false;
                inversion item = a.inversion.Where(x => x.id == idItem).FirstOrDefault();

                item.prioridad = prioridad;
                item.recurso = recurso;
                item.justificacion = justificacion;
                item.precioUnitario = precioUnitario;
                item.cantidad = cantidad;
                item.total = total;
                item.aportePropio = aportePropio;
                item.aporteInversion = aporteInversion;
                item.idTipoInversion = idTipoInversion;

                //actualiza campos totales inversion en postulacion
                //PostulacionExtension.actualizaTotalesPostulacion(item.idPostulacion, total, aportePropio);
                
                a.SaveChanges();

            }
        }

        public void eliminarItemInversion(int idItemInversion)
        {
            using (var a = new Models.emprendeEntities())
            {
                inversion item = new inversion();

                a.Configuration.LazyLoadingEnabled = false;

                item = a.inversion.Where(x => x.id == idItemInversion).FirstOrDefault();

                a.inversion.Remove(item);
                a.SaveChanges();

            }
        }

        public static List<inversion> consultarMatrizInversion(int idPostulacion)
        {
            using (var a = new Models.emprendeEntities())
            {
                List<inversion> matrizInversion = new List<inversion>();

                a.Configuration.LazyLoadingEnabled = false;

                matrizInversion = a.inversion.Where(x => x.idPostulacion == idPostulacion).OrderByDescending(x => x.prioridad).ToList();

                return matrizInversion;
            }
                

        }

        public static void guardarResumenInversion(int idPostulacion, string resumen, decimal totalInversion)
        {
            using (var a = new Models.emprendeEntities())
            {
                CON_postulaciones postulacion = a.CON_postulaciones.Where(x => x.id == idPostulacion).FirstOrDefault();
                postulacion.resumenInversion = resumen;
                postulacion.totalInversion = totalInversion;

                a.SaveChanges();
            }
        }

        public static inversion consultarItemInversion(int idItemInversion)
        {
            using (var a = new Models.emprendeEntities())
            {

                a.Configuration.LazyLoadingEnabled = false;

                inversion item = a.inversion.Where(x => x.id == idItemInversion).FirstOrDefault();
                return item;
                
            }
        }
    }
    
}