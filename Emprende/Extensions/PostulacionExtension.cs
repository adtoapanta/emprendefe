﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Emprende.Models;
namespace Emprende.Extensions
{
    public class PostulacionExtension
    {
        public int idConvocatoria;
        public int idEtapa { get; set; }
        public List<Models.PostulacionDetalle> postulaciones;
        public List<Models.EtapasConvocatoria> etapas;
        public Models.CON_convocatorias convocatoria;


        //atributos sp [COB_SP_ConsultaPostulacionesPersona]
        public string nombreEmprendimiento { get; set; } // nombre convocatoria
        public string nombreConvocatoria { get; set; } // nombre convocatoria
        public string nombreEtapa { get; set; } // nombre convocatoria
        public int idPostulacion { get; set; } //idPostulacion
        public PostulacionExtension()
        { }

        public PostulacionExtension(int idConvocatoria) {
            if (idConvocatoria == 0) throw new Exception("Seleccione una convocatoria");
            this.idConvocatoria = idConvocatoria;
            Load();
        }

        public PostulacionExtension(int idConvocatoria, int idEtapa) {
            this.idConvocatoria = idConvocatoria;
            this.idEtapa = idEtapa;
            LoadPostulacionesFiltro();
        }

        private void Load() {
            using (var baseE = new Models.emprendeEntities())
            {
                baseE.Configuration.LazyLoadingEnabled = false;//No requerimos q busque las relaciones
                postulaciones = baseE.Database.SqlQuery<Models.PostulacionDetalle>("SELECT * FROM VW_PostulantesDetalle WHERE idConvocatoria=@idConvocatoria", new System.Data.SqlClient.SqlParameter("idConvocatoria", idConvocatoria)).ToList();
                etapas = baseE.Database.SqlQuery<Models.EtapasConvocatoria>("SELECT * FROM "+Models.EtapasConvocatoria.tabla).ToList();
                convocatoria = baseE.CON_convocatorias.Where(x => x.id == idConvocatoria).FirstOrDefault();
            }
        }

        private void LoadPostulacionesFiltro() {
            using (var baseE = new Models.emprendeEntities()) {
                baseE.Configuration.LazyLoadingEnabled = false;//No requerimos q busque las relaciones
                postulaciones = baseE.Database.SqlQuery<Models.PostulacionDetalle>("SELECT * FROM VW_PostulantesDetalle WHERE idConvocatoria=@idConvocatoria AND idEtapa=@idEtapa", 
                    new SqlParameter("idConvocatoria", idConvocatoria), new SqlParameter("idEtapa", idEtapa)).ToList();
            }
        }


        public string GetJsonPostulaciones() {
            return JsonConvert.SerializeObject(postulaciones);
        }
        public static List<PostulacionExtension> consultarPostulacionesPersona(int idPersona)
        {

            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                return a.Database.SqlQuery<PostulacionExtension>("COB_SP_ConsultaPostulacionesPersona @idPersona", new SqlParameter("idPersona", idPersona)).ToList();
            }
        }
        public static CON_postulaciones consultarPostulacionesId(int idPostulacion)
        {
            CON_postulaciones postulacion = new CON_postulaciones();
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                postulacion = a.CON_postulaciones.Include("CON_convocatorias").Include("Emprendimientos").Where(x => x.id == idPostulacion).FirstOrDefault();
                return postulacion;
            }
        }

        public static List<CON_ProveedoresCapacitacion> GetProveedoresCapacitacion() {
            List<CON_ProveedoresCapacitacion> prov;
            using (var baseE = new Models.emprendeEntities()) {
                baseE.Configuration.LazyLoadingEnabled = false;
                prov = baseE.CON_ProveedoresCapacitacion.ToList();
            }

            return prov;
        }

        public void postular(int idConvocatoria, int idEmprendimiento)
        {
            Mail.PostulacionMail email;
            CON_postulaciones postulacion;
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                postulacion = new CON_postulaciones();
                postulacion.idConvocatoria = idConvocatoria;
                postulacion.idEmprendimiento = idEmprendimiento;
                postulacion.fecha = DateTime.Now;
                postulacion.idEtapa = (int)IndiceEtapasConvocatoria.Inscripción; //primera etapa del catalogo 
                postulacion.esAprobado = false;
                postulacion.revisadoEtapa = postulacion.apruebaEtapa = true;

                a.CON_postulaciones.Add(postulacion);
                a.SaveChanges();

                crearRequisitosPostulante(idConvocatoria, postulacion.id);

            }

            //Enviar correo de postulacion
            email = new Mail.PostulacionMail(postulacion.id);
            email.EnviarCorreo();
        }

        public void crearRequisitosPostulante(int idConvocatoria, int idPostulacion)
        {
            
            using (var a = new Models.emprendeEntities())
            {
                a.Database.ExecuteSqlCommand("CON_SP_CrearRequisitosPostulante @idConvocatoria, @idPostulacion", new SqlParameter("idConvocatoria", idConvocatoria), new SqlParameter("idPostulacion", idPostulacion));
            }
        }

        public static PostulacionDetalle GetPostulacion(int idPostulacion) {
            Models.PostulacionDetalle postulacion;
            using (var baseE = new Models.emprendeEntities()) {
                postulacion = baseE.Database.SqlQuery<Models.PostulacionDetalle>("SELECT * FROM VW_PostulantesDetalle WHERE id=@id", new System.Data.SqlClient.SqlParameter("id", idPostulacion)).FirstOrDefault();
            }
            return postulacion;
        }

        public static void terminarEtapaPostulacion(int idPostulacion)
        {
            using (var a = new Models.emprendeEntities())
            {
                CON_postulaciones postulacion = a.CON_postulaciones.Where(x => x.id == idPostulacion).FirstOrDefault();
                postulacion.revisadoEtapa = postulacion.apruebaEtapa = true;

                a.SaveChanges();
            }
        }

        public static void SubirArchivoDatos(int idArchivoCargado,int idPostulante,string documento, string descripcion, HttpPostedFileBase archivo) {
            string comment = "";
                VtcArchivos.Funciones.SubirDocumento(archivo,idArchivoCargado,documento, descripcion, ref comment, idTipo: (int)TipoArchivos.ArchivoDatos);
            if (comment != string.Empty) throw new Exception(comment);
        }

        public static void actualizaTotalesPostulacion(int idPostulacion, decimal totalInversion, decimal inversionPropia)
        {
            using (var a = new Models.emprendeEntities())
            {
                CON_postulaciones postulacion = a.CON_postulaciones.Where(x => x.id == idPostulacion).FirstOrDefault();
                postulacion.totalInversion += totalInversion;
                postulacion.inversionPropia += inversionPropia;

                a.SaveChanges();
            }
        }

        public static string consultarResumenInversion(int idPostulacion)
        {
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                string resumen = a.CON_postulaciones.Where(x => x.id == idPostulacion).FirstOrDefault().resumenInversion;

                return resumen;
            }
        }
    }
}