﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emprende.Models;
using System.Data.Entity;
using Newtonsoft.Json;
using System.Data.SqlClient;
namespace Emprende.Extensions
{
    
    public class EmprendimientoExtension
    {
        private Emprendimientos emprendimiento;

        public EmprendimientoExtension()
        { }

        public Emprendimientos crearEmprendimiento(int idPersona, string nombre)
        {
            using (var a = new Models.emprendeEntities())
            {
                emprendimiento = new Emprendimientos();
                emprendimiento.idPersona = idPersona;
                emprendimiento.nombre = nombre;

                a.Emprendimientos.Add(emprendimiento);
                a.SaveChanges();

                ModeloNegocioExtension.crearCanvasEmprendimiento(emprendimiento.id);
                return emprendimiento;
            }
        }

        public static Emprendimientos completarEmprendimiento(int idEmprendimiento, string descripcion, string ruc, Boolean tieneProducto,int? tieneVentas, int? tiempoVentas, MIS_Direcciones direccion, int bandera ,bool estaConstituida, int idMedioInformacion)
        {
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                Emprendimientos emprendimiento;
                emprendimiento = a.Emprendimientos.Where(x => x.id == idEmprendimiento).FirstOrDefault();
                emprendimiento.descripcion = descripcion;
                emprendimiento.ruc = ruc;
                emprendimiento.tieneProducto = tieneProducto;
                emprendimiento.tieneVentas = tieneVentas;
                emprendimiento.tiempoVentas = tiempoVentas;
                emprendimiento.estaConstituida = estaConstituida;
                emprendimiento.idMediosInformacion = idMedioInformacion;

                //define la etapa del emprendimiento. ids quemados..


                if (tieneProducto == false)
                {
                    emprendimiento.idEstado = 20; //ideacion                    
                }
                else
                {
                    if (tieneVentas == 1 || tieneVentas ==2 )
                    {
                        emprendimiento.idEstado = 21; //prototipo
                    }
                    else
                    {
                        switch (tiempoVentas)
                        {
                            case 1:
                                emprendimiento.idEstado = 21; //prototipo
                                break;
                            case 2:
                                emprendimiento.idEstado = 22;  // vtas <1
                                break;
                            case 3:
                                emprendimiento.idEstado = 28; // vtas > 1
                                break;
                            default:
                                emprendimiento.idEstado = 22;
                                break;
                        }                       
                    }
                }

                //



                if (bandera == 1) // direccion propia del emprendimiento
                {
                    emprendimiento.idDireccion = DireccionesExtension.agreagarDireccion(direccion.idTipoLugar, emprendimiento.id, 2, direccion.linea1, direccion.linea2,
                    direccion.numero, "Emprendimiento", direccion.referencia, direccion.barrio, (int)direccion.idParroquia).id;
                }
                else // misma direccion que el emprendedor
                {

                    emprendimiento.idDireccion = PersonasExtension.consultarDireccionPersona(emprendimiento.idPersona).id;
                }
                emprendimiento.completo = true;

                a.SaveChanges();
                return emprendimiento;
            }
        }

        //parcxhe para scoring
        public static int opcionEtapaEmp(int idEtapa) {

            int opcion=0;

            switch (idEtapa)
            {
                case 20:                   
                    opcion = 443;
                    break;

                case 21:
                    opcion = 444;
                    break;

                case 22:
                    opcion = 445;
                    break;

                case 28:
                    opcion = 446;
                    break;


                default:
                    opcion = 445;
                    break;                    
            }


            return opcion;           
            
        }

        public static List<Emprendimientos> consultarEmprendimientosPorPersona(int idPersona)
        {
            List<Emprendimientos> emprendimientos = new List<Emprendimientos>();
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                emprendimientos = a.Emprendimientos.Where(x => x.idPersona == idPersona).OrderByDescending(x=> x.completo).ToList();
            }
            return emprendimientos;
        }

        public static List<Emprendimientos> consultarEmprendimientosCompletosPorPersona(int idPersona)
        {
            List<Emprendimientos> emprendimientos = new List<Emprendimientos>();
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;
                emprendimientos = a.Emprendimientos.Where(x => x.idPersona == idPersona && x.completo == true).ToList();
            }
            return emprendimientos;
        }

        public static Emprendimientos consultarEmprendimientoId(int idEmprendimiento)
        {
            Emprendimientos emprendimiento;
            using (var a = new Models.emprendeEntities())
            {
                a.Configuration.LazyLoadingEnabled = false;

                emprendimiento = a.Emprendimientos.Include("MIS_Direcciones.Parroquias.Ciudades.Provincias").Where(x => x.id == idEmprendimiento).FirstOrDefault();
            }
            return emprendimiento;
        }
    }
}