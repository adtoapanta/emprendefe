﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emprende.Models;

namespace Emprende.Extensions
{
    public class ReporteConsolidadoExtension
    {
        public string nombreConvocatoria { get; set; }
        public string tipoConvocatoria { get; set; }
        public string documentoEmprendedor { get; set; }
        public string nombreEmprendedor { get; set; }
        public string nombreEmprendimiento { get; set; }
        public string etapaPostulacion { get; set; }
        public string correoEmprendedor { get; set; }
        public string telefonoEmprendedor { get; set; }
        public string celularEmprendedor { get; set; }
        public string direccionPersona { get; set; }
        public string direccionEmprendimiento { get; set; }

        public static List<ReporteConsolidadoExtension> consultarReporteConsolidado()
        {
            DateTime actual;
            DateTime inicioMes;
            actual = DateTime.Now;
            inicioMes = new DateTime(actual.Year, actual.Month, 1); ;

            using (var a = new Models.emprendeEntities())
            {
                return a.Database.SqlQuery<ReporteConsolidadoExtension>("CON_SP_ConsultaReportaConsolidado").ToList();
            }
        }



    }


}