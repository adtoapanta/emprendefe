
ALTER TABLE [Emprendimientos]
ALTER COLUMN brief nvarchar(max); 

--25/04/2019

--agregar campo correo a dbo.CON_ProveedoresCapacitacion

alter table dbo.CON_ProveedoresCapacitacion
add correo nvarchar(100)

update dbo.CON_ProveedoresCapacitacion set correo = ''

--25/04/2019
--relacion FK_postulaciones_ProveedoresCapacitacion 

ALTER TABLE con_postulaciones
ADD CONSTRAINT FK_postulaciones_ProveedoresCapacitacion
FOREIGN KEY (idProveedorCapacitacion) REFERENCES CON_ProveedoresCapacitacion(id);

--29/04/2019
--Agregar usuario que esta permitido modificar la postulacion
alter table CON_Postulaciones add idUsuarioAsignado int null


-----
CREATE VIEW [dbo].[VW_UsuariosAdministradores] as 
SELECT USR_User.id,MIS_Personas.nombre,MIS_Personas.apellido 
FROM MIS_Personas 
INNER JOIN USR_User ON MIS_Personas.idUsuario = USR_User.id
INNER JOIN USR_UserRole ON USR_User.id = USR_UserRole.UserId
INNER JOIN USR_Role ON USR_UserRole.RoleId = USR_Role.Id
WHERE USR_Role.Name = 'Admin'

GO


----

ALTER view [dbo].[VW_PostulantesDetalle] as  
SELECT CON_postulaciones.* ,Emprendimientos.nombre as emprendimiento, 
MIS_Personas.nombre +' '+ MIS_Personas.apellido as emprendedor, MIS_Personas.cedula as cedula,  
EE.nombre as estadoEmprendimiento, EP.nombre as etapaPostulacion,
MIS_Personas.nombre, MIS_Personas.apellido, MIS_personas.correo,MIS_personas.telefono,MIS_personas.celular,
CM.miembros as comite, PF.miembros as prefactibilidad,
UA.nombre +' '+ UA.apellido as usuarioAsignado
FROM CON_postulaciones  
INNER JOIN Emprendimientos ON CON_postulaciones.idEmprendimiento = Emprendimientos.id  
INNER JOIN MIS_Personas ON Emprendimientos.idPersona = MIS_Personas.id  
LEFT JOIN (SELECT id,nombre FROM ItemsCatalogos WHERE idCatalogo=5) as EE ON Emprendimientos.idEstado = EE.id  
LEFT JOIN (SELECT id,nombre FROM ItemsCatalogos WHERE idCatalogo=1) as EP ON CON_postulaciones.idEtapa = EP.id  
LEFT JOIN VW_UsuariosAdministradores UA ON CON_postulaciones.idUsuarioAsignado = UA.id
LEFT JOIN [CON_FN_ComiteMiembros](1) AS PF ON CON_postulaciones.id = PF.idPostulacion
LEFT JOIN [CON_FN_ComiteMiembros](2) AS CM ON CON_postulaciones.id = CM.idPostulacion


GO

-- 30/04/2019

--agregar campo latitud y longitud a MIS_Direcciones 

alter table dbo.mis_Direcciones
add latitud decimal(12,10);

alter table dbo.mis_Direcciones
add longitud decimal(12,10);

update dbo.mis_Direcciones set latitud = 0
update dbo.mis_Direcciones set longitud = 0

