namespace Emprende.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class idPersona : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.USR_User", "idPersona", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.USR_User", "idPersona");
        }
    }
}
