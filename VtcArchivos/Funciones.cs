﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace VtcArchivos
{
    public class Funciones
    {
        internal static string pathVtcArchivos = null;

        public static void SetPathVtcArchivos(string path = null)
        {
            if (pathVtcArchivos == null) {
                if (path == null)
                {
                    pathVtcArchivos = System.Web.HttpContext.Current.Server.MapPath("~/Archivos/");
                }
                else
                {
                    pathVtcArchivos = path;
                }
            }
        }

        public static int SubirArchivo(HttpPostedFileBase file,int idReferencia,string nombreArchivoDescriptivo,string descripcion, ref string comentario,bool destacado=false,int? idTipo=null, int? idCategoria=null, int? idGrupo=null, bool saveIndice =false)
        {
            Model.ARC_Archivos archivo;
            Model.ARC_Indice ind;
            int retorno=0;
            try {
                SetPathVtcArchivos();
                if (file != null && file.ContentLength > 0)
                {
                    using (var baseArc = new Model.ArcEntities())
                    {
                        archivo = new Model.ARC_Archivos();
                        archivo.nombre = nombreArchivoDescriptivo;
                        archivo.descripcion = descripcion;
                        archivo.nombreArchivo = Path.GetFileName(file.FileName);
                        archivo.tamanno = file.ContentLength;
                        archivo.extension = Path.GetExtension(file.FileName).Replace(".","");
                        archivo.idTipo = idTipo;
                        archivo.idReferencia = idReferencia;
                        archivo.peso = file.ContentLength.ToString();
                        archivo.idCategoria = idCategoria;
                        archivo.idGrupo = idGrupo;
                        archivo.destacado = destacado;
                        archivo.fecha = DateTime.Today;
                        archivo.idPublico = Guid.NewGuid().ToString();
                        archivo.excluido = false;
                        baseArc.ARC_Archivos.Add(archivo);
                        baseArc.SaveChanges();
                        string newPath = Path.Combine(pathVtcArchivos, string.Format("{0}.{1}", archivo.id, archivo.extension));
                        file.SaveAs(newPath);
                        retorno = archivo.id;

                        if (saveIndice) {
                            ind = new Model.ARC_Indice();
                            ind.idArchivo = archivo.id;
                            ind.texto =string.Format("{0},{1},{2}", archivo.descripcion, archivo.nombre,archivo.nombreArchivo);
                            baseArc.ARC_Indice.Add(ind);
                            baseArc.SaveChanges();
                        }
                    }

                }
                else
                {
                    comentario = "Archivo vacio.";
                }
            }
            catch (DbEntityValidationException e)
            {
                string error="";
                foreach (var eve in e.EntityValidationErrors)
                {
                    error += string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    error += "\n|";
                    foreach (var ve in eve.ValidationErrors)
                    {
                        error += string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                comentario = error;
                retorno=0;
            }
            catch (Exception e)
            {
                comentario = "Error en VtcArchivos " + e.Message;
                retorno = 0;
            }

            return retorno;
        }

        public static bool EliminarArchivo(int idArchivo,ref string msg, bool elimarFisico = false) {
            try
            {
                SetPathVtcArchivos();
                //q el msg tenga cadena no significa q exista error
                return Extension.Archivo.EliminarArchivo(idArchivo,null, null, ref msg, elimarFisico);
            }
            catch (Exception e)
            {
                msg = e.Message;
                return false;
            }
        }

        public static bool EliminarArchivo(int idTipo, int idReferencia,ref string msg,bool elimarFisico = false)
        {
            try {
                SetPathVtcArchivos();
                //q el msg tenga cadena no significa q exista error
                return Extension.Archivo.EliminarArchivo(null, idReferencia, idTipo, ref msg, elimarFisico);
            }
            catch (Exception e) {
                msg = e.Message;
                return false;
            }
            
        }

        public static FileContentResult DescargaArchivo(string fileId,ref string comentario,string razonDescarga = "")
        {
            Extension.Archivo arc;

            try {
                Funciones.SetPathVtcArchivos();

                arc = new Extension.Archivo(fileId,razonDescarga);
                return new FileContentResult(arc.FileContents, "MIMEType"){FileDownloadName = arc.FileName};
            }
            catch (Exception e) {
                comentario = "Error en VtcArchivos "+e.Message;
                return null;
            }
            
        }

        public static FileContentResult DescargaArchivo(int idTipo, int idReferencia, ref string comentario, string razonDescarga = "")
        {
            Extension.Archivo arc;

            try
            {
                Funciones.SetPathVtcArchivos();
                arc = new Extension.Archivo(idTipo,idReferencia, razonDescarga);
                return new FileContentResult(arc.FileContents, "MIMEType") { FileDownloadName = arc.FileName };
            }
            catch (Exception e)
            {
                comentario = "Error en VtcArchivos " + e.Message;
                return null;
            }

        }

        public static FileContentResult DescargaArchivoConPermiso(string fileId,int? idUsuario,int? idRol, ref string comentario,string razonDescarga="")
        {
            Extension.Archivo arc;

            try
            {
                Funciones.SetPathVtcArchivos();

                arc = new Extension.Archivo(fileId,idUsuario,idRol,razonDescarga);
                return new FileContentResult(arc.FileContents, "MIMEType") { FileDownloadName = arc.FileName };
            }
            catch (Exception e)
            {
                comentario = "Error en VtcArchivos " + e.Message;
                return null;
            }

        }

        public static bool SubirDocumento(HttpPostedFileBase file, int idDocumentoCargado,string nombreArchivo, string descripcion, ref string comentario, bool destacado = false, int? idTipo = null, int? idCategoria = null, int? idGrupo = null, bool saveIndice = false) {
            int idArchivo;
            string error = string.Empty;

            try {
                idArchivo = SubirArchivo(file, idDocumentoCargado, descripcion, nombreArchivo, ref error, destacado, idTipo, idCategoria, idGrupo, saveIndice);
                if (error != string.Empty) {
                    throw new Exception(error);
                }
                using (var docB = new Model.ArcEntities())
                {
                    var d = docB.ARC_DocumentosCargados.Where(x => x.id == idDocumentoCargado).FirstOrDefault();
                    d.idArchivo = idArchivo;
                    d.estaEntregado = true;
                    docB.SaveChanges();
                }
                return true;
            }
            catch (Exception e) {
                comentario = e.Message;
            }
            
            return false;
        }

        
        public static bool VerificarDocumento(int? idTipo, int? idReferencia,int? idDocumento,ref string comentario, bool verificado = true) {
            string error = string.Empty;
            Model.ARC_DocumentosCargados doc;
            try
            {
                using (var docB = new Model.ArcEntities())
                {
                    if(idDocumento != null) doc = docB.ARC_DocumentosCargados.Where(x => x.id == idDocumento).FirstOrDefault();
                    else doc = docB.ARC_DocumentosCargados.Where(x => x.idReferencia == idReferencia && x.idTipo==idTipo).FirstOrDefault();
                    doc.estaVerificado = verificado;
                    docB.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                comentario = e.Message;
            }

            return false;

        }

        public static string CrearDocumentosCargados(int idTipoNecesarios, int idReferenciaNecesarios, int idTipoCargados, int idReferenciaCargados) {
            string respuesta = string.Empty;
            Extension.DocumentosRequeridos req;
            try {
                req = new Extension.DocumentosRequeridos(idTipoNecesarios,idReferenciaNecesarios,idTipoCargados,idReferenciaCargados);
                req.CrearCargados();
                return respuesta;
            }
            catch (Exception e) {
                return e.Message;
            }
        }
        public static string CrearDocumentosCargados(List<VtcArchivos.Model.ARC_DocumentosCargados> documentosCargados, int idReferenciaCargados, int idTipoCargados)
        {
            string respuesta = string.Empty;
            Extension.DocumentosRequeridos req;
            try
            {
                req = new Extension.DocumentosRequeridos(0,0, idTipoCargados, idReferenciaCargados);
                req.CrearCargadosPorDocumentosCargados(documentosCargados);
                return respuesta;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /// <summary>
        /// Se requiere que ne la sentencia sql se incluya el parametro "excluido", no se muestran los archivos excluidos
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="urlSitio"></param>
        /// <param name="urlDescarga"></param>
        /// <param name="comentario"></param>
        /// <param name="resolucion_"></param>
        /// <returns></returns>
        public static List<Extension.ArchivoVisible> GetListaArchivos(string sql, string urlSitio,string urlDescarga, ref string comentario, Extension.Resolucion resolucion_ = Extension.Resolucion._32px) {
            List<Extension.ArchivoVisible> archivosV = null;

            try {
                comentario = string.Empty;
                using (var baseArc = new Model.ArcEntities()) {
                    archivosV = baseArc.Database.SqlQuery<Extension.ArchivoVisible>(sql).ToList();
                    archivosV = archivosV.Where(x => (x.excluido??false) == false).ToList();//Excluyendo los eliminados
                    archivosV = archivosV.Select(arc => { arc.resolucion =resolucion_; arc.urlSitio = urlSitio; arc.urlDescarga = urlDescarga; return arc; }).ToList();
                }
            }
            catch (Exception e) {
                comentario = e.Message;
            }

            return archivosV;
        }

        public static List<Model.ARC_DocumentosNecesarios> GetDocumentosNecesarios(int idRefNecesarios,ref string comentario)
        {
            List<Model.ARC_DocumentosNecesarios> archivosV = null;

            try
            {
                comentario = string.Empty;
                using (var baseArc = new Model.ArcEntities())
                {
                    baseArc.Configuration.LazyLoadingEnabled = false;
                    archivosV = baseArc.ARC_DocumentosNecesarios.Include("ARC_Documentos").Where(x => x.idReferencia==idRefNecesarios).ToList();
                }
            }
            catch (Exception e)
            {
                comentario = e.Message;
            }

            return archivosV;
        }
        public static List<Model.ARC_Documentos> GetDocumentos(int idCategoria, ref string comentario)
        {
            List<Model.ARC_Documentos> archivos = null;

            try
            {
                comentario = string.Empty;
                using (var baseArc = new Model.ArcEntities())
                {
                    baseArc.Configuration.LazyLoadingEnabled = false;
                    archivos = baseArc.ARC_Documentos.Where(x => x.idCategoria == idCategoria).ToList();
                }
            }
            catch (Exception e)
            {
                comentario = e.Message;
            }

            return archivos;
        }

        public static List<Model.ARC_DocumentosCargados> GetDocumentosArchivosCargados(int idRef,int idTipo, ref string comentario) {
            List<Model.ARC_DocumentosCargados> archivosC = null;

            try
            {
                comentario = string.Empty;
                using (var baseArc = new Model.ArcEntities())
                {
                    baseArc.Configuration.LazyLoadingEnabled = false;
                    
                    archivosC = baseArc.ARC_DocumentosCargados.Include("ARC_Documentos").Include("ARC_Archivos").Where(x => x.idReferencia == idRef && x.idTipo==idTipo).ToList();
                }
            }
            catch (Exception e)
            {
                comentario = e.Message;
            }

            return archivosC;
        }

        public static List<Model.ARC_Archivos> GetListaArchivos(int idReferencia, int idTipo,ref string msg) {
            List<Model.ARC_Archivos> archivo = null;
            try
            {
                using (var baseArc = new Model.ArcEntities())
                {
                    baseArc.Configuration.LazyLoadingEnabled = false;
                    archivo = baseArc.ARC_Archivos.Where(x => x.idReferencia == idReferencia && x.idTipo == idTipo && x.excluido == false).OrderByDescending(y => y.id).ToList();
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }
            return archivo;
        }

        public static Model.ARC_Archivos GetArchivo(int idReferencia, int idTipo,ref string msg) {
            Model.ARC_Archivos archivo = null;
            try {
                using (var baseArc = new Model.ArcEntities()) {
                    archivo = baseArc.ARC_Archivos.Where(x => x.idReferencia == idReferencia && x.idTipo == idTipo && x.excluido == false).OrderByDescending(y => y.id).FirstOrDefault();
                }
            }
            catch (Exception e) {
                msg = e.Message;
            }
            return archivo;
        }

        public static Model.ARC_Archivos GetArchivo(int idArchivo,ref string msg)
        {
            Model.ARC_Archivos archivo = null;
            try
            {
                using (var baseArc = new Model.ArcEntities())
                {
                    archivo = baseArc.ARC_Archivos.Where(x => x.id == idArchivo && x.excluido==false).OrderByDescending(y => y.id).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }
            return archivo;
        }

        public static List<Model.ARC_Archivos> GetArchivos(int idReferencia,List<int> tiposArchivos, ref string msg)
        {
            List<Model.ARC_Archivos> archivos = null;
            try
            {
                using (var baseArc = new Model.ArcEntities())
                {
                    baseArc.Configuration.LazyLoadingEnabled = false;
                    archivos = baseArc.ARC_Archivos.Where(x => x.idReferencia == idReferencia && x.excluido == false && tiposArchivos.Contains(x.idTipo??0)).OrderByDescending(y => y.idTipo).ToList();
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }
            return archivos;
        }

    }
}

