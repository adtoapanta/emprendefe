﻿1. Debe existir la carpeta ~/Archivos
	En caso de producci'on debe tener permisos de escritura esta carpeta

2. El proyecto que la usa requiere EntityFramework

3. E web.config se le debe annadir el string de conexi'on
	<add name="ArcEntities" connectionString="metadata=res://*/Model.ArcModel.csdl|res://*/Model.ArcModel.ssdl|res://*/Model.ArcModel.msl;provider=System.Data.SqlClient;provider connection string=&quot;data source=sqldesarrollo9208.cloudapp.net;initial catalog=Test;user id=vertice;password=vertice.123;MultipleActiveResultSets=True;App=EntityFramework&quot;" providerName="System.Data.EntityClient" />

4. Agregar en el proyecto la carpeta /Images y copiar el contenido de Resources/Images de VtcArchivos
5. Crear las tablas o correr el SQL:

____________________________________________________________________________________________________________________________

CREATE TABLE [dbo].[ARC_Archivos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](250) NULL,
	[descripcion] [nvarchar](250) NULL,
	[nombreArchivo] [nvarchar](250) NULL,
	[tamanno] [int] NULL,
	[extension] [nvarchar](4) NULL,
	[idTipo] [int] NULL,
	[idReferencia] [int] NULL,
	[peso] [nvarchar](50) NULL,
	[fecha] [datetime] NULL,
	[idCategoria] [int] NULL,
	[idGrupo] [int] NULL,
	[destacado] [bit] NOT NULL,
	[idPublico] [varchar](max) NULL,
	excluido bit default 0 null,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
---------------------------------------------

CREATE TABLE [dbo].[ARC_Indice](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idArchivo] [int] NOT NULL,
	[texto] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[ARC_Indice]  WITH CHECK ADD  CONSTRAINT [FK_Indice_Archivos] FOREIGN KEY([idArchivo])
REFERENCES [dbo].[ARC_Archivos] ([id])
GO

ALTER TABLE [dbo].[ARC_Indice] CHECK CONSTRAINT [FK_Indice_Archivos]
GO

------------------------------------------------

--CREATE TABLE [dbo].[ARC_Permisos](
--	[id] [int] IDENTITY(1,1) NOT NULL,
--	[idArchivo] [int] NOT NULL,
--	[idUsuario] [int] NULL,
--	[idRol] [int] NULL
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[ARC_Permisos]  WITH CHECK ADD  CONSTRAINT [FK_Permisos_Archivos] FOREIGN KEY([idArchivo])
--REFERENCES [dbo].[ARC_Archivos] ([id])
--GO

--ALTER TABLE [dbo].[ARC_Permisos] CHECK CONSTRAINT [FK_Permisos_Archivos]
--GO
--------------------------------------------------
CReate table dbo.ARC_PermisoUsuario(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idArchivo] [int] NOT NULL,
	[idUsuario] [int] NULL
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ARC_PermisoUsuario]  WITH CHECK ADD  CONSTRAINT [FK_PermisoUsuario_Archivos] FOREIGN KEY([idArchivo])
REFERENCES [dbo].[ARC_Archivos] ([id])
GO
ALTER TABLE [dbo].[ARC_PermisoUsuario] CHECK CONSTRAINT [FK_PermisoUsuario_Archivos]
GO
-------------------------------------------------
CReate table dbo.ARC_PermisoRol(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idArchivo] [int] NOT NULL,
	[idRol] [int] NULL
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ARC_PermisoRol]  WITH CHECK ADD  CONSTRAINT [FK_PermisoRol_Archivos] FOREIGN KEY([idArchivo])
REFERENCES [dbo].[ARC_Archivos] ([id])
GO
ALTER TABLE [dbo].[ARC_PermisoRol] CHECK CONSTRAINT [FK_PermisoRol_Archivos]
GO
--------------------------------------------------


CREATE TABLE [dbo].[ARC_Documentos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idCategoria] [int] NOT NULL,
	[nombre] [nvarchar](250) NOT NULL,
	[descripcion] [nvarchar](1000) NULL,
	[esFormato] [bit] NULL,
	[pathFormato] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--------------------------------------------------

CREATE TABLE [dbo].[ARC_DocumentosNecesarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idDocumento] [int] NULL,
	[idTipo] [int] NULL,
	[idReferencia] [int] not NULL,
	[esExcepcionable] [bit] NULL default 0,
	[esObligatorio] [bit] NULL default 0,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ARC_DocumentosNecesarios]  WITH CHECK ADD  CONSTRAINT [FK_Documentos_DocumentosNecesarios] FOREIGN KEY([idDocumento])
REFERENCES [dbo].[ARC_Documentos] ([id])
GO

--------------------------------------------------

CREATE TABLE [dbo].[ARC_DocumentosCargados](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idDocumento] [int] not NULL,
	[idArchivo] [int] NULL,
	[idTipo] [int] NULL,
	[idReferencia] [int] not NULL,
	[estaEntregado] [bit] NULL default 0,
	[observaciones] [nvarchar](150) NULL default '',
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


ALTER TABLE [dbo].[ARC_DocumentosCargados]  WITH CHECK ADD  CONSTRAINT [FK_Documentos_DocumentosCargados] FOREIGN KEY([idDocumento])
REFERENCES [dbo].[ARC_Documentos] ([id])
GO

ALTER TABLE [dbo].[ARC_DocumentosCargados]  WITH CHECK ADD CONSTRAINT [FK_Archivos_DocumentosCargados] FOREIGN KEY([idArchivo])
REFERENCES [dbo].[ARC_Archivos] ([id])
GO

CREATE TABLE [dbo].[ARC_BitacoraDescarga](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idArchivo] [int] not NULL,
	[idUsuario] [int] NULL,
	[razon] [nvarchar](max) NULL,
	[fecha] [datetime] NULL,
	[observacion] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ARC_BitacoraDescarga]  WITH CHECK ADD  CONSTRAINT [FK_Bitacora_Archivos] FOREIGN KEY([idArchivo])
REFERENCES [dbo].[ARC_Archivos] ([id])
GO
______________________________________________________________________________________________________________________