﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VtcArchivos.Model;

namespace VtcArchivos.Extension
{
    public class DocumentosRequeridos
    {
        private int idTipoNecesario { get; set; }
        private int idReferenciaNecesario { get; set; }
        private int idTipoCargado { get; set; }
        private int idReferenciaCargado { get; set; }

        public DocumentosRequeridos(int idTipoNecesarios, int idReferenciaNecesarios, int idTipoCargados, int idReferenciaCargados) {
            idTipoNecesario = idTipoNecesarios;
            idReferenciaNecesario = idReferenciaNecesarios;
            idTipoCargado = idTipoCargados;
            idReferenciaCargado = idReferenciaCargados;
        }

        public void CrearCargados() {
            List<ARC_DocumentosNecesarios> necesarios;
            ARC_DocumentosCargados car;
            using (var docB = new Model.ArcEntities()) {
                necesarios = docB.ARC_DocumentosNecesarios.Where(x => x.idTipo == idTipoNecesario && x.idReferencia == idReferenciaNecesario).ToList();
                if (necesarios.Count > 0) {
                    foreach (ARC_DocumentosNecesarios n in necesarios)
                    {
                        car = new ARC_DocumentosCargados() { idDocumento = Convert.ToInt32(n.idDocumento), idArchivo = null, idTipo = idTipoCargado, idReferencia = idReferenciaCargado, estaEntregado = false, observaciones = string.Empty , estaVerificado=false};
                        docB.ARC_DocumentosCargados.Add(car);
                    }
                    docB.SaveChanges();
                }
                
            }
        }

        public void CrearCargadosPorDocumentosCargados(List<VtcArchivos.Model.ARC_DocumentosCargados> documentos) {
            ARC_DocumentosCargados cargado;
            using (var docB = new Model.ArcEntities())
            {
                foreach (ARC_DocumentosCargados idd in documentos) {//foreach va en orden segun documentacion
                    cargado = new ARC_DocumentosCargados() { idDocumento = idd.idDocumento, idArchivo = null, idTipo = idTipoCargado, idReferencia = idReferenciaCargado, estaEntregado = false, observaciones = idd.observaciones, estaVerificado=false };
                    docB.ARC_DocumentosCargados.Add(cargado);
                }
                docB.SaveChanges();
            }

            }

    }
}
