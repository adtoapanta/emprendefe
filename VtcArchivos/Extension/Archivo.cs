﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VtcArchivos.Extension
{
    internal class Archivo
    {
        private int id = 0;
        private int? idTipo = null;
        private int? idReferencia = null;
        private string idPublico;
        private int? idUsuario;
        private int? idRol;
        private string razon;
        public byte[] FileContents { get; set; }
        public string FileName { get; set; }

        public Archivo(int idT, int idR, string razon_)
        {
            idTipo = idT;
            idReferencia = idR;
            razon = razon_;
            this.CargarContenido();
            this.GuardarBitacoraDescarga();
        }

        public Archivo(string idP, string razon_) {
            idPublico = idP;
            razon = razon_;
            this.CargarContenido();
            this.GuardarBitacoraDescarga();
        }

        public Archivo(string idA, int? idU, int? idR, string razon_) {
            this.idPublico = idA;
            this.idUsuario = idU;
            this.idRol = idR;
            this.razon = razon_;
            if (PermitirDescargar())
            {
                this.CargarContenido();
                this.GuardarBitacoraDescarga();
            }
            else {
                throw new Exception("No tiene permiso para descargar el archivo.");
            }

        }

        private void SetIdFromIdPublico(Model.ArcEntities baseArc)
        {
            VtcArchivos.Model.ARC_Archivos archivo;
            if (this.id == 0) {
                if (string.IsNullOrEmpty(idPublico))
                {
                    archivo = baseArc.ARC_Archivos.Where(x => x.idTipo == idTipo && x.idReferencia == idReferencia).FirstOrDefault();
                }
                else {
                    archivo = baseArc.ARC_Archivos.Where(x => x.idPublico == idPublico).FirstOrDefault();
                }
                
                this.id = archivo.id;
            }

        }

        private bool PermitirDescargar()
        {
            bool permite = false;
            //Primero verificamos si existe restriccion por rol
            using (var baseArc = new Model.ArcEntities()) {
                SetIdFromIdPublico(baseArc);
                if (idRol != null && idUsuario != null)
                {
                    permite = PermitirDescargarRol(baseArc) && PermitirDescargarUsuario(baseArc);
                }
                else if (idRol != null)
                {
                    permite = PermitirDescargarRol(baseArc);
                }
                else if (idUsuario != null)
                {
                    permite = PermitirDescargarUsuario(baseArc);
                }
                else {
                    permite = true;
                }
            }
            return permite;
        }

        private bool PermitirDescargarRol(Model.ArcEntities baseArc) {
            var permiR = baseArc.ARC_PermisoRol.Where(x => x.idArchivo == id).ToList();
            if (permiR.Count == 0)
            {
                return true;
            }
            else {
                var pp = permiR.Where(x => x.idRol == idRol);
                return pp != null;
            }
        }
        private bool PermitirDescargarUsuario(Model.ArcEntities baseArc)
        {
            var permiU = baseArc.ARC_PermisoUsuario.Where(x => x.idArchivo == id).ToList();
            if (permiU.Count == 0)
            {
                return true;
            }
            else
            {
                var pu = permiU.Where(x => x.idUsuario == idUsuario);
                return pu != null;
            }
        }

        private void CargarContenido() {
            Model.ARC_Archivos archivo;
            string path;

            using (var baseArc = new Model.ArcEntities())
            {
                SetIdFromIdPublico(baseArc);
                archivo = baseArc.ARC_Archivos.Where(x => x.id == id).FirstOrDefault();
            }
            if (archivo != null)
            {
                if (archivo.excluido??false) throw new Exception("El archivo fue eliminado.");
                path = string.Format("{0}{1}.{2}", Funciones.pathVtcArchivos, archivo.id, archivo.extension);
                FileName = archivo.nombreArchivo;
                FileContents = System.IO.File.ReadAllBytes(@path);
            }
            else {
                throw new Exception(string.Format("No existe Archivo con id {0}", this.id));
            }
        }

        private void GuardarBitacoraDescarga() {
            VtcArchivos.Model.ARC_BitacoraDescarga bitacora;
            using (var baseArc = new Model.ArcEntities())
            {
                bitacora = new VtcArchivos.Model.ARC_BitacoraDescarga() {
                    idArchivo = this.id,
                    idUsuario = this.idUsuario,
                    razon = this.razon,
                    fecha = DateTime.Now,
                    observacion = ""
                };
                baseArc.ARC_BitacoraDescarga.Add(bitacora);
                baseArc.SaveChanges();
            }
        }

        public static bool EliminarArchivo(int? id, int? idReferencia, int? idTipo,ref string msg ,bool eliminarFisico=false)
        {
            Model.ARC_Archivos archivo = null;
            

            using (var baseArc = new Model.ArcEntities())
            {
                if (id != null)
                {
                    archivo = baseArc.ARC_Archivos.Where(x => x.id == id).FirstOrDefault();
                }
                else if (idReferencia != null && idTipo != null)
                {
                    archivo = baseArc.ARC_Archivos.Where(x => x.idTipo == idTipo && x.idReferencia == idReferencia).FirstOrDefault();
                }

                if (archivo != null)
                {
                    if (eliminarFisico) {
                        var exterminado = EliminarArchivoFisico(archivo.id.ToString()+"."+archivo.extension);
                        if (!exterminado) msg = "Archivo físico no eliminado.";
                    }
                    archivo.excluido = true;
                    baseArc.Entry(archivo).State = EntityState.Modified;
                    baseArc.SaveChanges();
                }
                else {
                    msg = "Archivo no existe en la base de archivos";
                    return false;
                }

            }
            return true;
        }

        private static bool EliminarArchivoFisico(string nombreArchivo){
            string path;

            path = string.Format("{0}{1}", Funciones.pathVtcArchivos, nombreArchivo);

            if (System.IO.File.Exists(@path)) {
                System.IO.File.Delete(@path);
                return true;
            }

            return false;
        }

    }
}
