﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VtcArchivos.Extension
{
    public class ArchivoVisible
    {
        public string idPublico { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public string extension { get; set; }
        public bool? excluido { get; set; }

        internal Resolucion resolucion { get; set; } = Resolucion._32px;
        internal string urlSitio { get; set; } = string.Empty;

        public string urlIconoExtension
        {
            get
            {
                if (urlSitio != string.Empty)
                {
                    return urlSitio + "/Images/" + GetResolucion() + "/" + extension.ToLower() + ".png";
                }
                return "";
            }
        }

        private string urlDescarga_ = string.Empty;
        public string urlDescarga
        {
            set
            {
                urlDescarga_ = value;
            }
            get
            {
                if (urlDescarga_ != string.Empty)
                {
                    if (urlDescarga_.Contains("http:") || urlDescarga_.Contains("https:"))
                    {
                        return urlDescarga_ + "/" + idPublico.ToString();
                    }
                    else
                    {
                        return urlSitio + urlDescarga_ + "/" + idPublico;
                    }

                }
                return "";
            }
        }
        public string urlThumb
        {
            get
            {
                //TODOno implementado thumbs
                if (urlSitio != string.Empty)
                {
                    return urlSitio + "/Images/16px/" + extension.ToLower() + ".png";
                }
                return "";
            }
        }

        private string GetResolucion()
        {
            switch (this.resolucion)
            {
                case Resolucion._16px:
                    return "16px";
                case Resolucion._32px:
                    return "32px";
                case Resolucion._48px:
                    return "48px";
                case Resolucion._512px:
                    return "512px";
            }
            return "32px";
        }


        public ArchivoVisible()
        {


        }




    }

    public enum Resolucion
    {
        _16px = 1, _32px = 2, _48px = 3, _512px = 4
    }
}
