//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VtcArchivos.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class ARC_Documentos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ARC_Documentos()
        {
            this.ARC_DocumentosCargados = new HashSet<ARC_DocumentosCargados>();
            this.ARC_DocumentosNecesarios = new HashSet<ARC_DocumentosNecesarios>();
        }
    
        public int id { get; set; }
        public int idCategoria { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public Nullable<bool> esFormato { get; set; }
        public string pathFormato { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ARC_DocumentosCargados> ARC_DocumentosCargados { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ARC_DocumentosNecesarios> ARC_DocumentosNecesarios { get; set; }
    }
}
