﻿var SeguimientoJS = {
    id: 0,
    idAcuerdo: 0,
    idResponsable: null,
    fechaPlanificado: new Date(),
    estaHecho: false,
    fechaHecho: null,
    cumplimiento: null,
    avance: 0,
    objetivos: '',
    observaciones: '',

    checkVars: function () {
        console.log('-- Seguimiento --');
        
        console.log('id');
        console.log(this.id);
        console.log('idAcuerdo');
        console.log(this.idAcuerdo);
        console.log('idResponsable');
        console.log(this.idResponsable);
        console.log('fechaPlanificado');
        console.log(this.fechaPlanificado);
        console.log('estaHecho');
        console.log(this.estaHecho);
        console.log('fechaHecho');
        console.log(this.fechaHecho);
        console.log('cumplimiento');
        console.log(this.cumplimiento);
        console.log('avance');
        console.log(this.avance);
        console.log('objetivos');
        console.log(this.objetivos);
        console.log('observaciones');
        console.log(this.observaciones);
    }
}

function getObjNuevoSeguimiento() {
    //var ResponsableSeguimiento = $("#slcNuevoSegResponsable").val();
    var FechaPlanificada = $("#txtNuevoSegFechaPlan").val();
    var Objetivos = $("#txtNuevoSegObjetivos").val();

    //creo el objeto
    console.log('creo el objeto Seguimiento');
    var miSeguimientoJS = Object.create(SeguimientoJS);
    console.log('objeto seguimiento nuevo');
    miSeguimientoJS.checkVars();

    miSeguimientoJS.idResponsable = null; //ResponsableSeguimiento;
    miSeguimientoJS.fechaPlanificado = FechaPlanificada;
    miSeguimientoJS.objetivos = Objetivos;

    console.log('objeto seguimiento nuevo con variables');
    miSeguimientoJS.checkVars();
    return miSeguimientoJS;
}