﻿var htclibjs = {

    //MANDAR POR DEFAULT UN token?

    Ajax: function (args) {
        /// <summary>Función que ejecuta una petición AJAX.</summary> y maneja cosas standar
        $.ajax({
            type: args.type == undefined ? "POST" : args.type,
            url: SITEROOT + args.url,
            contentType: "application/json; charset=utf-8",
            data: args.data == undefined ? {} : args.data,
            dataType: "json",
            timeout: args.timeout == undefined ? 300000 : args.timeout,
            beforeSend: args.beforeSend == undefined ? function () { } : args.beforeSend(),
            async: args.async == undefined ? true : args.async,
            success: function (r) { htclibjs.manejaSuccessAjax(args.success, r); },
            error: function (err) { htclibjs.manejaErrorAjax(args.error, err); }
        });
    },


    manejaSuccessAjax: function (manejador, r) {

        if (r.exitoso) {
            manejador(r);
        }
        else {
            console.log("Logueo de errores: " + r.data);
            console.log(r);
            alert("Exitoso pero con errores: " + r.data);
        }

    },

    manejaErrorAjax: function (manejador, err) {
        console.log("Logueo de errores: " + err);

        console.log("error Fer");
        try {
            parser = new DOMParser();
            xmlDoc = parser.parseFromString(err.responseText, "text/xml");
            var nodoTitulo = xmlDoc.getElementsByTagName("title")[0];
            //si hay algo escrito en el título, lo busco 
            if (nodoTitulo.childNodes.length > 0) {
                var textoError = nodoTitulo.childNodes[0].nodeValue;
                console.log('Error: ' + textoError);
            }
        } catch (e) {
            //si lo anterior falla, sólo logeo directamente el error 
        }

        console.log(err);

        var mostrar = true;

        if (err != null && "status" in err) {
            var especificacion = "";
            switch (err.status) {
                case 0:
                    if (err.statusText === "timeout") { especificacion = ""; }
                    mostrar = false;
                    break;
                case 404:
                    especificacion = "No encontrado";
                    break;
                case 500:
                    especificacion = "Problema de servidor";
                    break;
                default:
                    mostrar = false;
                    break;
            }

            if (mostrar == true) {
                alert("Se ha detectado un error, por favor realice una captura de pantalla y envíela a sistemas.    Error al conectar con el servidor:" + especificacion + " " + err.status + " " + err.statusText);
            } else {

            }


        } else {
            alert("Error no determinado al conectar con el servidor. Vuelva a intentar");
        }

        // el cliente quiere manejar el error
        if (manejador != undefined) { manejador(err); }

    },



}

function getOutDate(d) {//as Date
    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = d.getFullYear() + '/' +
        (month < 10 ? '0' : '') + month + '/' +
        (day < 10 ? '0' : '') + day;

    return output;
}

function getDateFromJson(data) {
    var fecha;

    if (dateIsEmpty(data))
        return '';
    else {
        if (typeof data !== 'undefined') {
            fecha = new Date(parseInt(data.substr(6)));
            return getOutDate(fecha);
        } else
            return data;
    }
}

function dateIsEmpty(data) {//
    var cincuentayalgo = new Date(1950, 1, 2);
    var numeroFecha;
    var fecha;

    if (typeof data !== 'undefined' && data !== null) {
        numeroFecha = parseInt(data.substr(6));
        fecha = new Date(numeroFecha);

        if (fecha.getTime() < cincuentayalgo.getTime()) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}
function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function getDateForInput(d) {
    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = d.getFullYear() + '-' +
        (month < 10 ? '0' : '') + month + '-' +
        (day < 10 ? '0' : '') + day;

    return output;
}