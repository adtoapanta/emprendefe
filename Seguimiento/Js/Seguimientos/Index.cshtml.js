﻿var _acuerdos = []; //todos los acuerdos
var _acuerdosFiltrado = []; //filtrado sólo los no finalizados 
var arrayAcuerdos = []; //array que se está usando (puede ser _acuerdos o _acuerdosFiltrado)
var _funcionarios = [];
var _acuerdoActual = {};
var _seguimientoActual = {};
var archivosSubir = {};

$(document).ready(function () { init(); });

function init() {
    console.log('init');

    getFuncionarios();

    initHome();
    initNuevoAcuerdo();
    initHacerSeguimiento();
    initSiguienteSeguimiento();
    initPostergarSeguimiento();
}

//Funcionarios
function getFuncionarios() {
    console.log('cargo funcionarios');

    htclibjs.Ajax({
        url: "/General/getFuncionarios",
        data: {},
        success: function (r) {
            console.log('funcionarios');
            console.log(r.data);
            _funcionarios = r.data;
            //cargaSlc(_funcionarios, "slcNuevoAcuerdoResponsable", false);
            //cargaSlc(_funcionarios, "slcNuevoSegResponsable", false);
            //cargaSlc(_funcionarios, "slcSigSegResponsable", false);
        }
    });
}
function cargaSlc(data, nombreObjeto, cargarSeleccionar) {

    var select = $('#' + nombreObjeto);

    if (cargarSeleccionar) {
        select.append('<option value=' + '0' + '>' + '-- seleccionar --' + '</option>')
    }

    $.each(data, function (key, registro) {
        select.append('<option value=' + registro.id + '>' + registro.nombre + '</option>');
    });
}

//HOME
function initHome() {
    traeAcuerdos();

    $('input[type=radio][name=radioAcuerdos]').change(function () {

        if (this.value == '0') {
            arrayAcuerdos = _acuerdosFiltrado;
            llenaTablaAcuerdos();
        }
        else {
            arrayAcuerdos = _acuerdos;
            llenaTablaAcuerdos();
        }
    });
}
function traeAcuerdos() {
    var idE = $("#lblIdEntidad").text();

    htclibjs.Ajax({
        url: "/Acuerdos/getAcuerdos",
        data: JSON.stringify({
            id: idE
        }),
        success: function (r) {
            console.log('Acuerdos');
            console.log(r.data);

            _acuerdos = r.data;

            //filtro sólo los pendientes
            _acuerdosFiltrado = $.grep(_acuerdos, function (item, index) {
                if (item.estaCumplido == 1) {
                    return item;
                }
            }, true);

            arrayAcuerdos = _acuerdosFiltrado;
            llenaTablaAcuerdos();
        }
    });
}
function llenaTablaAcuerdos() {
    console.log('llenaTablaAcuerdos');

    $("#tblAcuerdos").DataTable({
        data: arrayAcuerdos,
        aaSorting: [],
        bDestroy: true,
        bFilter: false,
        bPaginate: false,
        bLengthChange: false,
        bInfo: false,
        //  language: espannol,
        aoColumns: [
            {
                mData: 'acuerdo', title: 'Acuerdo', mRender: function (data, type, full) {
                    var link = "<a href='" + '/Acuerdos/ver/' + full.id + "'>" + data + "</a>";
                    return link;
                }
            },
            {
                mData: 'UsFecha', title: 'Ultimo Seguimiento', mRender: function (data, type, full) {
                     return getDateFromJson(data);
                 }
            },
            {
                mData: 'UsCumplimiento', title: 'Cumplido', mRender: function (data, type, full) {
                    var retorno = '';

                    switch (data) {
                        case 0:
                            //No cumplido
                            retorno = '<i class="fas fa-times-circle text-danger"></i>';
                            break;
                        case 1:
                            //Cumplido parcial
                            retorno = '<i class="fas fa-adjust"></i>';
                            break;
                        case 2:
                            //Cumplido
                            retorno = '<i class="fas fa-check-circle text-success"></i>';
                            break;   
                    }

                    return retorno;
                }
            },
            {
                mData: 'PsFecha', title: 'Próximo Seguimiento', mRender: function (data, type, full) {
                    if (data == null) {
                        if (full.estaCumplido) 
                            return 'Finalizado';
                        else 
                            return 'N/A';                        
                    }
                    else {
                        var fecha = new Date(parseInt(data.substr(6)));
                        var hoy = new Date();

                        var day_as_milliseconds = 86400000;
                        var diff_in_millisenconds = fecha - hoy;
                        var diff_in_days = diff_in_millisenconds / day_as_milliseconds;
                        var dias = Math.trunc(diff_in_days);

                        var fechaS = getDateFromJson(data);

                        var retorno = '';

                        //link postergar
                        var dataName = 'idAcuerdo';
                        var dataValue = full.id;
                        var clase = 'PostergarSeg';
                        var modal = 'ModalPostergarSeguimiento';
                        var texto = 'postergar';
                        var lnkPostergar = getHtmlLink(dataName, dataValue, clase, modal, texto);

                        if (diff_in_days <= 5) {
                            retorno = '<span class="text-danger">' + fechaS + ' (' + dias + ')' + '</span>   ' + lnkPostergar;
                        }
                        else {
                            retorno = '<span class="text-success">' + fechaS + ' (' + dias + ')' + '</span>   ';
                        }
                        
                        return retorno;
                    }
                }
            },
            {
                mData: 'PsObjetivos', title: 'Objetivos', mRender: function (data){
                    return data;
                }
            },
            {
                mData: 'id', title: '', mRender: function (data, type, full) {
                    var button = '';

                    var estaCumplido = full.estaCumplido;

                    if (!estaCumplido) {
                        var idProxSeg = full.PsId;

                        if (idProxSeg != null && idProxSeg > 0) {
                            var dataName = 'idAcuerdo';
                            var dataValue = data;
                            var clase = 'hacerSeg';
                            var claseBoton = 'btn-primary';
                            var modal = 'ModalHacerSeguimiento';
                            var buttonText = 'Hacer seguimiento';
                            button = getHtmlButton(dataName, dataValue, clase, modal, buttonText, claseBoton);
                        }
                        else {
                            var dataName = 'idAcuerdo';
                            var dataValue = data;
                            var clase = 'SiguienteSeg';
                            var claseBoton = 'btn-warning';
                            var modal = 'ModalSiguienteSeguimiento';
                            var buttonText = 'Siguiente seguimiento';
                            button = getHtmlButton(dataName, dataValue, clase, modal, buttonText, claseBoton);
                        }
                    }
                    //si ya está cumplido no muestro nada 

                    return button;
                }
            }
        ],
        'columnDefs': [
            {
                "targets": 1, 
                "className": "text-center"
            }
        ]
    });

    $(".hacerSeg").click(function (e) {
        e.preventDefault();
        console.log('click .hacerSeg');

        var idAcuerdo = $(this).data("idacuerdo");
        console.log('idAcuerdo');
        console.log(idAcuerdo);
        $('#ModalHacerSeguimiento').attr("data-idAcuerdo", idAcuerdo);
        
        for (var i = 0; i < arrayAcuerdos.length; i++) {
            var actual = arrayAcuerdos[i];

            if (actual.id == idAcuerdo) {
                _acuerdoActual = actual;

                //cargo seguimiento
                $('#ModalHacerSeguimiento').attr("data-idSeg", _acuerdoActual.PsId);
                //$('#lblHSResponsable').text(_acuerdoActual.PsResponsable);
                $('#lblHSFechaPlan').text(getDateFromJson(_acuerdoActual.PsFechaPlanificado));
                $('#lblHSObjetivos').text(_acuerdoActual.PsObjetivos);

                $('#lblHSNombreAcuerdo').text(_acuerdoActual.acuerdo);
            }
        }
        console.log('_acuerdoActual');
        console.log(_acuerdoActual);
    });

    $(".SiguienteSeg").click(function (e) {
        e.preventDefault();
        console.log('click .SiguienteSeg');

        var idAcuerdo = $(this).data("idacuerdo");
        console.log('idAcuerdo');
        console.log(idAcuerdo);

        for (var i = 0; i < arrayAcuerdos.length; i++) {
            var actual = arrayAcuerdos[i];

            if (actual.id == idAcuerdo) {
                _acuerdoActual = actual;
                $('#lblSSNombreAcuerdo').text(_acuerdoActual.acuerdo);

                if (_acuerdoActual.esPeriodico) {
                    var fechaUs = new Date(parseInt(_acuerdoActual.UsFecha.substr(6)));
                    var fechaSug = addDays(fechaUs, _acuerdoActual.periodicidad);
                    cargoFechaSugerida(fechaSug);
                }
                
            }
        }
        console.log('_acuerdoActual');
        console.log(_acuerdoActual);
    });

    $(".PostergarSeg").click(function (e) {
        e.preventDefault();
        console.log('click .PostergarSeg');

        var idAcuerdo = $(this).data("idacuerdo");
        console.log('idAcuerdo');
        console.log(idAcuerdo);

        for (var i = 0; i < arrayAcuerdos.length; i++) {
            var actual = arrayAcuerdos[i];

            if (actual.id == idAcuerdo) {
                _acuerdoActual = actual;
                $('#lblPSNombreAcuerdo').text(_acuerdoActual.acuerdo);
            }
        }
        console.log('_acuerdoActual');
        console.log(_acuerdoActual);
    });
}
function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}
function getHtmlButton(dataName, dataValue, clase, modal, buttonText, claseBoton) {
    var button = '<button type="button"';
    button += "data-" + dataName + "='" + dataValue + "' ";
    button += 'class="btn '+ claseBoton + ' ' + clase + '" ';
    button += 'data-toggle="modal" ';
    button += 'data-target="#' + modal + '"> ';
    button += buttonText + '</button>';
    return button;
}
function getHtmlLink(dataName, dataValue, clase, modal, texto) {
    var lnk = '<a href="#"';
    lnk += "data-" + dataName + "='" + dataValue + "' ";
    lnk += 'class="' + clase + '" ';
    lnk += 'data-toggle="modal" ';
    lnk += 'data-target="#' + modal + '"> ';
    lnk += texto + '</a>';
    return lnk;
}

//NUEVO ACUERDO
function initNuevoAcuerdo() {
    console.log('init Nuevo Acuerdo');

    $('#chkNuevoAcuerdoEsPeriodico').click(function () {
        if (this.checked) {
            $('#divNuevoAcuerdoPeriodicidad').show();
        }
        else {
            $('#divNuevoAcuerdoPeriodicidad').hide();
        }
    });

    $('#chkNuevoAcuerdoEsDatos').click(function () {
        if (this.checked) {
            $('#divNuevoAcuerdoIndicadores').show();
        }
        else {
            $('#divNuevoAcuerdoIndicadores').hide();
        }
    });

    $('#chkNuevoAcuerdoEsPlanAccion').click(function () {
        if (this.checked) {
            $('#divNuevoAcuerdoPlanAccion').show();
        }
        else {
            $('#divNuevoAcuerdoPlanAccion').hide();
        }
    });

    $('#btnNuevoAcuerdoGuardar').click(function () {
        console.log('guardando NuevoAcuerdo');
        var objAcuerdo = getObjNuevoAcuerdo();
        var objSeguimiento = getObjNuevoSeguimiento();

        guardaNuevoAcuerdo(objAcuerdo, objSeguimiento);
    });
}
function guardaNuevoAcuerdo(pAcuerdo, pSeguimiento) {
    console.log('guardaAcuerdo');

    htclibjs.Ajax({
        url: "/Acuerdos/GuardarAcuerdo",
        data: JSON.stringify({
            acuerdo: pAcuerdo,
            seguimiento: pSeguimiento
        }),
        success: function (r) {
            console.log(r.data);
            initHome();
        }
    });
}

//HACER SEGUIMIENTO
function initHacerSeguimiento() {
    fileSelectInit();
    $("#divHSLoading").hide();
    $("#divHSFooterBotones").show();
}
function fileSelectInit() {

    // Esto sirve para que funcione el input type file modificado
    $(':file').on('fileselect', function (event, numFiles, label) {
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

        //console.log(numFiles);
        console.log(label);
    });

    $("#fileUpload").on('change', function (e) {
        // Esto sirve para que funcione el input type file modificado
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);

        //controlar que no se suba más de un archivo
        archivosSubir.files = e.target.files;
        console.log("change");
        console.log(archivosSubir);
    });

    $("#btnHSGuardar").bind("click", function (event) {
        event.preventDefault();

        guardarHacerSeguimiento();
    });
}
function guardarHacerSeguimiento() {
    console.log("envío form");
    startAjax();

    try {
        //busco parametros 
        var cumplimiento = $("#slcHSCumplimiento").val();
        var avance = $("#txtHSAvance").val();
        var obs = $("#txtHSObservaciones").val();
        var idSeg = _acuerdoActual.PsId;

        // Creo un formdata para añadir lo que se va a enviar 
        var data = new FormData();
        //añado el archivo si fue seleccionado
        var arrayDeArchivos = 'files' in archivosSubir;
        if (arrayDeArchivos) {
            $.each(archivosSubir.files, function (key, value) {
                data.append(key, value);
            });
        }

        //añado otras variables 
        data.append("pCumplimiento", cumplimiento);
        data.append("pAvance", avance);
        data.append("pObs", obs);
        data.append("pIdSeg", idSeg);

        //defino el url de envío
        console.log("urlPath");
        var urlPath = '/Seguimientos/HacerSeguimiento';
        var url = SITEROOT + urlPath;
        console.log(url);

        //defino llamada ajax
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (r, textStatus, jqXHR) {
                console.log("success");
                if (r.exitoso) {

                    vacíoSeguimientoAct();

                    var tipo = typeof r.data;                  
                    if (tipo !== 'boolean') {
                        var fecha = new Date(parseInt(r.data.substr(6)));
                        cargoFechaSugerida(fecha);
                    }

                    endAjax(true);

                } else {
                    endAjax(false, r.data);
                }
                initHome();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Handle errors here
                //console.log(r);
                console.log('ERRORS: ' + textStatus);

                endAjax(false, textStatus);
            }
        });
    } catch (e) {
        endAjax(false, e.message)
    }
    
}
function startAjax() {
    //muestro loading
    var divLoading = $('#divHSLoading');
    divLoading.show();

    //deshabilito el botón
    var uploadButton = $('#btnHSGuardar');
    uploadButton.prop("disabled", true);
    uploadButton.attr('class', 'btn btn-light');

    $('#divHsError').hide();
    $('#divSuccess').hide();
}
function endAjax(success, mensajeError) {
    $('#divHSError').hide();

    //oculto loading
    var divLoading = $('#divHSLoading');
    divLoading.hide();
    //habilito el botón
    var uploadButton = $('#btnHSGuardar');
    uploadButton.prop("disabled", false);
    uploadButton.attr('class', 'btn-success btn-lg');

    if (!success) {
        console.log(mensajeError);
        $('#divHSError').html("Error: " + mensajeError);

        console.log("divHSError.show()");
        $('#divHSError').show();
        $('#divSuccess').hide();
    }
    else {
        $('#ModalHacerSeguimiento').modal('hide'); // cerrar
        $('#ModalSiguienteSeguimiento').modal('show'); // abrir
    }
}
function vacíoSeguimientoAct() {
    console.log('vacio seguimiento ACTUAL');

    var archivosSubir = {};
    var _seguimientoActual = {};

    $('#slcHSCumplimiento').val(0);
    $("#txtHSAvance").val("");
    $("#txtHSObservaciones").val("");

    //reinicio FileUploads
    ReiniciarfileUploads();
}
function ReiniciarfileUploads() {
    // 1) borro el valor de los inputs realcionados a los fileUpload 
    $(':file').parents('.input-group').find(':text').val('');

    //2) creo nuevos fileUpload para que se borre su valor 'HasFiles'
    clearFileInput("fileUpload");

    //3) reinicio los eventos change y fileselect de los nuevos fileUpload 
    //para que se vea los nuevos archivos seleccionados 
    fileSelectInit();
}
function clearFileInput(id) {
    var oldInput = document.getElementById(id);

    var newInput = document.createElement("input");

    newInput.type = "file";
    newInput.id = oldInput.id;
    newInput.name = oldInput.name;
    newInput.className = oldInput.className;
    newInput.style.cssText = oldInput.style.cssText;
    // TODO: copy any other relevant attributes 

    oldInput.parentNode.replaceChild(newInput, oldInput);
}

function cargoFechaSugerida(fecha) {
    //cuando es periódico 
    //viene la fecha sugerida del siguiente seguimiento 

    
    var sFecha = fecha.toISOString().substr(0, 10);
    document.getElementById("txtSigSegFechaPlan").value = sFecha;

    $("#lblSigSegSugerenciaFecha").text("Este acuerdo es periódico se sugiere planificar el seguimiento para el: " + sFecha);
    $('#divSigSegSugerenciaFecha').show();
}

//SIGUIENTE SEGUIMIENTO 
function initSiguienteSeguimiento() {
    console.log("init Siguiente seguimiento");

    $('#divCerrarAcuerdo').show();
    $('#divSiguienteSeguimiento').hide();
    $('#divSSLoading').hide();

    $('input[type=radio][name=radioSiguiente]').change(function () {
        switch ($(this).val()) {
            case '0':
                $('#divCerrarAcuerdo').show();
                $('#divSiguienteSeguimiento').hide();
                break;
            case '1':
                $('#divCerrarAcuerdo').hide();
                $('#divSiguienteSeguimiento').show();
                break;
        }
    });

    $('#btnSSGuardar').click(function () {
        console.log('_acuerdoActual');
        console.log(_acuerdoActual);

        var radioValue = $("input[name='radioSiguiente']:checked").val();
        console.log(radioValue);

        var idAcuerdo = _acuerdoActual.id;
        

        if (radioValue == 0) {
            htclibjs.Ajax({
                url: "/Acuerdos/CerrarAcuerdo",
                data: JSON.stringify({
                    pIdAcuerdo: idAcuerdo
                }),
                success: function (r) {
                    console.log(r.data);
                    //actualizar tabla acuerdos 
                    initHome();
                }
            });
        }
        else {
            var idResp = $('#slcSigSegResponsable').val();
            var fechaPlan = $('#txtSigSegFechaPlan').val();
            var objetivos = $('#txtSigSegObjetivos').val();

            htclibjs.Ajax({
                url: "/Seguimientos/GuardaSiguienteSeguimiento",
                data: JSON.stringify({
                    pIdAcuerdo: idAcuerdo, pIdResp: idResp, pFecha: fechaPlan, pObj: objetivos
                }),
                success: function (r) {
                    console.log(r.data);
                    vacíoSiguienteSeguimiento();
                    //actualizar tabla acuerdos 
                    initHome();
                }
            });
        }

        
    });
}
function vacíoSiguienteSeguimiento() {
    console.log('vacio seguimiento');

    var _acuerdoActual = {}; //vacío acuerdo porque ya no necesito interactuar 
    $("#txtSigSegFechaPlan").val("");   
    $("#txtSigSegObjetivos").val("");

    $("#lblSigSegSugerenciaFecha").text("");
    $("#divSigSegSugerenciaFecha").hide();
}

//POSTERGAR SEGUIMIENTO
function initPostergarSeguimiento() {
    $('#divPostLoading').hide();

    $('#btnPostGuardar').click(function () {
        var idSigSeguimiento = _acuerdoActual.PsId;
        var NuevaFecha = $('#txtPostNuevaFecha').val();

        htclibjs.Ajax({
            url: "/Seguimientos/PostergarSeguimiento",
            data: JSON.stringify({
                pIdSigSeguimiento: idSigSeguimiento,
                pNuevaFecha: NuevaFecha
            }),
            success: function (r) {
                console.log('r');
                console.log(r);

                if (r.exitoso) {
                    $('#ModalPostergarSeguimiento').modal('hide');

                    //actualizar tabla acuerdos 
                    initHome();
                }
                else {
                    console.log(r.data);
                    $('#divPostError').html("Error: " + r.data);

                    console.log("divPostError.show()");
                    $('#divPostError').show();
                }
            }
        });

    });
}

//borrar esto
function testarrow() {
    var materials = [
        'hidrógeno',
        'Helio',
        'Litio',
        'Berilio'
    ];

    console.log(materials.map(material => material.length))
}