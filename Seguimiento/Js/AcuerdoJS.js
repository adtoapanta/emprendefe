﻿var AcuerdoJS = {
    id: 0,
    idEntidad: 0,
    idTipo: null,
    nombre: '',
    descripcion: '',
    idResponsable: null,
    fecha: new Date(),
    fechaFinal: new Date(),
    estaCumplido: false,
    fechaCumplido: null,
    idUltimoSeguimiento: null,
    idProximoSeguimiento: 0,
    numSeguimientos: 0,
    kpi: '',
    avance: 0,
    esPeriodico: false,
    PERIODICIDAD: 0,
    esDatos: false,
    esEvidencia: false,
    esPlanAccion: false,
    idPlan: 0,
    esDocumentos: false,
    esChecklist: false,

    checkVars: function () {
        console.log('--- Acuerdo ---');
        console.log('Nombre');
        console.log(this.nombre);
        console.log('Descripcion');
        console.log(this.descripcion);
        console.log('ResponsableAcuerdo');
        console.log(this.idResponsable);
        console.log('fechaFinal');
        console.log(this.fechaFinal);
        console.log('KPI');
        console.log(this.KPI);
        console.log('esPeriodico');
        console.log(this.esPeriodico);
        console.log('Periodicidad');
        console.log(this.PERIODICIDAD);
        console.log('esDatos');
        console.log(this.esDatos);
        console.log('esPlanAccion');
        console.log(this.esPlanAccion);
        console.log('idPlan');
        console.log(this.idPlan);
        console.log('esDocumentos');
        console.log(this.esDocumentos);
    }
};

function getObjNuevoAcuerdo() {
    //acuerdo
    var Nombre = $("#txtNuevoAcuerdoNombre").val();
    var Descripcion = $("#txtNuevoAcuerdoDescripcion").val();
    var ResponsableAcuerdo = null; //$("#slcNuevoAcuerdoResponsable").val();
    var fechaFinal = $("#txtNuevoAcuerdoFechaFinal").val();
    var KPI = $("#txtNuevoAcuerdoKPI").val();
    console.log('KPI');
    console.log(KPI);

    var esPeriodico = $("#chkNuevoAcuerdoEsPeriodico").is(':checked');
    var Periodicidad = $("#txtNuevoAcuerdoPeriodicidad").val();
    var esDatos = $("#chkNuevoAcuerdoEsDatos").is(':checked');
    var esPlanAccion = $("#chkNuevoAcuerdoEsPlanAccion").is(':checked');
    var idPlan = $("#txtNuevoAcuerdoIdPlan").val();
    var esDocumentos = $("#chkNuevoAcuerdoEsDocumentos").is(':checked');
    var idEntidad = $("#lblIdEntidad").text();

    let idTipo = $("#txtNuevoAcuerdoTipo").val();

    //creo el objeto
    console.log('creo el objeto Acuerdo');
    var miAcuerdoJS = Object.create(AcuerdoJS);

    miAcuerdoJS.idEntidad = idEntidad;
    miAcuerdoJS.nombre = Nombre;
    miAcuerdoJS.descripcion = Descripcion;
    miAcuerdoJS.idResponsable = ResponsableAcuerdo;
    if (fechaFinal == '') { fechaFinal = new Date(); }
    miAcuerdoJS.fechaFinal = fechaFinal;
    miAcuerdoJS.KPI = KPI;
    miAcuerdoJS.esPeriodico = esPeriodico;
    if (miAcuerdoJS.esPeriodico) {
        miAcuerdoJS.PERIODICIDAD = Periodicidad;
    }
    miAcuerdoJS.esDatos = esDatos;
    miAcuerdoJS.esPlanAccion = esPlanAccion;
    if (miAcuerdoJS.esPlanAccion) {
        miAcuerdoJS.idPlan = 0; //aquí hacer todo lo del archivo
    }
    miAcuerdoJS.esDocumentos = esDocumentos;
    miAcuerdoJS.idTipo = idTipo;

    return miAcuerdoJS;
};