﻿using Seguimiento.Extensions;
using Seguimiento.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace Seguimiento.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}