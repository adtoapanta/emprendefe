﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Seguimiento.Models;
using Seguimiento.Extensions;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;

namespace Seguimiento.Controllers
{
    public class SeguimientosController : Controller
    {
        // GET: Seguimientos
        public ActionResult Index(int idTipo, int idRef)
        {
            Entidad entidad = new Entidad();
            using (var db = new seguimientoEntities())
            {
                entidad = new Entidad(idRef, idTipo, db);
            }

            ViewBag.entidad = entidad;
            return View();
        }

        public ActionResult Nuevo(int id)
        {
            if (id > 0)
            {
                SEG_seguimientos seguimiento = new SEG_seguimientos();
                using (var db = new seguimientoEntities())
                {
                    seguimiento = db.SEG_seguimientos.Include("MIS_Funcionarios").Include("MIS_funcionarios.Mis_Personas").Include("SEG_Acuerdos").Where(s => s.id == id).FirstOrDefault();
                }

                if (seguimiento.id > 0)
                {
                    ViewBag.seguimiento = seguimiento;
                    return View();
                }
                else
                {
                    return RedirectToAction("error");
                }
            }
            else
            {
                return RedirectToAction("error");
            }


        }

        public JsonResult HacerSeguimiento()
        {
            Resultado resultado;
            try
            {
                DateTime FechaRetorno = new DateTime();
                HttpContext context = System.Web.HttpContext.Current;
                using (var db = new seguimientoEntities())
                {
                    HacerSeguimiento hacerSeg = new HacerSeguimiento(db, context);
                    hacerSeg.guardoSeguimientoYArchivo();

                    if (hacerSeg.acuerdo.esPeriodico)
                    {
                        FechaRetorno = DateTime.Today.AddDays(hacerSeg.acuerdo.PERIODICIDAD);
                        resultado = new Resultado(true, FechaRetorno);
                    }
                    else
                        resultado = new Resultado(true, true);
                }
            }
            catch (Exception e)
            {
                resultado = new Resultado(false, "Error al hacer seguimiento: " + e.Message);
            }

            return Json(resultado);
        }

        public JsonResult GuardaSiguienteSeguimiento(int pIdAcuerdo, int pIdResp, DateTime pFecha, string pObj)
        {
            Resultado resultado;
            try
            {
                HttpContext context = System.Web.HttpContext.Current;
                using (var db = new seguimientoEntities())
                {
                    SEG_seguimientos sigSeg = new SEG_seguimientos();
                    sigSeg.idAcuerdo = pIdAcuerdo;
                    sigSeg.idResponsable = pIdResp;
                    sigSeg.fechaPlanificado = pFecha;
                    sigSeg.objetivos = pObj;
                    sigSeg.estaHecho = false;
                    sigSeg.avance = 0;

                    db.SEG_seguimientos.Add(sigSeg);
                    db.SaveChanges();

                    SEG_Acuerdos acuerdo = db.SEG_Acuerdos.Where(a => a.id == pIdAcuerdo).First();
                    acuerdo.idProximoSeguimiento = sigSeg.id;
                    db.SaveChanges();
                }

                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                resultado = new Resultado(false, "Error al guardar siguiente seguimiento: " + e.Message);
            }

            return Json(resultado);
        }

        public JsonResult PostergarSeguimiento( int pIdSigSeguimiento, DateTime pNuevaFecha)
        {
            Resultado resultado;
            try
            {
                if (pIdSigSeguimiento > 0)
                {
                    using (var db = new seguimientoEntities())
                    {
                        SEG_seguimientos sigSeg = db.SEG_seguimientos.Where(s => s.id == pIdSigSeguimiento).First();
                        sigSeg.fechaPlanificado = pNuevaFecha;

                        //TODO: aquí debo guardar el control de cambios...

                        db.SaveChanges();
                    }

                    resultado = new Resultado(true, true);
                }
                else
                {
                    resultado = new Resultado(false, "Error al postergar siguiente seguimiento: " + "idSeguimiento es 0");
                }
                
            }
            catch (Exception e)
            {
                resultado = new Resultado(false, "Error al postergar siguiente seguimiento: " + e.Message);
            }

            return Json(resultado);
        }
    }
}