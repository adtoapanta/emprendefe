﻿using Seguimiento.Extensions;
using Seguimiento.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Seguimiento.Controllers
{
    public class GeneralController : Controller
    {
        // GET: General
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult getFuncionarios()
        {
            Resultado resultado;
            try
            {
                List<MIS_Funcionarios> funcionarios = new List<MIS_Funcionarios>();
                using (var db = new seguimientoEntities())
                {
                    funcionarios = db.MIS_Funcionarios.Include("Mis_Personas").ToList();
                }

                List<IdNombre> listaFuncionarios = new List<IdNombre>();
                foreach (MIS_Funcionarios funcionario in funcionarios)
                {
                    IdNombre func = new IdNombre();
                    func.id = funcionario.id;
                    func.nombre = funcionario.MIS_Personas.nombre + " "+ funcionario.MIS_Personas.apellido;
                    listaFuncionarios.Add(func);
                }

                resultado = new Resultado(true, listaFuncionarios);
            }
            catch (Exception e)
            {

                //Logger.Write("Error en cargar acuerdos" + e.Message);
                resultado = new Resultado(false, "Error al cargar acuerdos: " + e.Message);
            }

            return Json(resultado);
        }
    }
}