﻿using Seguimiento.Extensions;
using Seguimiento.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Seguimiento.Controllers
{
    public class AcuerdosController : Controller
    {
        // GET: Acuerdos
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult getAcuerdos(int id)
        {
            Resultado resultado;
            try
            {
                List<Acuerdo> acuerdos = new List<Acuerdo>();
                List<SEG_VW_Acuerdos> acuerdosvW = new List<SEG_VW_Acuerdos>();
                using (var db = new seguimientoEntities())
                {
                    db.Configuration.LazyLoadingEnabled = true;

                    Entidad entidad = new Entidad(id, db);
                    entidad.cargaAcuerdos();
                    acuerdos = entidad.acuerdosObj;
                    acuerdosvW = entidad.acuerdosVw;
                }

                resultado = new Resultado(true, acuerdosvW);
            }
            catch (Exception e)
            {
                //Logger.Write("Error en cargar acuerdos" + e.Message);
                resultado = new Resultado(false, "Error al cargar acuerdos: " + e.Message);
            }

            return Json(resultado);
        }

        public JsonResult GuardarAcuerdo(SEG_Acuerdos acuerdo, SEG_seguimientos seguimiento)
        {
            Resultado resultado;
            try
            {
                if (acuerdo.idEntidad > 0)
                {
                    using (var db = new seguimientoEntities())
                    {
                        //acuerdo 
                        acuerdo.fecha = DateTime.Today;
                        
                        db.SEG_Acuerdos.Add(acuerdo);
                        db.SaveChanges(); //guardo para poner id del acuerdo en el seguimiento

                        //seguimiento 
                        seguimiento.idAcuerdo = acuerdo.id;
                        db.SEG_seguimientos.Add(seguimiento);
                        db.SaveChanges(); //guardo para poner id de proximo seguimiento en acuerdo 

                        acuerdo.idProximoSeguimiento = seguimiento.id;
                        db.SaveChanges();
                    }
                }
                resultado = new Resultado(true, true);
            }
            catch (DbEntityValidationException ex)
            {
                string errorCompleto = "";

                foreach (DbEntityValidationResult item in ex.EntityValidationErrors)
                {
                    // Get entry
                    DbEntityEntry entry = item.Entry;
                    string entityTypeName = entry.Entity.GetType().Name;

                    // Display or log error messages
                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",
                                 subItem.ErrorMessage, entityTypeName, subItem.PropertyName);
                        errorCompleto += "/n" + message;
                    }
                }

                resultado = new Resultado(false, "Error al cargar acuerdos: " + errorCompleto);
            }
            catch (Exception e)
            {
                //Logger.Write("Error en cargar acuerdos" + e.Message);
                resultado = new Resultado(false, "Error al cargar acuerdos: " + e.Message);
            }


            return Json(resultado);
        }

        public JsonResult CerrarAcuerdo(int pIdAcuerdo)
        {
            Resultado resultado;
            try
            {
                using (var db = new seguimientoEntities())
                {
                    //acuerdo 
                    SEG_Acuerdos acuerdo = db.SEG_Acuerdos.Where(a => a.id == pIdAcuerdo).First();
                    acuerdo.estaCumplido = true;
                    acuerdo.fechaCumplido = DateTime.Today;
                    acuerdo.idProximoSeguimiento = null;
                    db.SaveChanges();
                }

                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                resultado = new Resultado(false, e.Message);
            }

            return Json(resultado);
        }
    }
}