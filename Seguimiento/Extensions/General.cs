﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Seguimiento.Extensions
{
    public class General
    {

        public static DateTime getDateFromObject(object obj)
        {
            try
            {
                DateTime fecha = new DateTime();
                if (obj is DateTime)
                    fecha = (DateTime)obj;
                else if (obj is string)
                {
                    try
                    {
                        fecha = DateTime.ParseExact(obj.ToString().Trim(), "dd-MM-yyyy", CultureInfo.CurrentCulture);
                    }
                    catch (Exception)
                    {
                        string formatoEsperado = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;
                        throw new Exception("error al convertir " + obj.ToString() + " a fecha - CultureInfo: " + CultureInfo.CurrentCulture + " - objectType: " + obj.GetType().Name + " - se esperaba: " + formatoEsperado);
                    }
                }
                else
                    throw new Exception("error al convertir  a fecha - CultureInfo: " + CultureInfo.CurrentCulture);

                return fecha;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

    }
}