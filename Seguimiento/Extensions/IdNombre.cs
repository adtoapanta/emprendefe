﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seguimiento.Extensions
{
    public class IdNombre
    {
        public int id { get; set; }
        public string nombre { get; set; }

        public IdNombre() { }
    }
}