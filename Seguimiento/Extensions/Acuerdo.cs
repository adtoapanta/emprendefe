﻿using Seguimiento.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Seguimiento.Extensions
{
    public class Acuerdo
    {
        public SEG_Acuerdos objAcuerdo;
        public SEG_seguimientos siguienteSeg;
        public string sigSegResponsable;

        public Acuerdo(seguimientoEntities db, SEG_Acuerdos pObjAcuerdo)
        {
            try
            {
                this.objAcuerdo = pObjAcuerdo;
                if (this.objAcuerdo.idProximoSeguimiento != null && this.objAcuerdo.idProximoSeguimiento > 0)
                {
                    this.siguienteSeg = db.SEG_seguimientos.Include("MIS_Funcionarios").Include("MIS_Funcionarios.MIS_Personas").Where(s => s.id == pObjAcuerdo.idProximoSeguimiento).First();
                    this.sigSegResponsable = siguienteSeg.MIS_Funcionarios.MIS_Personas.nombre + " " + siguienteSeg.MIS_Funcionarios.MIS_Personas.apellido;
                    DbEntityEntry acuerdoEntry = db.Entry(this.siguienteSeg);
                    acuerdoEntry.State = System.Data.Entity.EntityState.Detached;
                }               
            }
            catch (Exception e)
            {
                throw new Exception("consntructor objeto acuerdo: " + e.Message);
            }
        }
    }
}