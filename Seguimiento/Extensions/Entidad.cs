﻿using Seguimiento.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Seguimiento.Extensions
{
    public class Entidad
    {
        seguimientoEntities db;

        public int id;
        public string nombre;
        public int idRef;
        public int idTipo;

        public SEG_Entidades entidadEntity;
        public List<SEG_Acuerdos> acuerdos;
        public List<SEG_VW_Acuerdos> acuerdosVw;
        public List<Acuerdo> acuerdosObj;

        public Entidad() { }

        public Entidad(int pIdRef, int pIdTipo, seguimientoEntities pDb)
        {
            this.db = pDb;
            entidadEntity = db.SEG_Entidades.Where(e => e.idRef == pIdRef).Where(e => e.idTipo == pIdTipo).FirstOrDefault();

            if (entidadEntity == null)
            {
                entidadEntity = new SEG_Entidades();
                entidadEntity.idTipo = pIdTipo;
                entidadEntity.idRef = pIdRef;

                db.SEG_Entidades.Add(entidadEntity);
                db.SaveChanges();    
            }

            //después de crear la entidad o si ya fue creada antes pero es null o no encontrada, actualizo el nombre
            if (entidadEntity.nombre == null || entidadEntity.nombre == "-No encontrado-")
            {
                string sql = string.Format("SEG_SP_ActualizaNombreEntidad {0} ", entidadEntity.id);
                db.Database.ExecuteSqlCommand(sql);

                //recargo la información que se generó en la base con el SP anterior 
                db.Entry<SEG_Entidades>(entidadEntity).Reload();
            }

            //todo esto ya no debería ir
            this.id = entidadEntity.id;
            this.nombre = entidadEntity.nombre;
            this.idTipo = pIdTipo;
            this.idRef = pIdRef;
        }

        public Entidad(int pId, seguimientoEntities db)
        {
            this.id = pId;
            this.db = db;

            SEG_Entidades seg_entidades = db.SEG_Entidades.Where(e => e.id == pId).FirstOrDefault();

            if (seg_entidades != null)
            {
                this.id = seg_entidades.id;
                this.nombre = seg_entidades.nombre;
                this.idRef = seg_entidades.idRef ?? 0;
                this.idTipo = seg_entidades.idTipo ?? 0;
            }
            else
            {
                throw new Exception("entidad no encontrada");
            }
        }

        internal void cargaAcuerdos()
        {
            try
            {
                if (this.id > 0)
                {
                    //sólo esto sería lo nuevo
                    acuerdosVw = db.SEG_VW_Acuerdos.Where(i => i.idEntidad == this.id).ToList();
                }
                else
                {
                    throw new Exception("Entidad No encontrada");
                }
            }
            catch (Exception e)
            {
                throw new Exception("Entidad - Carga Acuerdos - " + e.Message);
            }
        }

        void entitiesDetails()
        {
            using (db)
            {
                //get student whose StudentId is 1
                var objEntidad = db.SEG_Entidades.Find(1);

                //edit student name
                objEntidad.nombre = "Edited name";

                //get DbEntityEntry object for student entity object
                var entry = db.Entry(objEntidad);

                //get entity information e.g. full name
                Console.WriteLine("Entity Name: {0}", entry.Entity.GetType().FullName);

                //get current EntityState
                Console.WriteLine("Entity State: {0}", entry.State);

                Console.WriteLine("********Property Values********");

                foreach (var propertyName in entry.CurrentValues.PropertyNames)
                {
                    Console.WriteLine("Property Name: {0}", propertyName);

                    //get original value
                    var orgVal = entry.OriginalValues[propertyName];
                    Console.WriteLine("     Original Value: {0}", orgVal);

                    //get current values
                    var curVal = entry.CurrentValues[propertyName];
                    Console.WriteLine("     Current Value: {0}", curVal);
                }

            }
        }
    }
}