﻿using Seguimiento.Models;
using System.Collections.Specialized;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;

namespace Seguimiento.Extensions
{
    public class HacerSeguimiento
    {
        HttpContext context;
        NameValueCollection form;
        seguimientoEntities db;

        int avance;
        int cumplimiento;
        string observaciones;
        int idSeg;
        public SEG_Acuerdos acuerdo;

        public HacerSeguimiento(seguimientoEntities pDb, HttpContext pContext)
        {
            this.context = pContext;
            this.form = this.context.Request.Form;
            this.db = pDb;

            ObtengoDatosDelForm();
        }

        public void ObtengoDatosDelForm()
        {
            //recibo parametros del form
            avance = Convert.ToInt32(form["pAvance"]);
            cumplimiento = Convert.ToInt32(form["pCumplimiento"]);
            observaciones = form["pObs"];
            idSeg = Convert.ToInt32(form["pIdSeg"]);
        }

        public void guardoSeguimientoYArchivo()
        {
            try
            {
                //guardo seguimiento
                SEG_seguimientos seguimiento = db.SEG_seguimientos.Where(s => s.id == idSeg).First();

                seguimiento.avance = avance;
                seguimiento.cumplimiento = cumplimiento;
                seguimiento.observaciones = observaciones;
                seguimiento.estaHecho = true;
                seguimiento.fechaHecho = DateTime.Today;
                int idArchivo = getIdInforme();
                if (idArchivo != 0)
                    seguimiento.idArchivoInforme = idArchivo;

                //actualizo valores del siguiente acuerdo

                acuerdo = db.SEG_Acuerdos.Where(a => a.id == seguimiento.idAcuerdo).First();
                acuerdo.idUltimoSeguimiento = seguimiento.id;
                acuerdo.idProximoSeguimiento = null;
                acuerdo.numSeguimientos += 1;

                db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                string errorCompleto = "";

                foreach (DbEntityValidationResult item in ex.EntityValidationErrors)
                {
                    // Get entry
                    DbEntityEntry entry = item.Entry;
                    string entityTypeName = entry.Entity.GetType().Name;

                    // Display or log error messages
                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",
                                 subItem.ErrorMessage, entityTypeName, subItem.PropertyName);
                        errorCompleto += "/n" + message;
                    }
                }

                throw new Exception("DbEntityValidationException al guardar seguimiento: " + errorCompleto);
            }
            catch (Exception e)
            {
                throw new Exception("guardar seguimiento: " + e.Message);
            }

        }

        int getIdInforme()
        {
            string coment = "";
            int idArchivo = 0;
            HttpPostedFile miArchivo;

            //guardo archivo Informe
            if (this.context.Request.Files.Count > 0)
            {
                miArchivo = this.context.Request.Files[0];
                if ((miArchivo != null) && (miArchivo.ContentLength > 0) && !string.IsNullOrEmpty(miArchivo.FileName))
                {
                    try
                    {
                        HttpPostedFileBase filebase = new HttpPostedFileWrapper(miArchivo);
                        idArchivo = VtcArchivos.Funciones.SubirArchivo(filebase, 1, "Informe de seguimiento", "", ref coment);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("No se pudo leer el archivo: " + e.Message);
                    }
                }
            }

            return idArchivo;
        }
    }
}