//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Seguimiento.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MIS_Funcionarios
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MIS_Funcionarios()
        {
            this.SEG_Acuerdos = new HashSet<SEG_Acuerdos>();
            this.SEG_seguimientos = new HashSet<SEG_seguimientos>();
        }
    
        public int id { get; set; }
        public int idPersona { get; set; }
        public string cargo { get; set; }
    
        public virtual MIS_Personas MIS_Personas { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SEG_Acuerdos> SEG_Acuerdos { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SEG_seguimientos> SEG_seguimientos { get; set; }
    }
}
