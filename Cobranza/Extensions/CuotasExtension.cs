﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cobranza.Models;
namespace Cobranza.Extensions
{
    public class CuotasExtension
    {
        public List<COB_cuotas> consultarCuotasVencidas(int idDeuda)
        {
            List<COB_cuotas> cuotas = new List<COB_cuotas>();
            using (var a = new Models.emprendeEntities())
            {

                cuotas = a.COB_cuotas.Where(x => x.idDeuda == idDeuda && x.fechaVence < DateTime.Today).ToList();

            }
            return cuotas;
        }

        public List<COB_cuotas> consultarEstadoCuenta(int idDeuda)
        {
            List<COB_cuotas> cuotas = new List<COB_cuotas>();
            using (var a = new Models.emprendeEntities())
            {
                cuotas = a.COB_cuotas.Where(x => x.idDeuda == idDeuda).OrderBy(x=> x.fechaVence).ToList();
            }
            return cuotas;
        }

        public List<COB_cuotas> consultarCuotasNoPagadasDeuda(int idDeuda)
        {
            List<COB_cuotas> cuotas = new List<COB_cuotas>();
            using (var a = new Models.emprendeEntities())
            {

                cuotas = a.COB_cuotas.Where(x => x.idDeuda == idDeuda && x.esPagada == false ).OrderBy(x => x.fechaVence).ToList();

            }
            return cuotas;
        }

        public List<COB_cuotas> consultarCuotasNoPagadasDeudor(int idDeudor) 
        {
            List<COB_cuotas> cuotas = new List<COB_cuotas>();
            List<COB_deudas> deudas = new List<COB_deudas>();
            using (var a = new Models.emprendeEntities())
            {

                deudas = a.COB_deudas.Include("COB_cuotas").Where(x => x.idDeudor == idDeudor ).ToList();

                foreach (var x in deudas)
                {
                    
                    cuotas.AddRange(x.COB_cuotas.Where(z => z.esPagada == false).OrderBy(z => z.fechaVence).ToList());
                }
            }
            cuotas = cuotas.OrderBy(z => z.fechaVence).ToList();
            return cuotas;
        }

    }
}