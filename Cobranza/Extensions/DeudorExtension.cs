﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cobranza.Models;

namespace Cobranza.Extensions
{
    public class DeudorExtension
    {
        public COB_deudores consultarDeudor(int idDeudor)
        {
            COB_deudores deudor = new COB_deudores();

            using (var a = new Models.emprendeEntities())
            {
                deudor = a.COB_deudores.Where(x => x.id == idDeudor).FirstOrDefault();
            }
            return deudor;
        }

        public COB_VW_deudores consultarDeudor(int idtipo, int idRef)
        {
            COB_VW_deudores deu;
            string sql = "SELECT * FROM COB_VW_deudores WHERE idTipo=@idTipo AND idRef=@idRef";

            using (var baseEE = new Models.emprendeEntities())
            {
                deu = baseEE.Database.SqlQuery<Models.COB_VW_deudores>(sql, new System.Data.SqlClient.SqlParameter("idTipo", idtipo), new System.Data.SqlClient.SqlParameter("idRef", idRef)).FirstOrDefault();
            }
            return deu;
        }

    }
}