﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cobranza.Models;
using System.Data.SqlClient;
using System.Data;
using System.Data.OleDb;
using Spire.Xls;
using System.Globalization;

namespace Cobranza.Extensions
{
    public class PagosExtension
    {
        public void ingresarPago(int idDeudor, decimal valor, bool esConfirmado, int tipoPago, string documento)
        {

            COB_pagos pago = new COB_pagos();

            using (var a = new Models.emprendeEntities())
            {

                pago.idDeudor = idDeudor;
                pago.fecha = DateTime.Today;
                pago.valor = valor;
                pago.esConfirmado = esConfirmado;
                pago.idTipoPago = tipoPago;
                pago.documento = documento;
                a.COB_pagos.Add(pago);
                a.SaveChanges();

                distribuirPago(idDeudor, valor, pago.id);
            }

        }

        public void ingresarPago(int idDeudor, decimal valor, bool esConfirmado, int tipoPago, string documento, DateTime fechaPago)
        {

            COB_pagos pago = new COB_pagos();

            if (valor != 0)
            {
                using (var a = new Models.emprendeEntities())
                {

                    pago.idDeudor = idDeudor;
                    pago.fecha = fechaPago;
                    pago.valor = Math.Round(valor, 2);
                    pago.esConfirmado = esConfirmado;
                    pago.idTipoPago = tipoPago;
                    pago.documento = documento;
                    a.COB_pagos.Add(pago);
                    a.SaveChanges();

                    distribuirPago(idDeudor, valor, pago.id, fechaPago);
                }
            }



        }

        private void distribuirPago(int idDeudor, decimal valorPago, int idPago)
        {
            PagosCuotasExtension distribucion = new PagosCuotasExtension();
            CuotasExtension cuota = new CuotasExtension();

            distribucion.ingresarDistribuciones(cuota.consultarCuotasNoPagadasDeudor(idDeudor), idPago, valorPago);

            actualizarDeuda();
        }
        private void distribuirPago(int idDeudor, decimal valorPago, int idPago, DateTime fechaPago)
        {
            PagosCuotasExtension distribucion = new PagosCuotasExtension();
            CuotasExtension cuota = new CuotasExtension();

            distribucion.ingresarDistribuciones(cuota.consultarCuotasNoPagadasDeudor(idDeudor), idPago, valorPago, fechaPago);

            actualizarDeuda();
        }
        public List<COB_pagos> consultarPagosDeudor(int idDeudor)
        {
            List<COB_pagos> pagos = new List<COB_pagos>();

            using (var a = new Models.emprendeEntities())
            {
                pagos = a.COB_pagos.Where(x => x.idDeudor == idDeudor).ToList();
            }
            return pagos;
        }
        public void actualizarDeuda() // corre el sp que actualiza valores de las deudas en la base de datos
        {
            SqlConnection conn = new SqlConnection("Data Source = 184.168.194.78; Initial Catalog = emprende; user id = emprende; password = Crisfe!007");
            conn.Open();
            SqlCommand cmd = new SqlCommand("COB_SP_ActualizaDeudas", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void probarExcel()
        {

        }

        public void ingresarPagosFromFile(string filepath)
        {
            //creacion de variable cedula deudor leida desde el archivo
            string cedula;

            //creacion de variables que toman datos del pago 
            bool esConfirmado;
            int deudorId;
            decimal valorPago;
            int idTipoPago;
            string tipoPago;
            DateTime fechaPago;
            string documento;

            //lectura del archivo
            Workbook workbook = new Workbook(); // objeto de la clase spire
            workbook.LoadFromFile(filepath); // lectura del archivo en la ubicacion especifica            
            Worksheet sheet = workbook.Worksheets["Hoja1"]; //lectura de la sheet en la posicion 0            
            DataTable dt = sheet.ExportDataTable(); // guardar la tabla en dataset


            //valida que existan las columnas a ser leidas en el archivo
            if (dt.Columns.Contains("Contrapartida") && dt.Columns.Contains("Valor pro.") && dt.Columns.Contains("Fecha pro.") && dt.Columns.Contains("Referencia") && dt.Columns.Contains("Forma pago"))
            {
                //recorrido del data table
                foreach (DataRow dtRow in dt.Rows)
                {

                    //inicializacion de variables para el pago
                    cedula = dtRow["Contrapartida"].ToString();
                    deudorId = getDeudorId(cedula);
                    // valida que exista el deudor
                    if (deudorId != 0)
                    {
                        valorPago = Convert.ToDecimal(dtRow["Valor pro."].ToString());
                        valorPago = Math.Round(valorPago, 2);
                        IFormatProvider culture = new CultureInfo("en-US", true);
                        //fechaAux = dtRow["Fecha pro."].ToString().Split(' ');
                        //fechaAux = fechaAux[0].Split('/');
                        fechaPago = Convert.ToDateTime(dtRow["Fecha pro."].ToString()); //new DateTime(Convert.ToInt32(fechaAux[2]), Convert.ToInt32(fechaAux[1]), Convert.ToInt32(fechaAux[0]) );                        
                        documento = dtRow["Referencia"].ToString();
                        tipoPago = dtRow["Forma pago"].ToString();
                        idTipoPago = getTipoPago(tipoPago);
                        esConfirmado = true;

                        //ingreso del pago con funcion sobrecargada
                        ingresarPago(deudorId, valorPago, esConfirmado, idTipoPago, documento, fechaPago);
                    }

                }
            }
            else
            {
                throw new System.ArgumentException("Archivo no se encuentra en el formato correcto", "error");
            }


        }

        public int getDeudorId(string cedula) //recupera el id del deudor pasandole el parametro cedula
        {
            int idDeudor = 0;
            using (var a = new Models.emprendeEntities())
            {
                int.TryParse(a.Database.SqlQuery<int>("SP_COB_getDeudor @cedula", new SqlParameter("cedula", cedula)).FirstOrDefault().ToString(), out idDeudor);
                return idDeudor;
            }

        }

        private int getTipoPago(string tipoPago) // recupera el id de tipo pago pasandole el tipo de pago
        {
            int idTipoPago = 0;

            if (tipoPago.Equals("EFE"))
                idTipoPago = 1;
            else
                idTipoPago = 2;

            return idTipoPago;
        }

    }


}