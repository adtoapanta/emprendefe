﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cobranza.Extensions
{
    public class TablaVencidosExtension
    {
        //campos tablaVencidos
        public int id { get; set; }
        public string nombreDeudor { get; set; }
        public string deuda { get; set; }
        public int maxVencido { get; set; }
        public decimal valorVencido { get; set; }

        //Campos ver todas las deudas 
        public decimal capital { get; set; }
        public decimal vigente { get; set; }
        public decimal vencido { get; set; }
        public int idDeudor { get; set; }
        public int idDeuda { get; set; }
        public int dias { get; set; }
        public Boolean castigado { get; set; }

        public List<TablaVencidosExtension> consultarTablaVencidos()
        {
            using (var a = new Models.emprendeEntities())
            {
                return a.Database.SqlQuery<TablaVencidosExtension>("COB_SP_ConsultaTablaVencidos").ToList();
            }
        }

        public List<TablaVencidosExtension> consultarTodasDeudas()
        {
            using (var a = new Models.emprendeEntities())
            {
                return a.Database.SqlQuery<TablaVencidosExtension>("COB_SP_ConsultaTodasDeudas").ToList();
            }
        }
    }
}