﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Cobranza.Extensions
{
    public class TodosPagosExtension
    {
        public int id { get; set; }
        public string Deudor { get; set; }
        public DateTime fecha { get; set; }
        public decimal capital { get; set; }
        public decimal interes { get; set; }
        public decimal total { get; set; }

        public List<TodosPagosExtension> consultarTodosPagos()
        {
            DateTime actual;
            DateTime inicioMes;
            actual = DateTime.Now;
            inicioMes = new DateTime(actual.Year, actual.Month, 1); ;

            using (var a = new Models.emprendeEntities())
            {
                return a.Database.SqlQuery<TodosPagosExtension>("COB_SP_pagosFechas @desde, @hasta", new SqlParameter("desde", inicioMes), new SqlParameter("hasta", actual)).ToList();
            }
        }
        public List<TodosPagosExtension> consultarTodosPagos(DateTime desde, DateTime hasta)
        {
            
            using (var a = new Models.emprendeEntities())
            {
                return a.Database.SqlQuery<TodosPagosExtension>("COB_SP_pagosFechas @desde, @hasta", new SqlParameter("desde", desde), new SqlParameter("hasta", hasta)).ToList();
            }
        }
    }
}