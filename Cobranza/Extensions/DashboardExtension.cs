﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Cobranza.Extensions
{
    public class DashboardExtension
    {
        public ResumenCartera resumenCartera;
        public PagosTotales pagosTotales;

        public decimal interesPorVencer { get; set; }
        public decimal capitalPorVencer { get; set; }
        public decimal interesVencido { get; set; }
        public decimal capitalVencido { get; set; }
        public decimal totalVencido { get; set; }
        public decimal totalPorVencer { get; set; }
        public decimal mora { get; set; }

        public decimal capital { get; set; }
        public decimal interes { get; set; }
        public decimal total { get; set; }
        public DateTime inicioMes { get; set; }
        public DateTime actual { get; set; }

        public DashboardExtension()
        {
            resumenCartera = new ResumenCartera();
            pagosTotales = new PagosTotales();

            resumenCartera = resumenCartera.consultarResumenCartera();
            pagosTotales = pagosTotales.consultarPagosTotales();

            interesPorVencer = resumenCartera.interesPorVencer;
            capitalPorVencer = resumenCartera.capitalPorVencer;
            interesVencido = resumenCartera.interesVencido;
            capitalVencido = resumenCartera.capitalVencido;

            totalPorVencer = interesPorVencer + capitalPorVencer;
            totalVencido = interesVencido + capitalVencido;
            mora = Math.Round( (capitalVencido / (capitalPorVencer + capitalVencido))*100,2 );

            capital = pagosTotales.capital;
            interes = pagosTotales.interes;
            total = pagosTotales.total;
            actual = DateTime.Now;
            inicioMes = new DateTime(actual.Year, actual.Month, 1); 
        }

        public DashboardExtension getDashboard()
        {
            DashboardExtension dash = new DashboardExtension();
            return dash;
        }

    }

    public class ResumenCartera
    {
        public decimal interesPorVencer { get; set; }
        public decimal capitalPorVencer { get; set; }
        public decimal interesVencido { get; set; }
        public decimal capitalVencido { get; set; }

        
        public ResumenCartera consultarResumenCartera()
        {
            using (var a = new Models.emprendeEntities())
            {
                return a.Database.SqlQuery<ResumenCartera>("COB_SP_ConsultaResumenCartera").FirstOrDefault();
            }
        }
    }

    public class PagosTotales
    {
        public decimal capital { get; set; }
        public decimal interes { get; set; }
        public decimal total { get; set; }

        

        public PagosTotales consultarPagosTotales()
        {
            
            DateTime actual = DateTime.Now;
            DateTime inicioMes = new DateTime(actual.Year, actual.Month, 1);            
            using (var a = new Models.emprendeEntities())
            {
                
                return a.Database.SqlQuery<PagosTotales>("COB_SP_pagosTotales @desde, @hasta", new SqlParameter("desde", inicioMes), new SqlParameter("hasta", actual)).DefaultIfEmpty(new PagosTotales()).FirstOrDefault() ;

            }
        }
    }
}