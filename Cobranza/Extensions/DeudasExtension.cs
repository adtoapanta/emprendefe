﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cobranza.Models;
using System.Data.SqlClient;
namespace Cobranza.Extensions
{
    public class DeudasExtension
    {
        public List<COB_deudas> consultarDeudasDeudor(int idDeudor)
        {
            List<COB_deudas> deudas = new List<COB_deudas>();
            using (var a = new Models.emprendeEntities())
            {
                
                deudas = a.COB_deudas.Where(x => x.idDeudor == idDeudor).ToList();
                
            }
            return deudas;
        }

        public List<COB_deudas> consultarTodasDeudas()
        {
            List<COB_deudas> deudas = new List<COB_deudas>();
            using (var a = new Models.emprendeEntities())
            {

                deudas = a.COB_deudas.ToList();

            }
            return deudas;
        }

        public static void crearCorte(DateTime fecha)
        {

            using (var a = new Models.emprendeEntities())
            {
                a.Database.ExecuteSqlCommand("COB_SP_EjecutaCorte @fecha", new SqlParameter("fecha", fecha));
            }
        }

    }

    
}