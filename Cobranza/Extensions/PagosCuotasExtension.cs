﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cobranza.Models;
namespace Cobranza.Extensions
{
    public class PagosCuotasExtension
    {
        public decimal ingresarDistribucion(COB_cuotas cuota, int pagoId, decimal valor)
        {

            using (var a = new Models.emprendeEntities())
            {
                COB_pagosCuotas pagoCuota = new COB_pagosCuotas();
                COB_cuotas cuotaActualizar = a.COB_cuotas.Where(x => x.id == cuota.id).FirstOrDefault(); //trae objeto cuota que se esta pagando

                //extrae datos de cuota a pagar y pago utilizado
                decimal valorCuota = (decimal)cuota.valor;
                decimal valorInteres = (decimal)cuota.interes;
                decimal valorCapital = (decimal)cuota.capital;
                decimal valorInteresPagado = (decimal)cuota.pagoInteres;
                decimal valorCapitalPagado = (decimal)cuota.pagoCapital;

                valorCuota = Math.Round(valorCuota, 2);
                valorInteres = Math.Round(valorInteres, 2);
                valorCapital = Math.Round(valorCapital, 2);
                valorInteresPagado = Math.Round(valorInteresPagado, 2);
                valorCapitalPagado = Math.Round(valorCapitalPagado, 2);

                decimal valorPago = Math.Round(valor,2); //variable que tiene el valor disponible para hacer los pagos

                decimal aPagar = 0; //variable que tiene el valor a pagar 

                if (valorInteres > valorInteresPagado) // pagar interes
                {
                    aPagar = valorInteres - valorInteresPagado; // lo que tengo que pagar
                    if (aPagar > valorPago)
                    {
                        aPagar = valorPago;
                        valorPago = 0;
                        valorInteresPagado += aPagar;
                    }
                    else
                    {
                        valorPago -= aPagar;
                        valorInteresPagado += aPagar;
                    }
                    pagoCuota.interes = aPagar;
                    cuotaActualizar.pagoInteres = valorInteresPagado; //actualiza valor pagado de interes a la cuota
                }
                else
                {
                    pagoCuota.interes = 0;
                }
                if (valorCapital > valorCapitalPagado) // pagar capital
                {
                    aPagar = valorCapital - valorCapitalPagado; // lo que tengo que pagar
                    if (aPagar > valorPago)
                    {
                        aPagar = valorPago;
                        valorPago = 0;
                        valorCapitalPagado += aPagar;
                    }
                    else
                    {
                        valorPago -= aPagar;
                        valorCapitalPagado += aPagar;
                    }
                    pagoCuota.capital = aPagar;
                    cuotaActualizar.pagoCapital = valorCapitalPagado; //actualiza valor pagado de capita a la cuota
                }
                else
                {
                    pagoCuota.capital = 0;
                }

                //asigna valores de distribucion para guardar en tabla pagoCuota
                pagoCuota.idCuota = cuota.id;
                pagoCuota.idPago = pagoId;
                pagoCuota.valor = pagoCuota.interes + pagoCuota.capital;


                //asigna valores actualizados a la cuota gestionada
                cuotaActualizar.pagado = cuotaActualizar.pagoCapital + cuotaActualizar.pagoInteres;
                if (cuotaActualizar.pagado == cuotaActualizar.valor)//verifica que la cuota sea pagada en su totalidad
                    cuotaActualizar.esPagada = true;
                else
                    cuotaActualizar.esPagada = false;

                cuotaActualizar.fechaPago = DateTime.Today;

                //Agregar y Guardar cambios con entity framework
                a.COB_pagosCuotas.Add(pagoCuota); //agrega registro a tabla pagoCuotas
                a.SaveChanges(); // guarda los cambios 


                return valorPago; //saldo restante del valor del pago despues de haberse aplicado
            }


        }
        public decimal ingresarDistribucion(COB_cuotas cuota, int pagoId, decimal valor, DateTime fechaPago)
        {

            using (var a = new Models.emprendeEntities())
            {
                COB_pagosCuotas pagoCuota = new COB_pagosCuotas();
                COB_cuotas cuotaActualizar = a.COB_cuotas.Where(x => x.id == cuota.id).FirstOrDefault(); //trae objeto cuota que se esta pagando

                //extrae datos de cuota a pagar y pago utilizado
                decimal valorCuota = (decimal)cuota.valor;
                decimal valorInteres = (decimal)cuota.interes;
                decimal valorCapital = (decimal)cuota.capital;
                decimal valorInteresPagado = (decimal)cuota.pagoInteres;
                decimal valorCapitalPagado = (decimal)cuota.pagoCapital;

                valorCuota = Math.Round(valorCuota, 2);
                valorInteres = Math.Round(valorInteres, 2);
                valorCapital = Math.Round(valorCapital, 2);
                valorInteresPagado = Math.Round(valorInteresPagado, 2);
                valorCapitalPagado = Math.Round(valorCapitalPagado, 2);

                decimal valorPago = Math.Round(valor, 2); //variable que tiene el valor disponible para hacer los pagos

                decimal aPagar = 0; //variable que tiene el valor a pagar 

                if (valorInteres > valorInteresPagado) // pagar interes
                {
                    aPagar = valorInteres - valorInteresPagado; // lo que tengo que pagar
                    if (aPagar > valorPago)
                    {
                        aPagar = valorPago;
                        valorPago = 0;
                        valorInteresPagado += aPagar;
                    }
                    else
                    {
                        valorPago -= aPagar;
                        valorInteresPagado += aPagar;
                    }
                    pagoCuota.interes = aPagar;
                    cuotaActualizar.pagoInteres = valorInteresPagado; //actualiza valor pagado de interes a la cuota
                }
                else
                {
                    pagoCuota.interes = 0;
                }
                if (valorCapital > valorCapitalPagado) // pagar capital
                {
                    aPagar = valorCapital - valorCapitalPagado; // lo que tengo que pagar
                    if (aPagar > valorPago)
                    {
                        aPagar = valorPago;
                        valorPago = 0;
                        valorCapitalPagado += aPagar;
                    }
                    else
                    {
                        valorPago -= aPagar;
                        valorCapitalPagado += aPagar;
                    }
                    pagoCuota.capital = aPagar;
                    cuotaActualizar.pagoCapital = valorCapitalPagado; //actualiza valor pagado de capita a la cuota
                }
                else
                {
                    pagoCuota.capital = 0;
                }

                //asigna valores de distribucion para guardar en tabla pagoCuota
                pagoCuota.idCuota = cuota.id;
                pagoCuota.idPago = pagoId;
                pagoCuota.valor = pagoCuota.interes + pagoCuota.capital;


                //asigna valores actualizados a la cuota gestionada
                cuotaActualizar.pagado = cuotaActualizar.pagoCapital + cuotaActualizar.pagoInteres;
                if (cuotaActualizar.pagado == cuotaActualizar.valor)//verifica que la cuota sea pagada en su totalidad
                    cuotaActualizar.esPagada = true;
                else
                    cuotaActualizar.esPagada = false;

                cuotaActualizar.fechaPago = fechaPago;

                //Agregar y Guardar cambios con entity framework
                a.COB_pagosCuotas.Add(pagoCuota); //agrega registro a tabla pagoCuotas
                a.SaveChanges(); // guarda los cambios 


                return valorPago; //saldo restante del valor del pago despues de haberse aplicado
            }


        }

        public void ingresarDistribuciones(List<COB_cuotas> cuotas, int pagoId, decimal pagoValor)
        {

            decimal saldoRestante = pagoValor;
            int contador = 0;
            while (saldoRestante > 0)
            {
                if (contador < cuotas.Count)
                {
                    saldoRestante = ingresarDistribucion(cuotas[contador], pagoId, saldoRestante);
                    contador++;
                }
                else
                    return;
            }


        }
        public void ingresarDistribuciones(List<COB_cuotas> cuotas, int pagoId, decimal pagoValor, DateTime fechaPago)
        {

            decimal saldoRestante = pagoValor;
            int contador = 0;
            while (saldoRestante > 0)
            {
                if (contador < cuotas.Count)
                {
                    saldoRestante = ingresarDistribucion(cuotas[contador], pagoId, saldoRestante, fechaPago);
                    contador++;
                }
                else
                    return;
            }


        }
    }
}
