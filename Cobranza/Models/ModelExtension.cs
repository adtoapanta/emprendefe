﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cobranza.Models
{
    public class COB_VW_deudores {
        public int id { get; set; }
        public string nombre { get; set; }
        public string cedula { get; set; }
        public int idTipo { get; set; }
        public int idRef { get; set; }
        public COB_VW_deudores() { }
    }
}