﻿$(document).ready(function () { initDashboard(); });

function initDashboard() {
    console.log("init");

    //$('#btnGuardarPago').click(function (e) {
    //    console.log("boton clic");
    //    guardarPago();
    //});

    consultarTablaVencidos();
}

function consultarTablaVencidos() {
    console.log("Ingreso consultar tabla Vencidos");

    //var idDeu = $("#txtidDeudorAgregarPago").val();

    htclibjs.Ajax({
        url: "/Dashboard/consultarTablaVencidas",
        data: JSON.stringify({
            //idDeudor: idDeu
        }),
        success: function (r) {

            var totalVencido = 0;
            var trHTML =
                '<thead>' +
                '<tr>' +
                '<th>Deudor</th>' +
                //'<th>Descripcion</th>' +
                '<th>Días Vencidos</th>' +
                '<th>Valor Vencido</th>' +
                //fila vacia donde va el boton de editar
                //'<th></th>' + 
                '</tr > ' +
                '</thead> ' +

                '<tbody> '

                ;

            $.each(r.data, function (i, item) {

                var fecha = getDateFromJson(item.fecha);
                var url = SITEROOT + '/Pagos/Index/';
                trHTML +=

                    '<tr>' +
                    '<td id="tdnombreDeudorVencidos' + item.id + '"><a href="' + url + item.id + '">' + item.nombreDeudor + '</a></td>' +
                    //'<td id="tdDescripcionDeudaVencidos' + item.id + '">' + item.deuda + '</td>' +
                    '<td id="tdDiasVencidoVencidos' + item.id + '" style="text-align: right">' + item.maxVencido + '</td>' +
                    '<td id="tdValorVencidoVencidos' + item.id + '" style="text-align: right"> $ ' + addCommas(item.valorVencido.toFixed(2)) + '</td>' +
                    //boton editar de la tabla
                    //'<td id="tdIdVencidos' + item.id + '"> <button class="btnSeleccionarVencidos btn btn-primary" value="' + item.id + '" type="button" onclick=window.location.href="' + url + item.id + '"  >Ver Detalles</button> </td>' + 
                    '</tr> ';
                totalVencido += item.valorVencido;

            });

            trHTML += '</tbody>' +

                '<tfoot>' +
                '<tr>' +
                '<th>Total</th>' +
                '<th></th>' +
                '<th style="text-align: right" >$' + addCommas(totalVencido.toFixed(2)) + '</th>' +
                '</tr>' +
                '</tfoot> ';



            $('#tblVencidos').html('');
            $('#tblVencidos').append(trHTML);

            $('#tblVencidos').DataTable({


                language: spanishDataTable
            });


            //accion al hacer clic en boton de la tabla
            $('.btnSeleccionarDeuda').on('click', function (e) {

                selecionarDeuda($(this));
                //$('#modalEditarConvocatoria').modal('show'); 

            });
        }
    });

}

var spanishDataTable = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registros",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla",
    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}