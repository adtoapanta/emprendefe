﻿$(document).ready(function () { initDeudas(); });

function initDeudas() {
    console.log("init");

    //$('#btnGuardarPago').click(function (e) {
    //    console.log("boton clic");
    //    guardarPago();
    //});

    consultarTodasDeudas();

}

function exportTable() {

    var table = $('#tblTodasDeudas').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf'],
        language: spanishDataTable
    });

    table.buttons().container()
        .appendTo('#content-wrapper .col-md-6:eq(0)');

}

function consultarTodasDeudas() {
    console.log("Ingreso consultar todas las deudas");


    htclibjs.Ajax({
        url: "/Deudas/consultarTodasDeudas",
        data: JSON.stringify({

        }),
        success: function (r) {

            var totalVigente = 0;
            var totalVencido = 0;
            var trHTML =
                '<thead>' +
                '<tr>' +
                '<th>Deudor</th>' +
                '<th>Capital</th>' +
                '<th>Tipo Gestión</th>' +
                '<th>Vigente</th>' +
                '<th>Vencido</th>' +
                '<th>Total</th>' +
                //'<th></th>' + //fila vacia donde va el boton de editar
                '</tr > ' +
                '</thead> ' +

                '<tbody> '

                ;

            $.each(r.data, function (i, item) {

                var fecha = getDateFromJson(item.fecha);
                var url = SITEROOT + '/Pagos/Index/';
                trHTML +=

                    '<tr>' +
                    '<td id="tdNombreDeudorTodasDeudas' + item.idDeuda + '"><a href="' + url + item.idDeudor + '">' + item.nombreDeudor + '</a></td>' +
                '<td id="tdCapitalTodasDeudas' + item.idDeuda + '" style="text-align: right">$' + addCommas(item.capital.toFixed(2)) + '</td>' +
                    '<td id="tdTipoGestionTodasDeudas' + item.idDeuda + '">' + getTipoGestion(item.dias, item.castigado) + '</td>' +
                '<td id="tdVigenteTodasDeudas' + item.idDeuda + '" style="text-align: right">$' + addCommas(item.vigente.toFixed(2)) + '</td>' +
                '<td id="tdVencidoTodasDeudas' + item.idDeuda + '" style="text-align: right">$' + addCommas(item.vencido.toFixed(2)) + '</td>' +
                '<td id="tdTotalTodasDeudas' + item.iidDeuda + '" style="text-align: right">$' + addCommas((item.vigente + item.vencido).toFixed(2)) + '</td>' +
                    //'<td id="tdIdDeuda' + item.id + '"> <button class="btnSeleccionarDeuda btn btn-primary" value="' + item.id + '" type="button"   >Detalles</button> </td>' + //boton editar de la tabla
                    '</tr> ';
                totalVigente += item.vigente;
                totalVencido += item.vencido;

            });

            trHTML +=
                '</tbody>' +
                '<tfoot>' +
                '<tr>' +
                '<th>Total</th>' +
                '<th></th>' +
                '<th></th>' +
                '<th style="text-align: right" >$' + addCommas(totalVigente.toFixed(2)) + '</th>' +
                '<th style="text-align: right" >$' + addCommas(totalVencido.toFixed(2)) + '</th>' +
                '<th style="text-align: right" >$' + addCommas((totalVigente + totalVencido).toFixed(2)) + '</th>' +
                '</tr>' +
                '</tfoot> ';

            $('#tblTodasDeudas').html('');
            $('#tblTodasDeudas').append(trHTML);

            exportTable();


            //accion al hacer clic en boton de la tabla
            //$('.btnSeleccionarDeuda').on('click', function (e) {

            //    selecionarDeuda($(this));
            //    //$('#modalEditarConvocatoria').modal('show'); 

            //});
        }
    });

}

function getTipoGestion(maxDias, esCastigado) {
    var tipoGestion = '';
    console.log(esCastigado);
    if (esCastigado == true)
        tipoGestion = "Castigado"
    else {
        if (maxDias <= 30) {
            tipoGestion = 'Gest. Administrativa';
        }
        else if (maxDias <= 60) {
            tipoGestion = 'Gest. Operaciones';
        }
        else if (maxDias > 60) {
            tipoGestion = 'Gest. Judicial';
        }
    }
    return tipoGestion;
}

var spanishDataTable = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registros",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla",
    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}