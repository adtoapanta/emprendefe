﻿$(document).ready(function () { initCargarPagos(); });

function initCargarPagos() {
    console.log("init");

    $('#btnCargarPagos').click(function (e) {

        console.log('boton click');
        subirArchivo();

    });


}

function cargarPagos() {

    var upload = $('#uploadPagos').get(0);
    var files = upload.files;
    
    //var myID = 3; //uncomment this to make sure the ajax URL works
    if (files.length > 0) {
        if (window.FormData !== undefined) {
            var data = new FormData();
            for (var x = 0; x < files.length; x++) {
                data.append("file" + x, files[x]);
            }

            console.log(data);

            $.ajax({
                type: "POST",
                url: '/Pagos/getArchivosPagos',
                contentType: false,
                processData: false,
                data: data,
                success: function (result) {
                    console.log(result);
                } 

            });
        } else {
            alert("This browser doesn't support HTML5 file uploads!");
        }
    }
}

function subirArchivo() {
    //var divLoading = $('#divLoading');
    //var testArray = 'files' in listaArchivos;
    // absorbe los archivos del componente
    var btnCargarpagos = new BtnLoad("btnCargarPagos");
    var upload = $('#uploadPagos').get(0);
    var testArray = upload.files;

    console.log('before if');
    if (testArray) {

        console.log('After if');

        // Create a formdata object and add the files
        var data = new FormData();
        $.each(upload.files, function (key, value) {
            data.append(key, value);
        });
        

        //URL
        console.log("urlPath");
        var url = SITEROOT + '/Pagos/uploadExcel';
        console.log(url);

        btnCargarpagos.Start();

        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (r, textStatus, jqXHR) {
                console.log("success");
                console.log(r);
                alert(r.data);
                btnCargarpagos.Stop();
            },
            
            error: function (jqXHR, textStatus, errorThrown) {
                // Handle errors here
                alert('error al cargar archivo');
                btnCargarpagos.Stop();
            }
        });
    }
    else {
        console.log("archivo no seleccionado");
        alert("Se debe seleccionar al menos un archivo ");
        
    }
}