﻿$(document).ready(function () { initCrearCorte(); });

function initCrearCorte() {
    console.log("init");

    $('#btnCrearCorte').click(function (e) {
        crearCorte();        
    });

}

function crearCorte() {

    var fecha = $('#txtFechaCorte').val();

    var boton = new BtnLoad("btnCrearCorte");
    boton.Start();
    htclibjs.Ajax({
        url: "/Deudas/crearCorte",
        data: JSON.stringify({
            fecha: fecha
        }),
        success: function (r) {

            toastr.success('', 'Operación exitosa', { positionClass: "toast-bottom-right" });
            boton.Stop();
        },
        succesError: function (r) {

            boton.Stop();

        }        

    });

}