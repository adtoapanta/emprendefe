﻿$(document).ready(function () { initPagos(); });

function initPagos() {
    console.log("init");

    $('#btnGuardarPago').click(function (e) {
        console.log("boton clic");
        guardarPago();
    });

    consultarDeudasDeudor();

    consultarPagosDeudor();

    exportTable();

    $('#tblPagos').DataTable({

        language: spanishDataTable
    });

}


function guardarPago() {
    console.log("Ingreso a la funcion Agregar Pago");
    var idDeu = $("#txtidDeudorAgregarPago").val();
    var valorPago = $("#txtValorAgregarPago").val();
    var confirmado; //= $("#slcTipoPagoAgregarPago").val();
    var tipo = $("#slcTipoPagoAgregarPago").val();
    var documentoPago = $("#txtDocumentoAgregarPago").val();

    if (valorPago == '') {
        alert('Ingrese valor del pago');
    }
    else if (tipo == '') {
        alert('Seleccione un tipo de pago');
    }
    else if (tipo == 1 || tipo == 2) {
        confirmado = true;
    }
    else {
        confirmado = false;
    }

    htclibjs.Ajax({
        url: "/Pagos/ingresarPago",
        data: JSON.stringify({
            idDeudor: idDeu, valor: valorPago, esConfirmado: confirmado, tipoPago: tipo, documento: documentoPago
        }),
        success: function (r) {

            alert("Pago Guardado");
            consultarDeudasDeudor();
            consultarPagosDeudor();
            $('#tblCuotas').html(''); // limpia tabla de cuotas 

        }
    });

}

function consultarDeudasDeudor() {
    console.log("Ingreso consultar deudas deudor");
    var idDeu = $("#txtidDeudorAgregarPago").val();
    htclibjs.Ajax({
        url: "/Deudas/consultarDeudasDeudor",
        data: JSON.stringify({
            idDeudor: idDeu
        }),
        success: function (r) {


            var trHTML =
                '<thead>' +
                '<tr>' +
                '<th>Fecha</th>' +
                '<th>Capital</th>' +
                '<th>Concepto</th>' +
                '<th>Vencido</th>' +
                '<th>Por Vencer</th>' +
                '<th>Total</th>' +
                '<th>Tipo Gestion</th>' +
                '<th></th>' + //fila vacia donde va el boton de editar
                '</tr > ' +
                '</thead> ' +

                '<tbody> '

                ;

            $.each(r.data, function (i, item) {

                var fecha = getDateFromJson(item.fecha);
                var vencido = item.interesVencido + item.capitalVencido;
                var porVencer = item.interesPorVencer + item.capitalPorVencer;
                var total = vencido + porVencer;
                trHTML +=

                    '<tr>' +
                    '<td id="tdFechaDeuda' + item.id + '">' + fecha + '</td>' +
                    '<td id="tdCapitalDeuda' + item.id + '" style="text-align: right">$' + addCommas(item.capital.toFixed(2)) + '</td>' +
                    '<td id="tdNombreDeuda' + item.id + '">' + item.nombre + '</td>' +
                    '<td id="tdVencidoDeuda' + item.id + '" style="text-align: right">$' + addCommas(vencido.toFixed(2)) + '</td>' +
                    '<td id="tdPorVencerDeuda' + item.id + '" style="text-align: right">$' + addCommas(porVencer.toFixed(2)) + '</td>' +
                    '<td id="tdTotalDeuda' + item.id + '" style="text-align: right">$' + addCommas(total.toFixed(2)) + '</td>' +
                    '<td id="tdTipoGestionDeuda' + item.id + '" style="text-align: right">' + getTipoGestion(item.maxVencido, item.esCastigado) + '</td>' +
                    '<td id="tdIdDeuda' + item.id + '"> <button class="btnSeleccionarDeuda btn btn-primary" value="' + item.id + '" type="button"   >Detalles</button> </td>' + //boton editar de la tabla
                    '</tr> ';

            });

            trHTML += '</tbody>'

            $('#tblDeudas').html('');
            $('#tblDeudas').append(trHTML);

            //$('#tblDeudas').DataTable();


            //accion al hacer clic en boton de la tabla
            $('.btnSeleccionarDeuda').on('click', function (e) {

                selecionarDeuda($(this));
                //$('#modalEditarConvocatoria').modal('show'); 

            });
        }
    });

}

function selecionarDeuda(boton) {

    console.log("Intento de seleccionar convocatoria");
    var idDeuda = $(boton).val();

    consultarCuotasDeuda(idDeuda);

}

function exportTable() {

    var table = $('#tblCuotas').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf'],
        language: spanishDataTable
    });

    table.buttons().container()
        .appendTo('#content-wrapper .col-md-6:eq(0)');

}

function consultarCuotasDeuda(idDeuda) {
    console.log("Ingreso consultar cuotas deuda");

    htclibjs.Ajax({
        url: "/Deudas/consultarEstadoCuenta",
        data: JSON.stringify({
            idDeuda: idDeuda
        }),
        success: function (r) {


            var trHTML =
                '<thead>' +
                '<tr>' +
                //'<th></th>' + //fila vacia donde va el boton de editar
                '<th>#</th>' +
                '<th>Fecha</th>' +
                '<th>Capital</th>' +
                '<th>Aporte</th>' +
                '<th>Total</th>' +
                '<th>Estado</th>' +
                '<th>F. Pago</th>' +
                '<th>Pagado</th>' +
                '<th>Pagado Capital</th>' +
                '<th>Pagado Aporte</th>' +
                '<th>Saldo Capital</th>' +
                '<th>Saldo Aporte</th>' +
                '</tr > ' +
                '</thead> ' +

                '<tbody> '

                ;

            $.each(r.data, function (i, item) {
                var saldoCapital = item.capital - item.pagoCapital;
                var saldoInteres = item.interes - item.pagoInteres;
                var fechaVence = getDateFromJson(item.fechaVence);
                var fechaPago = getDateFromJson(item.fechaPago);
                var vencido;

                if (item.esPagada == 1)
                    vencido = 'Cancelada';
                else {
                    if (new Date(fechaVence).getTime() < $.now()) {
                        vencido = 'Vencido';
                    }
                    else
                        vencido = 'Vigente';
                }

                trHTML +=

                    '<tr>' +
                    //'<td id="tdIdCuota' + item.id + '"> <button class="btnSeleccionarCuota" value="' + item.id + '" type="button" data-toggle="modal" data-target="#modalEditarConvocatoria"  >Editar</button> </td>' + //boton editar de la tabla
                    '<td id="tdNumeroCuota' + item.id + '">' + item.numeroCuota + '</td>' +
                    '<td id="tdFechaVenceCuota' + item.id + '">' + fechaVence + '</td>' +
                    '<td id="tdCapitalCuota' + item.id + '" style="text-align: right">$' + addCommas(item.capital.toFixed(2)) + '</td>' +
                    '<td id="tdInteresCuota' + item.id + '" style="text-align: right">$' + addCommas(item.interes.toFixed(2)) + '</td>' +
                    '<td id="tdValorCuota' + item.id + '" style="text-align: right">$' + addCommas(item.valor.toFixed(2)) + '</td>' +
                    '<td id="tdEstadoCuota' + item.id + '">' + vencido + '</td>' +
                    '<td id="tdFechaPagoCuota' + item.id + '">' + fechaPago + '</td>' +
                    '<td id="tdPagadoCuota' + item.id + '" style="text-align: right">$' + addCommas(item.pagado.toFixed(2)) + '</td>' +
                    '<td id="tdPagadoCapitalCuota' + item.id + '" style="text-align: right">$' + addCommas(item.pagoCapital.toFixed(2)) + '</td>' +
                    '<td id="tdPagadoInteresCuota' + item.id + '" style="text-align: right">$' + addCommas(item.pagoInteres.toFixed(2)) + '</td>' +
                    '<td id="tdSaldoCapitalCuota' + item.id + '" style="text-align: right">$' + addCommas(saldoCapital.toFixed(2)) + '</td>' +
                    '<td id="tdSaldoInteresCuota' + item.id + '" style="text-align: right">$' + addCommas(saldoInteres.toFixed(2)) + '</td>' +
                    '</tr> ';

            });

            trHTML += '</tbody>'

            $('#tblCuotas').html('');
            $('#tblCuotas').append(trHTML);



        }
    });

}

function consultarPagosDeudor() {
    console.log("Ingreso consultar deudas deudor");
    var idDeu = $("#txtidDeudorAgregarPago").val();
    htclibjs.Ajax({
        url: "/Pagos/consultarPagosDeudor",
        data: JSON.stringify({
            idDeudor: idDeu
        }),
        success: function (r) {


            var trHTML =
                '<thead>' +
                '<tr>' +
                //'<th></th>' + //fila vacia donde va el boton de editar
                '<th>Fecha</th>' +
                '<th>Valor</th>' +
                '<th>Tipo</th>' +
                '<th>Documento</th>' +
                '</tr > ' +
                '</thead> ' +

                '<tbody> '

                ;

            $.each(r.data, function (i, item) {

                var fechaPago = getDateFromJson(item.fecha);


                trHTML +=

                    '<tr>' +
                    //'<td id="tdIdPago' + item.id + '"> <button class="btnSeleccionarPago btn btn-primary" value="' + item.id + '" type="button">Detalles</button> </td>' + //boton editar de la tabla
                    '<td id="tdFechaPago' + item.id + '">' + fechaPago + '</td>' +
                    '<td id="tdValorPago' + item.id + '" style="text-align: right">$' + addCommas(item.valor.toFixed(2)) + '</td>' +
                    '<td id="tdTipoPago' + item.id + '">' + getTipoPago(item.idTipoPago) + '</td>' +
                    '<td id="tdDocumentoPago' + item.id + '">' + item.documento + '</td>' +
                    '</tr> ';

            });

            trHTML += '</tbody>'

            $('#tblPagos').html('');
            $('#tblPagos').append(trHTML);




        }
    });

}

function getTipoPago(idTipo) {
    var tipoPago = '';
    if (idTipo == 1) {
        tipoPago = 'Efectivo';
    }
    else if (idTipo == 2) {
        tipoPago = 'Transferencia';
    }
    else if (idTipo == 3) {
        tipoPago = 'Cheque';
    }
    return tipoPago;
}

function getTipoGestion(maxDias, esCastigado) {
    var tipoGestion = '';
    console.log(esCastigado);
    if (esCastigado == true)
        tipoGestion = "Castigado"
    else {
        if (maxDias <= 30) {
            tipoGestion = 'Gest. Administrativa';
        }
        else if (maxDias <= 60) {
            tipoGestion = 'Gest. Operaciones';
        }
        else if (maxDias > 60) {
            tipoGestion = 'Gest. Judicial';
        }
    }
    return tipoGestion;
}

var spanishDataTable = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registros",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla",
    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}