﻿$(document).ready(function () { initVerPagos(); });

function initVerPagos() {
    console.log("init");

    $('#btnConsultarPagos').click(function (e) {
        console.log("boton clic");
        consultarTodosLosPagosParametros();
    });

    consultarTodosLosPagos();


}

function exportTable() {

    var table = $('#tblTodosPagos').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf'],
        language: spanishDataTable
    });

    table.buttons().container()
        .appendTo('#content-wrapper .col-md-6:eq(0)');

}

function consultarTodosLosPagos() {
    console.log("Ingreso consultar deudas deudor");


    htclibjs.Ajax({
        url: "/Pagos/consultarTodosPagos",
        data: JSON.stringify({

        }),
        success: function (r) {

            var totalCapital = 0;
            var totaAporte = 0;
            var total = 0;

            var trHTML =
                '<thead>' +
                '<tr>' +
                '<th>Deudor</th>' +
                '<th>Fecha</th>' +
                '<th>Capital</th>' +
                '<th>Aporte</th>' +
                '<th>Total</th>' +
                //'<th></th>' + //fila vacia donde va el boton de editar
                '</tr > ' +
                '</thead> ' +

                '<tbody> '

                ;

            $.each(r.data, function (i, item) {

                var fecha = getDateFromJson(item.fecha);

                trHTML +=

                    '<tr>' +
                    '<td id="tdDeudorTodosPagos' + item.id + '">' + item.Deudor + '</td>' +
                    '<td id="tdFechaTodosPagos' + item.id + '" style="text-align: right">' + fecha + '</td>' +
                    '<td id="tdCapitalTodosPagos' + item.id + '" style="text-align: right"> $' + addCommas(item.capital.toFixed(2)) + '</td>' +
                    '<td id="tdInteresTodosPagos' + item.id + '" style="text-align: right">$' + addCommas(item.interes.toFixed(2)) + '</td>' +
                    '<td id="tdTotalTodosPagos' + item.id + '" style="text-align: right"> $' + addCommas(item.total.toFixed(2)) + '</td>' +
                    //'<td id="tdIdDeuda' + item.id + '"> <button class="btnSeleccionarDeuda btn btn-primary" value="' + item.id + '" type="button"   >Detalles</button> </td>' + //boton editar de la tabla
                    '</tr> ';
                totaAporte += item.interes;
                totalCapital += item.capital;
                total += item.total;

            });

            trHTML +=
                '</tbody>' +
                '<tfoot>' +
                '<tr>' +
                '<th>Total</th>' +
                '<th></th>' +
                '<th style="text-align: right" >$' + addCommas(totalCapital.toFixed(2)) + '</th>' +
                '<th style="text-align: right" >$' + addCommas(totaAporte.toFixed(2)) + '</th>' +
                '<th style="text-align: right" >$' + addCommas(total.toFixed(2)) + '</th>' +
                '</tr>' +
                '</tfoot> ';

            $('#tblTodosPagos').html('');
            $('#tblTodosPagos').append(trHTML);

            exportTable();

            //$('#tblTodosPagos').DataTable();


            //accion al hacer clic en boton de la tabla
            //$('.btnSeleccionarDeuda').on('click', function (e) {

            //    selecionarDeuda($(this));
            //    //$('#modalEditarConvocatoria').modal('show'); 

            //});
        }
    });

}



function consultarTodosLosPagosParametros() {
    console.log("Ingreso consultar todos los pagos");

    var desd = $('#txtDesdeTodosPagos').val();
    var hast = $('#txtHastaTodosPagos').val();

    htclibjs.Ajax({
        url: "/Pagos/consultarTodosPagosParametros",
        data: JSON.stringify({
            desde: desd, hasta: hast
        }),
        success: function (r) {

            var totalCapital = 0;
            var totaAporte = 0;
            var total = 0;

            var trHTML =
                '<thead>' +
                '<tr>' +
                '<th>Deudor</th>' +
                '<th>Fecha</th>' +
                '<th>Capital</th>' +
                '<th>Aporte</th>' +
                '<th>Total</th>' +
                //'<th></th>' + //fila vacia donde va el boton de editar
                '</tr > ' +
                '</thead> ' +

                '<tbody> '

                ;

            $.each(r.data, function (i, item) {

                var fecha = getDateFromJson(item.fecha);

                trHTML +=

                    '<tr>' +
                    '<td id="tdDeudorTodosPagos' + item.id + '">' + item.Deudor + '</td>' +
                    '<td id="tdFechaTodosPagos' + item.id + '" style="text-align: right">' + fecha + '</td>' +
                    '<td id="tdCapitalTodosPagos' + item.id + '" style="text-align: right">$' + addCommas(item.capital.toFixed(2)) + '</td>' +
                    '<td id="tdInteresTodosPagos' + item.id + '" style="text-align: right">$' + addCommas(item.interes.toFixed(2)) + '</td>' +
                    '<td id="tdTotalTodosPagos' + item.id + '" style="text-align: right">$' + addCommas(item.total.toFixed(2)) + '</td>' +
                    //'<td id="tdIdDeuda' + item.id + '"> <button class="btnSeleccionarDeuda btn btn-primary" value="' + item.id + '" type="button"   >Detalles</button> </td>' + //boton editar de la tabla
                    '</tr> ';
                totaAporte += item.interes;
                totalCapital += item.capital;
                total += item.total;

            });

            trHTML +=
                '</tbody>' +
                '<tfoot>' +
                '<tr>' +
                '<th>Total</th>' +
                '<th></th>' +
                '<th style="text-align: right" >' + addCommas(totalCapital.toFixed(2)) + '</th>' +
                '<th style="text-align: right" >' + addCommas(totaAporte.toFixed(2)) + '</th>' +
                '<th style="text-align: right" >' + addCommas(total.toFixed(2)) + '</th>' +
                '</tr>' +
                '</tfoot> ';

            $('#tblTodosPagos').html('');
            $('#tblTodosPagos').append(trHTML);

            $('#txtDescripcion').text('Período: ' + desd + ' - ' + hast);

            //exportTable();


            //accion al hacer clic en boton de la tabla
            //$('.btnSeleccionarDeuda').on('click', function (e) {

            //    selecionarDeuda($(this));
            //    //$('#modalEditarConvocatoria').modal('show'); 

            //});
        }
    });

}

var spanishDataTable = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registros",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla",
    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}
