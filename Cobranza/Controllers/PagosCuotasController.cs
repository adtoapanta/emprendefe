﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranza.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;
namespace Cobranza.Controllers
{
    public class PagosCuotasController : Controller
    {
        // GET: PagosCuotas
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult agregarDistribucion()
        {
            CuotasExtension cuota = new CuotasExtension();

            Cobranza.Models.COB_pagos pago = new Cobranza.Models.COB_pagos();
            pago.id = 45;
            pago.idDeudor = 1;
            pago.valor = 150;
            Resultado resultado;
            try
            {
                PagosCuotasExtension pagoCuota = new PagosCuotasExtension();
                
                pagoCuota.ingresarDistribuciones(cuota.consultarCuotasNoPagadasDeuda(1),45,200);
                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Logger.Write("Error en consultar cuotas vencidas" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return null;
        }
    }
}