﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranza.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;
namespace Cobranza.Controllers
{
    public class DeudasController : Controller
    {
        // GET: Deudas
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Corte()
        {
            return View();
        }

        public JsonResult consultarDeudasDeudor(int idDeudor)
        {
            Resultado resultado;
            try
            {
                DeudasExtension deudas = new DeudasExtension();
                
                resultado = new Resultado(true, deudas.consultarDeudasDeudor(idDeudor));
            }
            catch (Exception e)
            {
                Logger.Write("Error en consultar deudas deudor" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado);
        }

        public JsonResult consultarEstadoCuenta(int idDeuda)
        {
            Resultado resultado;
            try
            {
                CuotasExtension estadoCuenta = new CuotasExtension();
                resultado = new Resultado(true, estadoCuenta.consultarEstadoCuenta(idDeuda));
            }
            catch (Exception e)
            {
                Logger.Write("Error en consultar estado de cuenta" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado);
        }

        public JsonResult consultarTodasDeudas()
        {
            Resultado resultado;
            try
            {
                TablaVencidosExtension deudas = new TablaVencidosExtension();
                resultado = new Resultado(true, deudas.consultarTodasDeudas());
            }
            catch (Exception e)
            {
                Logger.Write("Error en consultar todas las deudas" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado,JsonRequestBehavior.AllowGet);
        }

        public JsonResult crearCorte(DateTime fecha)
        {
            Resultado resultado;
            try
            {
                DeudasExtension.crearCorte(fecha);                
                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Logger.Write("Error en Crear corte" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}