﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranza.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;
namespace Cobranza.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Dashboard
        public ActionResult Index()
        {
            DashboardExtension dashboard = new DashboardExtension();
            
            //variables de resumen cartera
            ViewBag.capitalPorVencer = "$ " + Convert.ToDecimal(dashboard.capitalPorVencer).ToString("N");
            ViewBag.interesPorVencer = "$ " + Convert.ToDecimal(dashboard.interesPorVencer).ToString("N");
            ViewBag.totalPorVencer = "$ " + Convert.ToDecimal(dashboard.totalPorVencer).ToString("N");
            ViewBag.capitalVencido = "$ " + Convert.ToDecimal(dashboard.capitalVencido).ToString("N");
            ViewBag.interesVencido = "$ " + Convert.ToDecimal(dashboard.interesVencido).ToString("N");
            ViewBag.totalVencido = "$ " + Convert.ToDecimal(dashboard.totalVencido).ToString("N");
            ViewBag.mora =  dashboard.mora + " %";

            //variables de pagos del mes
            ViewBag.capital = "$ " + Convert.ToDecimal(dashboard.capital).ToString("N");
            ViewBag.interes = "$ " + Convert.ToDecimal(dashboard.interes).ToString("N");
            ViewBag.total = "$ " + Convert.ToDecimal(dashboard.total).ToString("N");
            ViewBag.descripcion = "Período: " + dashboard.inicioMes.Day.ToString() + " - " + dashboard.actual.Day.ToString() + " de " + dashboard.actual.ToString("MMMM");
            ViewBag.inicioMes = dashboard.inicioMes;
            ViewBag.actual = dashboard.actual;

            return View();
        }

        public JsonResult consultarTablaVencidas()
        {
            Resultado resultado;
            try
            {

                TablaVencidosExtension tablaVencidos = new TablaVencidosExtension();
                resultado = new Resultado(true, tablaVencidos.consultarTablaVencidos());
            }
            catch (Exception e)
            {
                Logger.Write("Error en consultar tabla vencidos" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado,JsonRequestBehavior.AllowGet);
        }

        public JsonResult consultarResumenCartera()
        {
            Resultado resultado;
            try
            {
                              
                resultado = new Resultado(true, new DashboardExtension());
            }
            catch (Exception e)
            {
                Logger.Write("Error en consultar resumen cartera" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        
    }
}