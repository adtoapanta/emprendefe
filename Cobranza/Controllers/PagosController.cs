﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Cobranza.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Cobranza.Models;

namespace Cobranza.Controllers
{
    public class PagosController : Controller
    {
        // GET: Pagos
        public ActionResult Index()
        {
            int idDeudor;
            ViewBag.id = Url.RequestContext.RouteData.Values["id"];
            idDeudor = Convert.ToInt32(ViewBag.id);
            DeudorExtension deudor = new DeudorExtension();
            ViewBag.nombreDeudor = deudor.consultarDeudor(idDeudor).nombre;

            return View();

        }

        public ActionResult Deudor()
        {
            int idTipo, idReferencia;
            COB_VW_deudores elDeudor;

            idTipo = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"]);
            idReferencia = Convert.ToInt32(Url.RequestContext.RouteData.Values["id2"]);

            DeudorExtension deudor = new DeudorExtension();
            elDeudor = deudor.consultarDeudor(idTipo, idReferencia);
            
            ViewBag.id = elDeudor.id;                        
            ViewBag.nombreDeudor = elDeudor.nombre;

            return View("Index");

        }

        public ActionResult PagosTotales()
        {
            DateTime actual = DateTime.Now;
            DateTime inicioMes = new DateTime(actual.Year, actual.Month, 1);
            ViewBag.descripcion = "Período: " + inicioMes.Day.ToString() + " - " + actual.Day.ToString() + " de " + actual.ToString("MMMM");
            return View();

        }

        public ActionResult CargarPagos()
        {


            return View();

        }
        public JsonResult ingresarPago(int idDeudor, decimal valor, bool esConfirmado, int tipoPago, string documento)
        {
            Resultado resultado;
            try
            {
                PagosExtension pagosE = new PagosExtension();
                pagosE.ingresarPago(idDeudor, valor, esConfirmado, tipoPago, documento);

                resultado = new Resultado(true, true);
            }
            catch (Exception e)
            {
                Logger.Write("Error en ingresar pagos" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado);
        }

        public JsonResult consultarPagosDeudor(int idDeudor)
        {
            Resultado resultado;
            try
            {
                PagosExtension pagosE = new PagosExtension();
                resultado = new Resultado(true, pagosE.consultarPagosDeudor(idDeudor));
            }
            catch (Exception e)
            {
                Logger.Write("Error en consultar pagos deudor" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado);
        }
        public JsonResult consultarDeudor(int idDeudor)
        {
            Resultado resultado;
            try
            {
                DeudorExtension deudorE = new DeudorExtension();
                resultado = new Resultado(true, deudorE.consultarDeudor(idDeudor));
            }
            catch (Exception e)
            {
                Logger.Write("Error en consultar deudor" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado);
        }

        public JsonResult consultarTodosPagos()
        {
            Resultado resultado;
            try
            {
                TodosPagosExtension todosPagos = new TodosPagosExtension();
                resultado = new Resultado(true, todosPagos.consultarTodosPagos());
            }
            catch (Exception e)
            {
                Logger.Write("Error en consultar Todos los pagos" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado);
        }

        public JsonResult consultarTodosPagosParametros(DateTime desde, DateTime hasta)
        {
            Resultado resultado;
            try
            {
                TodosPagosExtension todosPagos = new TodosPagosExtension();
                resultado = new Resultado(true, todosPagos.consultarTodosPagos(desde, hasta));
            }
            catch (Exception e)
            {
                Logger.Write("Error en consultar Todos los pagos" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado);
        }              

        //[HttpPost]
        public JsonResult uploadExcel()
        {
            Resultado resultado;
            string filePath = AppDomain.CurrentDomain.BaseDirectory + @"Upload\pagos.xlsx";

            //try
            //{
                PagosExtension pago = new PagosExtension();
                HttpPostedFile miArchivo;
                HttpContext context = System.Web.HttpContext.Current;
                System.Collections.Specialized.NameValueCollection form = context.Request.Form;

                if (context.Request.Files.Count > 0)
                {
                    miArchivo = context.Request.Files[0];

                    if ((miArchivo != null) && (miArchivo.ContentLength > 0) && !string.IsNullOrEmpty(miArchivo.FileName))
                    {

                        miArchivo.SaveAs(filePath);
                        pago.ingresarPagosFromFile(filePath);
                        resultado = new Resultado(true, "Pagos cargados con correctamente");
                    }
                    else
                    {
                        throw new System.ArgumentException("Error al cargar pagos", "error");
                    }
                }
                else
                {
                    throw new System.ArgumentException("Error al cargar pagos", "error");
                }
                
            //}
            //catch (Exception e)
            //{
                //Logger.Write("Error en Cargar pagos desde archivo" + e.Message);
                //resultado = new Resultado(false, e.Message);
            //}
            return Json(resultado);

        }

    }
}