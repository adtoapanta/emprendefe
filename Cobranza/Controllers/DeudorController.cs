﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranza.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace Cobranza.Controllers
{
    public class DeudorController : Controller
    {
        // GET: Deudor
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult consultarDeudor(int idDeudor)
        {
            Resultado resultado;
            try
            {
                DeudorExtension deudorE = new DeudorExtension();
                resultado = new Resultado(true, deudorE.consultarDeudor(idDeudor));
            }
            catch (Exception e)
            {
                Logger.Write("Error en consultar deudor" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado);
        }
    }
}