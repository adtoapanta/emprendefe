﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranza.Extensions;
using Microsoft.Practices.EnterpriseLibrary.Logging;
namespace Cobranza.Controllers
{
    public class CuotasController : Controller
    {
        // GET: Cuotas
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult getCuotasVencidas(int idDeuda)
        {
            Resultado resultado;
            try
            {
                CuotasExtension cuotas = new CuotasExtension();

                resultado = new Resultado(true, cuotas.consultarCuotasVencidas(idDeuda));
            }
            catch (Exception e)
            {
                Logger.Write("Error en consultar cuotas vencidas" + e.Message);
                resultado = new Resultado(false, e.Message);
            }
            return Json(resultado);
        }

        public JsonResult consultarCuotasParaPagar()
        {
            List<Cobranza.Models.COB_cuotas> cuotas = new List<Models.COB_cuotas>();
            CuotasExtension cuota = new CuotasExtension();
            cuotas = cuota.consultarCuotasNoPagadasDeudor(1);

            return null;
        }
    }
}