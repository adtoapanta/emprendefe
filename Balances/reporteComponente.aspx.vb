﻿Public Partial Class reporteComponente
    Inherits System.Web.UI.Page
    Public idModelo As Integer
    Public idSegmento As Integer
    Public dtCampos As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.idModelo = Me.Request("idModelo")
        Me.idSegmento = Me.Request("idSeg")
        Try
            'htcLib.espacio.inicializa(SacLib.GetConexion, htcLib.tiposConexion.Web)

            Me.cargaDatosModelo()
            Me.cargaComponentes()
        Catch ex As Exception
            Me.Response.Write("Error : " & ex.Message)
        Finally
            htcLib.espacio.cerrar()
        End Try
    End Sub
    Private Sub cargaDatosModelo()
        Dim miModBal As modeloBalance
        miModBal = New modeloBalance(Me.idModelo)

        With miModBal
            Me.lblModelo.Text = .nombre
            Me.lblDescripcion.Text = .descripcion
            Me.dtCampos = .getCamposXModelo
        End With
        Me.cargaVariables()
    End Sub
    Private Sub cargaComponentes()
        Dim sql As New System.Text.StringBuilder
        Dim dt As DataTable

        sql.Append("select sac_componentes.nombre,sac_componentes.id ")
        sql.Append("from sac_componentes,sac_componentesXsegmento ")
        sql.AppendFormat("where idModelo={0} and idComponente in(select id from sac_componentes where idModulo=1 )", Me.idModelo)
        sql.AppendFormat("and idSegmento={0} and sac_componentesXsegmento.idComponente=sac_componentes.id", Me.idSegmento)

        dt = htcLib.espacio.ManejadorBD.traetabla(sql.ToString)

        For Each dr As DataRow In dt.Rows
            Me.cargarangosComponentes(dr("id"), dr("nombre"))
        Next


    End Sub
    Private Sub cargaVariables()
        Me.dtlCampos.DataSource = dtCampos
        Me.dtlCampos.DataBind()
    End Sub
    Private Sub cargarangosComponentes(ByVal idComponente As Long, ByVal nombre As String)
        Dim dt As DataTable

        Select Case idComponente

            Case Balance.Componente.Apalancamiento
                dt = rangoApalancamiento.getRangosScore(Me.idModelo)
            Case Balance.Componente.CapacidadPago
                dt = rangoCapacidad.getRangosScore(Me.idModelo)
                Me.tdCapacidad.Visible = True
                Me.grdCapacidad.DataSource = dt
                Me.grdCapacidad.DataBind()
            Case Balance.Componente.NivelEndeudamiento
                dt = rangoNivelEndeudamiento.getRangosScore(Me.idModelo)
                Me.tdEndeudamiento.Visible = True
                Me.grdEndeudamiento.DataSource = dt
                Me.grdEndeudamiento.DataBind()
            Case Balance.Componente.ventas
                dt = rangoVenta.getRangosScore(Me.idModelo)
                Me.tdVentas.Visible = True
                Me.grdVentas.DataSource = dt
                Me.grdVentas.DataBind()

        End Select

    End Sub
    

End Class