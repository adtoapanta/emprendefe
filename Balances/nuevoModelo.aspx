﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MPModulo.Master" CodeBehind="nuevoModelo.aspx.vb" Inherits="Balances.nuevoModelo" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <table cellpadding="0" cellspacing="0" width="100%" >
        <tr>
            <td class="esp">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="esp">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="titulo "  >
                            Configuración balances</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="bordes"  >
                            <table cellpadding="0" cellspacing="0" width ="100%">
                                <tr>
                                    <td class="grdTitulo">
                                        &nbsp;</td>
                                    <td class="grdTitulo">
                                        Nuevo modelo Balance</td>
                                    <td class="grdTitulo">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="esp">
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td class="esp">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td class="subtitulo">
                                        <table cellpadding="0" cellspacing="0" Width="100%">
                                            <tr>
                                                <td>
                                                    Nombre:</td>
                                                <td>
                                        <asp:TextBox ID="txtNombre" runat="server" Width="206px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Dias Validez:</td>
                                                <td>
                                                    <asp:TextBox ID="txtDiasValidez" runat="server" 
                                                        onkeypress="return controlCaracteresNum(event);" Width="65px" ></asp:TextBox>
                                                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                        ControlToValidate="txtDiasValidez" ErrorMessage="Ingrese un valor"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="width:100px" valign="top"  >
                                                    Descripción:</td>
                                                <td>
                                        <asp:TextBox ID="txtDescripcion" runat="server" Width="400px" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                        <asp:Button ID="btnContinuar" runat="server" Text="Continuar" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="esp">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="esp">
                &nbsp;</td>
        </tr>
    </table>
    <script type ="text/javascript" >
        function controlCaracteresNum(e) {
            var key = window.event ? e.keyCode : e.which;

            if ((key < 58) && (key > 47)) {
                var keychar = String.fromCharCode(key);
                return keychar;
            }
            return false;
        }
    
    </script>
</asp:Content>
