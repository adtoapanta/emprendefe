﻿'Public Partial Class CreaBalance
'    Inherits System.Web.UI.Page
'    Dim idModelo As Integer

'    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


'        'verifico si es usuario ,sino se va a logingeneral de sacfinca
'        ' If vUsuarios.web.TraeUsuarioRegistrado.nivel = 0 Then SacLib.GoInicio()


'        Try
'            htcLib.espacio.inicializa(SacLib.GetConexion, htcLib.tiposConexion.Web)
'            Me.idModelo = Me.Request("idModelo")
'            If Not Me.IsPostBack Then refresca()

'        Catch ex As Exception
'            Me.Response.Write("Error : " & ex.Message)
'        Finally
'            htcLib.espacio.cerrar()
'        End Try

'    End Sub

'    Sub refresca()
'        cargaDetallesTipo()
'        creaArbol()
'        llenaArbol()
'        cargaCombo()
'        Me.lnkConfRangos.NavigateUrl = String.Format("configuracionRangosBalance.aspx?idModelo={0}", Me.idModelo)
'        Me.lnkConfiguraPatrimonio.NavigateUrl = String.Format("configuraRangosPatrimonio.aspx?idModelo={0}", Me.idModelo)
'        Me.lnkRangosNicelEndeudamiento.NavigateUrl = String.Format("configuraRangosNivelEndeudamiento.aspx?idModelo={0}", Me.idModelo)
'        Me.lnkRangosApalancamiento.NavigateUrl = String.Format("configuraRangosApalancamiento.aspx?idModelo={0}", Me.idModelo)
'        Me.lnkConfigVentas.NavigateUrl = String.Format("configuraRangosVenta.aspx?idModelo={0}", Me.idModelo)
'    End Sub

'    Private Sub cargaCombo()
'        SacLib.CargarWebControlIdNombre(Me.ddlTipo, "BAL_CamposClasificacion", "", "-Seleccione una opción-")
'    End Sub

'    Sub cargaDetallesTipo()
'        If Me.idModelo = 0 Then
'            'es nuevo
'            Me.lblFechaCreacion.Text = Format(Today, "d")
'        Else
'            Dim miTipoBal As New modeloBalance(Me.idModelo)
'            Me.lblNombre.Text = miTipoBal.nombre
'            Me.lblFechaCreacion.Text = miTipoBal.fechaCreacion
'            Me.lblDescripcion.Text = miTipoBal.descripcion
'        End If
'    End Sub

'#Region "crea arbol"
'    Private Sub creaArbol()
'        Dim sql As String = String.Format("BAL_SP_traeCampos")
'        Dim dt As DataTable = htcLib.espacio.ManejadorBD.traetabla(sql)
'        Dim dr As DataRow
'        Dim campoCambio As String = ""

'        For Each dr In dt.Rows
'            Dim itemA As New TreeNode
'            If campoCambio <> dr("tipoCampo") Then
'                Dim nombre As String = dr("tipoCampo")
'                itemA.Value = nombre
'                itemA.SelectAction = TreeNodeSelectAction.None
'                itemA.Expand()
'                Me.trvCampos.Nodes.Add(itemA)
'                creaHijos(dt.Select(String.Format("tipoCampo = '{0}'", nombre), "tipoCampo"), itemA)
'                campoCambio = nombre
'            End If
'        Next
'    End Sub
'    Private Sub creaHijos(ByVal lista() As DataRow, ByVal itemA As TreeNode)
'        Dim dr As DataRow
'        Dim itemHijo As TreeNode

'        For Each dr In lista
'            itemHijo = New TreeNode(dr("campo"), dr("id"))
'            itemHijo.SelectAction = TreeNodeSelectAction.None
'            itemA.ChildNodes.Add(itemHijo)
'        Next
'        'llenaGrid(idProducto)
'    End Sub
'#End Region

'    Sub llenaArbol()

'        'traigo los campos que ya pertenecen a este tipo de balance
'        Dim sql As String = String.Format("select * from {0} where idTipoBalance = {1}", "Bal_CamposXTipo", Me.idModelo)
'        Dim dt As DataTable = htcLib.espacio.ManejadorBD.traetabla(sql)


'        Dim encontro As Boolean
'        Dim nodo, nodohijo As TreeNode

'        'por cada campo que pertenece  a este tipo de balance
'        For Each dr As DataRow In dt.Rows
'            'recorro el arbol hasta encontrar el nodo de este campo
'            For Each nodo In Me.trvCampos.Nodes
'                For Each nodohijo In nodo.ChildNodes
'                    If nodohijo.Value = dr("idCampo") Then
'                        nodohijo.Checked = True
'                        encontro = True
'                        Exit For
'                    End If
'                Next
'                If encontro Then Exit For
'            Next
'            encontro = False
'        Next

'    End Sub

'    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click

'        'guardo el nombre del tipo de balance




'        'guardo los campos de este tipo de balance
'        Dim nodo, nodohijo As TreeNode

'        'recorro el arbol buscando los que esten chequeados
'        For Each nodo In Me.trvCampos.Nodes
'            For Each nodohijo In nodo.ChildNodes

'                'busco si ya existía en la base para editarlo
'                Dim miCampoXtipo As New CamposXTipo(Me.idModelo, nodohijo.Value)

'                If nodohijo.Checked Then
'                    With miCampoXtipo
'                        .idTipoBalance = Me.idModelo
'                        .idCampo = nodohijo.Value

'                        .guardar()
'                    End With

'                Else
'                    'si ya existía debo eliminarlo
'                    If miCampoXtipo.id <> 0 Then miCampoXtipo.eliminar()
'                End If
'            Next
'        Next
'    End Sub



'    Protected Sub btnNuevoCampo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevoCampo.Click
'        Dim hayError As Boolean = False

'        Try
'            htcLib.espacio.inicializa(SacLib.GetConexion, htcLib.tiposConexion.Web)

'            If valida() Then
'                guardaNuevoCampo()

'                Me.trvCampos.Nodes.Clear()
'                refrescaDiv()
'                refresca()
'            End If

'        Catch ex As Exception
'            hayError = True
'            Response.Write("Error:" & ex.Message)

'        Finally
'            htcLib.espacio.cerrar()
'        End Try

'        ' If Not hayError Then Me.Response.Redirect(String.Format("AdministraBalance.aspx?idModelo={0}", Me.idModelo))

'    End Sub

'    ''' <summary>
'    ''' Guarda un nuevo campo que puede ser utilizado en algun modelo de balance
'    ''' </summary>
'    ''' <remarks></remarks>
'    Private Sub guardaNuevoCampo()
'        Dim miCampo As Campo

'        miCampo = New Campo

'        With miCampo
'            .nombre = Me.txtNombre.Text
'            .idCamposClasificacion = Me.ddlTipo.SelectedValue
'            .guardar()
'        End With

'    End Sub

'    ''' <summary>
'    ''' Valida que los campos nombre y tipo esten llenos para crear un nuevo requisito
'    ''' </summary>
'    ''' <returns></returns>
'    ''' <remarks></remarks>
'    Private Function valida() As Boolean
'        Select Case True
'            Case Me.txtNombre.Text = ""
'                Return False
'            Case Me.ddlTipo.SelectedValue = 0
'                Return False

'            Case Else
'                Return True

'        End Select
'    End Function

'    Private Sub refrescaDiv()
'        Me.txtNombre.Text = ""
'        Me.ddlTipo.SelectedValue = 0
'    End Sub
'End Class