﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

''' <summary>
''' elementos que conforman el balance pueden ser de 4 tipos (clasificacion): Ingresos, Egresos, Activos y Pasivos
''' </summary>
Public Class Campo

    Public Shared miTabla As String = "Bal_Campos"

    Public id As Integer
    Public nombre As String
    Public idCamposClasificacion As Integer

    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmsoloLeo)> Public nombreClasificacion As String

    Sub New()
    End Sub

    Sub New(ByVal id As Integer)
        Dim sql As String = String.Format( _
        "select {0}.*, {1}.nombre as nombreClasificacion from {0} inner join {1} on {0}.idCamposClasificacion = {1}.id where {0}.id = {2} ", miTabla, "Bal_CamposClasificacion", id)
        htcLib.LM.cargaObjeto(sql, Me)
    End Sub

    Public Sub guardar()
        htcLib.LM.guardaobjeto(Me)
    End Sub

    Public Sub eliminar()
        Dim sql As String = String.Format("delete from {0} where id = {1}", miTabla, Me.id)
        htcLib.espacio.ManejadorBD.ejecuta(sql)
    End Sub


#Region "propiedadesDiagrama"
    'Public Property DetalleBalance() As CamposXTipo
    '    Get

    '    End Get
    '    Set(ByVal value As CamposXTipo)

    '    End Set
    'End Property
    'Public Property DatosBalance() As DatosBalance
    '    Get

    '    End Get
    '    Set(ByVal value As DatosBalance)

    '    End Set
    'End Property

    'Public Property CamposXTipo() As CamposXTipo
    '    Get

    '    End Get
    '    Set(ByVal value As CamposXTipo)

    '    End Set
    'End Property
#End Region

End Class
