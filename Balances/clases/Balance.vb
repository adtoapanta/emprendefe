﻿Public Class Balance
    Public Shared miTabla As String = "Bal_Balances"

    Public Const idModulo As Integer = 1

    Public id As Integer
    Public idPersona As Integer
    Public idModelo As Integer
    Public fecha As Date
    Public proyectado As Boolean

    Public numSolicitud As Integer
    Public deudasBuro As Decimal

    Public usarDeudaCliente As Boolean 'Esto se establece como excepción

    Public usarCuotaCliente As Boolean

    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public editable As Boolean
    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public cuota As Decimal


    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmsoloLeo)> Public txtModelo As String



    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public IngresosTotales As Decimal
    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public EgresosTotales As Decimal
    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public ActivosTotales As Decimal
    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public PasivosTotales As Decimal


    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Private Ingresos As ArrayList
    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Private Egresos As ArrayList
    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Private Activos As ArrayList
    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Private Pasivos As ArrayList


    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Private campos As ArrayList

    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Private mimodelo As modeloBalance
    Public cuotasBuro As Decimal
    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public cambioMonto As Boolean
    'JFR 05/31/2013 VARIABLE PARA GUARDAR LOS INGRESOS TOTALES CON EL 50% DE OTROS INGRESOS
    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public IngTot50x100OtrosIngresos As Decimal





    Public Enum Componente As Integer

        CapacidadPago = 1
        NivelEndeudamiento = 2
        Apalancamiento = 6
        Patrimonio = 14
        Indicadores = 17
        ventas = 37

    End Enum

#Region "Propiedades"


    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public ReadOnly Property misCampos() As ArrayList
        Get

            Dim sql As String = String.Format("BAL_SP_ValoresXBalance {0}", Me.id)

            If Me.campos Is Nothing Then
                Me.campos = htcLib.LM.CargaAL(sql, New DatosBalance)
            End If

            Return Me.campos

        End Get
    End Property

    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public ReadOnly Property ArrayIngresos() As ArrayList
        Get
            Dim clasificacion As Integer = DatosGenerales.clasificacionesBalance.ingreso
            Dim sql As String = String.Format("BAL_SP_ValoresXBalanceXCategoria {0},{1}", Me.id, clasificacion)

            If Me.Ingresos Is Nothing Then
                Me.Ingresos = htcLib.LM.CargaAL(sql, New DatosBalance)
            End If

            Return Me.Ingresos

        End Get
    End Property

    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public ReadOnly Property ArrayEgresos() As ArrayList
        Get
            Dim clasificacion As Integer = DatosGenerales.clasificacionesBalance.egreso
            Dim sql As String = String.Format("BAL_SP_ValoresXBalanceXCategoria {0},{1}", Me.id, clasificacion)

            If Me.Egresos Is Nothing Then
                Me.Egresos = htcLib.LM.CargaAL(sql, New DatosBalance)
            End If

            Return Me.Egresos


            Return htcLib.LM.CargaAL(sql, New DatosBalance)
        End Get
    End Property

    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public ReadOnly Property ArrayActivos() As ArrayList
        Get
            Dim clasificacion As Integer = DatosGenerales.clasificacionesBalance.activo
            Dim sql As String = String.Format("BAL_SP_ValoresXBalanceXCategoria {0},{1}", Me.id, clasificacion)

            If Me.Activos Is Nothing Then
                Me.Activos = htcLib.LM.CargaAL(sql, New DatosBalance)
            End If

            Return Me.Activos

        End Get
    End Property

    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public ReadOnly Property ArrayPasivos() As ArrayList
        Get
            Dim clasificacion As Integer = DatosGenerales.clasificacionesBalance.pasivo
            Dim sql As String = String.Format("BAL_SP_ValoresXBalanceXCategoria {0},{1}", Me.id, clasificacion)

            If Me.Pasivos Is Nothing Then
                Me.Pasivos = htcLib.LM.CargaAL(sql, New DatosBalance)
            End If

            Return Me.Pasivos
        End Get
    End Property
    Public Property egresoAUsar() As Decimal
        Get
            Dim mayor As Decimal
            Dim datoCuota As Decimal = Me.getDatoPagoCuotas
            Dim Ausar As Decimal
            Dim usarCuotaBuro As Boolean

            usarCuotaBuro = My.Settings.usarCuotaBuro

            If usarCuotaBuro Then

                If datoCuota > Me.cuotasBuro Then mayor = datoCuota Else mayor = Me.cuotasBuro

                Ausar = Me.EgresosTotales - datoCuota + mayor

                If Me.usarCuotaCliente Then
                    Return Me.EgresosTotales
                Else
                    Return Ausar
                End If

            Else
                Return Me.EgresosTotales
            End If

        End Get
        Set(ByVal value As Decimal)

        End Set
    End Property
    Public Property PasivoAusar() As Decimal

        Get
            Dim mayor As Decimal
            Dim usarBuro As Boolean = True

            Dim datoPasivo As Decimal = Me.getDatoDeudaSBS
            Dim Ausar As Decimal


            'todo
            usarBuro = My.Settings.usarBuro   'pilas si no hay en config queno de error


            If usarBuro Then

                If datoPasivo > Me.deudasBuro Then mayor = datoPasivo Else mayor = Me.deudasBuro
                Ausar = Me.PasivosTotales - datoPasivo + mayor

                If Me.usarDeudaCliente Then
                    Return Me.PasivosTotales
                Else
                    Return Ausar
                End If
            Else
                Return Me.PasivosTotales
            End If


        End Get
        Set(ByVal value As Decimal)
            'No hago nada, solo por compatibilidad con htclib
        End Set
    End Property

    Private Function getDatoPagoCuotas() As Decimal
        Dim dato As DatosBalance

        If Not Me.Egresos Is Nothing Then
            For Each dato In Me.Egresos
                If dato.idCampo = 15 Then
                    Return dato.valor
                End If
            Next
        End If

        'si no se usa el dato, usamos este
        Return Me.EgresosTotales

    End Function

    Private Function getDatoDeudaSBS() As Decimal
        Dim dato As DatosBalance

        If Not Me.Pasivos Is Nothing Then
            For Each dato In Me.Pasivos
                If dato.idCampo = 1000 Then
                    Return dato.valor
                End If
            Next
        End If

        'si no se usa el dato, usamos este
        Return Me.PasivosTotales

    End Function


#End Region


    ''' <summary>
    ''' nuevo balance según el cliente y el tipo
    ''' </summary>
    ''' <remarks>Trae el mas actual del cliente. Si no existe o ya no vale, crea uno nuevo</remarks>
    Sub New(ByVal idCliente As Int32, ByVal idModelo As Integer, ByVal forzarNuevo As Boolean, ByVal proyectado As Boolean)

        Dim crearNuevo As Boolean
        Dim proy As Integer

        proy = Convert.ToInt32(proyectado)

        If forzarNuevo Then
            crearNuevo = True
        Else
            Dim sql As String = String.Format("SELECT TOP 1 dbo.BAL_TipoBalance.nombre AS txtModelo, dbo.BAL_Balances.* FROM dbo.BAL_TipoBalance INNER JOIN dbo.BAL_Balances ON dbo.BAL_TipoBalance.id = dbo.BAL_Balances.idModelo WHERE idPersona = {0} and idModelo={1} AND proyectado={2} ORDER BY fecha DESC ", idCliente, idModelo, proy)
            htcLib.LM.cargaObjeto(sql, Me)


            'si no hay ningún balance, creo uno y lo guardo
            If Me.id = 0 Then
                crearNuevo = True
            Else
                mimodelo = New modeloBalance(Me.idModelo)
                Dim dias As Long = mimodelo.diasvalidez * -1

                If Me.fecha <= Today.AddDays(dias) And Not Me.fecha = New Date(1900, 1, 1) Then
                    crearNuevo = True
                    Me.id = 0
                Else
                    'Sirve el balance antiguo, así que lo cargo
                    crearNuevo = False
                    Me.LlenarTotales()
                    Me.editable = False
                End If
            End If
        End If


        'Si no hay balance o no sirve, creo uno nuevo
        If crearNuevo Then
            With Me
                .idPersona = idCliente
                .idModelo = idModelo
                .proyectado = proyectado

                'Le pongo esta fecha para que le permita editar cuando ingrese otra vez
                .fecha = New Date(1900, 1, 1)

                .Guardar()

                .CrearDatosBalanceVacio()
            End With

            Me.editable = True

        End If



    End Sub

    Private Sub Guardar()
        htcLib.LM.GuardaObjeto(Me)
    End Sub

    ''' <summary>
    ''' Elimina el balance y sus datos
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Eliminar()
        Dim sql As String = String.Format("delete from {0} where id = {1}", miTabla, Me.id)
        htcLib.espacio.ManejadorBD.ejecuta(sql)

        'Elimino los datos
        sql = String.Format("delete from {0} where idBalance = {1}", DatosBalance.mitabla, Me.id)
        htcLib.espacio.ManejadorBD.ejecuta(sql)

    End Sub

    ''' <summary>
    ''' calcula totales por ing, egr act, y pas
    ''' Deja cargadas las colecciones
    ''' </summary>
    ''' <remarks></remarks>
    Sub LlenarTotales()
        'JFR 05/31/2013 VARIABLES TMP PARA CALCULO DE INGRESOS CON 50% OTROS INGRESOS
        Dim ventasTmp As Decimal
        Dim OtrosIngTmp As Decimal

        Me.IngresosTotales = 0
        Me.EgresosTotales = 0
        Me.ActivosTotales = 0
        Me.PasivosTotales = 0

        'JFR ENCERO VARIABLES PARA CALCULO DE INGRESOS CON 50% OTROS INGRESOS
        Me.IngTot50x100OtrosIngresos = 0
        ventasTmp = 0
        OtrosIngTmp = 0

        'ingresos
        For Each miDatoBalance As DatosBalance In Me.ArrayIngresos
            Me.IngresosTotales += miDatoBalance.valor

            'JFR 05/31/2013 CONDICION PARA ASIGNAR VALORES VARIABLES TMP
            If miDatoBalance.idCampo = 4 Then
                ventasTmp = miDatoBalance.valor
            End If
            If miDatoBalance.idCampo = 3 Then
                OtrosIngTmp = miDatoBalance.valor * 0.5 '50% de Otros Ingresos
            End If

        Next

        'JFR 05/31/2013 CALCULO DE VARIABLE INGRESOS CON TOTAL VENTAS + 50% DE OTROS INGRESO
        Me.IngTot50x100OtrosIngresos = ventasTmp + OtrosIngTmp

        'egresos
        For Each miDatoBalance As DatosBalance In Me.ArrayEgresos
            If Not miDatoBalance.idCampo = 15 Then 'este campo no se suma
                Me.EgresosTotales += miDatoBalance.valor
            End If
        Next

        'activos
        For Each miDatoBalance As DatosBalance In Me.ArrayActivos
            Me.ActivosTotales += miDatoBalance.valor
        Next

        'pasivos
        For Each miDatoBalance As DatosBalance In Me.ArrayPasivos
            If Not miDatoBalance.idCampo = 1000 Then 'este campo no se suma, es parche para BP
                Me.PasivosTotales += miDatoBalance.valor
            End If
        Next

    End Sub


    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public ReadOnly Property Patrimonio() As Decimal
        Get
            Return Me.ActivosTotales - Me.PasivoAusar ' Me.PasivosTotales
        End Get
    End Property

    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public ReadOnly Property apalancamiento() As Decimal
        Get

            If Me.Patrimonio > 0 Then
                Return Me.PasivosTotales / Me.Patrimonio
            Else
                Return 0
            End If


        End Get
    End Property


    ''' <summary>
    ''' Crea los campos vacíos para el balance, según el modelo de balance
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CrearDatosBalanceVacio()
        Dim sqlCamposXTipo As String = String.Format("select idCampo from Bal_CamposXTipo where idTipoBalance = {0}", Me.idModelo)
        Dim dt As DataTable = htcLib.espacio.ManejadorBD.traetabla(sqlCamposXTipo)
        Dim miDatoBalance As DatosBalance

        Me.campos = New ArrayList

        For Each dr As DataRow In dt.Rows
            miDatoBalance = New DatosBalance
            miDatoBalance.idBalance = Me.id
            miDatoBalance.idCampo = dr(0)
            miDatoBalance.valor = 0

            miDatoBalance.guardar()

            Me.misCampos.Add(miDatoBalance)
        Next
    End Sub


    Private Sub recargarDatos()
        Me.Ingresos = Nothing
        Me.Egresos = Nothing
        Me.Activos = Nothing
        Me.Pasivos = Nothing
        Me.LlenarTotales()

    End Sub


    ''' <summary>
    ''' calcula los componentes del balance: capacidad de pago, nivel de endeudamiento guarda las calificaciones
    ''' y guarda el balance y un resumen
    ''' </summary>
    ''' <param name="idSolicitud">id de solicitud a la que pertenece el balance</param>
    ''' <remarks>crea un objeto ComponentesXSolicitud y guarda los datos obtenidos</remarks>
    Public Sub guardaResumen()

        Dim miente As Boolean = False

        recargarDatos()
        'miente = Me.comparaDeudasConBuro
        miente = False


        'verifico si miente:


        'a cada componente, ñle mando si miente, para que lo agregue en su calificacion

        Me.CalcularCapacidadPago(cuota)
        'Me.CalcularNIvelEndeudamiento(miSolicitud, miente)
        'Me.CalcularApalancamiento(miSolicitud, miente)
        'Me.calculaPatrimonio(miSolicitud, miente)
        'Me.calculaIndicadores(miSolicitud, False)
        'Me.calculaComponenteVentas(miSolicitud, miente)


        'TODO: analizar que se guerda, calcularlo y guardar
        'En las funciones de calculo, ya se calculan las variables que se guardan como resumen
        Me.Guardar()



    End Sub
    Private Function comparaDeudasConBuro() As Boolean
        Dim deudasBalance As Decimal = Me.getDatoDeudaSBS * 1.2

        If Me.deudasBuro > deudasBalance Then
            Return True
        End If

    End Function


    ''' <summary>
    ''' Calculoa cuota vs disponible y busca la calificacion en los rangos segun el modelo
    ''' Guarda el compXsolicitud
    ''' </summary>
    ''' <param name="miSolicitud"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CalcularCapacidadPago(cuota As Decimal) As Boolean
        Dim miModelo As modeloBalance = New modeloBalance(Me.idModelo)
        Dim disponible As Decimal
        Dim rango As rangoCapacidad

        'JFR 05/31/2013 SE COMENTA PARA CALCULAR DISPONIBLE CON 50% OTROS INGRESOS
        'disponible = Me.IngTot50x100OtrosIngresos - Me.egresoAUsar
        disponible = Me.IngresosTotales - Me.egresoAUsar '(Me.EgresosTotales + Me.cuotasBuro)


        'cuota vs disponible
        Dim cuotaVsDisponible As Decimal = 0
        'disponible =0
        If disponible <= 0 Then
            cuotaVsDisponible = 1000
        Else
            cuotaVsDisponible = cuota / disponible * 100
        End If

        'Todo Juan Pablo aqui hay que cambiar el monto o el plazo a las solicitudes para que la cuota vs disponible me permita aprobar.

        'Por el momento este cambio no aplica

        'If cuotaVsDisponible >= 70 And cuotaVsDisponible < 100 Then
        '    cuotaVsDisponible = Me.calculaMontoPlazo(miSolicitud, disponible, cuotaVsDisponible)
        'End If


        rango = miModelo.getRango(cuotaVsDisponible)



        Return rango.permiteAprobar


    End Function

    'Private Function calculaMontoPlazo(ByVal sol As SACFinca.Solicitud, ByVal disponible As Decimal, ByVal cvsd As Decimal) As Decimal
    '    'primero calculo el nuevo mosnto(Sol Grupales o individuales) o plazo (solo individuales)
    '    'Luego envio estos parametros a la funcion de la solicitud cambia condiciones.
    '    Dim nuevaCuota As Decimal
    '    Dim nohayError As Boolean
    '    Dim nuevaCvsD As Decimal
    '    Dim cambioPlazo As Boolean = False

    '    'Para individuales primero cambio el plazo, luego cambio el monto
    '    If sol.idCiclo = 0 Then
    '        For j As Integer = 0 To 12
    '            If sol.numPagos = 12 Then Exit For

    '            sol.numPagos = sol.numPagos + 1
    '            nuevaCuota = sol.CalcularCuotaMes()
    '            nuevaCvsD = (nuevaCuota / disponible) * 100

    '            If nuevaCvsD <= 70 Then
    '                cambioPlazo = True
    '                Exit For
    '            End If

    '        Next
    '    End If

    '    If cambioPlazo = False Then
    '        For i As Integer = 0 To sol.monto Step 100
    '            sol.monto = sol.monto - 100
    '            nuevaCuota = sol.CalcularCuotaMes()
    '            nuevaCvsD = (nuevaCuota / disponible) * 100
    '            If nuevaCvsD <= 70 Then Exit For

    '        Next

    '    End If

    '    'nohayError = sol.cambiaMonto(sol.monto, sol.idFrecuenciaPagos, sol.numPagos)


    '    If nohayError Then ' si no hay error devuelvo la nueva CuotaVSDisponible
    '        Return nuevaCvsD
    '    Else
    '        Return cvsd 'Si existe algun error devuelvo la misma CuotaVSDisponible
    '    End If

    'End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="miSolicitud"></param>
    ''' <returns></returns>
    '''' <remarks></remarks>
    'Private Function CalcularNIvelEndeudamiento() As Boolean

    '    Dim miModelo As modeloBalance = New modeloBalance(Me.idModelo) 'nuevo
    '    Dim rango As rangoNivelEndeudamiento

    '    Dim pasivYDeuda As Decimal = Me.PasivoAusar
    '    Dim nivel As Decimal


    '    'Por si no existe, no lo calccula
    '    If Not miCXS1.idComponente = 0 Then
    '        If Me.Patrimonio > 0 Then
    '            nivel = pasivYDeuda / Me.Patrimonio
    '        Else
    '            nivel = 10
    '        End If

    '        nivel = nivel * 100

    '        rango = miModelo.getRangosNivelEndeudamineto(nivel)


    '        With miCXS1
    '            .permiteAprobar = rango.permiteAprobar
    '            .riesgo = rango.riesgo
    '            .detalle = String.Format("Total pasivos + crédito=  {0:n0}; Patrimonio={1:n0}; endeudamiento= {2:n0}%", pasivYDeuda, Me.Patrimonio, nivel)
    '            .idReferencia = Me.id
    '            .idModelo = Me.idModelo
    '            .nivelAprobacionRequerido = rango.nivelAprobacionRequerido

    '            If miente Then
    '                miCXS1.tieneObservaciones = True
    '                miCXS1.detalle = miCXS1.detalle & String.Format(" Tiene diferencias con el buró")
    '            End If
    '            .Guardar()
    '            'If nivel > 70 Then
    '            '    .permiteAprobar = False
    '            '    .riesgo = SacLib.NivelesRiesgo.Alto
    '            'Else
    '            '    .permiteAprobar = True
    '            '    .riesgo = SacLib.NivelesRiesgo.NoAplica
    '            'End If

    '            '.detalle = String.Format("Total pasivos + crédito=  {0:n0}; Patrimonio={1:n0}; endeudamiento= {2:n0}%", pasivYDeuda, Me.Patrimonio, nivel)

    '        End With


    '        Return rango.permiteAprobar
    '    Else
    '        Return True
    '    End If




    'End Function
    '''' <summary>
    ''' Calcula todo el apalancamiento
    ''' </summary>
    ''' <param name="miSolicitud">objeto solicitud</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    'Private Function CalcularApalancamiento(ByVal miSolicitud As SACFinca.Solicitud, ByVal miente As Boolean) As Boolean
    '    Dim miRangoApalancamiento As rangoApalancamiento 'nuevo

    '    Dim miModelo As modeloBalance = New modeloBalance(Me.idModelo) 'nuevo

    '    Dim miCXS1 As New SACFinca.ComponenteXSolicitud(Balance.Componente.Apalancamiento, Me.MiSujeto.id)

    '    Dim nivel As Decimal

    '    'Por si no existe, no lo calccula
    '    If Not miCXS1.idComponente = 0 Then
    '        If Me.ActivosTotales > 0 Then
    '            nivel = (Me.PasivoAusar + miSolicitud.monto) / (Me.ActivosTotales)
    '        Else
    '            nivel = 10
    '        End If

    '        nivel = nivel * 100

    '        miRangoApalancamiento = miModelo.getRangoApalancamiento(nivel) 'nuevo


    '        With miCXS1
    '            .permiteAprobar = miRangoApalancamiento.permiteAprobar
    '            .riesgo = miRangoApalancamiento.riesgo
    '            .detalle = String.Format("Total pasivos =  {0:n0}; Total activos={1:n0}; Endeudamiento= {2:n0}%", Me.PasivoAusar, Me.ActivosTotales, nivel)
    '            .nivelAprobacionRequerido = miRangoApalancamiento.nivelAprobacionRequerido
    '            .idReferencia = Me.id
    '            .idModelo = Me.idModelo

    '            If miente Then
    '                miCXS1.tieneObservaciones = True
    '                miCXS1.detalle = miCXS1.detalle & String.Format(" Tiene diferencias con el buró")
    '            End If

    '            .Guardar()

    '        End With


    '        Return miRangoApalancamiento.permiteAprobar
    '    Else
    '        Return True
    '    End If






    'End Function


    'Private Function calculaPatrimonio(ByVal miSolicitud As SACFinca.Solicitud, ByVal miente As Boolean)
    '    Dim miRangoPatrimonio As rangoPatrimonio  'nuevo
    '    Dim miModelo As modeloBalance = New modeloBalance(Me.idModelo)

    '    Dim miCXS1 As New SACFinca.ComponenteXSolicitud(Balance.Componente.Patrimonio, Me.MiSujeto.id)

    '    'verifico que exista
    '    If Not miCXS1.idComponente = 0 Then
    '        Dim nivel As Decimal

    '        'If Me.Patrimonio > 0 Then
    '        nivel = Me.Patrimonio
    '        'Else
    '        'nivel = 10
    '        ' End If

    '        'nivel = nivel * 100

    '        miRangoPatrimonio = miModelo.getRangoPatrimonio(nivel)

    '        With miCXS1
    '            .permiteAprobar = miRangoPatrimonio.permiteAprobar
    '            .detalle = String.Format("Patrimonio :{0:n2}", nivel)


    '            .nivelAprobacionRequerido = miRangoPatrimonio.nivelAprobacionRequerido
    '            .idReferencia = Me.id
    '            .idModelo = Me.idModelo
    '            .riesgo = miRangoPatrimonio.riesgo

    '            If miente Then
    '                miCXS1.tieneObservaciones = True
    '                miCXS1.detalle = miCXS1.detalle & String.Format(" Tiene diferencias con el buró")
    '                'todo: solo para MInga, hacerlo configurable
    '                'If .nivelAprobacionRequerido < 2 Then .nivelAprobacionRequerido = 2

    '            End If

    '            .Guardar()

    '        End With


    '        Return miCXS1.permiteAprobar
    '    Else
    '        Return True
    '    End If





    'End Function
    'Private Function calculaComponenteVentas(ByVal miSolicitud As SACFinca.Solicitud, ByVal miente As Boolean) As Boolean

    '    Dim miModelo As modeloBalance = New modeloBalance(Me.idModelo)
    '    Dim miRangoVenta As rangoVenta
    '    Dim ventas As Decimal
    '    Dim exp As Boolean
    '    Dim miCXS1 As New SACFinca.ComponenteXSolicitud(Balance.Componente.ventas, Me.MiSujeto.id)

    '    'verifico que exista
    '    If Not miCXS1.idComponente = 0 Then
    '        ventas = Me.getCampo(4)
    '        If Me.deudasBuro > 0 Then
    '            exp = True
    '        Else
    '            exp = False
    '        End If

    '        miRangoVenta = miModelo.getRangoVentas(ventas, miSolicitud.monto, exp)

    '        With miCXS1
    '            .permiteAprobar = miRangoVenta.permiteAprobar
    '            .detalle = String.Format("Ventas: ${0}, Deudas: ${1} ", Format(ventas, "0.00"), Format(Me.deudasBuro, "0.00"))
    '            .nivelAprobacionRequerido = miRangoVenta.nivelAprobacion
    '            .idReferencia = Me.id
    '            .idModelo = Me.idModelo
    '            .riesgo = miRangoVenta.riesgo


    '            If miente Then
    '                miCXS1.tieneObservaciones = True
    '                miCXS1.detalle = miCXS1.detalle & String.Format(" Tiene diferencias con el buró")

    '            End If

    '            .Guardar()

    '        End With

    '        Return miCXS1.permiteAprobar
    '    Else
    '        Return True
    '    End If

    'End Function
    'Private Function calculaIndicadores(ByVal miSolicitud As SACFinca.Solicitud, ByVal miente As Boolean)

    '    Dim ventas, monto, resultado As Decimal
    '    Dim detalle As New System.Text.StringBuilder
    '    Dim actcte, pascte, liquidez, capital As Decimal
    '    Dim margen, costos As Decimal
    '    Dim inventarios, rotacion As Decimal


    '    Dim miCXS1 As New SACFinca.ComponenteXSolicitud(Balance.Componente.Indicadores, Me.MiSujeto.id)

    '    'verifico que exista
    '    If Not miCXS1.idComponente = 0 Then

    '        'Monto/ventas 1
    '        ventas = Me.getCampo(4)
    '        monto = miSolicitud.monto

    '        If ventas > 0 Then
    '            resultado = monto / ventas * 100
    '            If resultado > 80 Then miCXS1.riesgo = 5 Else miCXS1.riesgo = 1
    '            detalle.AppendFormat("Monto/ventas :{0:n2}% -", resultado)
    '        Else
    '            resultado = -1
    '            miCXS1.riesgo = 0 'por ser lo primero que reviso
    '            detalle.Append(" - ")
    '        End If


    '        'Capital de trabajo y liquidez
    '        actcte = Me.getACTCTE
    '        pascte = Me.getPasCTE

    '        If actcte <> -1 And pascte <> -1 Then
    '            'liquidez
    '            If pascte > 0 Then
    '                liquidez = actcte / pascte
    '                If liquidez < 1 Then
    '                    miCXS1.riesgo = 5
    '                Else
    '                    If miCXS1.riesgo = 0 Then miCXS1.riesgo = 1
    '                End If
    '                detalle.AppendFormat("Liquidez:{0:n2} - ", liquidez)
    '            Else
    '                liquidez = 0
    '                If miCXS1.riesgo = 0 Then miCXS1.riesgo = 1
    '                detalle.AppendFormat("Liquidez:N/A - ")
    '            End If

    '            'capital trabajo
    '            capital = actcte - pascte
    '            detalle.AppendFormat("Cap. Trab:{0:n0} - ", capital)

    '        Else
    '            detalle.Append(" - ")
    '        End If

    '        'MARGEN
    '        If ventas > 0 Then
    '            costos = Me.getCampo(7)
    '            margen = (ventas - costos) / ventas
    '            margen = margen * 100
    '            detalle.AppendFormat("MG:{0:n0}% -", margen)
    '        End If

    '        'ROTACION INVENTARIOS
    '        inventarios = Me.getCampo(117)
    '        costos = Me.getCampo(7)

    '        If inventarios <> -1 And costos > 0 Then
    '            rotacion = inventarios / costos * 30
    '            detalle.AppendFormat("Rotacion:{0:n0} -", rotacion)
    '        End If

    '        'RESUMEN FINAL
    '        With miCXS1
    '            .detalle = detalle.ToString
    '            .permiteAprobar = True
    '            .nivelAprobacionRequerido = 1
    '            .idReferencia = Me.id
    '            .idModelo = Me.idModelo

    '            .Guardar()

    '        End With


    '        Return miCXS1.permiteAprobar


    '    Else
    '        Return True
    '    End If


    'End Function

    Private Function getACTCTE()
        Dim Efectivo, Depositos, CtasXCobrar, Inventario As Decimal

        Efectivo = Me.getCampo(114)
        Depositos = Me.getCampo(115)
        CtasXCobrar = Me.getCampo(116)
        Inventario = Me.getCampo(117)

        If Efectivo <> -1 And Depositos <> -1 And CtasXCobrar <> -1 And Inventario <> -1 Then
            Return Efectivo + Depositos + CtasXCobrar + Inventario
        Else
            Return -1
        End If


    End Function

    Private Function getPasCTE()
        Dim creditos, CtasXPagar, OtrasDeudas As Decimal

        creditos = Me.getCampo(119)
        CtasXPagar = Me.getCampo(120)
        OtrasDeudas = Me.getCampo(121)

        If creditos <> -1 And CtasXPagar <> -1 And OtrasDeudas <> -1 Then
            Return creditos + CtasXPagar + OtrasDeudas
        Else
            Return -1
        End If


    End Function





    Public Function getCampo(ByVal idCampo As Long) As Decimal
        Dim dato As DatosBalance

        If Not Me.Ingresos Is Nothing Then
            For Each dato In Me.Ingresos
                If dato.idCampo = idCampo Then
                    Return dato.valor
                End If
            Next
        End If

        If Not Me.Egresos Is Nothing Then
            For Each dato In Me.Egresos
                If dato.idCampo = idCampo Then
                    Return dato.valor
                End If
            Next
        End If

        If Not Me.Activos Is Nothing Then
            For Each dato In Me.Activos
                If dato.idCampo = idCampo Then
                    Return dato.valor
                End If
            Next
        End If

        If Not Me.Pasivos Is Nothing Then
            For Each dato In Me.Pasivos
                If dato.idCampo = idCampo Then
                    Return dato.valor
                End If
            Next
        End If


        'busco en arreglo general:
        If Not Me.misCampos Is Nothing Then
            For Each dato In Me.misCampos
                If dato.idCampo = idCampo Then
                    Return dato.valor
                End If
            Next
        End If

        'si no se usa el dato, usamos este
        Return -1

    End Function

End Class