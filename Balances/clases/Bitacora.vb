﻿

Public Class Bitacora
    Public Shared mitabla As String = "SAC_Bitacora"
    Public id As Long
    Public idPersona As Long
    Public idModelo As Long
    Public idReferencia As Long
    <htcLib.ATfechaconHora(True)> Public fecha As Date

    Public observacion As String

    Public Sub New()
    End Sub

    Public Sub New(ByVal idPersona As Long, ByVal idEvaluacion As Long, ByVal observacion As String)
        Me.idPersona = idPersona
        Me.idModelo = 2 'balances
        Me.idReferencia = idEvaluacion
        Me.fecha = Today
        Me.observacion = observacion
    End Sub

    Public Function Guardar() As Boolean
        Dim retorno As Boolean

        If Me.observacion.Length > 0 Then
            htcLib.LM.GuardaObjeto(Me)
            retorno = True
        End If

        Return retorno
    End Function

End Class
