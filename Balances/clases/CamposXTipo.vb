﻿Imports System.Web

''' <summary>
''' cuerpo del balance contiene la definicion y los campos propios del tipo de balance requerido
''' </summary>
Public Class CamposXTipo
    Public Shared miTabla As String = "Bal_CamposXTipo"
    Public id As Integer
    Public idTipoBalance As Integer
    Public idCampo As Integer

    Sub New()
    End Sub

    Sub New(ByVal id As Integer)
        Dim sql As String = String.Format("select * from {0} where id= {1}", miTabla, id)
        htcLib.LM.cargaObjeto(sql, Me)
    End Sub

    Sub New(ByVal idTipoBalance As Integer, ByVal idCampo As Integer)
        Dim sql As String = String.Format("select * from {0} where idTipoBalance = {1} and idCampo= {2}", miTabla, idTipoBalance, idCampo)
        htcLib.LM.cargaObjeto(sql, Me)
    End Sub

    Public Sub guardar()
        htcLib.LM.guardaObjeto(Me)
    End Sub

    Public Sub eliminar()
        Dim sql As String = String.Format("delete from {0} where id = {1}", miTabla, Me.id)
        htcLib.espacio.ManejadorBD.ejecuta(sql)
    End Sub




End Class
