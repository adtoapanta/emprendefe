﻿Module DatosGenerales

    Public Enum clasificacionesBalance
        ingreso = 1
        egreso = 2
        activo = 3
        pasivo = 4
    End Enum

    ''' <summary>
    ''' carga el dropDownList enviado con los registros de la tabla enviada
    ''' </summary>
    ''' <param name="ddl">dropDownList que se llenará</param>
    ''' <param name="tabla">tabla donde estan los datos</param>
    ''' <param name="incluirCampoTodos">true para insertar un campo "todos" en el ddl con id=0</param>
    ''' <remarks></remarks>
    Sub cargaDropDownList(ByVal ddl As DropDownList, ByVal tabla As String, Optional ByVal incluirCampoTodos As Boolean = False)
        Dim sql As String = String.Format("select * from {0} order by id", tabla)
        Dim alItems As ArrayList = htcLib.LM.cargaAL(sql, New htcLib.idNombre)

        If incluirCampoTodos Then
            Dim miIdNombre As New htcLib.idNombre(0, " - todos - ")
            alItems.Insert(0, miIdNombre)
        End If

        ddl.DataTextField = "nomProp"
        ddl.DataValueField = "idProp"
        ddl.DataSource = alItems
        ddl.DataBind()
    End Sub




End Module
