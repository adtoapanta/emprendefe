﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

Public Class DatosBalance

    Public Shared mitabla As String = "BAL_DatosBalance"

    Public id As Integer
    Public idBalance As Integer
    Public idCampo As Integer
    Public valor As Decimal

#Region "Propiedades"
    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public ReadOnly Property PropId() As Integer
        Get
            Return Me.id
        End Get
    End Property

    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public ReadOnly Property PropIdBalance() As Integer
        Get
            Return Me.idBalance
        End Get
    End Property

    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public ReadOnly Property PropIdCampo() As Integer
        Get
            Return Me.idCampo
        End Get
    End Property

    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public ReadOnly Property PropValor() As Decimal
        Get
            Return Me.valor
        End Get
    End Property

    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public ReadOnly Property clasificacion() As String
        Get
            Dim miCampo As New Campo(idCampo)
            Return miCampo.nombreClasificacion
        End Get
    End Property

    <htcLib.AtGuardable(htcLib.lmTipoGuardo.lmNno)> Public ReadOnly Property campo() As String
        Get
            Dim miCampo As New Campo(idCampo)
            Return miCampo.nombre
        End Get
    End Property
#End Region


    Sub New()
    End Sub

    Sub New(ByVal id As Integer)
        Dim sql As String = String.Format("select * from {0} where id = {1}", mitabla, id)
        htcLib.LM.cargaObjeto(sql, Me)
    End Sub

    Sub New(ByVal idcampo As Integer, ByVal idBal As Integer)
        Dim sql As String = String.Format("select * from {0} where idCampo = {1} and idBalance = {2}", mitabla, idcampo, idBal)
        htcLib.LM.cargaObjeto(sql, Me)
    End Sub

    Public Sub guardar()
        htcLib.LM.guardaobjeto(Me)
    End Sub

    Public Sub eliminar()
        Dim sql As String = String.Format("delete from {0} where id = {1}", mitabla, Me.id)
        htcLib.espacio.ManejadorBD.ejecuta(sql)
    End Sub

End Class
