﻿Public Class rangoCapacidad

    Public Shared miTabla As String = "BAL_RangosCapacidad"

    Public id
    Public nombre As String
    Public idModelo As Long
    Public desde As Long
    Public hasta As Long
    Public riesgo As Long
    Public permiteAprobar As Boolean
    Public nivelAprobacionRequerido As Long

    Public Sub New(ByVal id_ As Long)
        Dim sql As String

        sql = String.Format("SELECT * FROM {0} WHERE id={1}", miTabla, id_)
        htcLib.LM.cargaObjeto(sql, Me)
    End Sub

    Sub New(ByVal idmodelo As Long, ByVal indice As Long)
        Dim sql As String

      
        sql = String.Format("BAL_SP_RangosCapacidad {0},{1}", idmodelo, indice)

        htcLib.LM.cargaObjeto(sql, Me)

    End Sub

    Public Sub guardar()
        htcLib.LM.GuardaObjeto(Me)
    End Sub

    Public Sub eliminar()
        Dim sql As String = String.Format("delete  from {0} where id = {1}", miTabla, Me.id)
        htcLib.espacio.ManejadorBD.ejecuta(sql)
    End Sub

    Public Shared Function getRangosScore(ByVal idModelo As Integer) As DataTable
        Dim sql As String

        sql = String.Format("select * from BAL_VIEW_traeRangos where idModelo={0}", idModelo)
        Return htcLib.espacio.ManejadorBD.traetabla(sql)

    End Function

    Public Shared Function getUltimoRango(ByVal idModelo As Integer) As Integer
        Dim sql As String

        sql = String.Format("select max (id) from BAL_VIEW_traeRangos where idModelo={0}", idModelo)
        Return htcLib.espacio.ManejadorBD.ejecutaEscalar(sql, htcLib.tiposdatos.entero)

    End Function

    Public Shared Function getUltimoValor(ByVal idModelo As Integer) As Decimal
        Dim sql As String

        sql = String.Format("select max (hasta) from BAL_VIEW_traeRangos where idModelo={0}", idModelo)
        Return htcLib.espacio.ManejadorBD.ejecutaEscalar(sql, htcLib.tiposdatos.entero)

    End Function



End Class
