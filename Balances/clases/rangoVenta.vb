﻿Public Class rangoVenta


    Public Shared miTabla As String = "BAL_RangosVentas"
    Public id As Integer
    Public idModelo As Integer
    Public ventasDesde As Decimal
    Public ventasHasta As Decimal
    Public montoMaximoDesde As Decimal
    Public montoMaximoHasta As Decimal
    Public experiencia As Boolean
    Public riesgo As Integer
    Public permiteAprobar As Boolean
    Public nivelAprobacion As Integer


    Sub New()
    End Sub

    Sub New(ByVal id As Integer)
        Dim sql As String = String.Format("select * from {0} where id= {1}", miTabla, id)
        htcLib.LM.cargaObjeto(sql, Me)
    End Sub
    Sub New(ByVal idModelo As Integer, ByVal ventas As Decimal, ByVal monto As Decimal, ByVal experiencia As Boolean)
        Dim sql As String

        sql = String.Format("BAL_SP_RangosVentas {0},{1},{2},{3}", idModelo, ventas, monto, experiencia)
        htcLib.LM.cargaObjeto(sql, Me)

    End Sub

    Public Sub guardar()
        htcLib.LM.guardaObjeto(Me)
    End Sub

    Public Sub eliminar()
        Dim sql As String = String.Format("delete from {0} where id = {1}", miTabla, Me.id)
        htcLib.espacio.ManejadorBD.ejecuta(sql)
    End Sub

    Public Shared Function getRangosScore(ByVal idModelo As Integer) As DataTable
        Dim sql As String

        sql = String.Format("select * from BAL_VIEW_traeRangosVentas where idModelo={0}", idModelo)
        Return htcLib.espacio.ManejadorBD.traetabla(sql)

    End Function

End Class
