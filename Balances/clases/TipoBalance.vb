﻿Imports System.Web

''' <summary>
''' Definición del balance
''' </summary>
Public Class modeloBalance
    Public Shared miTabla As String = "Bal_TipoBalance"

    Public id As Integer
    Public nombre As String
    Public bloqueado As Boolean
    Public fechaCreacion As Date
    Public descripcion As String
    Public diasvalidez As Long

  
    Sub New()
    End Sub


    Sub New(ByVal id As Integer)
        Dim sql As String = String.Format("select * from {0} where id = {1}", miTabla, id)
        htcLib.LM.cargaObjeto(sql, Me)
    End Sub

    Public Sub guardar()
        htcLib.LM.guardaObjeto(Me)
    End Sub

    Public Sub eliminar()
        Dim sql As String = String.Format("delete from {0} where id = {1}", miTabla, Me.id)
        htcLib.espacio.ManejadorBD.ejecuta(sql)
    End Sub

    Public Function getRango(ByVal indice As Long) As rangoCapacidad
        Dim rc As New rangoCapacidad(Me.id, indice)
        Return rc
    End Function
    'nuevo
    Public Function getRangoApalancamiento(ByVal indice As Long) As rangoApalancamiento
        Dim rA As New rangoApalancamiento(Me.id, indice)
        Return rA
    End Function

    Public Function getRangosNivelEndeudamineto(ByVal indice As Long) As rangoNivelEndeudamiento
        Dim rne As New rangoNivelEndeudamiento(Me.id, indice)
        Return rne

    End Function
    Public Function getRangoPatrimonio(ByVal indice As Long) As rangoPatrimonio
        Dim rp As New rangoPatrimonio(Me.id, indice)
        Return rp

    End Function
    Public Function getRangoVentas(ByVal ventas As Decimal, ByVal monto As Decimal, ByVal experiencia As Boolean)
        Dim rv As New rangoVenta(Me.id, ventas, monto, experiencia)
        Return rv
    End Function

    Public Function getCamposXModelo() As DataTable
        Dim sql As String

        sql = String.Format("select BAL_Campos.nombre from BAL_CamposXTipo,BAL_Campos where  BAL_CamposXTipo.idCampo=BAL_Campos.id and BAL_CamposXTipo.idTipoBalance={0}", Me.id)

        Return htcLib.espacio.ManejadorBD.traetabla(sql)

    End Function
End Class
