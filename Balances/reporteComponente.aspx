﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="reporteComponente.aspx.vb"
    Inherits="Balances.reporteComponente" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        |
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="subtitulo">
                    BALANCES
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td valign="top" class="subtitulo">
                    Modelo:<asp:Label ID="lblModelo" runat="server" CssClass="subtitulo"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <span class="subtitulo">Descripcion:</span>
                    <asp:Label ID="lblDescripcion" runat="server" CssClass="texto"></asp:Label>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="subtitulo">
                    Datos.
                </td>
            </tr>
            <tr>
                <td>
                   
                    <asp:DataList ID="dtlCampos" runat="server" CellPadding="0" CellSpacing="10" 
                        RepeatColumns="4">
                        <ItemTemplate>
                            <asp:Label ID="lblCampos" runat="server" Text='<%# eval("nombre") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:DataList>
                     &nbsp;
                </td>
                </tr>
                <tr>
                    <td class="subtitulo">
                        Componentes
                    </td>
                </tr>
                
                <tr>
                    <td>
                        &nbsp;
                    </td>
            </tr>
                
                <tr>
                    <td id="tdCapacidad" runat="server" visible="false"   >
                        <table cellpadding="0" cellspacing="0" width="100%" >
                            <tr>
                                <td class="subtitulo" >
                                    Capacidad de pago</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="grdCapacidad" runat="server" AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:BoundField DataField="desde" HeaderText="Desde" />
                                            <asp:BoundField DataField="hasta" HeaderText="Hasta" />
                                            <asp:BoundField DataField="permiteAprobar" HeaderText="Permite Aprobar" />
                                            <asp:BoundField DataField="nombreNivel" HeaderText="Nivel de Aprobacion" />
                                            <asp:BoundField DataField="nombreRiesgo" HeaderText="Riesgo" />
                                        </Columns>
                                    </asp:GridView>
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </td>
            </tr>
                
                <tr>
                    <td id="tdEndeudamiento" runat="server" visible="false" >
                        <table cellpadding="0" cellspacing="0" width="100%" >
                            <tr>
                                <td class="subtitulo" >
                                    Nivel de endeudamiento</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="grdEndeudamiento" runat="server" AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:BoundField DataField="desde" HeaderText="Desde" />
                                            <asp:BoundField DataField="hasta" HeaderText="Hasta" />
                                            <asp:BoundField DataField="permiteAprobar" HeaderText="Permite Aprobar" />
                                            <asp:BoundField DataField="nombreNivel" HeaderText="Nivel de Aprobacion" />
                                            <asp:BoundField DataField="nombreRiesgo" HeaderText="Riesgo" />
                                        </Columns>
                                    </asp:GridView>
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </td>
            </tr>
                
                <tr>
                    <td id="tdApalancamiento" runat="server" visible="false"  >
                        &nbsp;</td>
            </tr>
                
                <tr>
                    <td id="tdVentas" runat="server" visible="false"  >
                    
                        <table cellpadding="0" cellspacing="0" width="100%" >
                            <tr>
                                <td class="subtitulo">
                                    Ventas</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="grdVentas" runat="server" AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:BoundField DataField="ventasDesde" HeaderText="Ventas desde" />
                                            <asp:BoundField DataField="ventasHasta" HeaderText="Ventas hasta" />
                                            <asp:BoundField DataField="ventasDesde" HeaderText="Ventas desde" />
                                            <asp:BoundField DataField="montoMaximoDesde" HeaderText="Monto desde" />
                                            <asp:BoundField DataField="montoMaximoHasta" HeaderText="Monto hasta" />
                                            <asp:BoundField DataField="experiencia" HeaderText="Deudas en buro" />
                                            <asp:BoundField DataField="nombreNivel" HeaderText="Nivel de Aprobacion" />
                                            <asp:BoundField DataField="nombreRiesgo" HeaderText="Riesgo" />
                                        </Columns>
                                    </asp:GridView>
                                    </td>
                            </tr>
                        </table>
                    </td>
            </tr>
        </table>
        |
    </div>
    </form>
</body>
</html>
