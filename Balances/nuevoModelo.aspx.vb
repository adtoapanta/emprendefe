﻿Public Partial Class nuevoModelo
    Inherits System.Web.UI.Page
    Private idM As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If vUsuarios.web.TraeUsuarioRegistrado.nivel = 0 Then SacLib.GoInicio()

        idM = Me.Request("idModelo")

        If idM <> 0 Then
            Me.Response.Redirect(String.Format("AdministraBalance.aspx?idModelo={0}", idM))
        End If

    End Sub

    Protected Sub btnContinuar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnContinuar.Click
        Dim miModelo As modeloBalance
        Dim idModelo As Integer
        Dim hayError As Boolean = False
        Try
            'htcLib.espacio.inicializa(SacLib.GetConexion, htcLib.tiposConexion.Web)

            miModelo = New modeloBalance
            miModelo.nombre = Me.txtNombre.Text
            miModelo.descripcion = Me.txtDescripcion.Text
            miModelo.bloqueado = False
            miModelo.fechaCreacion = Today
            miModelo.diasvalidez = Me.txtDiasValidez.Text
            miModelo.guardar()
            idModelo = miModelo.id

        Catch ex As Exception
            hayError = True
            Me.Response.Write(ex.ToString)
        Finally
            htcLib.espacio.cerrar()
        End Try

        If Not hayError Then Me.Response.Redirect(String.Format("AdministraBalance.aspx?idModelo={0}", idModelo))

    End Sub

End Class