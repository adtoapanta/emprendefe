<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MPModulo.Master"
    CodeBehind="CrearBalance.aspx.vb" Inherits="Balances.CrearBalance" %>

<%@ Register Src="controles/fechaCalendario.ascx" TagName="fechaCalendario" TagPrefix="uc1" %>
<%@ Register Src="controles/datosSolicitud.ascx" TagName="datosSolicitud" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
    
        function habilitaDiv() {
            var div = document.getElementById('divObservaciones');
            div.style.display = "";
        }

        function SumaTotales(nombreGrid, nombreLabel, idTxt, nombreCorto) {//resibe el grid,el label donde se desplega el resultado

            var tabla = document.getElementById(nombreGrid);            // un identificador de cada txt que esta dentro del grid
            var valor = 0.0;                                            // y el nombre corto del grid
            var nombre;
            var nombrelbl;
            var valorlbl;
            celdas = tabla.rows;

            for (i = 0; i <= celdas.length; i++) {//recorro las filas del grid

                if (i >= 2) {//el nombre del control varia dependiendo del nombre del grid, y empiezan ciempre desde el 2 ejem '_ctl02
                    nombre = 'ctl00_ContentPlaceHolder1_' + nombreCorto + '_ctl0' + i + '_txtValor' + idTxt;
                    nombrelbl = 'ctl00_ContentPlaceHolder1_' + nombreCorto + '_ctl0' + i + '_lblNombre' + idTxt;

                    if (i > 9) {// cuando el valor es mayor a 9 se cambia '_ctl0 por ctl
                        nombre = 'ctl00_ContentPlaceHolder1_' + nombreCorto + '_ctl' + i + '_txtValor' + idTxt;
                        nombrelbl = 'ctl00_ContentPlaceHolder1_' + nombreCorto + '_ctl' + i + '_lblNombre' + idTxt;
                    }
                    valorlbl = document.getElementById(nombrelbl).innerHTML;
                    if (valorlbl != 1000)   {
                            if (valorlbl != 15) {
                            valor += parseFloat(document.getElementById(nombre).value);
                               }
                        
                    }
                   
                    if (isNaN(valor) == true) {
                        document.getElementById(nombreLabel).innerHTML = "El valor ingresado no es valido";
                    } else {
                        document.getElementById(nombreLabel).innerHTML = valor;

                    }


                }
            }
        }

        function controlCaracteresNum(e) {
            var key = window.event ? e.keyCode : e.which;

            if ((key < 58) && (key > 47)) {
                var keychar = String.fromCharCode(key);
                return keychar;
            }
            return false;
        }
       
    </script>

    <table cellpadding="0" cellspacing="0" style="width: 100%" onkeypress="return noenter(event);">
        <tr>
            <td class="esp">
                &nbsp;
            </td>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td class="titulo">
                BALANCE
                <asp:Label ID="lblGarante" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td class="titulo">
                &nbsp;&nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <uc2:datosSolicitud ID="datosSolicitud1" runat="server" Visible="false" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="esp">
                &nbsp;
            </td>
            <td>
                &nbsp;
                <asp:Label ID="lblNuevoBalance" runat="server" Visible="False"></asp:Label>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td class="texto">
                <table cellpadding="0" cellspacing="0" class="bordes" style="width: 100%">
                    <tr class="grdTitulo">
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            Balance
                            <asp:Label ID="lblTipoBalance" runat="server"></asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="esp">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td class="subtitulo" style="width: 180px">
                                        Fecha levantamiento :
                                    </td>
                                    <td style="width: 300px">
                                        <uc1:fechaCalendario ID="fcFecha" runat="server" />
                                        <asp:Label ID="lblFecha" runat="server" Visible="False"></asp:Label>
                                    </td>
                                    <td id="tdNumSol" runat="server">
                                        <asp:Label ID="lblErrorFecha" runat="server" CssClass="alerta" Text="La fecha de levantamiento no puede ser mayor a hoy"
                                            Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 180px">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblProyectado" runat="server" RepeatDirection="Horizontal"
                                            Visible="False">
                                            <asp:ListItem Value="True">S�</asp:ListItem>
                                            <asp:ListItem Selected="True" Value="False">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnEditar" runat="server" Text="Editar" Width="110px" OnClientClick="javascript:habilitaDiv();return false;" />
                                        &nbsp;&nbsp;
                                        <asp:Button ID="btnNuevoBalance" runat="server" Text="Nuevo balance" Width="110px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="esp">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
                <asp:Label ID="lblNoBalance" runat="server" CssClass="alerta" Visible="False"></asp:Label>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td class="texto" runat="server" id="TD_DatosBalance">
                <table cellpadding="0" cellspacing="0" class="bordes" style="width: 100%">
                    <tr class="grdTitulo">
                        <td class="esp">
                            &nbsp;
                        </td>
                        <td>
                            Datos de balance
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="esp">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="esp">
                            &nbsp;
                        </td>
                        <td>
                            <table style="width: 100%">
                                <tr>
                                    <td valign="top">
                                        <asp:GridView ID="gvCamposIngresos" runat="server" AutoGenerateColumns="False" ForeColor="#333333"
                                            Width="90%">
                                            <Columns>
                                                <asp:BoundField DataField="campo" HeaderText="Ingresos">
                                                    <HeaderStyle CssClass="grdPrimeraColumna" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtValor" runat="server" onKeyUp="SumaTotales('ctl00_ContentPlaceHolder1_gvCamposIngresos','ctl00_ContentPlaceHolder1_lblIngresosTotales','','gvCamposIngresos');"
                                                            Text='<%# eval("PropValor","{0:N}") %>' Style="height: 20px; font-size: small"></asp:TextBox>
                                                        &nbsp;<asp:Label ID="lblNombre" runat="server" Text='<%# eval("PropIdCampo") %>'
                                                            Style="display: none"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="grdSubtitulo" />
                                        </asp:GridView>
                                    </td>
                                    <td valign="top">
                                        <asp:GridView ID="gvCamposEgresos" runat="server" AutoGenerateColumns="False" ForeColor="#333333"
                                            Width="90%">
                                            <Columns>
                                                <asp:BoundField DataField="campo" HeaderText="Egresos">
                                                    <HeaderStyle CssClass="grdPrimeraColumna" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtValor0" runat="server" onKeyUp="SumaTotales('ctl00_ContentPlaceHolder1_gvCamposEgresos','ctl00_ContentPlaceHolder1_lblEgresosTotales','0','gvCamposEgresos');"
                                                            Text='<%# eval("PropValor","{0:N}") %>' CssClass="textBoxGrid"></asp:TextBox>
                                                        &nbsp;<asp:Label ID="lblNombre0" runat="server" Text='<%# eval("PropIdCampo") %>'
                                                            Style="display: none"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="grdSubtitulo" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style="font-weight: bold;">
                                        Ingresos totales:&nbsp;
                                        <asp:Label ID="lblIngresosTotales" runat="server" Text="0"></asp:Label>
                                    </td>
                                    <td valign="top" style="font-weight: bold">
                                        Egresos totales:&nbsp;
                                        <asp:Label ID="lblEgresosTotales" runat="server" Text="0"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:GridView ID="gvCamposActivos" runat="server" AutoGenerateColumns="False" ForeColor="#333333"
                                            Width="90%">
                                            <Columns>
                                                <asp:BoundField DataField="campo" HeaderText="Activos">
                                                    <HeaderStyle CssClass="grdPrimeraColumna" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtValor1" runat="server" onKeyUp="SumaTotales('ctl00_ContentPlaceHolder1_gvCamposActivos','ctl00_ContentPlaceHolder1_lblActivosTotales','1','gvCamposActivos');"
                                                            Text='<%# eval("PropValor","{0:N}") %>' CssClass="textBoxGrid"></asp:TextBox>
                                                        &nbsp;<asp:Label ID="lblNombre1" runat="server" Text='<%# eval("PropIdCampo") %>'
                                                            Style="display: none"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="grdSubtitulo" />
                                        </asp:GridView>
                                    </td>
                                    <td valign="top">
                                        <asp:GridView ID="gvCamposPasivos" runat="server" AutoGenerateColumns="False" ForeColor="#333333"
                                            Width="90%">
                                            <Columns>
                                                <asp:BoundField DataField="campo" HeaderText="Pasivos">
                                                    <HeaderStyle CssClass="grdPrimeraColumna" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtValor2" runat="server" onKeyUp="SumaTotales('ctl00_ContentPlaceHolder1_gvCamposPasivos','ctl00_ContentPlaceHolder1_lblPasivosTotales','2','gvCamposPasivos');"
                                                            Text='<%# eval("PropValor","{0:N}") %>' CssClass="textBoxGrid"></asp:TextBox>
                                                        &nbsp;<asp:Label ID="lblNombre2" runat="server" Text='<%# eval("PropIdCampo") %>'
                                                            Style="display: none"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="grdSubtitulo" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style="font-weight: bold;">
                                        Activos totales:&nbsp;
                                        <asp:Label ID="lblActivosTotales" runat="server" Text="0"></asp:Label>
                                    </td>
                                    <td valign="top" style="font-weight: 700">
                                        Pasivos totales:&nbsp;
                                        <asp:Label ID="lblPasivosTotales" runat="server" Text="0"></asp:Label>
                                        &nbsp;&nbsp;&nbsp; Deudas seg�n bur�:<asp:Label ID="lblDeudasBuro" runat="server">0</asp:Label>
                                        &nbsp;&nbsp;
                                        <asp:LinkButton ID="btnActualizar" runat="server">Actualizar</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style="font-weight: bold;">
                                        &nbsp;
                                    </td>
                                    <td valign="top" style="font-weight: 700">
                                        Cuotas seg�n buro:
                                        <asp:Label ID="lblCuotasBuro" runat="server"></asp:Label>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td align="center">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <table cellpadding="4" cellspacing="4" width="100%" class=" bordes1">
                                <tr>
                                    <td style="width: 450px">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td id="tdPatrimonio" runat="server" class="subtitulo" style="width: 80px">
                                                    Patrimonio:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPatrimonio" runat="server"></asp:Label>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <asp:Button ID="btnUsar" runat="server" Text="Usar deuda cliente" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="tdDisponible" runat="server" class="subtitulo" style="width: 80px">
                                                    Disponible:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDisponibleUsar" runat="server"></asp:Label>
                                                    <asp:Button ID="btnUsarCuotaCliente" runat="server" Text="Usar cuota cliente" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width="60%">
                                            <tr id="tdDispSolCliente" runat="server">
                                                <td >
                                                   <span class="subtitulo"  >Disponible seg�n solicitud:</span>&nbsp;
                                                   
                                                </td>
                                                <td>
                                                <div id="DS" style="position: relative" >0</div>
                                                </td>
                                            </tr>
                                            <tr id="tdDisponibleCliente" runat="server">
                                                <td class="subtitulo" style="width: 210px">
                                                    Disponible con Buro:
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblDisponible" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="tdPatrimonioCliente" runat="server">
                                                <td class="subtitulo">
                                                    Patrimonio:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPatrimonioCliente" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td align="center">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <table>
                    <tr>
                        <td style="margin-left: 40px">
                            <asp:Button ID="btnGuardarTodo" runat="server" Text="Continuar" Height="25px" />
                        </td>
                        <td>
                            <asp:Button ID="btnContinuar" runat="server" Text="Continuar" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td class="esp">
                &nbsp;
            </td>
        </tr>
    </table>
    <div id="divObservaciones" style="display: none;">
        <div>
            <table class="grdTitulo" width="500px">
                <tr>
                    <td style="width: 478px">
                        <asp:Label ID="lblObservacion" runat="server"></asp:Label>
                    </td>
                    <td style="text-align: right">
                        <asp:LinkButton ID="lBtnCerrar" runat="server" OnClientClick="javascript:ocultaDiv();return false;"
                            CssClass="link" ForeColor="White">X</asp:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
        <table cellpadding="1" class="bordes" cellspacing="1" style="width: 500px">
            <tr>
                <td class="esp">
                    &nbsp;
                </td>
                <td style="width: 175px">
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="subtitulo" style="width: 175px" valign="top">
                    Observaciones:
                </td>
                <td>
                    <asp:TextBox ID="txtObservacion" runat="server" Height="53px" TextMode="MultiLine"
                        Width="280px"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="esp">
                    &nbsp;
                </td>
                <td style="width: 175px">
                </td>
                <td class="subtitulo">
                    <asp:Label ID="lblErrorObservaciones" runat="server" Text="No se ha ingresado observacion"
                        Visible="False" CssClass="alerta"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="esp">
                    &nbsp;
                </td>
                <td class="subtitulo" style="width: 175px">
                    &nbsp;
                </td>
                <td>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td style="width: 175px">
                </td>
                <td>
                    <asp:Button ID="btnGuardarObservacion" runat="server" Text="Guardar" Width="70px"
                        Style="height: 26px" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                    <asp:Button ID="btnCancelar" runat="server" OnClientClick="javascript:ocultaDiv();return false;"
                        Text="Cancelar" Width="70px" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>

    <script type="text/javascript">
        function ocultaDiv() {
            var div = document.getElementById('divObservaciones');
            div.style.display = "none";
        }
        function noenter(evento) {
            return !(evento.keyCode == 13);
        }
        ocultaDiv();

        function calcula() {
            var patrimonio;
            var ingresos = parseFloat(document.getElementById('ctl00_ContentPlaceHolder1_lblIngresosTotales').innerHTML.replace(",", ""));
            var egresos = parseFloat(document.getElementById('ctl00_ContentPlaceHolder1_lblEgresosTotales').innerHTML.replace(",", ""));
            var activos = parseFloat(document.getElementById('ctl00_ContentPlaceHolder1_lblActivosTotales').innerHTML.replace(",", ""));
            var pasivos = parseFloat(document.getElementById('ctl00_ContentPlaceHolder1_lblPasivosTotales').innerHTML.replace(",", ""));
            var dBuro = parseFloat(document.getElementById('ctl00_ContentPlaceHolder1_lblDeudasBuro').innerHTML.replace(",", ""));
            var cuotasBuro = parseFloat(document.getElementById('ctl00_ContentPlaceHolder1_lblCuotasBuro').innerHTML.replace(",", ""));
            var valorUsar; 
            var cuotasCliente=parseFloat(document.getElementById('ctl00_ContentPlaceHolder1_gvCamposEgresos_ctl04_txtValor0').value.replace(",", ""));
            //calculo patrimonio
            if (dBuro > pasivos) {
                patrimonio = activos - dBuro;
            }
            else {
                patrimonio = activos - pasivos;
            }


            document.getElementById('ctl00_ContentPlaceHolder1_lblPatrimonioCliente').innerHTML = patrimonio;
            
            if (patrimonio < 0) {
                document.getElementById('ctl00_ContentPlaceHolder1_lblPatrimonioCliente').style.color = "red";
            }
            else {
                document.getElementById('ctl00_ContentPlaceHolder1_lblPatrimonioCliente').style.color = "";

            }
            //Calculo el disponible
          
            var mayor;
            var Ausar;
            var disponible;
            var disponibleSol;
            if (cuotasCliente > cuotasBuro ){
            mayor=cuotasCliente; 
            }else
            {
            mayor = cuotasBuro
            }
            
            Ausar = egresos - cuotasCliente + mayor
            disponible=ingresos - Ausar;
            disponibleSol =ingresos -egresos;
                        
            document.getElementById('ctl00_ContentPlaceHolder1_lblDisponible').innerHTML = redondeo2decimales(disponible);
            document.getElementById('DS').innerHTML = redondeo2decimales(disponibleSol);
            
            if (disponible < 0) {
                document.getElementById('ctl00_ContentPlaceHolder1_lblDisponible').style.color = "red";
            }
            else {
                document.getElementById('ctl00_ContentPlaceHolder1_lblDisponible').style.color = "";

            }
            
            if (disponibleSol < 0) {
                document.getElementById('DS').style.color = "red";
            }
            else {
                document.getElementById('DS').style.color = "";

            }
            
            

        }
        function redondeo2decimales(numero)
{
	var original=parseFloat(numero);
	var result=Math.round(original*100)/100 ;
	return result;
}
        
        window.setInterval("calcula()", 100);
        
    </script>

</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="head">
</asp:Content>
