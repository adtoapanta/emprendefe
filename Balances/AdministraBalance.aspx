<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MPModulo.Master" CodeBehind="AdministraBalance.aspx.vb" Inherits="Balances.CreaBalance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="esp">
                &nbsp;</td>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td class="titulo">
                CONFIGURACION DE&nbsp; BALANCE</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td style="text-align: right" >
                <asp:HyperLink ID="lnkConfigVentas" runat="server" CssClass="link">Configura ventas</asp:HyperLink>
                &nbsp;&nbsp;&nbsp;
                <asp:HyperLink ID="lnkConfRangos" runat="server" CssClass="link">Configurar capacidad de pago</asp:HyperLink>
            &nbsp;&nbsp;
                <asp:HyperLink ID="lnkConfiguraPatrimonio" runat="server" CssClass="link">Configura 
                rangos patrimonio</asp:HyperLink>
&nbsp;
                <asp:HyperLink ID="lnkRangosNicelEndeudamiento" runat="server" CssClass="link">Configura 
                rangos nivel de endeudamiento</asp:HyperLink>
&nbsp;
                <asp:HyperLink ID="lnkRangosApalancamiento" runat="server" CssClass="link">Configura 
                rangos apalancamiento</asp:HyperLink>
&nbsp;&nbsp;&nbsp;
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td class="titulo">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td >
                                                    <table width="100%" >
                                                        <tr>
                                                            <td class="subtitulo" style=" width:150px"  >
                                                                Nombre balance:</td>
                                                            <td>
                                                                <asp:Label ID="lblNombre" runat="server" CssClass="subtitulo"></asp:Label>
                                                            </td>
                                                            <td class="subtitulo">
                                                                Descripci�n</td>
                                                            <td>
                                                                <asp:Label ID="lblDescripcion" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="subtitulo">
                                                                Fecha de creaci�n:&nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblFechaCreacion" runat="server" class="subtitulo"></asp:Label>
                                                            </td>
                                                            <td>
                                                                &nbsp;</td>
                                                            <td>
                                                                &nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="esp">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td class="texto">
                <table cellpadding="0" cellspacing="0" class="bordes" style="width: 100%">
                    <tr class="grdTitulo">
                        <td>
                            &nbsp;</td>
                        <td>
                            Balance existente</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="esp">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td style="text-align: right">
                                                    <asp:HyperLink ID="lnkNuevoRequisito"  NavigateUrl  ="javascript:abrirIngreso('divNuevoDocumento', 370, 100);"
                                                        runat="server" CssClass="link">Nuevo campo</asp:HyperLink>
                                                                                                    </td>
                        <td class="esp">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                        <ContentTemplate>
                                                            <asp:TreeView ID="trvCampos" runat="server" ImageSet="Arrows" 
                                                        ShowCheckBoxes="Leaf">
                                                            </asp:TreeView>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="btnNuevoCampo" EventName="Click" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                        <td class="esp">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td class="esp">
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                                        <asp:Button ID="btnGuardar" runat="server" CssClass="boton" Text="Guardar" />
                                    </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="esp">
                &nbsp;</td>
        </tr>
    </table>
    
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate >
    <div id="divNuevoDocumento"  style=" display:none;     border-style: none; border-color: inherit; border-width: 2px; background-color: #EEEEEE;
                            position: absolute">
                            <div>
                                <table class="grdTitulo" width="500px">
                                    <tr>
                                        <td style="width: 470px">
                                            &nbsp; Nuevo campo
                                        </td>
                                        <td style="text-align: right"><a class="grdTitulo"   href="javascript:ocultaObj('divNuevoDocumento')">X</a> &nbsp; </td>
                                        
                                    </tr>
                                </table>
                            </div>
    <table id="Table1" border="0" cellpadding="0" cellspacing="0" class="bordes" width="500px">
                                           
                                            <tr>
                                                <td class="esp">
                                                    &nbsp;</td>
                                                <td style="text-align: right">
                                                    <asp:Label ID="lblError" runat="server"></asp:Label>
                                                    &nbsp;&nbsp;</td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="esp">
                                                    &nbsp;</td>
                                                <td >
                                                    <table cellpadding="5px" cellspacing="5px"   >
                                                        <tr>
                                                            <td class="texto" >
                                                                Nombre:</td>
                                                            <td>
                                                                <asp:TextBox ID="txtNombre" runat="server" Width="263px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="texto"   >
                                                                Tipo de Requisito:</td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlTipo" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                       
                                                           <tr>
                                                           <td>
                                                               &nbsp;</td>
                                                           <td>
                <asp:Button ID="btnNuevoCampo" runat="server" Text="Guardar" OnClientClick ="javascript:ocultaObj('divNuevoDocumento')"   />
                                                           </td>
                                                           </tr>                                                     
                                                    </table>
                                                                                                    </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                                <td class="esp">
                                                </td>
                                            </tr>
                                            </table>
    </div>
    </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnNuevoCampo" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    
</asp:Content>
