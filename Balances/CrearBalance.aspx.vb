﻿'Public Partial Class CrearBalance
'    Inherits System.Web.UI.Page

'    Private IdBalanceURL As Integer
'    Private IdModeloBalance As Integer
'    Private idSujeto As Integer
'    Private idFuncionario As Integer

'    Private MiBalance As Balance

'    'Private idEtapaActual As SacLib.EtapasSolicitud

'    'Private MiSujeto As SACFinca.Sujeto


'    Private Enum Operaciones As Integer
'        nuevo = 1
'        editar = 2
'    End Enum

'    Private Property idBalance()
'        Get
'            If Me.IdBalanceURL = 0 Then
'                Return Me.ViewState("idBalance")
'            Else
'                Return Me.IdBalanceURL
'            End If
'        End Get
'        Set(ByVal value)
'            Me.ViewState("idBalance") = value
'        End Set
'    End Property

'    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Dim nivel As Long

'        Me.IdBalanceURL = Me.Request("idRef")
'        Me.idSujeto = Me.Request("idSuj")
'        Me.IdModeloBalance = Me.Request("idMod")

'        'puede venir idBalance o idTipoBalance, siempre viene idSolicitud
'        If vUsuarios.web.TraeUsuarioRegistrado.nivel = 0 Then SacLib.GoInicio()

'        Me.idFuncionario = SacLib.cookieSAC.idFuncionario

'        ''Determino en que etapa estoy:
'        ''Me queda la duda de si obtener la etapa de la solicitudo segun usuario...por eso la vuelta con 2 variables

'        nivel = vUsuarios.web.TraeUsuarioRegistrado().nivel
'        If nivel = 4 Then idEtapaActual = SacLib.EtapasSolicitud.Digitación Else idEtapaActual = SacLib.EtapasSolicitud.Aprobación
'        Try
'            htcLib.espacio.inicializa(SacLib.GetConexion, htcLib.tiposConexion.Web)
'            Me.creaSujeto()

'            If Not IsPostBack Then


'                Me.datosSolicitud1.llenaDatos(Me.MiSujeto.idSolicitud, False)

'                refresca()
'            End If

'            Catch ex As Exception
'            Me.Response.Write("Error : " & ex.Message)
'        Finally
'            htcLib.espacio.cerrar()
'        End Try



'    End Sub

'    Private Sub creaSujeto()

'        Me.MiSujeto = New SACFinca.Sujeto(Me.idSujeto)
'        'si es garante mustro un label que dice garante
'        If MiSujeto.idTipoSujeto = 3 Then
'            Me.lblGarante.Text = "del Garante"
'        End If
'    End Sub


'    ''' <summary>
'    ''' Muestra y esconde controles y opciones segun etapa, permisos, etc
'    ''' </summary>
'    ''' <remarks></remarks>
'    Private Sub permisosYAccesos()
'        Dim pE As Long
'        Dim usarCuotaBuro As Boolean
'        pE = Me.Request("edtmdl")

'        If pE = 1 Then idEtapaActual = SacLib.EtapasSolicitud.Aprobación

'        Me.btnUsar.Visible = False
'        Me.btnUsarCuotaCliente.Visible = False

'        Select Case idEtapaActual
'            Case SacLib.EtapasSolicitud.Digitación
'                usarCuotaBuro = My.Settings.usarCuotaBuro

'                If Not usarCuotaBuro Then Me.tdDisponibleCliente.Style.Value = "display:none"

'                'si soy digitador oculto lo de patrimonio
'                Me.tdPatrimonio.Visible = False
'                Me.tdDisponible.Visible = False
'                Me.lblPatrimonio.Visible = False
'                Me.lblDisponibleUsar.Visible = False
'                Me.btnGuardarTodo.Visible = True
'                Me.btnContinuar.Visible = False

'                'Verifico acesos según el balance y políticas
'                If MiBalance.editable Then
'                    'Si el balance es un balance vacío, nuevo, lo dejo editar, no muestro botones.
'                    Bloquear(False)
'                    Me.btnEditar.Visible = False
'                    Me.btnNuevoBalance.Visible = False
'                Else
'                    'Si es un balance antiguo, lo bloqueo, y solo puede editar si aplasta el boton
'                    Bloquear(True)
'                    'Todo No dejar editar si el balance esta usado en otra solicitud
'                    If Me.idBalance = 0 Then
'                        Me.btnEditar.Visible = False
'                    Else
'                        Me.btnEditar.Visible = True
'                    End If
'                    Me.btnNuevoBalance.Visible = True

'                    Me.lblFecha.Visible = True



'                End If

'            Case Else 'Case SacLib.EtapasSolicitud.Aprobación, SacLib.EtapasSolicitud.Tramitado, SacLib.EtapasSolicitud.Archivado
'                Me.tdPatrimonioCliente.Style.Value = "display:none"
'                Me.tdDisponibleCliente.Style.Value = "display:none"
'                Me.tdDispSolCliente.Style.Value = "display:none"
'                Bloquear(True)
'                Me.lblFecha.Visible = True
'                Me.btnGuardarTodo.Visible = False
'                Me.btnEditar.Visible = False
'                Me.btnNuevoBalance.Visible = False

'                Me.btnContinuar.Visible = True

'                'Boton para cambiar y que se use siempre la información del cliennte NO la del buró
'                If vUsuarios.web.TraeUsuarioRegistrado.tengoPermiso(16) And My.Settings.usarBuro = True Then
'                    Me.btnUsar.Visible = True
'                    Me.btnUsarCuotaCliente.Visible = True
'                Else
'                    Me.btnUsar.Visible = False
'                    Me.btnUsarCuotaCliente.Visible = False
'                End If


'        End Select



'    End Sub


'    Sub refresca(Optional ByVal forzarEditar As Boolean = False)



'        Me.cargaBalance()

'        If forzarEditar Then Me.MiBalance.editable = True

'        CargarDatosGenerales()
'        LlenarBalance()

'        permisosYAccesos()


'    End Sub


'    ''' <summary>
'    ''' Instancia el objeto global Balance
'    ''' </summary>
'    ''' <remarks>No sabemos si viene idBalance o idModelo por lo que comprobamos eso e instanciamos el objeto según
'    ''' </remarks>
'    Sub cargaBalance()
'        Dim pE As Long = Me.Request("edtmdl")

'        If Me.IdBalance > 0 Then
'            Me.MiBalance = New Balance(Me.idBalance, Me.MiSujeto)

'        ElseIf Me.IdModeloBalance > 0 Then
'            Me.MiBalance = New Balance(Me.MiSujeto, Me.IdModeloBalance, False)
'            'Se crea un balance este rato, o se carga uno antiguo. Así que paso ese id para usarlo al guardar
'            Me.idBalance = MiBalance.id

'        Else
'            Response.Write("ERROR EXTRAÑO")
'        End If
'        'Si estoy en digitacion se actualiza siempre este valor.
'        If pE = 1 Then
'            Me.MiBalance.pegaDatosDesdeBuro(Me.MiSujeto)
'        Else
'            Me.MiBalance.pegaDatosDesdeBuro(Me.MiSujeto, True)
'        End If

'    End Sub


'    ''' <summary>
'    ''' llena datos generales balance
'    ''' </summary>
'    ''' <remarks></remarks>
'    Sub CargarDatosGenerales()


'        With Me.MiBalance

'            'Me.lblCliente.Text = nombreCliente
'            Me.lblTipoBalance.Text = .txtModelo

'            If .editable Then
'                Me.fcFecha.Fecha = Today

'            Else
'                Me.fcFecha.Fecha = .fecha

'                Me.rblProyectado.SelectedValue = .proyectado

'                Me.lblFecha.Text = .fecha

'            End If

'        End With

'    End Sub

'    ''' <summary>
'    ''' Llena grids con los datos del balance
'    ''' </summary>
'    ''' <remarks></remarks>
'    Sub LlenarBalance()

'        'llena los gridview desde los array de balance
'        Me.gvCamposIngresos.DataSource = Me.MiBalance.ArrayIngresos
'        Me.gvCamposIngresos.DataBind()

'        Me.gvCamposEgresos.DataSource = Me.MiBalance.ArrayEgresos
'        Me.gvCamposEgresos.DataBind()

'        Me.gvCamposActivos.DataSource = Me.MiBalance.ArrayActivos
'        Me.gvCamposActivos.DataBind()

'        Me.gvCamposPasivos.DataSource = Me.MiBalance.ArrayPasivos
'        Me.gvCamposPasivos.DataBind()

'        'llena totales
'        Me.lblIngresosTotales.Text = MiBalance.IngresosTotales.ToString("n2")
'        Me.lblEgresosTotales.Text = MiBalance.EgresosTotales.ToString("n2")
'        Me.lblActivosTotales.Text = MiBalance.ActivosTotales.ToString("n2")
'        Me.lblPasivosTotales.Text = MiBalance.PasivosTotales.ToString("n2")
'        Me.lblDeudasBuro.Text = MiBalance.deudasBuro.ToString("n2")
'        Me.lblCuotasBuro.Text = MiBalance.cuotasBuro.ToString("n2")

'        Me.lblPatrimonio.Text = MiBalance.Patrimonio.ToString("n2")
'        'JFR 05/31/2013 SE COMENTA PARA CALCULAR DISPONIBLE CON 50% OTROS INGRESOS
'        Me.lblDisponibleUsar.Text = (MiBalance.IngresosTotales - MiBalance.egresoAUsar).ToString("n2")
'        'Me.lblDisponibleUsar.Text = (MiBalance.IngTot50x100OtrosIngresos - MiBalance.egresoAUsar).ToString("n2")

'    End Sub

'    Protected Sub btnGuardarTodo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardarTodo.Click
'        Dim hayError As Boolean

'        Try
'            htcLib.espacio.inicializa(SacLib.GetConexion, htcLib.tiposConexion.Web)

'            If Me.verificaFechaBalance Then

'                Me.cargaBalance()

'                'guardo y muestro primero los datos Generales
'                pasaDatosGenerales()

'                'guardo y muestro los datos del balance
'                pasoDatosBalance()

'                'calculo el balance y guardo todo
'                Me.MiBalance.guardaResumen()
'            Else
'                Me.lblErrorFecha.Visible = True
'                hayError = True
'            End If

'        Catch ex As Exception
'            Me.Response.Write("Error : " & ex.Message)
'            hayError = True
'        Finally
'            htcLib.espacio.cerrar()
'        End Try

'        If Not hayError Then SACFinca.AdministradorComponenteSujeto.Redireccionar()

'    End Sub

'    Private Function verificaFechaBalance() As Boolean
'        Dim fecha As Date = Me.fcFecha.Fecha

'        If fecha > Today Then
'            Return False

'        End If

'        Return True

'    End Function

'    Sub pasaDatosGenerales()

'        Dim nuevo As Boolean = False

'        With Me.MiBalance

'            .proyectado = Me.rblProyectado.SelectedValue

'            .fecha = Me.fcFecha.Fecha

'            '.Guardar()

'        End With


'    End Sub


'    ''' <summary>
'    ''' Pasa los datos de un grid y los guarda. Modularmente se pueden pasar los 4 grids.
'    ''' </summary>
'    ''' <param name="grid"></param>
'    ''' <param name="num"></param>
'    ''' <remarks></remarks>
'    Private Sub GuardadatosGrid(ByVal grid As GridView, ByVal num As String)

'        For Each dgr As GridViewRow In grid.Rows
'            'creo una instancia del control label donde está el id del codigo
'            Dim miLabel As Label = dgr.FindControl("lblNombre" & num)
'            Dim idCampo As Integer = miLabel.Text

'            'creo una instancia del control textbox donde está el valor
'            Dim miText As TextBox = dgr.FindControl("txtValor" & num)
'            Dim valor As Decimal
'            If miText.Text = "" Then
'                valor = 0
'            Else
'                valor = miText.Text
'            End If

'            'cambio el valor en datosBalance
'            Dim miDatoBalance As New DatosBalance(idCampo, Me.IdBalance)

'            With miDatoBalance
'                .idBalance = Me.IdBalance
'                .idCampo = idCampo
'                .valor = valor

'                .guardar()
'            End With
'        Next

'    End Sub


'    Sub pasoDatosBalance()

'        'INGRESOS
'        Me.GuardadatosGrid(Me.gvCamposIngresos, "")

'        'EGRESOS
'        Me.GuardadatosGrid(Me.gvCamposEgresos, "0")

'        'ACTIVOS
'        Me.GuardadatosGrid(Me.gvCamposActivos, "1")

'        'PASIVOS
'        Me.GuardadatosGrid(Me.gvCamposPasivos, "2")

'    End Sub


'    ''' <summary>
'    ''' Crea un balance nuevo para esa solicitud (cliente)
'    ''' </summary>
'    ''' <param name="sender"></param>
'    ''' <param name="e"></param>
'    ''' <remarks></remarks>
'    Protected Sub btnNuevoBalance_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevoBalance.Click

'        Dim idNuevo As Long
'        Dim url As String
'        Dim huboError As Boolean = False
'        Dim redir As String
'        Try

'            htcLib.espacio.inicializa(SacLib.GetConexion, htcLib.tiposConexion.Web)

'            'si no tengo el idModelo lo obtengo del balance
'            If Me.IdModeloBalance = 0 Then
'                Me.MiBalance = New Balance(Me.IdBalanceURL, Me.MiSujeto)
'                Me.IdModeloBalance = MiBalance.idModelo
'            End If

'            'creo un balance nuevo, forzando nuevo
'            Me.MiBalance = New Balance(Me.MiSujeto, Me.IdModeloBalance, True)
'            idNuevo = Me.MiBalance.id
'            Me.tdNumSol.Visible = True
'            redir = Me.Server.UrlEncode(Me.Request("redir"))
'            'Para evitarme líos, me redirecciono a la página limpia, con este id de balance
'            'Le mando una variable para que me deje editar sin molestar
'            url = String.Format("crearBalance.aspx?idRef={0}&idSuj={1}&redir={2}", idNuevo, Me.MiSujeto.id, redir)

'        Catch ex As Exception
'            Me.Response.Write("Error : " & ex.Message)
'            huboError = True
'        Finally
'            htcLib.espacio.cerrar()
'        End Try


'        If Not huboError Then Response.Redirect(url)



'    End Sub

'    ''' <summary>
'    ''' guarda la bitácora de cambio y deja habilitados los campos para ser editados
'    ''' </summary>
'    Protected Sub btnGuardarObservacion_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardarObservacion.Click

'        Dim url As String

'        Try
'            'aquí guardo la bitácora
'            htcLib.espacio.inicializa(SacLib.GetConexion, htcLib.tiposConexion.Web)


'            If Me.txtObservacion.Text <> "" Then
'                Me.lblObservacion.Visible = False
'                Me.refresca(True)
'                'Bloqueo el campo 1000, que no se debe poder cambiar
'                Me.bloquearCampo1000()
'                SACFinca.Bitacora.creaBitacora(Me.MiSujeto.idSolicitud, SACFinca.Eventos.CambioBalance, , Me.txtObservacion.Text)
'            Else

'            End If

'        Catch ex As Exception
'            Me.Response.Write("Error : " & ex.Message)
'        Finally
'            htcLib.espacio.cerrar()
'        End Try

'    End Sub

'    ''' <summary>
'    ''' Revisa todos los controles y a algunos de ellos los deshabilita
'    ''' </summary>
'    ''' <param name="block">para bloquear o no</param>
'    ''' <remarks>si bloquea es true deshabilita</remarks>
'    Sub Bloquear(Optional ByVal block As Boolean = True)

'        Dim siBlock As Boolean

'        If block Then
'            siBlock = False
'        Else
'            siBlock = True
'        End If

'        Dim miControl As Control = Me.Master.FindControl("ContentPlaceHolder1")

'        If miControl.HasControls Then

'            For Each miChildControl As Control In miControl.Controls

'                'Bloqueo todos los controles segun el tipo
'                If TypeOf miChildControl Is TextBox Then
'                    Dim miText As TextBox
'                    miText = CType(miChildControl, TextBox)
'                    If miText.ID <> "txtObservacion" Then
'                        miText.ReadOnly = Not siBlock
'                    End If
'                End If

'                If TypeOf miChildControl Is CheckBox Then
'                    Dim miCheck As CheckBox
'                    miCheck = CType(miChildControl, CheckBox)
'                    miCheck.Enabled = siBlock
'                End If

'                If TypeOf miChildControl Is DropDownList Then
'                    Dim miDdl As DropDownList
'                    miDdl = CType(miChildControl, DropDownList)
'                    miDdl.Enabled = siBlock
'                End If

'                If TypeOf miChildControl Is RadioButton Then
'                    Dim miRadioButton As RadioButton
'                    miRadioButton = CType(miChildControl, RadioButton)
'                    miRadioButton.Enabled = siBlock
'                End If

'                If TypeOf miChildControl Is RadioButtonList Then
'                    Dim miRadioButtonList As RadioButtonList
'                    miRadioButtonList = CType(miChildControl, RadioButtonList)
'                    miRadioButtonList.Enabled = siBlock
'                End If

'            Next

'            'Bloqueo grids y calendarios
'            Me.gvCamposActivos.Enabled = siBlock
'            Me.gvCamposEgresos.Enabled = siBlock
'            Me.gvCamposIngresos.Enabled = siBlock
'            Me.gvCamposPasivos.Enabled = siBlock


'            Me.fcFecha.Visible = siBlock

'        End If

'    End Sub


'    Protected Sub btnContinuar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnContinuar.Click

'        Response.Redirect(SacLib.GetRegresarModulos())

'    End Sub


'    Protected Sub btnUsar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUsar.Click

'        Try
'            htcLib.espacio.inicializa(SacLib.GetConexion, htcLib.tiposConexion.Web)
'            Me.MiBalance = New Balance(Me.idBalance, Me.MiSujeto)

'            Me.MiBalance.usarDeudaCliente = True
'            Me.MiBalance.guardaResumen()

'            refresca()

'        Catch ex As Exception
'            Me.Response.Write("Error : " & ex.Message)
'        Finally
'            htcLib.espacio.cerrar()
'        End Try

'    End Sub
'    Sub bloquearCampo1000()
'        Dim r As System.Web.UI.WebControls.GridViewRow

'        For Each r In Me.gvCamposPasivos.Rows
'            If CType(r.FindControl("lblNombre2"), Label).Text = 1000 Then
'                r.Enabled = False
'            End If
'        Next

'    End Sub
'    Protected Sub btnActualizar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnActualizar.Click
'        Dim miBalance = New Balance(Me.idBalance, Me.MiSujeto)
'        miBalance.pegaDatosDesdeBuro(Me.MiSujeto, True)
'        miBalance.guardaResumen()
'        Me.refresca()

'    End Sub



'    Protected Sub btnUsarCuotaCliente_Click1(ByVal sender As Object, ByVal e As EventArgs) Handles btnUsarCuotaCliente.Click
'        Try
'            htcLib.espacio.inicializa(SacLib.GetConexion, htcLib.tiposConexion.Web)
'            Me.MiBalance = New Balance(Me.idBalance, Me.MiSujeto)

'            Me.MiBalance.usarCuotaCliente = True
'            Me.MiBalance.guardaResumen()

'            refresca()

'        Catch ex As Exception
'            Me.Response.Write("Error : " & ex.Message)
'        Finally
'            htcLib.espacio.cerrar()
'        End Try

'    End Sub

'    Private Sub gvCamposEgresos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCamposEgresos.RowDataBound
'        Dim lblIDCampo As Label
'        Dim idCampo As Integer
'        Dim txtValor As TextBox

'        If e.Row.RowType = DataControlRowType.DataRow Then
'            lblIDCampo = e.Row.FindControl("lblNombre0")
'            txtValor = e.Row.FindControl("txtValor0")
'            idCampo = lblIDCampo.Text

'            If idCampo = 15 Then txtValor.Enabled = False

'        End If

'    End Sub
'End Class