function redimTBL(){
var Tam = TamVentana();
  var y = 0;
  y = parseInt(Tam[1])-30;
  document.getElementById("ctl00_tblMaster").style.height = y + 'px';
}

window.onload = function(){
redimTBL();
}
 
window.onresize = function() {
  redimTBL();
};

var txtObj; //Objeto global de manejo de focus
function refrescaIngreso(NObj){
    editor.closePanel(0);
    window.setTimeout("editor.openPanel(0)",500);
    
    txtObj = getObj(contenido(NObj));
    window.setTimeout("enfoca()",500);
    
}

function enfoca(){

    try{
     txtObj.focus();
    }
    catch(e){
     editor.openPanel(0);
     window.setTimeout("enfoca()",400);
    }

}

//carga flujos manuales Fijas
function cargaFM(id, fecha, valor, interes)
{

 refrescaIngreso("txtFFecha");

 if (id!=0){
     getObj(contenido("btnAgregar")).value = "Cambiar"; 
     document.getElementById("ctl00_ContentPlaceHolder1_txtFFecha").value = fecha;
 }else{
    getObj(contenido("btnAgregar")).value = "Agregar";
    document.getElementById("ctl00_ContentPlaceHolder1_txtFFecha").value = new Date().toDDMMYYYYString();
 }
 
 document.getElementById("ctl00_ContentPlaceHolder1_txtIdFM").value = id;
 document.getElementById("ctl00_ContentPlaceHolder1_txtFValor").value = valor;
 document.getElementById("ctl00_ContentPlaceHolder1_txtFInteres").value = interes;
 getObj(contenido("btnAgregar")).value = "Guardar";
 
 }
//carga Grupo de flujos manuales
function cargaGrupo(id, nombre, sensible, clasificacion, tipoDeFlujoManual){

  var grupoAceptado = true;

 if (sensible == 'True'){
        
     if (tipoDeFlujoManual < 2){
        deshabilita(contenido('rdbPeriodico'));
        sugiere("lblAviso", "Aviso: Los grupos 'sensibles' no se pueden asignar a flujos periodicos");
        getObj(contenido('rdbFijo')).checked = 'checked';
        getObj(contenido('rdbPeriodico')).checked = '';
        getObj(contenido("tblDatosSensibles")).style.display = "";
     }else{
        alert("Error: flujos manuales peri\xF3dicos no pueden ser asignados a grupos sensibles.\nPara asignar a \xE9ste grupo, primero debe cambiarlo a flujo 'Fijo'");
        grupoAceptado = false;
     }
    
 }else{
 
     habilita(contenido('rdbPeriodico'));
     getObj(contenido("tblDatosSensibles")).style.display = "none";
 }

if (grupoAceptado ){
 
     document.getElementById("ctl00_ContentPlaceHolder1_lblProducto").innerHTML = nombre;
     document.getElementById("ctl00_ContentPlaceHolder1_txtIdP").value = id;
     
     if (clasificacion == 2){
         if (tipoDeFlujoManual == 1){
            sugiere("lblSugerenciaFijos", "Se sugiere utilizar negativos (-) para el monto.");
         }else{
           if (tipoDeFlujoManual > 1)
            sugiere("lblSugerenciaModelados", "Se sugiere utilizar negativos (-) para el monto.");
         }
     }else{
         if (tipoDeFlujoManual == 1){
            sugiere("lblSugerenciaFijos", "Se sugiere utilizar positivos para el monto.");
         }else{
           if (tipoDeFlujoManual > 1)
            sugiere("lblSugerenciaModelados", "Se sugiere utilizar positivos para el monto.");
         }
     }
 
 }
 cerrarArbol();
 
}

//productos temporales
function cargaRama(id, nombre){
     document.getElementById("ctl00_ContentPlaceHolder1_lblProducto").innerHTML = nombre;
     document.getElementById("ctl00_ContentPlaceHolder1_txtIdP").value = id;
     cerrarArbol();
     
}

function abrirArbol(){
 noOcultaObj('divArbol');
 getObj(contenido('lnkArbol')).href = 'javascript:cerrarArbol();';
}

function cerrarArbol(){
 ocultaObj('divArbol');
 getObj(contenido('lnkArbol')).href = 'javascript:abrirArbol();';
}

//Carga de modelo estad�stico de Valor de Vencimiento Incierto
function cargaRME(id, dia, porcentaje){
         refrescaIngreso("txtMEDia");
         document.getElementById("ctl00_ContentPlaceHolder1_txtIdME").value = id;
         document.getElementById("ctl00_ContentPlaceHolder1_txtMEDia").value = dia;
         document.getElementById("ctl00_ContentPlaceHolder1_txtMEPorcentaje").value = porcentaje;
         getObj(contenido("btnAgregar")).value = "Cambiar";
         
      
}

//funci�n de agregaci�n de cuenta y grupo
function adhiereCuenta(idGrupo, signo){
        setItem("respondeme.aspx",contenido("txtBCuenta"),"&signo=" + signo + "&ig=" + idGrupo, 2)
        
        asg("txtBCuenta","");
        asg("txtIdC", "");
}
function selecciona(idCuenta, cuenta){
    asg("txtBCuenta", cuenta);
    asg("txtIdC", idCuenta);
  
    document.getElementById("resultados").style.display="none";
}
//sugiere las cuentas que coincidan con el texto en la caja
function listarCuentas(){   
        getItems("respondeme.aspx",contenido("txtBCuenta"),"resultados",contenido("Panel1"),1); 
}

function eliminaTodoBitacora(idBitacora, conf){
    //conf es la configuraci�n con la que trabaja respondeme.aspx, 
    //4 es para eliminar flujos y 5 para operaciones
    
    exeItem("respondeme.aspx",idBitacora,"",conf);
}


//POPUP's

//pop up de configuracion general
function PopUpEdtHacer(id){
        
     var ruta = "edtTarea.aspx?id=" + id;
     window.open(ruta, '', 'width=900,height=380,status=no,resizable=no,scrollbars=no,toolbar=no');

}
//pop up de confirmaci�n de hecho en tarea
function PopUpEdtHecho(event, instruccion){

    lblAsg("ctl00_lblInstrucciones", instruccion);
    
    var x = event.clientX;
	var y = event.clientY;
	
	x = x - 300;
	y = y - 10;
    abrirIngreso("boxB", x, y);
}

function NoRevision(){

    var valor = getObj(contenido("chkNoRevision")).checked;
    if (valor){
        getObj(contenido("txtFechaRevision")).disabled = "disabled";
        asg("txtFechaRevision","");
        getObj("Img1").disabled = "disabled";
    }else{
        getObj(contenido("txtFechaRevision")).disabled = "";
        asg("txtFechaRevision",new Date().toDDMMYYYYString());
        getObj("Img1").disabled = "";
    }

}
//



//function cambiarIndex() {

//    document.getElementById('parametros').style.zindex = "0" ;

////alert(  document.getElementById('parametros').style.z-index);

//}

function cargaRequisito() {

    var x = (window.screen.width - 500) / 2;
    var y = (window.screen.height - 550) / 2;
    abrirIngreso('parametros', x, y);

    //asg("txtIdRequisito", idRequisito);
    //asg("txtEstadoAnterior", idEstadoDocumento);
    //lblAsg(contenido("lblNombreDocumento"), nombreRequisito);
    //lblAsg(contenido("lblEstadoAnterior"), nombreEstadoDocumento);
    //getCheckedRadio();  //id del radio button list 

}



//function OpcionObtenerSeleccionada(ctrlOpcionesId) {

//    var posicion = 0;

//    var item = document.getElementById(ctrlOpcionesId + "_" + posicion);

//    alert(ctrlOpcionesId + "_" + posicion);

//    while (item != null) {

//        if (item.checked) {

//            alert("hoa" + item.value);
//            return item.value;

//        } else {

//            posicion += 1;

//            item = document.getElementById(ctrlOpcionesId + "_" + (posicion));
//        }
//    }
//}

function getCheckedRadio(idEstadoDocumento) {
    var radioButtons = document.getElementsByName("ctl00$ContentPlaceHolder1$rblEstados");
      for (var x = 0; x < radioButtons.length; x++) {
          radioButtons[0].checked = true ;
          
       // alert("dos" + radioButtons[x].value);

         // if (idEstadoDocumento >= radioButtons.length) {
           //   radioButtons[idEstadoDocumento - radioButtons.length].SelectedValue = idEstadoDocumento;
              
             // radioButtons[x].checked = false ;
          //}
          //else {
            //  radioButtons[idEstadoDocumento].SelectedValue = idEstadoDocumento;
              //radioButtons[x].checked = false;

             
          //}
       
           //  radioButtons[x].items.value;

        //alert("You checked " + radioButtons[idEstadoDocumento - 1].id + " which has the value " + radioButtons[idEstadoDocumento - 1].value);
        
    }
}



function repParametros(rep, fisico){

    var x = (window.screen.width - 500) / 2;
	var y = (window.screen.height - 450) / 2;
	
    asg("txtIdRep", rep);
    asg("txtEsFisico",fisico);

    abrirIngreso('parametros',x,y);

}

//MODELOS DE BANDAS
//funci�n de carga de bandas de edtModeloBandas.aspx
function cargaBanda(id, dias, factor){
    
    refrescaIngreso("txtFactorSensi")
    
    asg("txtBid", id);
    if (dias>=10000){
     getObj(contenido("chkUltimaBanda")).checked = "checked";
     asg("txtDias", "");
     getObj(contenido("txtDias")).disabled = "disabled";
    }else{
     getObj(contenido("chkUltimaBanda")).checked = "";
     getObj(contenido("txtDias")).disabled = "";
     asg("txtDias", dias);
    }
    
    getObj(contenido("chkUltimaBanda")).disabled = "disabled";
    asg("txtFactorSensi", factor);
    
    if (id != 0)
    asg("btnAgregar", "Guardar");
    else
    asg("btnAgregar", "Agregar")
    
   
    
}

function deshabilitaDiasUltimaBanda(){
    
    if (getObj(contenido("chkUltimaBanda")).checked){
        //asg("txtDias","");
        deshabilita(contenido("txtDias"));
    }else{
        habilita(contenido("txtDias"));
    }
}

function validaNumero(NObj, topeMinimo, topeMaximo){

var numero = val(contenido(NObj));

if (numero < topeMinimo) 
   asg(NObj,topeMinimo);
   
if (numero > topeMaximo){
   var i = 0;
   while (numero>topeMaximo){
       if (i == 0){
         numero = numero/100;
         i=1;
       }else{
         numero = numero/10;
       }
   }
   asg(NObj, numero);   
}

if (topeMinimo > numero || numero > topeMaximo){
alert("El n\xFAmero ingresado est\xE1 fuera de los l\xEDmites. \nL\xEDmite m\xEDnimo: " + topeMinimo + "\nL\xEDmite m\xE1ximo: " + topeMaximo);
return undefined;
}

}

function eliminaMB(id){
exeItem("respondeme.aspx",id,"", 11);
reFresh();
}


//CARGAS MASIVAS

  function abreSeleccionArchivo(){
        var division;
        var inputFile;
        inputFile =document.getElementById("ctl00_ContentPlaceHolder1_fluArchivo");
        division=document.getElementById("idArchivosServidor");
        division.style.display = "";
        inputFile.disabled=true;
        }
        
        function seleccionArchivo(){
        var txt;
        var lst;
        txt=document.getElementById("ctl00_ContentPlaceHolder1_txtArchivo");
        lst=document.getElementById("ctl00_ContentPlaceHolder1_lstArchivos");
        txt.value=lst.value;
        }
        
        function ocultaArchivos(){
        var division;
        var inputFile;
        var txt;
        txt=document.getElementById("ctl00_ContentPlaceHolder1_txtArchivo");
        inputFile =document.getElementById("ctl00_ContentPlaceHolder1_fluArchivo");
        division=document.getElementById("idArchivosServidor");
        division.style.display = "none";
        inputFile.disabled=false;
        txt.value="";
        }
        
 //SUGERENCIA DE POSIVOS Y NEGATIVOS
 
 function sugiere(nombreLbl, texto){
 lblAsg(contenido(nombreLbl), texto);
 toolTip(contenido(nombreLbl), 8000);
 }
 
  function abrirContenidoOculto(nomObj){
   noOcultaObj(nomObj);
 }
 
 
 //FUNCI�N DE EDICI�N DE MODELOS DE L�NEAS
function cargaModeloLinea(id,nombre){

    refrescaIngreso('txtNombre');

    asg("txtMLId", id); 
    asg("txtNombre", nombre);
    
    if (id==0){
    getObj(contenido("btnAgregra")).value = "Agregar"
    }else{
    getObj(contenido("btnAgregra")).value = "Guardar"
    }
    
}

function cargaDivisa(id,nombre){
    asg("txtDId", id); 
    asg("txtNombre", nombre);
    
    if (id==0){
    getObj(contenido("btnAgregar")).value = "Agregar"
    }else{
    getObj(contenido("btnAgregar")).value = "Guardar"
    }

}

function cargaTasa(){
refrescaIngreso('txtNombre');
asg("txtNombre","");
asg("txtValor","0");

}

//funci�n de consulta

//sugiere las cuentas que coincidan con el texto en la caja

function activaIngresoCuentas(){
ocultaObj("lstCuentas");
noOcultaObj(contenido("tblCuentasRelacionadasIngreso"));
ocultaObj(contenido('tblEdicionCuenta'));
}

function listarCuentas(NObj, conf)
{   
        getItems("respondeme.aspx",contenido(NObj),"resultados",contenido("Panel1"),conf); 
}
////pregunta si el c�digo del concepto elejido existe, y de ser as� lo agrega en la base en la tabla cuentasXConcepto
//function preguntaConcepto(idConcepto, signo)
//{
//              
//        setItem("respondeme.aspx",contenido("txtCuenta"),"&signo=" + signo + "&ic=" + idConcepto, 8);
//}
//funci�n que adhiere un c�digo de cuenta a un concepto
function adhiere(){
        add(contenido("txtCuentas"),val(contenido("txtCuenta")),"; ");
        asg("txtCuenta","");
}

//carga la selecci�n de una sugerencia
function cargaCuentaSeleccionada(idCuenta, nombre) {
            asg("txtCuenta", nombre);
            ocultaObj("resultados");
}
//opciones de cuenta
function opcionCuenta(event, codigo, idConcepto, signo){
    var x = event.clientX;
	var y = event.clientY;
	
	noOcultaObj(contenido("tblEdicionCuenta"));
	
	var elemento = getObj(contenido("tblEdicionCuenta"));
	
	elemento.style.left = x + "px";
    elemento.style.top = y + "px";
    
    asg("txtCntId", codigo);
    asg("txtCncId", idConcepto);
    
    getObj(contenido("rdbSuma")).checked = (signo>0);
    getObj(contenido("rdbResta")).checked = (signo<0);
}

function reportaGuardado(){
toolTip("divGuardado",16000);
}

function valBoton(nBtn){
 asg(nBtn,"Guardar");
}

function txtLimpia(NObj, msg){
  var texto = val(contenido(NObj));
  
  if (texto == msg)
    asg(NObj, '');
}

function txtNuevo(NObj, msg){
  var texto = val(contenido(NObj));
  
  if (texto == '')
    asg(NObj, msg);
  
}

function subeAGNivel(id){
exeItem("respondeme.aspx",id,"", 12);
reFresh();
}
function bajaAGNivel(id){
exeItem("respondeme.aspx",id,"", 13);
reFresh();
}

//OPCION 3.ASPX DE FLUJOS.FECHA ANTES DE FECHA DE CORTE
function XDias(){
    var obj = getObj(contenido('rdbXDias'));
    if (obj.checked)
      habilita(contenido('txtXDias'));
    else
      deshabilita(contenido('txtXDias'));

}

//valoresvencimientoIncierto.aspx
function configCalendar(txtNObj){
            var imagen = txtNObj.id.replace("imgCal","txtRevision");
            var texto = txtNObj.id.replace("txtRevision","imgCal");
            Calendar.setup({
                 inputField : txtNObj.id.replace("imgCal","txtRevision"),
                 button : txtNObj.id
            });
}
function deshabilitaProximaRevision(chkObj){
        var txt = chkObj.id.replace("CheckBox1", "txtRevision");
        var imagen = chkObj.id.replace("CheckBox1","imgCal");
        
        if (chkObj.checked){
           asgx(txt,"");
           ocultaObj(txt);
           ocultaObj(imagen);
        }else{
           asgx(txt,new Date().toDDMMYYYYString());
           noOcultaObj(txt);
           noOcultaObj(imagen);
           habilita(txt); 
        }
            
        
        
}

function cargando(){
ocultaObj(contenido('TDBoton'));
noOcultaObj(contenido('TDCargando'));
}

//Navegaci�n

function navegaArbol(event,id){

getItemsNavegacion("resultados.aspx",id,"resultados",contenido("Panel1")); 
    var x = event.clientX;
	var y = event.clientY;
	
	
	var elemento = getObj("resultados");
	
	elemento.style.left = x + "px";
    elemento.style.top = y + "px";
}

function archivoSeleccion(){
    var chkArchivo = getObj(contenido('rdbArchivo'));
    
    if (chkArchivo.checked){
        habilita(contenido('fluArchivo'));
        deshabilita(contenido('cmbExcepcion'));
        asg('btnCarga', 'Carga');
    }else{
        deshabilita(contenido('fluArchivo'));
        habilita(contenido('cmbExcepcion'));
        asg('btnCarga', 'Excepci\xF3n');
    }

}

function inicioOpciones(Obj){

    var x = (window.screen.width - 600) / 2;
	var y = (window.screen.height - 700) / 2;

    abrirIngreso(Obj,x,y);

}

function validaRestaurarBDD(obj){
    var lst = getObj(contenido("lstArchivos"));
    
    var r = false;
    //alert(lst.selectedIndex);
    if (lst.selectedIndex >= 0)
      r = true;
    else{
      alert("Debe elegir un punto de restauraci\xF3n para guardar");
      r = false;
      }
    
    obj = false;
    
    return r;
}

function activaChk(check){
            switch (check) {
                case 'dias':
                    document.getElementById('ctl00_ContentPlaceHolder1_chkDias').checked = true;
                    break;
                case 'crecimiento':
                    document.getElementById('ctl00_ContentPlaceHolder1_chkCrecimiento').checked = true;
                    break;
                case 'renovacion':
                    document.getElementById('ctl00_ContentPlaceHolder1_chkRenovacion').checked = true;
                    break;
                case 'retraso':
                    document.getElementById('ctl00_ContentPlaceHolder1_chkRetraso').checked = true;
                    break;
                case 'morosidad':
                    document.getElementById('ctl00_ContentPlaceHolder1_chkMorosidad').checked = true;
                    break;
            }
        }

function cargaTasaAdministracion(id, nombre, idDivisa){
     var x = (window.screen.width - 600) / 2;
	var y = (window.screen.height - 700) / 2;

    abrirIngreso('tblEditarDivisa',x,y);
    
    asg("txtIDivisa", id);
    lblAsg(contenido("lblTasa"), nombre);
    asg("ddlDivisasEdicion", idDivisa);
}