﻿
//redimensiona el marco azul en la ventana principal.master
function TamVentana() {
  var Tamanyo = [0, 0];
  if (typeof window.innerWidth != 'undefined')
  {
    Tamanyo = [
        window.innerWidth,
        window.innerHeight
    ];
  }
  else if (typeof document.documentElement != 'undefined'
      && typeof document.documentElement.clientWidth !=
      'undefined' && document.documentElement.clientWidth != 0)
  {
 Tamanyo = [
        document.documentElement.clientWidth,
        document.documentElement.clientHeight
    ];
  }
  else   {
    Tamanyo = [
        document.getElementsByTagName('body')[0].clientWidth,
        document.getElementsByTagName('body')[0].clientHeight
    ];
  }
  return Tamanyo;
}

//Objeto HTTP
function getHTTPObject() {
    var xmlhttp;
       
    if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
      try {
            xmlhttp = new XMLHttpRequest();
          } catch (e) {
           xmlhttp = false;
          }
    }
   return xmlhttp;
}

//función de redimensión
function redimTBL(){
    var Tam = TamVentana();
    var y = 0;
    y = parseInt(Tam[1])-30;
    document.getElementById("ctl00_tblMaster").style.height = y + 'px';
}

//Adhición a fecha
Date.prototype.toDDMMYYYYString = function () {return isNaN (this) ? 'NaN' : [this.getDate() > 9 ? this.getDate() : '0' + this.getDate(), this.getMonth() > 8 ? this.getMonth() + 1 : '0' + (this.getMonth() + 1) , this.getFullYear()].join('/')}

//Funcion de mensajes
function b(mensaje){

    alert ("mensaje: " + mensaje);
}

//nombres
function contenido(obj){
    return "ctl00_ContentPlaceHolder1_" + obj;
}
function contenido2(obj){
    return "ctl00$ContentPlaceHolder1$" + obj;
}

//retorna un objeto con ese id
function getObj(obj){
    return document.getElementById(obj);
}

//funcion de asignación de valores
//funciona con:
//  -TextBox
//  -DropDonwList
function asg(NObj, valor){

    var nCompleto = "ctl00_ContentPlaceHolder1_" + NObj;

    if (document.getElementById(nCompleto))
        document.getElementById(nCompleto).value = valor;
    else{
        nCompleto = "ctl00$ContentPlaceHolder1$" + NObj;   
        document.getElementById(nCompleto).value = valor;
    }
}
function asgx(NObj, valor){
    document.getElementById(NObj).value = valor;
}

function lblAsg(NObj, valor){
    document.getElementById(NObj).innerHTML = valor;
}
function add(NObj, valor, separacion){

    if (document.getElementById(NObj).value == ""){
        document.getElementById(NObj).value = valor;
    }else{
        document.getElementById(NObj).value = document.getElementById(NObj).value + separacion + valor;
    }

}
function val(NObj){
    return document.getElementById(NObj).value;
}

//funcion que devuelve el copdigo ascii de una tecla
function GetKeyCode(e)
{
	if (e) {
		return e.charCode ? e.charCode : e.keyCode;
	}
	else {
		return window.event.charCode ? window.event.charCode : window.event.keyCode;
	}
}

//Presentación 

function ocultaObj(nomObj){
    getObj(nomObj).style.display = "none";
}

function noOcultaObj(nomObj){
    getObj(nomObj).style.display = "";
}

function deshabilita(NObj){
    getObj(NObj).disabled="disabled";
}

function habilita(NObj){
    getObj(NObj).disabled="";
}
//Tecla enter
//función que en un textbox al presionar enter ejecuta un botón
function TeclaEnter(myfield,e,Nombre_Boton) 
{ 
    var keycode; 
    if (window.event) keycode = window.event.keyCode; 
    else if (e) keycode = e.which; 
        else return true; 

    if (keycode == 13) 
    { 
    getObj(Nombre_Boton).click();
    return false; 
    } 
    else 
    return true; 
}
//onKeyPress="javascript:return TeclaEnter(this,event,'btnAnadir')">

//bloquea el funcionamiento de la tecla enter en un textbox
function BloqueaEnter(myfield,e){
    var keycode; 
    if (window.event) keycode = window.event.keyCode; 
    else if (e) keycode = e.which; 
        else return true; 

    if (keycode == 13) 
        return false; 
    else 
        return true; 
}
//onKeyPress="javascript:return TeclaEnter(this,event)">

//función que en un textbox al presionar enter se enfoca en otro objeto, si seleccionado es true entonces también selecciona el contenido
function TeclaEnterEnfoca(myfield,e,Nombre_Obj,selecciona) 
{ 
    var keycode; 
    if (window.event) keycode = window.event.keyCode; 
    else if (e) keycode = e.which; 
        else return true; 

    if (keycode == 13) 
    { 
    getObj(Nombre_Obj).focus();
    if (selecciona) getObj(Nombre_Obj).select();
    return false; 
    } 
    else 
    return true; 
}
//onKeyPress="javascript:return TeclaEnterEnfoca(this,event,'btnAnadir', false)">

//no probado
/***********************************************
* Disable "Enter" key in Form script- By Nurul Fadilah(nurul@REMOVETHISvolmedia.com)
* This notice must stay intact for use
* Visit http://www.dynamicdrive.com/ for full source code
***********************************************/
/*
function handleEnter (field, event) {
//var field = document.getElementById(field1);
var next=0, found=false;

if(event.keyCode!=13) return;
for(var i=0;i<field.form.elements.length;i++) {
if(field.name==field.form.elements.item(i).name){
next=i+1;
found=true
break;
}
}
while(found){
if( field.form.elements.item(next).disabled==false && field.form.elements.item(next).type!='hidden'){
field.form.elements.item(next).focus();
field.form.elements.item(next).select();
break;
}
else{
if(next<field.form.elements.length-1)
next=next+1;
else
break;
}
}
return false;
}*/



//Componente: Suggest Box

var divSugerencias;//todo el div correspondiente a la sugerencia (permite aparecer y desaparecer)
var divContenidoPanel;//objeto en donde se insertará el select (Objeto en donde se inserta la respuesta de la página de filtro)
var altoSugerencia = 400;//máxima altura del cuadro del suggest box
var httpResponseGrupodeLineas;

//paginaListado: página la cual procesa el requerimiento
//cajaSugerencia: caja de texto en donde se introducen las sugerencias
//posSugerencias:div en el cual se presenta todo el contenido de las sugerencias
//contenedor: objeto en el cual se colocan lo retornado por paginaListado
//confListado: número de indice para realizar el proceso
function getItems(paginaListado, cajaSugerencia, posSugerencias, contenedor, confListado) {

    var idValue = document.getElementById(cajaSugerencia).value;
    var myRandom = parseInt(Math.random()*99999999);  // cache buster, garantiza que la página a consultar sea con datos frescos
    
    divSugerencias = posSugerencias;
    divContenidoPanel = contenedor;
    httpResponseGrupodeLineas = 1;
    
    if (idValue != ""){    
        http.open("GET", paginaListado + "?param=" + escape(idValue) + "&rand=" + myRandom + "&conf=" + confListado, true);
        http.onreadystatechange = handleHttpResponse;
        http.send(null);
    }else{
        document.getElementById(divSugerencias).style.display="none"; 
    }
}

function getItemsNavegacion(paginaListado, idArbol, posSugerencias, contenedor) {

    var myRandom = parseInt(Math.random()*99999999);  // cache buster, garantiza que la página a consultar sea con datos frescos
    
    divSugerencias = posSugerencias;
    divContenidoPanel = contenedor;
    httpResponseGrupodeLineas = 2;
    
    if (idArbol != ""){    
        http.open("GET", paginaListado + "?param=" + idArbol + "&rand=" + myRandom, true);
        http.onreadystatechange = handleHttpResponse;
        http.send(null);
    }else{
        document.getElementById(divSugerencias).style.display="none"; 
    }
}

//Función de realización de la respuesta de la consulta
/*
function handleHttpResponse() {

        if (http.readyState == 4) { //== 4 significa que no tiene ningún problema              
        /*******actividades con los resultados de "http.responseText" ********************
        }
}

*/

function handleHttpResponse() {

        if (http.readyState == 4) {               
                           
                if (http.responseText != ""){
                
                document.getElementById(divSugerencias).style.display=""; 
                document.getElementById(divContenidoPanel).style.height = "";
                document.getElementById(divContenidoPanel).style.width = "";
                
                var htmlTxt = http.responseText;
                if (httpResponseGrupodeLineas==2)
                    htmlTxt = htmlTxt.substring(htmlTxt.indexOf("<table"),htmlTxt.indexOf("</table>")+8);
                document.getElementById(divContenidoPanel).innerHTML=htmlTxt;
            
                if (document.getElementById(divContenidoPanel).offsetHeight > altoSugerencia)
                    document.getElementById(divContenidoPanel).style.height = altoSugerencia + "px";
                    
                document.getElementById(divContenidoPanel).style.width = document.getElementById(divContenidoPanel).offsetWidth + 15;
                    //document.getElementById(divContenidoPanel).style.width = "500px";
                }
        }
}

//función simple de acción 
var cp = 0;
function setItem(paginaComprovacion, cajaComprovacion, extras, confPregunta){
    cp = confPregunta ;
    var idValue = document.getElementById(cajaComprovacion).value;
    var myRandom = parseInt(Math.random()*99999999);  // cache buster, garantiza que la página a consultar sea con datos frescos
    
    if (idValue != ""){    
        http.open("GET", paginaComprovacion + "?param=" + escape(idValue) + extras  + "&rand=" + myRandom + "&conf=" + confPregunta, true);
        http.onreadystatechange = handleHttpResponseSet;//handleHttpResponseSet ---> es una función la cual recopila la información que ha retornado de la página solicitada
        http.send(null);
    }

}

function handleHttpResponseSet() {

       

        if (http.readyState == 4)         
          if (cp == 8){
            if (http.responseText == '1')
            adhiere();
            else
            asg("txtCuenta", http.responseText);
          }else{      
            asg("txtLstCuentas",http.responseText);
          }   
     
}


//función de básica acción de acción 
function exeItem(paginaComprovacion, param, extras, confPregunta){

    var idValue = param;
    var myRandom = parseInt(Math.random()*99999999);  // cache buster, garantiza que la página a consultar sea con datos frescos

   
    http.open("GET", paginaComprovacion + "?param=" + escape(idValue) + extras  + "&rand=" + myRandom + "&conf=" + confPregunta, true);
    http.onreadystatechange = handleHttpResponseExe;//handleHttpResponseSet ---> es una función la cual recopila la información que ha retornado de la página solicitada
    http.send(null);
  

}

function handleHttpResponseExe() {

    if (http.readyState == 4) 
        reFresh(); 
}

var http = getHTTPObject(); 

/*Para funcionar la caja de sugerencias necesita de onKeyUp y la llamada a getItems y el div en donde se desplegarán las sugerencias llamada "divSugerencias"

<asp:TextBox ID="txtArticulo" runat="server"  onKeyUp="listarArticulos();"></asp:TextBox>
<div id="resultados" style="position:relative; top: 2px;
   left: 0px; display:none; 
   border: 0px solid black; z-index:1">
</div>

también es necesario de la construcción de la funcion loadrecord para ver los cambios enb la selección de una sugerencia

ejemp:
       function loadrecord(nombre, id, costo) {
            asg("txtArticulo", nombre);
            asg("txtCosto", costo);
            asg("txtAid", id);
            document.getElementById("resultados")
                  .style.display="none"; 
            }
*/

//página
function reFresh(){
      location.reload(true);
}


//CLICK'S

//Para quitar el control del click derecho en una página
var message="->B!G<-  - Derechos Reservados© 2008";
function click(e) {
	if (document.all) {
		if (event.button==2||event.button==3) {
			alert(message);//función para ejecución en el click
			return false;
		}
	}
	if (document.layers) {
		if (e.which == 3) {
			alert(message);//función para ejecución en el click
			return false;
		}
	}
}
//estas funciones son necesarias para iniciar el funcionamiento
//    en este caso específico se deshabilita el click derecho de toda la página
//if (document.layers) {
//	document.captureEvents(Event.MOUSEDOWN);
//}
//document.onmousedown=click;

function toolTip(nombreObj, tiempo){
//alert(nombreObj);
 noOcultaObj(nombreObj);
 window.setTimeout("ocultaObj('" + nombreObj + "');", tiempo);
}


//VALIDADOR DE FECHAS


function esDigito(sChr){
var sCod = sChr.charCodeAt(0);
return ((sCod > 47) && (sCod < 58));
}
function valSep(oTxt){
var bOk = false;
bOk = bOk || ((oTxt.value.charAt(2) == "-") && (oTxt.value.charAt(5) == "-"));
bOk = bOk || ((oTxt.value.charAt(2) == "/") && (oTxt.value.charAt(5) == "/"));
return bOk;
}
function finMes(oTxt){
var nMes = parseInt(oTxt.value.substr(3, 2), 10);
var nRes = 0;
switch (nMes){
case 1: nRes = 31; break;
case 2: nRes = 29; break;
case 3: nRes = 31; break;
case 4: nRes = 30; break;
case 5: nRes = 31; break;
case 6: nRes = 30; break;
case 7: nRes = 31; break;
case 8: nRes = 31; break;
case 9: nRes = 30; break;
case 10: nRes = 31; break;
case 11: nRes = 30; break;
case 12: nRes = 31; break;
}
return nRes;
}
function valDia(oTxt){
var bOk = false;
var nDia = parseInt(oTxt.value.substr(0, 2), 10);
bOk = bOk || ((nDia >= 1) && (nDia <= finMes(oTxt)));
return bOk;
}
function valMes(oTxt){
var bOk = false;
var nMes = parseInt(oTxt.value.substr(3, 2), 10);
bOk = bOk || ((nMes >= 1) && (nMes <= 12));
return bOk;
}
function valAno(oTxt){
var bOk = true;
var nAno = oTxt.value.substr(6);
bOk = bOk && ((nAno.length == 2) || (nAno.length == 4));
if (bOk){
for (var i = 0; i < nAno.length; i++){
bOk = bOk && esDigito(nAno.charAt(i));
}
}
return bOk;
}
function valFecha(oTxt){
    var bOk = true;
    if (oTxt.value != ""){
        bOk = bOk && (valAno(oTxt));
        bOk = bOk && (valMes(oTxt));
        bOk = bOk && (valDia(oTxt));
        bOk = bOk && (valSep(oTxt));
        if (!bOk){
            alert("Fecha inválida");
            oTxt.value = new Date().toDDMMYYYYString();
            oTxt.focus();
        }
    }else{
        oTxt.value = new Date().toDDMMYYYYString();
    }
    
}
function valFechaNoNecesario(oTxt){
    var bOk = true;
    if (oTxt.value != ""){
        bOk = bOk && (valAno(oTxt));
        bOk = bOk && (valMes(oTxt));
        bOk = bOk && (valDia(oTxt));
        bOk = bOk && (valSep(oTxt));
        if (!bOk){
            alert("Fecha inválida");
            oTxt.value = new Date().toDDMMYYYYString();
            oTxt.focus();
        }
    }
}

// valida si el char es número punto o coma
function esDigitoNumero(sChr){
var sCod = sChr.charCodeAt(0);
return ((sCod > 47) && (sCod < 58) || (sCod > 43) && (sCod < 47));
}

//valida si es formato número
function valNumero(oTxt){

var bOk = true;
var corregido = "";
var punto = 0;

var esPuntoOComa = false;

for (var i = oTxt.value.length - 1; i >= 0 ; i--){

    bOk = bOk && esDigitoNumero(oTxt.value.charAt(i));

    if (oTxt.value.charAt(i) == '.' || oTxt.value.charAt(i) == ',')  esPuntoOComa = true; else esPuntoOComa = false;

    if (esPuntoOComa) punto ++;

    if (esDigitoNumero(oTxt.value.charAt(i)) && (!esPuntoOComa || (esPuntoOComa && punto< 2))) {
         corregido = oTxt.value.charAt(i) + corregido;
    }else{
        bOk = false;
    }
}

if (bOk != true){
    alert('El numero ingresado no es correcto');
    oTxt.value = corregido;
    }
}
